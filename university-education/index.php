<?php get_header(); 
	
$header_background = university_education_header_title_index();
$university_education_theme_option['kode-header-style'] = (empty($university_education_theme_option['kode-header-style']))? 'header-style-1': $university_education_theme_option['kode-header-style'];
$page_caption = '';
$page_background = ''; $page_title = get_bloginfo('name'); 
$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true));
if(!empty($university_education_post_option['page-caption'])){ $page_caption = $university_education_post_option['page-caption'];} ?>
<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!--KF INR BANNER DES Wrap Start-->
				<div class="kf_inr_ban_des">
					<div class="inr_banner_heading">
						<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
					</div>
				   
					<div class="kf_inr_breadcrumb">
						
					</div>
				</div>
				<!--KF INR BANNER DES Wrap End-->
			</div>
		</div>
	</div>
</div>
<div class="content kode-main-content-k">
	<div class="container">
		<div class="row">
		<?php 
			
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			
			$university_education_sidebar = array(
				'type'=>$university_education_theme_option['archive-sidebar-template'],
				'left-sidebar'=>$university_education_theme_option['archive-sidebar-left'], 
				'right-sidebar'=>$university_education_theme_option['archive-sidebar-right']
			); 			
			$university_education_sidebar = university_education_get_sidebar_class($university_education_sidebar);
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="<?php echo esc_attr($university_education_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="kode-main-content <?php echo esc_attr($university_education_sidebar['center'])?>">
				<?php		
					// set the excerpt length
					if( !empty($university_education_theme_option['archive-num-excerpt']) ){
						global $university_education_excerpt_length; $university_education_excerpt_length = 55;
						add_filter('excerpt_length', 'university_education_set_excerpt_length');
					} 

					
					$university_education_lightbox_id++;
					$university_education_post_settings['title-num-fetch'] = 200;
					$university_education_post_settings['excerpt'] = 'full';
					$university_education_post_settings['thumbnail-size'] = 'full';			
					$university_education_post_settings['blog-style'] = $university_education_theme_option['archive-blog-style'];							
				
					echo '<div class="kode-blog-list-full kode-fullwidth-blog row">';
					if($university_education_post_settings['blog-style'] == 'blog-full'){
						$university_education_post_settings['blog-info'] = array('author', 'date', 'category', 'comment');
						echo university_education_get_blog_full($wp_query);
					}else{
						$university_education_post_settings['blog-info'] = array('date', 'comment');
						$university_education_post_settings['blog-info-widget'] = true;
						
						//$blog_size = str_replace('blog-1-', '', $university_education_theme_option['archive-blog-style']);
						$blog_size = 1;
						echo university_education_get_blog_grid($wp_query, $blog_size, 
							$university_education_theme_option['archive-thumbnail-size'], 'fitRows');
					}
					echo '<div class="clear"></div>';
					echo '</div>';
					remove_filter('excerpt_length', 'university_education_set_excerpt_length');
					
					$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
					echo university_education_get_pagination($wp_query->max_num_pages, $paged);													
				?>
			</div>
			<?php
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'right-sidebar' && $university_education_sidebar['right'] != ''){ ?>
				<div class="<?php echo esc_attr($university_education_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>