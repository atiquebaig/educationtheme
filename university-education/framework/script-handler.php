<?php
	/*	
	*	Kodeforest Include Script File
	*	---------------------------------------------------------------------
	*	This file use to include a necessary script when it's requires
	*	---------------------------------------------------------------------
	*/
	
	// Match the values
	if( !function_exists('university_education_match_page_builder') ){
		function university_education_match_page_builder($array, $item_type, $type, $data = array()){
			if(isset($array)){
				foreach($array as $item){
					if($item['item-type'] == $item_type && $item['type'] == $type){
						if(empty($data)){
							return true;
						}else{	
							if( strpos($item['option'][$data[0]], $data[1]) !== false ) return true;
						}
					}
					if($item['item-type'] == 'wrapper'){
						if( university_education_match_page_builder($item['items'], $item_type, $type) ) return true;
					}
				}
			}
			return false;
		}
	}
	
	//Add Scripts in Theme
	if(!is_admin()){
		add_action('wp_enqueue_scripts','university_education_register_non_admin_styles');
		add_action('wp_enqueue_scripts','university_education_register_non_admin_scripts');
		add_action('after_setup_theme','university_education_theme_slug_editor_styles' );
	}
	
	// Register all CSS
	if( !function_exists('university_education_register_non_admin_styles') ){
		function university_education_register_non_admin_styles(){	
			
			global $post,$post_id,$university_education_content_raw,$university_education_theme_option,$university_education_font_controller;		
			
			wp_deregister_style('ignitiondeck-base');
			wp_deregister_style('woocommerce-layout');
			wp_deregister_style('flexslider');
			
			
			wp_enqueue_style( 'style', get_stylesheet_uri() );  //Default Stylesheet	
			wp_enqueue_style( 'style-typo', UOE_PATH . '/css/themetypo.css' );  //Theme Typo
			
			wp_enqueue_style( 'style-bootstrap', UOE_PATH . '/css/bootstrap.css' );  //Bootstrap
			wp_enqueue_style( 'style-color', UOE_PATH . '/css/color.css' );  //Color Scheme
			wp_enqueue_style( 'style-custom', UOE_PATH . '/css/style-custom.css' ,false,'1.0','all' );  //Custom Style
			wp_enqueue_style( 'style-default', UOE_PATH . '/css/default.css' );  //Default WP style
			
			wp_enqueue_style( 'font-awesome', UOE_PATH . '/framework/include/frontend_assets/font-awesome/css/font-awesome.min.css' );  //Font Awesome
			
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'slider', array('slider-type', 'flexslider')) ){
				wp_enqueue_style( 'flexslider', UOE_PATH . '/framework/include/frontend_assets/flexslider/flexslider.css' );  //Flex Slider
			}
			
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'slider', array('slider-type', 'nivo-slider')) ){
				wp_enqueue_style( 'nivo-slider', UOE_PATH . '/framework/include/frontend_assets/nivo-slider/nivo-slider.css' );  //Nivo Slider	
			}
			
			wp_register_script('universityeducation-search-script', UOE_PATH.'/framework/include/frontend_assets/default/js/search.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-search-script');
			wp_enqueue_style( 'universityeducation-search-script', UOE_PATH . '/framework/include/frontend_assets/default/css/search.css' );  //Search CSs
			
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'gallery') ){
				wp_enqueue_style( 'style-prettyphoto', UOE_PATH . '/framework/include/frontend_assets/default/css/prettyphoto.css' );  //Prettyphoto
			}
			
			wp_enqueue_style( 'slidepush', UOE_PATH . '/framework/include/frontend_assets/slide-menu/slidepush.css' );  //Side push menu CSS
			wp_register_script('slidepush', UOE_PATH.'/framework/include/frontend_assets/slide-menu/slidepush.js', false, '1.0', true);
			wp_enqueue_script('slidepush'); //Side push menu Js
		
			wp_enqueue_style('universityeducation-chosen', UOE_PATH . '/framework/include/backend_assets/js/kode-chosen/chosen.min.css');
			wp_register_script('universityeducation-chosen', UOE_PATH . '/framework/include/backend_assets/js/kode-chosen/chosen.jquery.min.js');
			wp_enqueue_script('universityeducation-chosen'); // Select choosen
			
			wp_enqueue_style( 'style-component', UOE_PATH . '/framework/include/frontend_assets/dl-menu/component.css' );  //Responsive Menu
			wp_register_script('universityeducation-modernizr', UOE_PATH.'/framework/include/frontend_assets/dl-menu/modernizr.custom.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-modernizr');
			
			wp_register_script('universityeducation-dlmenu', UOE_PATH.'/framework/include/frontend_assets/dl-menu/jquery.dlmenu.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-dlmenu'); // dll menu
			
			if ( class_exists( 'bbPress' ) ) {
				wp_enqueue_style( 'universityeducation-bbp-css', UOE_PATH . '/css/bbpress.css' );  //bbpress CSS
			}
			
			wp_enqueue_style( 'kf-range-slider', UOE_PATH . '/css/range-slider.css' ); 
			
			if ( class_exists( 'BuddyPress' ) ) {
				wp_enqueue_style( 'universityeducation-buddy-press', UOE_PATH . '/css/buddypress.css' );  //BuddyPress CSS
			}
			wp_enqueue_style( 'style-woocommerce', UOE_PATH . '/framework/include/frontend_assets/default/css/woocommerce.css' );  //Woocommerce CSS
			
			wp_enqueue_style( 'style-svg-icon', UOE_PATH . '/css/svg-icon/svg-icon.css' );  //SVG Icons			
			
			wp_enqueue_style( 'style-shortcode', UOE_PATH . '/css/shortcode.css' );  //Shortcodes CSS
			wp_enqueue_style( 'style-widget', UOE_PATH . '/css/widget.css' );  //Custom Widgets CSS
			wp_enqueue_style( 'style-responsive', UOE_PATH . '/css/responsive.css' );  //Responsive	CSS		
			
			if(isset($university_education_theme_option['navigation-font-family'])){			
				$font_id = str_replace( ' ', '-', $university_education_theme_option['navigation-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-navi-default-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['navigation-font-family'])));
				}
			}
			
			if(isset($university_education_theme_option['h1-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['h1-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-h1-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['h1-font-family'])));
				}
			}
			
			if(isset($university_education_theme_option['h2-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['h2-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-h2-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['h2-font-family'])));
				}
			}
			if(isset($university_education_theme_option['h3-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['h3-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-h3-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['h3-font-family'])));
				}
			}
			
			if(isset($university_education_theme_option['h4-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['h4-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-h4-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['h4-font-family'])));
				}
			}
			
			if(isset($university_education_theme_option['h5-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['h5-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-h5-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['h5-font-family'])));
				}
			}
			
			if(isset($university_education_theme_option['h6-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['h6-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-h6-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['h6-font-family'])));
				}
			}
			
			if(isset($university_education_theme_option['body-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['body-font-family'] );
				if(class_exists('university_education_font_loader')){
					wp_enqueue_style( 'style-body-'.$font_id, esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['body-font-family'])));
				}
			}
		
			
		}
	}
	
	// Editor Styles
	if( !function_exists('university_education_theme_slug_editor_styles') ){
		function university_education_theme_slug_editor_styles() {
			global $post,$post_id,$university_education_content_raw,$university_education_theme_option,$university_education_font_controller;		
			if(isset($university_education_theme_option['body-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['body-font-family'] );			
				if(class_exists('university_education_font_loader')){
					add_editor_style( array( 'editor-style.css', esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['body-font-family'])) ) );
				}else{
					add_editor_style( array( 'editor-style.css', esc_url_raw('') ) );
				}
			}
		}
	}
	
	//Header Fonts
	add_action( 'admin_print_styles-appearance_page_custom-header', 'university_education_framework_custom_header_fonts' );
	if( !function_exists('university_education_framework_custom_header_fonts') ){
		function university_education_framework_custom_header_fonts() {
			global $post,$post_id,$university_education_content_raw,$university_education_theme_option,$university_education_font_controller;		
			if(isset($university_education_theme_option['body-font-family'])){
				$font_id = str_replace( ' ', '-', $university_education_theme_option['body-font-family'] );		
				if(class_exists('university_education_font_loader')){				
					wp_enqueue_style( 'theme-slug-fonts', esc_url_raw($university_education_font_controller->university_education_get_google_font_url($university_education_theme_option['body-font-family'])), array(), null );
				}
			}
		}
	}
	
    // Register all scripts
	if( !function_exists('university_education_register_non_admin_scripts') ){
		function university_education_register_non_admin_scripts(){
			
			global $post,$post_id,$university_education_content_raw,$university_education_post_option,$university_education_theme_option;	
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-slider');
			
			if ( is_singular() && get_option( 'thread_comments' ) ) 	wp_enqueue_script( 'comment-reply' );
			
			//BootStrap Script Loaded
			wp_register_script('universityeducation-bootstrap', UOE_PATH.'/js/bootstrap.min.js', array('jquery'), '1.0', true);
			wp_localize_script('universityeducation-bootstrap', 'ajax_var', array('url' => admin_url('admin-ajax.php'),'nonce' => wp_create_nonce('ajax-nonce')));
			wp_enqueue_script('universityeducation-bootstrap');
			wp_enqueue_style( 'universityeducation-bootstrap-slider', UOE_PATH . '/css/bootstrap-slider.css' );  //Bootstrap Slider
			wp_register_script('universityeducation-bootstrap-slider', UOE_PATH.'/js/bootstrap-slider.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-bootstrap-slider');
		
			wp_register_script('universityeducation-accordion', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.accordion.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-accordion'); // Accordion
			
			
			if(isset($university_education_theme_option['enable-one-page-header-navi']) && $university_education_theme_option['enable-one-page-header-navi'] == 'enable'){
				wp_register_script('universityeducation-singlepage', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.singlePageNav.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-singlepage'); //One Page Navigation
			}
			
			wp_register_script('universityeducation-filterable', UOE_PATH.'/framework/include/frontend_assets/default/js/filterable.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-filterable'); //Filterable Js
			
			wp_register_script('universityeducation-downcount', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery-downcount.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-downcount'); // Count down Js
			
			if( is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'image-gallery') ){
				wp_register_script('owl_carousel', UOE_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', UOE_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Image Gallery For Carousel
			}
			
			wp_register_script('selectrics-js', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.selectric.min.js', false, '1.0', true);
			wp_enqueue_script('selectrics-js');
			wp_enqueue_style( 'selectric-css', UOE_PATH . '/framework/include/frontend_assets/default/css/selectric.css' );  //Selectric CSS
				
			if( is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'blog') ){
				wp_register_script('owl_carousel', UOE_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', UOE_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Owl Carousel For Blog Slider
			}
			
			if( is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'gallery',array('style', 'gallery-slider')) ){
				wp_register_script('owl_carousel', UOE_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', UOE_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Owl Gallery Slider
			}
			
			wp_register_script('universityeducation-ajax-upload', UOE_PATH.'/js/ajax-upload.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-ajax-upload'); // Ajax Upload Js
			
			// Woo Slider
			if( is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'woo-slider') ){
				wp_register_script('owl_carousel', UOE_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', UOE_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //CSS And js slider for Woo Commerce
			}	

			// Event Calender
			if( is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'events', array('event-style', 'event-calendar-view')) ){
				wp_register_script('universityeducation-moment.min', UOE_PATH.'/framework/include/frontend_assets/calendar/moment.min.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-moment.min');
				wp_register_script('universityeducation-fullcalendar', UOE_PATH.'/framework/include/frontend_assets/calendar/fullcalendar.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-fullcalendar');
				
				wp_enqueue_style( 'universityeducation-fullcalendar', UOE_PATH . '/framework/include/frontend_assets/calendar/fullcalendar.css' );  //Event Calender Js
			}		

			// Testimonial Normal View
			if( (is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'testimonial', array('testimonial-style', 'normal-view'))) ||
			 (is_page() &&  university_education_match_page_builder($university_education_content_raw, 'item', 'teacher', array('teacher-type', 'slider'))) ||
			 is_singular('post') ||
			 ( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'property') ) ||
			 ( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'agent', array('agent-type', 'slider')) ) 
			){
				wp_register_script('owl_carousel', UOE_PATH.'/framework/include/frontend_assets/owl_carousel/owl_carousel.js', false, '1.0', true);
				wp_enqueue_script('owl_carousel');
				wp_enqueue_style( 'owl_carousel', UOE_PATH . '/framework/include/frontend_assets/owl_carousel/owl_carousel.css' );  //Font Awesome
			}
			
			if( is_search() || is_archive() || 
				( empty($university_education_theme_option['enable-flex-slider']) || $university_education_theme_option['enable-flex-slider'] != 'disable' ) ||
				( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'blog') ) ||
				( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'post-slider') ) ||
				( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'testimonial', array('testimonial-style', 'simple-view')) ) ||
				( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'slider', array('slider-type', 'bxslider')) ) ||
				( is_single() && strpos($university_education_post_option, '"post_media_type":"slider"') )){
				wp_enqueue_style( 'bx-slider', UOE_PATH . '/framework/include/frontend_assets/bxslider/bxslider.css' );  //Font Awesome
				wp_register_script('bx-slider', UOE_PATH.'/framework/include/frontend_assets/bxslider/jquery.bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('bx-slider');
				
			}

			wp_register_script('range-slider', UOE_PATH.'/js/range-slider.js', false, '1.0', true); //Range Slider JS
			wp_enqueue_script('range-slider');
			
			wp_register_script('universityeducation-waypoints-min', UOE_PATH.'/framework/include/frontend_assets/default/js/waypoints-min.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-waypoints-min');
			
			wp_register_script('universityeducation-bg-moving', UOE_PATH.'/framework/include/frontend_assets/default/js/bg-moving.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-bg-moving');
			
			//Custom Script Loaded
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'slider', array('slider-type', 'flexslider')) ){
				wp_enqueue_style( 'flexslider', UOE_PATH . '/framework/include/frontend_assets/flexslider/flexslider.css' );  //Font Awesome
				wp_register_script('universityeducation-flexslider', UOE_PATH.'/framework/include/frontend_assets/flexslider/jquery.flexslider.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-flexslider');
			}
			
			wp_enqueue_style( 'flexslider', UOE_PATH . '/framework/include/frontend_assets/flexslider/flexslider.css' );  //Font Awesome
			wp_register_script('universityeducation-flexslider', UOE_PATH.'/framework/include/frontend_assets/flexslider/jquery.flexslider.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-flexslider');
			
			//Enable RTL
			if(isset($university_education_theme_option['enable-rtl-layout'])){
				if($university_education_theme_option['enable-rtl-layout'] == 'enable'){
					wp_enqueue_style( 'universityeducation-rtl', UOE_PATH . '/css/rtl.css' );  //Font Awesome	
				}
			}
			
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'wrapper', 'full-size-wrapper', array('type', 'video')) ){
				wp_register_script('universityeducation-video', UOE_PATH.'/framework/include/frontend_assets/video_background/video.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-video');
				wp_register_script('universityeducation-bigvideo', UOE_PATH.'/framework/include/frontend_assets/video_background/bigvideo.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-bigvideo'); //Big Video Js
				
			}
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'slider', array('slider-type', 'bxslider')) ){
				wp_enqueue_style( 'bx-slider', UOE_PATH . '/framework/include/frontend_assets/bxslider/bxslider.css' );  //Bx Slider Js
				wp_register_script('bx-slider', UOE_PATH.'/framework/include/frontend_assets/bxslider/jquery.bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('bx-slider');
			}
			
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'post-slider') ){		
				wp_enqueue_style( 'bx-slider', UOE_PATH . '/framework/include/frontend_assets/bxslider/bxslider.css' );  //Post Slider CSS
				wp_register_script('bx-slider', UOE_PATH.'/framework/include/frontend_assets/bxslider/jquery.bxslider.min.js', false, '1.0', true);
				wp_enqueue_script('bx-slider'); //Post Slider Js
			}
			
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'gallery') ){
				wp_register_script('universityeducation-prettyphoto', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.prettyphoto.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-prettyphoto');
				wp_register_script('universityeducation-prettypp', UOE_PATH.'/framework/include/frontend_assets/default/js/pp.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-prettypp'); //Prettyphoto for Gallery images
			}	
			
			wp_enqueue_style( 'style-prettyphoto', UOE_PATH . '/framework/include/frontend_assets/default/css/prettyphoto.css' );  //Prettyphoto CSS & Js
			wp_register_script('universityeducation-prettyphoto', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.prettyphoto.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-prettyphoto');
			wp_register_script('universityeducation-prettypp', UOE_PATH.'/framework/include/frontend_assets/default/js/pp.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-prettypp');
			
			//if( is_page() && kode_match_page_builder($university_education_content_raw, 'item', 'slider', array('slider-type', 'nivo-slider')) ){
				wp_enqueue_style( 'nivo-slider', UOE_PATH . '/framework/include/frontend_assets/nivo-slider/nivo-slider.css' );  //Nivo Slider
				wp_register_script('universityeducation-nivo-slider', UOE_PATH.'/framework/include/frontend_assets/nivo-slider/jquery.nivo.slider.js', false, '1.0', true);
				wp_enqueue_script('universityeducation-nivo-slider');
			//}
			
			//CountDown Upcoming Event Timer
			if( is_page() && university_education_match_page_builder($university_education_content_raw, 'item', 'upcoming-event') ||
				is_singular( 'event' ) ){
				wp_register_script('custom_countdown', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.countdown.js', false, '1.0', true);
				wp_enqueue_script('custom_countdown');		
				
				wp_enqueue_style( 'custom_countdown', UOE_PATH . '/framework/include/frontend_assets/default/css/countdown.css' );  //Count Down timer
				
			}	

			//Js Easing
			wp_register_script('universityeducation-easing', UOE_PATH.'/framework/include/frontend_assets/jquery.easing.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-easing');
			
			//Nice Scroll
			if(isset($university_education_theme_option['enable-nice-scroll'])){
				if($university_education_theme_option['enable-nice-scroll'] == 'enable'){
					wp_register_script('universityeducation-nicescroll', UOE_PATH.'/framework/include/frontend_assets/default/js/jquery.nicescroll.min.js', false, '1.0', true);
					wp_enqueue_script('universityeducation-nicescroll');
				}
			}
			
			// Main Function Java Script
			wp_register_script('universityeducation-functions', UOE_PATH.'/js/functions.js', false, '1.0', true);
			wp_enqueue_script('universityeducation-functions');
			
		}
	}
	
	// set the global variable based on the opened page, post, ...
	add_action('wp', 'university_education_define_global_variable');
	if( !function_exists('university_education_define_global_variable') ){
		function university_education_define_global_variable(){
			global $post;		
			if( is_page() ){
				global $university_education_content_raw,$university_education_post_option;				
				$university_education_content_raw = json_decode(university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'university_education_content', true)), true);
				$university_education_content_raw = (empty($university_education_content_raw))? array(): $university_education_content_raw;
				$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true));
			}else if( is_single() || (!empty($post)) ){
				global $university_education_post_option;			
				$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true));
			}
			
			
		}
	}