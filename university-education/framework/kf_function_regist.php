<?php 
	/*	
	*	Kodeforest Framework Register
	*	---------------------------------------------------------------------
	*	This file includes the function to upload values on activation
	*	---------------------------------------------------------------------
	*/
	if (!function_exists('university_education_header_background')){	
		function university_education_header_background(){
			//Custom background Support	
			$args = array(
				'color-scheme'          => '',
				'default-image'          => '',
				'wp-head-callback'       => '_custom_background_cb',
				'admin-head-callback'    => '',
				'admin-preview-callback' => ''
			);

			//Custom Header Support	
			$defaults = array(
				'default-image'          => '',
				'random-default'         => false,
				'width'                  => 950,
				'height'                 => 200,
				'flex-height'            => false,
				'flex-width'             => false,
				'default-text-color'     => '',
				'header-text'            => true,
				'uploads'                => true,
				'wp-head-callback'       => '',
				'admin-head-callback'    => '',
				'admin-preview-callback' => '',
			);
			global $wp_version;
			if ( version_compare( $wp_version, '3.4', '>=' ) ){ 
				add_theme_support( 'custom-background', $args );
				add_theme_support( 'custom-header', $defaults );
			}
		}
	}
	university_education_header_background();

	if (function_exists('register_sidebar')){	
		function university_education_sidebar_creation(){
			global $university_education_theme_option;
			// default sidebar array
			$sidebar_attr = array(
				'name' => '',
				'description' => '',
				'before_widget' => '<div class="widget sidebar-recent-post sidebar_section %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>'
			);
			$item_class = 'col-md-4';
			
			if(isset($university_education_theme_option['footer-layout']) && $university_education_theme_option['footer-layout'] == 1){
				$item_class = 'col-md-6';
			}else if(isset($university_education_theme_option['footer-layout']) && $university_education_theme_option['footer-layout'] == 2){
				$item_class = 'col-md-4';
			}else if(isset($university_education_theme_option['footer-layout']) && $university_education_theme_option['footer-layout'] == 3){
				$item_class = 'col-md-3';
			}else{
				
			}
			$university_education_sidebar = array("Footer");
			foreach( $university_education_sidebar as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name;
				$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
				$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug;
				$sidebar_attr['before_widget'] = '<div id="%1$s" class="'.esc_attr($item_class).' col-sm-6 widget %2$s kode-widget">' ;
				$sidebar_attr['after_widget'] = '</div>' ;
				$sidebar_attr['before_title'] = '<h5 class="widget-title">';
				$sidebar_attr['after_title'] = '</h5><div class="clear"></div>' ;
				$sidebar_attr['description'] = 'Please place '.esc_attr(strtolower($sidebar_name)).' widget here' ;
				register_sidebar($sidebar_attr);
			}
			
			$university_education_sidebar = array("Side Menu");
			foreach( $university_education_sidebar as $sidebar_name ){
				$sidebar_attr['name'] = $sidebar_name;
				$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
				$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug ;
				$sidebar_attr['before_widget'] = '<div id="%1$s" class="kode-side-menu widget %2$s col-md-4 kode-sidebar-widget-area">' ;
				$sidebar_attr['after_widget'] = '</div>' ;
				$sidebar_attr['before_title'] = '<h2 class="widget-title">';
				$sidebar_attr['after_title'] = '</h2><div class="clear"></div>' ;
				$sidebar_attr['description'] = 'Please place '.esc_attr(strtolower($sidebar_name)).' widget here';
				register_sidebar($sidebar_attr);
			}
			
			if(!empty($university_education_theme_option['sidebar-element'])){
				$sidebar_id = 0;
				foreach( $university_education_theme_option['sidebar-element'] as $sidebar_name ){
					$sidebar_attr['name'] = $sidebar_name;
					$sidebar_slug = strtolower(str_replace(' ','-',$sidebar_name));
					$sidebar_attr['id'] = 'sidebar-' . $sidebar_slug;
					$sidebar_attr['before_widget'] = '<div id="%1$s" class="widget %2$s kode-widget">' ;
					$sidebar_attr['after_widget'] = '</div>' ;
					$sidebar_attr['before_title'] = '<h2 class="widget-title">';
					$sidebar_attr['after_title'] = '</h2><div class="clear"></div>' ;
					$sidebar_attr['description'] = 'Please place '.esc_attr(strtolower($sidebar_name)).' page widget here' ;
					register_sidebar($sidebar_attr);
				}		
			}
		}
		university_education_sidebar_creation();
	}
	
	
	
	// video size 
	if( !function_exists('university_education_get_video_size') ){
		function university_education_get_video_size( $size ){
			global $_wp_additional_image_sizes, $university_education_theme_option, $university_education_crop_video;

			// get video ratio
			if( !empty($university_education_theme_option['video-ratio']) && 
				preg_match('#^(\d+)[\/:](\d+)$#', $university_education_theme_option['video-ratio'], $number)){
				$ratio = $number[1]/$number[2];
			}else{
				$ratio = 16/9;
			}
			
			// get video size
			$video_size = array('width'=>620, 'height'=>9999);
			if( !empty($size) && is_numeric($size) ){
				$video_size['width'] = intval($size);
			}else if( !empty($size) && !empty($_wp_additional_image_sizes[$size]) ){
				$video_size = $_wp_additional_image_sizes[$size];
			}else if( !empty($size) && in_array($size, get_intermediate_image_sizes()) ){
				$video_size = array('width'=>get_option($size . '_size_w'), 'height'=>get_option($size . '_size_h'));
			}

			// refine video size
			if( $university_education_crop_video || $video_size['height'] == 9999 ){
				return array('width'=>$video_size['width'], 'height'=>intval($video_size['width'] / $ratio));
			}else if( $video_size['width'] == 9999 ){
				return array('width'=>intval($video_size['height'] * $ratio), 'height'=>$video_size['height']);
			}
			return $video_size;
		}	
	}	
	
	
	// modify a wordpress gallery style
	add_filter('gallery_style', 'university_education_gallery_style');
	if( !function_exists('university_education_gallery_style') ){
		function university_education_gallery_style( $style ){
			return str_replace('border: 2px solid #cfcfcf;', 'border-width: 1px; border-style: solid;', $style);
		}
	}
	
	// turn the page comment off by default
	add_filter( 'wp_insert_post_data', 'university_education_page_default_comments_off' );
	if( !function_exists('university_education_page_default_comments_off') ){
		function university_education_page_default_comments_off( $data ) {
			if( $data['post_type'] == 'page' && $data['post_status'] == 'auto-draft' ) {
				$data['comment_status'] = 0;
			} 

			return $data;
		}
	}	
	
	// add script and style to header area
	add_action( 'wp_head', 'university_education_head_script' );
	if( !function_exists('university_education_head_script') ){
		function university_education_head_script() {	
			global $university_education_theme_option;
			if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
			// show the user your favicon theme option		
				if( !empty($university_education_theme_option['favicon-id']) ){
					if( is_numeric($university_education_theme_option['favicon-id']) ){ 
						$favicon = wp_get_attachment_image_src($university_education_theme_option['favicon-id'], 'full');
						$university_education_theme_option['favicon-id'] = esc_url($favicon[0]);
					}
					echo '<link rel="shortcut icon" href="' . esc_url($university_education_theme_option['favicon-id']) . '" type="image/x-icon" />';
				}
			}
			//<!-- load the script for older ie version -->
		}
	}
	
	

	// add support to post and comment RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	
	

	// set up the content width based on the theme's design
	if ( !isset($content_width) ) $content_width = $university_education_theme_option['content-width'];	

	
	
	// add tinymce editor style
	add_action( 'init', 'university_education_add_editor_styles' );
	if( !function_exists('university_education_add_editor_styles') ){
		function university_education_add_editor_styles() {
			add_editor_style('/css/editor-style.css');
		}
	}
	
	
	
	// action to require the necessary wordpress function
 	add_action( 'after_setup_theme', 'university_education_theme_setup' );
	if( !function_exists('university_education_theme_setup') ){
		function university_education_theme_setup(){
			global $university_education_theme_option;
			// for translating the theme
			load_theme_textdomain( 'university-education', UOE_LOCAL_PATH . '/language/' );
			
			if(isset($university_education_theme_option['enable-one-page-header-navi']) && $university_education_theme_option['enable-one-page-header-navi'] == 'enable'){
				register_nav_menus( array(
					'main_menu'=> esc_html__( 'Main Navigation Responsive', 'university-education' ),
					'main_menu_responsive'=> esc_html__( 'Main Navigation Responsive', 'university-education' ),
					'main_menu_single'=> esc_html__( 'Main Navigation Single', 'university-education' ),
				));
			}else{
				register_nav_menus( array(
					'main_menu'=> esc_html__( 'Main Navigation', 'university-education' ),
					'main_menu_responsive'=> esc_html__( 'Main Navigation Responsive', 'university-education' ),
					'top_navigation'=> esc_html__( 'Top Navigation', 'university-education' ),
				));
			}
			
			
			// adds RSS feed links to <head> for posts and comments.			
			add_theme_support( 'automatic-feed-links' );
			
			//WooCommerce Support
			add_theme_support( 'woocommerce' );
			
			//title tags
			add_theme_support( 'title-tag' );
			
			// This theme supports a variety of post formats.
			add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );			
		}
	}
	
	add_filter('get_the_excerpt', 'university_education_strip_excerpt_link');	
	if( !function_exists('university_education_strip_excerpt_link') ){
		function university_education_strip_excerpt_link( $excerpt ) {
			return preg_replace('#^https?://\S+#', '', $excerpt);
		}
	}	
	if( !function_exists('university_education_set_excerpt_length') ){
		function university_education_set_excerpt_length( $length ){
			global $university_education_excerpt_length; return $university_education_excerpt_length ;
		}
	}	
	
	
	//ICL Languages
	
		if(!function_exists('university_education_languages_list')){
			function university_education_languages_list(){
				if(function_exists('icl_get_languages')){
					$languages = icl_get_languages('skip_missing=0&orderby=code');
					if(!empty($languages)){
						echo '<div class="btn-group">
							<button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" class="btn btn-default btn-lg dropdown-toggle">';
								foreach($languages as $l){
									if($l['active']){
										echo icl_disp_language($l['native_name'], $l['translated_name']);	
									}
								}							
								echo '<i aria-hidden="true" class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu">';
							foreach($languages as $l){
								echo '<li>';
								if($l['country_flag_url']){
									echo '<a href="'.$l['url'].'">';
									echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
									echo icl_disp_language($l['native_name'], $l['translated_name']);
									echo '</a>';
								}
							}
							echo '</ul>
						</div>';
					}
				}
			}
		}
	
	
	
	
