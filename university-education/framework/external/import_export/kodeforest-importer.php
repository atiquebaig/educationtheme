<?php
	/*	
	*	Kodeforest Import Variable Setting
	*/

	if( is_admin() ){
		add_action('university_education_import_end', 'university_education_add_import_action');
	}
	
	// init the $university_education_theme_option value and customizer value upon activation	
	
	add_action( 'after_switch_theme', 'university_education_get_default_themeoption' );
	if( !function_exists('university_education_get_default_themeoption') ){
		function university_education_get_default_themeoption() {
			$university_education_theme_option = get_option('university_education_admin_option', array());
			if(empty($university_education_theme_option)){
				$default_file = UOE_LOCAL_PATH . '/framework/external/import_export/theme_options.txt';
				$default_admin_option = 'logo=&logo-width=&logo-height=&logo-top-margin=5&logo-bottom-margin=&favicon-id=&enable-header-option=disable&enable-header-option=enable&kode-header-style=header-style-1&enable-top-bar=disable&enable-top-bar=enable&kode-header-navi-sidebar=slide-left&enable-top-bar-left=disable&enable-top-bar-left=enable&enable-language=disable&enable-language=enable&enable-login=disable&enable-login=enable&kode-top-bar-trans=transparent&top-bar-background-color=%23ffffff&enable-menu-fullwidth=disable&top-bar-left-text=&enable-sticky-menu=disable&enable-sticky-menu=enable&enable-one-page-header-navi=disable&enable-breadcrumbs=disable&enable-breadcrumbs=enable&enable-boxed-style=full-style&kode-body-style=body-color&body-bg-color=%23ffffff&body-background-image=&body-background-pattern=1&kode-body-position=body-scroll&enable-nice-scroll=disable&nice-scroll-color=%23ffffff&nice-scroll-radius=5px&nice-scroll-width=12px&nice-scroll-touch=true&enable-rtl-layout=disable&enable-responsive-mode=disable&enable-responsive-mode=enable&video-ratio=16%2F9&show-footer=disable&show-footer=enable&kode-footer-style=footer-style-1&footer-background-color=%23272727&footer-background-opacity=.8&footer-background-image=58&footer-layout=3&show-newsletter=disable&show-newsletter=enable&newsletter-title=Subscribe+weekly+newsletter&footer-logo-image=163&show-cards=disable&show-cards=enable&show-copyright=disable&show-copyright=enable&kode-copyright-text=%C2%A9+All+Rights+reserved.+Powered+By+KODEFOREST&kode-back-top=disable&kode-back-top=enable&subheader-background-color=%23000&subheader-background-opacity=0.6&subheader-bg-position=body-scroll&default-page-title=415&default-post-title-background=258&default-search-archive-title-background=245&default-404-title-background=99&color-scheme-one=%23e79800&body-background=%23ffffff&nav-area-background-color=%23ffffff&main-menu-text=%23666666&main-menu-link-background=%23ffffff&main-menu-active-link=%23ffffff&main-menu-active-link-bg=%23e79800&main-menu-link-background-on-hover=%23e79800&main-menu-link-color-on-hover=%23ffffff&main-menu-link-icon-color-on-hover=%23e79800&main-menu-link-active-color=%23ffffff&submenu-background=%23ffffff&submenu-bottom-border-color=%23ffffff&submenu-link-color=%23666666&submenu-hover-link-color=%23ffffff&submenu-hover-link-hover-bg=%23e79800&submenu-link-active-color=%23ffffff&submenu-link-active-bg-color=%23e79800&body-font-family=Source+Sans+Pro&body-font-size=16&body-line-height=26&body-font-weight=300&body-letter-spacing=0.2&body-text-transform=none&h1-font-family=Montserrat&h1-font-size=62&h1-line-height=72&h1-font-weight=500&h1-letter-spacing=5&h1-text-transform=uppercase&h2-font-family=Montserrat&h2-font-size=26&h2-line-height=36&h2-font-weight=500&h2-letter-spacing=&h2-text-transform=uppercase&h3-font-family=Montserrat&h3-font-size=30&h3-line-height=40&h3-font-weight=500&h3-letter-spacing=0.2&h3-text-transform=uppercase&h4-font-family=Montserrat&h4-font-size=24&h4-line-height=34&h4-font-weight=500&h4-letter-spacing=0.2&h4-text-transform=uppercase&h5-font-family=Montserrat&h5-font-size=20&h5-line-height=30&h5-font-weight=500&h5-letter-spacing=0.2&h5-text-transform=uppercase&h6-font-family=Montserrat&h6-font-size=18&h6-line-height=28&h6-font-weight=500&h6-letter-spacing=0.2&h6-text-transform=uppercase&navigation-font-family=Open+Sans&navigation-font-size=14&navigation-line-height=24&navigation-font-weight=700&navigation-letter-spacing=0.2&navigation-text-transform=uppercase&thumbnail-size=thumbnail&post-sidebar-template=no-sidebar&post-sidebar-left=blog&post-sidebar-right=blog&single-post-feature-image=disable&single-post-feature-image=enable&single-post-date=disable&single-post-date=enable&single-post-author=disable&single-post-author=enable&single-post-comments=disable&single-post-comments=enable&single-post-tags=disable&single-post-tags=enable&single-post-category=disable&single-post-category=enable&single-next-pre=disable&single-next-pre=enable&single-recommended-post=disable&single-recommended-post=enable&recommended-thumbnail-size=thumbnail&archive-sidebar-template=no-sidebar&archive-sidebar-left=blog&archive-sidebar-right=blog&archive-blog-style=blog-full&archive-col-size=3&archive-num-excerpt=25&single-event-feature-image=disable&single-event-feature-image=enable&single-event-feature-size=uoe-full-slider&single-event-date=disable&single-event-date=enable&single-event-organizer=disable&single-event-organizer=enable&single-event-tags=disable&single-event-tags=enable&single-event-gallery=disable&single-event-gallery=enable&single-related-events=disable&single-related-events=enable&single-related-events-meta=disable&single-related-events-meta=enable&single-event-related-size=uoe-post-thumbnail-size&single-event-comments=disable&single-event-comments=enable&single-teachers-feature-image=disable&single-teachers-feature-image=enable&single-teachers-comments=disable&single-teachers-comments=enable&single-teachers-rating=disable&single-teachers-rating=enable&woo-post-title=disable&woo-post-title=enable&woo-post-price=disable&woo-post-price=enable&woo-post-variable-price=disable&woo-post-variable-price=enable&woo-post-related=disable&woo-post-related=enable&woo-post-sku=disable&woo-post-sku=enable&woo-post-category=disable&woo-post-category=enable&woo-post-tags=disable&woo-post-tags=enable&woo-post-outofstock=disable&woo-post-outofstock=enable&woo-post-saleicon=disable&woo-post-saleicon=enable&woo-list-cart-btn=disable&woo-list-cart-btn=enable&woo-list-title=disable&woo-list-title=enable&woo-list-price=disable&woo-list-price=enable&woo-list-rating=disable&woo-list-rating=enable&all-products-per-row=3&all-products-sidebar=no-sidebar&all-products-sidebar-left=blog&all-products-sidebar-right=blog&delicious-header-social=&digg-header-social=&facebook-header-social=%23&flickr-header-social=&google-plus-header-social=%23&linkedin-header-social=%23&pinterest-header-social=%23&skype-header-social=&stumble-upon-header-social=&tumblr-header-social=&twitter-header-social=%23&vimeo-header-social=&youtube-header-social=&instagram-header-social=&enable-social-share=disable&enable-social-share=enable&digg-share=disable&digg-share=enable&facebook-share=disable&facebook-share=enable&google-plus-share=disable&google-plus-share=enable&linkedin-share=disable&linkedin-share=enable&my-space-share=disable&my-space-share=enable&pinterest-share=disable&pinterest-share=enable&reddit-share=disable&reddit-share=enable&stumble-upon-share=disable&stumble-upon-share=enable&twitter-share=disable&twitter-share=enable&instagram-share=disable&instagram-share=enable&bx-slider-effects=slide&bx-min-slide=1&bx-max-slide=1&bx-slide-margin=0&bx-arrow=disable&bx-arrow=enable&bx-pause-time=7000&bx-slide-speed=600&flex-slider-effects=fade&flex-pause-time=7000&flex-slide-speed=600&nivo-slider-effects=sliceDownRight&nivo-pause-time=7000&nivo-slide-speed=600&caption-title-color=%23ffffff&caption-background-color-switch=disable&caption-background-color=%23ffffff&title-font-size=30&caption-desc-color=%23ffffff&caption-btn-color-switch=disable&caption-font-size=30&caption-btn-color=%23020202&caption-btn-color-hover=%23ffffff&caption-btn-color-border=%23020202&caption-btn-color-bg=%23ffffff&caption-btn-hover-bg=%23020202&caption-btn-arrow-color=%23ffffff&caption-btn-arrow-hover=%23ffffff&caption-btn-arrow-bg=%23ffffff&caption-btn-arrow-hover-bg=%23ffffff&courses-search-page=445&sidebar-size=4&both-sidebar-size=3&sidebar-tbn=disable&sidebar-tbn=enable&sidebar-bg-color=%23f9f9f9&sidebar-padding-top=30&sidebar-padding-bottom=30&sidebar-padding-left=30&sidebar-padding-right=30&sidebar-element%5B%5D=blog&sidebar-element%5B%5D=contact&sidebar-element%5B%5D=event+detail&sidebar-element%5B%5D=news+list&sidebar-element%5B%5D=Shortcodes&sidebar-element%5B%5D=features&sidebar-element%5B%5D=Widgets+All&sidebar-element%5B%5D=left+sidebar&sidebar-element%5B%5D=right+sidebar&sidebar-element%5B%5D=woocommerce&mail-chimp-api=&mail-chimp-listid=&google-public-api=&google-secret-api=&google-map-api=&twitter-user-name=&twitter-consumer-api=&twitter-consumer-secret=&twitter-access-token=&twitter-access-token-secret=&twitter-show-num=5&twitter-cache=0';
				parse_str(university_education_stripslashes($default_admin_option), $k_option ); 
				$k_option = university_education_stripslashes($k_option);	
				update_option('university_education_admin_option', $k_option);
			}
		}
	}
	
	if( !function_exists('university_education_get_default_reset') ){
		function university_education_get_default_reset() {
			$default_file = UOE_LOCAL_PATH . '/framework/external/import_export/theme_options.txt';
			$default_admin_option = 'logo=&logo-width=&logo-height=&logo-top-margin=5&logo-bottom-margin=&favicon-id=&enable-header-option=disable&enable-header-option=enable&kode-header-style=header-style-1&enable-top-bar=disable&enable-top-bar=enable&kode-header-navi-sidebar=slide-left&enable-top-bar-left=disable&enable-top-bar-left=enable&enable-language=disable&enable-language=enable&enable-login=disable&enable-login=enable&kode-top-bar-trans=transparent&top-bar-background-color=%23ffffff&enable-menu-fullwidth=disable&top-bar-left-text=&enable-sticky-menu=disable&enable-sticky-menu=enable&enable-one-page-header-navi=disable&enable-breadcrumbs=disable&enable-breadcrumbs=enable&enable-boxed-style=full-style&kode-body-style=body-color&body-bg-color=%23ffffff&body-background-image=&body-background-pattern=1&kode-body-position=body-scroll&enable-nice-scroll=disable&nice-scroll-color=%23ffffff&nice-scroll-radius=5px&nice-scroll-width=12px&nice-scroll-touch=true&enable-rtl-layout=disable&enable-responsive-mode=disable&enable-responsive-mode=enable&video-ratio=16%2F9&show-footer=disable&show-footer=enable&kode-footer-style=footer-style-1&footer-background-color=%23272727&footer-background-opacity=.8&footer-background-image=58&footer-layout=3&show-newsletter=disable&show-newsletter=enable&newsletter-title=Subscribe+weekly+newsletter&footer-logo-image=163&show-cards=disable&show-cards=enable&show-copyright=disable&show-copyright=enable&kode-copyright-text=%C2%A9+All+Rights+reserved.+Powered+By+KODEFOREST&kode-back-top=disable&kode-back-top=enable&subheader-background-color=%23000&subheader-background-opacity=0.6&subheader-bg-position=body-scroll&default-page-title=415&default-post-title-background=258&default-search-archive-title-background=245&default-404-title-background=99&color-scheme-one=%23e79800&body-background=%23ffffff&nav-area-background-color=%23ffffff&main-menu-text=%23666666&main-menu-link-background=%23ffffff&main-menu-active-link=%23ffffff&main-menu-active-link-bg=%23e79800&main-menu-link-background-on-hover=%23e79800&main-menu-link-color-on-hover=%23ffffff&main-menu-link-icon-color-on-hover=%23e79800&main-menu-link-active-color=%23ffffff&submenu-background=%23ffffff&submenu-bottom-border-color=%23ffffff&submenu-link-color=%23666666&submenu-hover-link-color=%23ffffff&submenu-hover-link-hover-bg=%23e79800&submenu-link-active-color=%23ffffff&submenu-link-active-bg-color=%23e79800&body-font-family=Source+Sans+Pro&body-font-size=16&body-line-height=26&body-font-weight=300&body-letter-spacing=0.2&body-text-transform=none&h1-font-family=Montserrat&h1-font-size=62&h1-line-height=72&h1-font-weight=500&h1-letter-spacing=5&h1-text-transform=uppercase&h2-font-family=Montserrat&h2-font-size=26&h2-line-height=36&h2-font-weight=500&h2-letter-spacing=&h2-text-transform=uppercase&h3-font-family=Montserrat&h3-font-size=30&h3-line-height=40&h3-font-weight=500&h3-letter-spacing=0.2&h3-text-transform=uppercase&h4-font-family=Montserrat&h4-font-size=24&h4-line-height=34&h4-font-weight=500&h4-letter-spacing=0.2&h4-text-transform=uppercase&h5-font-family=Montserrat&h5-font-size=20&h5-line-height=30&h5-font-weight=500&h5-letter-spacing=0.2&h5-text-transform=uppercase&h6-font-family=Montserrat&h6-font-size=18&h6-line-height=28&h6-font-weight=500&h6-letter-spacing=0.2&h6-text-transform=uppercase&navigation-font-family=Open+Sans&navigation-font-size=14&navigation-line-height=24&navigation-font-weight=700&navigation-letter-spacing=0.2&navigation-text-transform=uppercase&thumbnail-size=thumbnail&post-sidebar-template=no-sidebar&post-sidebar-left=blog&post-sidebar-right=blog&single-post-feature-image=disable&single-post-feature-image=enable&single-post-date=disable&single-post-date=enable&single-post-author=disable&single-post-author=enable&single-post-comments=disable&single-post-comments=enable&single-post-tags=disable&single-post-tags=enable&single-post-category=disable&single-post-category=enable&single-next-pre=disable&single-next-pre=enable&single-recommended-post=disable&single-recommended-post=enable&recommended-thumbnail-size=thumbnail&archive-sidebar-template=no-sidebar&archive-sidebar-left=blog&archive-sidebar-right=blog&archive-blog-style=blog-full&archive-col-size=3&archive-num-excerpt=25&single-event-feature-image=disable&single-event-feature-image=enable&single-event-feature-size=uoe-full-slider&single-event-date=disable&single-event-date=enable&single-event-organizer=disable&single-event-organizer=enable&single-event-tags=disable&single-event-tags=enable&single-event-gallery=disable&single-event-gallery=enable&single-related-events=disable&single-related-events=enable&single-related-events-meta=disable&single-related-events-meta=enable&single-event-related-size=uoe-post-thumbnail-size&single-event-comments=disable&single-event-comments=enable&single-teachers-feature-image=disable&single-teachers-feature-image=enable&single-teachers-comments=disable&single-teachers-comments=enable&single-teachers-rating=disable&single-teachers-rating=enable&woo-post-title=disable&woo-post-title=enable&woo-post-price=disable&woo-post-price=enable&woo-post-variable-price=disable&woo-post-variable-price=enable&woo-post-related=disable&woo-post-related=enable&woo-post-sku=disable&woo-post-sku=enable&woo-post-category=disable&woo-post-category=enable&woo-post-tags=disable&woo-post-tags=enable&woo-post-outofstock=disable&woo-post-outofstock=enable&woo-post-saleicon=disable&woo-post-saleicon=enable&woo-list-cart-btn=disable&woo-list-cart-btn=enable&woo-list-title=disable&woo-list-title=enable&woo-list-price=disable&woo-list-price=enable&woo-list-rating=disable&woo-list-rating=enable&all-products-per-row=3&all-products-sidebar=no-sidebar&all-products-sidebar-left=blog&all-products-sidebar-right=blog&delicious-header-social=&digg-header-social=&facebook-header-social=%23&flickr-header-social=&google-plus-header-social=%23&linkedin-header-social=%23&pinterest-header-social=%23&skype-header-social=&stumble-upon-header-social=&tumblr-header-social=&twitter-header-social=%23&vimeo-header-social=&youtube-header-social=&instagram-header-social=&enable-social-share=disable&enable-social-share=enable&digg-share=disable&digg-share=enable&facebook-share=disable&facebook-share=enable&google-plus-share=disable&google-plus-share=enable&linkedin-share=disable&linkedin-share=enable&my-space-share=disable&my-space-share=enable&pinterest-share=disable&pinterest-share=enable&reddit-share=disable&reddit-share=enable&stumble-upon-share=disable&stumble-upon-share=enable&twitter-share=disable&twitter-share=enable&instagram-share=disable&instagram-share=enable&bx-slider-effects=slide&bx-min-slide=1&bx-max-slide=1&bx-slide-margin=0&bx-arrow=disable&bx-arrow=enable&bx-pause-time=7000&bx-slide-speed=600&flex-slider-effects=fade&flex-pause-time=7000&flex-slide-speed=600&nivo-slider-effects=sliceDownRight&nivo-pause-time=7000&nivo-slide-speed=600&caption-title-color=%23ffffff&caption-background-color-switch=disable&caption-background-color=%23ffffff&title-font-size=30&caption-desc-color=%23ffffff&caption-btn-color-switch=disable&caption-font-size=30&caption-btn-color=%23020202&caption-btn-color-hover=%23ffffff&caption-btn-color-border=%23020202&caption-btn-color-bg=%23ffffff&caption-btn-hover-bg=%23020202&caption-btn-arrow-color=%23ffffff&caption-btn-arrow-hover=%23ffffff&caption-btn-arrow-bg=%23ffffff&caption-btn-arrow-hover-bg=%23ffffff&courses-search-page=445&sidebar-size=4&both-sidebar-size=3&sidebar-tbn=disable&sidebar-tbn=enable&sidebar-bg-color=%23f9f9f9&sidebar-padding-top=30&sidebar-padding-bottom=30&sidebar-padding-left=30&sidebar-padding-right=30&sidebar-element%5B%5D=blog&sidebar-element%5B%5D=contact&sidebar-element%5B%5D=event+detail&sidebar-element%5B%5D=news+list&sidebar-element%5B%5D=Shortcodes&sidebar-element%5B%5D=features&sidebar-element%5B%5D=Widgets+All&sidebar-element%5B%5D=left+sidebar&sidebar-element%5B%5D=right+sidebar&sidebar-element%5B%5D=woocommerce&mail-chimp-api=&mail-chimp-listid=&google-public-api=&google-secret-api=&google-map-api=&twitter-user-name=&twitter-consumer-api=&twitter-consumer-secret=&twitter-access-token=&twitter-access-token-secret=&twitter-show-num=5&twitter-cache=0';
			parse_str(university_education_stripslashes($default_admin_option), $k_option ); 
			return $k_option = university_education_stripslashes($default_admin_option);		
		}
	}
	
	
	
	if( !function_exists('university_education_get_widget_area') ){
		function university_education_get_widget_area(){
			$development = get_option('sidebars_widgets');
			unset($development['array_version']);
				foreach($development as $key=>$value){
					$newval = str_replace('wp_inactive_widgets','',$key);
					$widgets[] = str_replace('orphaned_widgets_1','',$newval);
				}
				return array_filter($widgets);
				
		}
	}
	
	if( !function_exists('university_education_str_before') ){
		function university_education_str_before($subject, $needle)
		{
			$p = strpos($subject, $needle);
			return substr($subject, 0, $p);
		}
	}
	
	if( !function_exists('university_education_get_widget_name_value') ){
		function university_education_get_widget_name_value(){
			$development_myval = get_option('sidebars_widgets');
			foreach(university_education_get_widget_area() as $val){
				foreach($development_myval[$val] as $keys=>$values){
					$string_val = university_education_str_before($values, "-");
					$wid_val[$string_val] = 'widget_'.$string_val;
				}
				
			}
			return $wid_val;
				
		}
	}
	
?>