<?php
	/*	
	*	University Of Education Theme option color File
	*	---------------------------------------------------------------------
	*	This file contains the color option setting 
	*	---------------------------------------------------------------------
	*	http://stackoverflow.com/questions/2270989/what-does-apply-filters-actually-do-in-wordpress
	*/
	
	add_filter('university_education_themeoption_panel', 'university_education_register_property_theme_option_color');
	if( !function_exists('university_education_register_property_theme_option_color') ){
		function university_education_register_property_theme_option_color( $array ){	
			global $university_education_theme_option;
			//if empty
			if( empty($array['general']['options']) ){
				return $array;
			}
			$property_head =  array(
				'title' => esc_html__('Color Options', 'university-education'),				
				'icon' => 'fa-eyedropper',
			);
			
			$property_general =  array(
				'title' => esc_html__('General', 'university-education'),
				'options' => array(
					'color-scheme-one' => array(
						'title' => esc_html__('Color Scheme First', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change the theme skin Color of your site.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'body-background' => array(
						'title' => esc_html__('Body Background', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change the Overall Color Scheme of your site.',
						'default' => '#ffffff',
						'selector'=> ''
					),
				)
			);	
			
			$property_menu =  array(
				'title' => esc_html__('Navigation', 'university-education'),
				'options' => array(
					'nav-area-background-color' => array(
						'title' => esc_html__('Navigation Background Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Navigation Background Color Note: this will work only on Header Style 2 and Header Style 4.',
						'default' => '#ffffff',
						'wrapper-class'=> 'kode-header-style-wrapper header-style-4-wrapper header-style-2-wrapper',
						'selector'=> ''
					),
					'main-menu-text' => array(
						'title' => esc_html__('Menu Link Text Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Link Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'main-menu-link-background' => array(
						'title' => esc_html__('Menu Link Background Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Link background Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'main-menu-active-link' => array(
						'title' => esc_html__('Menu Active Link Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Active Link Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'main-menu-active-link-bg' => array(
						'title' => esc_html__('Menu Active Link Background', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Active Link background Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					
					'main-menu-link-background-on-hover' => array(
						'title' => esc_html__('Menu Link Hover Background Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Link Background Color on Hover.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'main-menu-link-color-on-hover' => array(
						'title' => esc_html__('Menu Link Color on Hover', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Link Color on Hover.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'main-menu-link-icon-color-on-hover' => array(
						'title' => esc_html__('Menu Link Icon Color on Hover', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Link Icon Color on Hover.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'main-menu-link-active-color' => array(
						'title' => esc_html__('Menu Active Link Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Main Menu Active Link Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-background' => array(
						'title' => esc_html__('Submenu | Background', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu background Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-bottom-border-color' => array(
						'title' => esc_html__('Submenu Border Bottom Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu Border Bottom Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-link-color' => array(
						'title' => esc_html__('Submenu | Link Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu Link Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-hover-link-color' => array(
						'title' => esc_html__('Submenu | Hover Link Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu Hover Link Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-hover-link-hover-bg' => array(
						'title' => esc_html__('Submenu | Hover Link Background', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu Hover Link Background.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-link-active-color' => array(
						'title' => esc_html__('Sub Menu Active Link Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu Active Link Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					'submenu-link-active-bg-color' => array(
						'title' => esc_html__('Sub Menu Active Link BG Color', 'university-education'),
						'type' => 'colorpicker',
						'description' => 'This Color Picker allows you to change Submenu Active Sub Menu Active Link BG Color.',
						'default' => '#ffffff',
						'selector'=> ''
					),
					
				)
			);
			
			$array['color-settings']['options']['general'] = $property_general;
			$array['color-settings']['options']['menu'] = $property_menu;
			
			return $array;
		}
	}