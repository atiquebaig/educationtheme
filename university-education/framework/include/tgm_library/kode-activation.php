<?php

add_action( 'tgmpa_register', 'university_education_register_required_plugins' );
if( !function_exists('university_education_register_required_plugins') ){
	function university_education_register_required_plugins(){
		$plugins = array(
			
		
			
			array(
				'name'     				=> 'Kode Importer',
				'slug'     				=> 'kode-importer', 
				'source'   				=> get_template_directory() . '/framework/include/tgm_library/plugins/kode-importer.zip',				
				'required' 				=> false,
				'force_activation' 		=> false,
				'force_deactivation' 	=> true, 
			),
			
			array(
				'name'     				=> 'Kode Testimonials',
				'slug'     				=> 'kode-testimonials', 
				'source'   				=> get_template_directory() . '/framework/include/tgm_library/plugins/kode-testimonials.zip',
				'required' 				=> true,
				'force_activation' 		=> false,
				'force_deactivation' 	=> false, 
			),	

			array(
				'name'     				=> 'Kode Teachers',
				'slug'     				=> 'kode-teachers', 
				'source'   				=> get_template_directory() . '/framework/include/tgm_library/plugins/kode-teachers.zip',
				'required' 				=> true,
				'force_activation' 		=> false,
				'force_deactivation' 	=> false, 
			),
			
			array(
				'name'     				=> 'Kode Courses',
				'slug'     				=> 'kode-courses', 
				'source'   				=> get_template_directory() . '/framework/include/tgm_library/plugins/kode-courses.zip',
				'required' 				=> true,
				'force_activation' 		=> false,
				'force_deactivation' 	=> false, 
			),
			
			array(
				'name'     				=> 'Kode Shortcode',
				'slug'     				=> 'kode-shortcode', 
				'source'   				=> get_template_directory() . '/framework/include/tgm_library/plugins/kode-shortcode.zip',
				'required' 				=> true,
				'force_activation' 		=> false,
				'force_deactivation' 	=> false, 
			),
			
			array('name' => 'Social Login', 'slug' => 'wordpress-social-login', 'required' => true),			
			array('name' => 'WooCommerce', 'slug' => 'woocommerce', 'required' => false),
			array('name' => 'Contact Form 7', 'slug' => 'contact-form-7', 'required' => true),
			
			array(
				'name'      => esc_attr__('Event Manager','university-education'),
				'slug'      => 'events-manager',
				'required'  => false,
			),

		);

		$config = array(
			'id'           => 'university-education',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'parent_slug'  => 'themes.php',            // Parent menu slug.
			'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => false,                   // Automatically activate plugins after installation or not.
			'message'      => '',                      // Message to output right before the plugins table.	
			
			'strings'      => array(
				'page_title'                      => esc_attr__( 'Install Required Plugins', 'university-education' ),
				'menu_title'                      => esc_attr__( 'Install Plugins', 'university-education' ),
				'installing'                      => esc_attr__( 'Installing Plugin: %s', 'university-education' ), // %s = plugin name.
				'oops'                            => esc_attr__( 'Something went wrong with the plugin API.', 'university-education' ),
				'notice_can_install_required'     => _n_noop(
					'This theme requires the following plugin: %1$s.',
					'This theme requires the following plugins: %1$s.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_can_install_recommended'  => _n_noop(
					'This theme recommends the following plugin: %1$s.',
					'This theme recommends the following plugins: %1$s.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_cannot_install'           => _n_noop(
					'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
					'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_ask_to_update'            => _n_noop(
					'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
					'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_ask_to_update_maybe'      => _n_noop(
					'There is an update available for: %1$s.',
					'There are updates available for the following plugins: %1$s.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_cannot_update'            => _n_noop(
					'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
					'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_can_activate_required'    => _n_noop(
					'The following required plugin is currently inactive: %1$s.',
					'The following required plugins are currently inactive: %1$s.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_can_activate_recommended' => _n_noop(
					'The following recommended plugin is currently inactive: %1$s.',
					'The following recommended plugins are currently inactive: %1$s.',
					'university-education'
				), // %1$s = plugin name(s).
				'notice_cannot_activate'          => _n_noop(
					'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
					'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
					'university-education'
				), // %1$s = plugin name(s).
				'install_link'                    => _n_noop(
					'Begin installing plugin',
					'Begin installing plugins',
					'university-education'
				),
				'update_link' 					  => _n_noop(
					'Begin updating plugin',
					'Begin updating plugins',
					'university-education'
				),
				'activate_link'                   => _n_noop(
					'Begin activating plugin',
					'Begin activating plugins',
					'university-education'
				),
				'return'                          => esc_attr__( 'Return to Required Plugins Installer', 'university-education' ),
				'plugin_activated'                => esc_attr__( 'Plugin activated successfully.', 'university-education' ),
				'activated_successfully'          => esc_attr__( 'The following plugin was activated successfully:', 'university-education' ),
				'plugin_already_active'           => esc_attr__( 'No action taken. Plugin %1$s was already active.', 'university-education' ),  // %1$s = plugin name(s).
				'plugin_needs_higher_version'     => esc_attr__( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'university-education' ),  // %1$s = plugin name(s).
				'complete'                        => esc_attr__( 'All plugins installed and activated successfully. %1$s', 'university-education' ), // %s = dashboard link.
				'contact_admin'                   => esc_attr__( 'Please contact the administrator of this site for help.', 'university-education' ),

				'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
			),
		
		);

		tgmpa( $plugins, $config );
	}
}