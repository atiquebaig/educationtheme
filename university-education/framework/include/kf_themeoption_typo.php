<?php
	/*	
	*	The BookStore Framework File
	*	---------------------------------------------------------------------
	*	This file contains the admin option setting 
	*	---------------------------------------------------------------------
	*	http://stackoverflow.com/questions/2270989/what-does-apply-filters-actually-do-in-wordpress
	*/
	
	add_filter('university_education_themeoption_panel', 'university_education_register_property_theme_typo');
	if( !function_exists('university_education_register_property_theme_typo') ){
		function university_education_register_property_theme_typo( $array ){	
			global $university_education_theme_option;
			//if empty
			if( empty($array['general']['options']) ){
				return $array;
			}
			$property_head =  array(
				'title' => esc_html__('Typography Options', 'university-education'),				
				'icon' => 'fa-paint-brush',
			);
			
			
			$property_theme_typo =  array(
				'title' => esc_html__('Paragraph', 'university-education'),
				'options' => array(
					'body-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'body-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size of the theme.', 'university-education'),
						'default' => '14',						
						'data-type' => 'pixel'											
					),				
					'body-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '24',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'body-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => 'normal',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'body-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'body-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),	
						),
						'default' => 'capitalize',
					),
				)
			);
			
			$property_theme_h1 =  array(
				'title' => esc_html__('Heading 1', 'university-education'),
				'options' => array(
					'h1-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'h1-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '62',						
						'data-type' => 'pixel'											
					),				
					'h1-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '62',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'h1-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => '400',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'h1-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'h1-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'capitalize',
					),
				)
			);
			
			
			$property_theme_h2 =  array(
				'title' => esc_html__('Heading 2', 'university-education'),
				'options' => array(
					'h2-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'h2-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '45',						
						'data-type' => 'pixel'											
					),				
					'h2-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '55',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'h2-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => '400',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'h2-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'h2-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'capitalize',
					),
				)
			);
			
			$property_theme_h3 =  array(
				'title' => esc_html__('Heading 3', 'university-education'),
				'options' => array(
					'h3-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'h3-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '30',						
						'data-type' => 'pixel'											
					),				
					'h3-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '40',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'h3-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => '400',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'h3-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'h3-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'capitalize',
					),
				)
			);
			
			$property_theme_h4 =  array(
				'title' => esc_html__('Heading 4', 'university-education'),
				'options' => array(
					'h4-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'h4-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '24',						
						'data-type' => 'pixel'											
					),				
					'h4-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '34',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'h4-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => '400',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'h4-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'h4-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'capitalize',
					),
				)
			);
			
			$property_theme_h5 =  array(
				'title' => esc_html__('Heading 5', 'university-education'),
				'options' => array(
					'h5-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'h5-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '20',						
						'data-type' => 'pixel'											
					),				
					'h5-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '30',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'h5-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => '400',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'h5-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'h5-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'capitalize',
					),
				)
			);
			
			$property_theme_h6 =  array(
				'title' => esc_html__('Heading 6', 'university-education'),
				'options' => array(
					'h6-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'h6-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '18',						
						'data-type' => 'pixel'											
					),				
					'h6-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '28',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'h6-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => '400',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'h6-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'h6-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize'	=> esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'capitalize',
					),
				)
			);
			
			
			$property_theme_nav =  array(
				'title' => esc_html__('Navigation', 'university-education'),
				'options' => array(
					'navigation-font-family' => array(
						'title' => esc_html__('Font Family', 'university-education'),
						'type' => 'font_option',
						'description'=> esc_html__('Please Select the Font Family from here.', 'university-education'),
						'default' => 'Arial, Helvetica, sans-serif',
						'data-type' => 'font_option',
						'selector' => 'body, input, textarea{ font-family: #gdlr#; }'
					),
					'navigation-font-size' => array(
						'title' => esc_html__('Font Size', 'university-education'),
						'type' => 'sliderbar',
						'description'=> esc_html__('This option will let you increase / decrease the font Size.', 'university-education'),
						'default' => '14',						
						'data-type' => 'pixel'											
					),				
					'navigation-line-height'=> array(
						'title'=> esc_html__('Line Height' ,'university-education'),
						'type'=> 'text',											
						'default' => '24',
						'description'=> esc_html__('Adjust line height For Example: 10', 'university-education')
					),
					'navigation-font-weight'=> array(
						'title'=> esc_html__('Font Weight' ,'university-education'),
						'type'=> 'text',											
						'default' => 'normal',
						'description'=> esc_html__('Adjust Font Weight For Example: normal, bold, 100 to 800', 'university-education')
					),
					'navigation-letter-spacing'=> array(
						'title'=> esc_html__('Letter Spacing' ,'university-education'),
						'type'=> 'text',											
						'default' => '0.2',
						'description'=> esc_html__('Adjust Letter Spacing. For Example: 5', 'university-education')
					),
					'navigation-text-transform' => array(
						'title' => esc_html__('Text Transform', 'university-education'),
						'type' => 'combobox',
						'description'=> esc_html__('You can manage transform uppercase or lowercase.', 'university-education'),
						'options' => array(
							'none'	=> esc_html__('None', 'university-education'),
							'uppercase' => esc_html__('Uppercase', 'university-education'),
							'lowercase'	=> esc_html__('Lowercase', 'university-education'),
							'capitalize' => esc_html__('Capitalize', 'university-education'),
						),
						'default' => 'uppercase',
					),
				)
			);
			
			
			
			$array['font-settings']['options']['body'] = $property_theme_typo;
			$array['font-settings']['options']['heading-1'] = $property_theme_h1;
			$array['font-settings']['options']['heading-2'] = $property_theme_h2;
			$array['font-settings']['options']['heading-3'] = $property_theme_h3;
			$array['font-settings']['options']['heading-4'] = $property_theme_h4;
			$array['font-settings']['options']['heading-5'] = $property_theme_h5;
			$array['font-settings']['options']['heading-6'] = $property_theme_h6;
			$array['font-settings']['options']['navigation'] = $property_theme_nav;
			
			
			return $array;
		}
	}