<?php
	/*	
	*	University Of Education Options File
	*	---------------------------------------------------------------------
	*	This file contains the admin option setting 
	*	---------------------------------------------------------------------
	*	http://stackoverflow.com/questions/2270989/what-does-apply-filters-actually-do-in-wordpress
	*/
	
	// create the main admin option
	if( is_admin() ){
		add_action('after_setup_theme', 'university_education_create_themeoption');
	}
	
	if( !function_exists('university_education_create_themeoption') ){
	
		function university_education_create_themeoption(){
		
			global $university_education_theme_option;
			if(!isset($university_education_theme_option['sidebar-element'])){$university_education_theme_option['sidebar-element'] = array('blog','contact');}
			// Theme Options Default data
			$university_education_default_data_array = array(
				'page_title' => 'Theme ' . esc_html__('Option', 'university-education'),
				'menu_title' => 'Theme Options',
				'menu_slug' => 'education',
				'save_option' => 'university_education_admin_option',
				'role' => 'edit_theme_options'
			);
			
			
			new university_education_themeoption_panel(
				
				// Theme Options Default data
				$university_education_default_data_array,
				
				// Theme Options setting
				apply_filters('university_education_themeoption_panel',
					array(
						// general menu
						'general' => array(
							'title' => esc_html__('General Settings', 'university-education'),
							'icon' => 'fa fa-diamond',
							'options' => array(
								'header-logo' => array(
									'title' => esc_html__('Logo Settings', 'university-education'),
									'options' => array(
										'logo' => array(
											'title' => esc_html__('Upload Logo', 'university-education'),
											'button' => esc_html__('Set As Logo', 'university-education'),
											'type' => 'upload',
											'description' => 'Please upload your logo supported for example jpeg, gif, png',
										),
										'logo-width' => array(
											'title' => esc_html__('Logo Width', 'university-education'),
											'type' => 'text',
											'description' => 'Please enter the width of the logo in Numbers E.g 20',
											'default' => '0',
											//'selector' => '.kode-logo{ margin-top: #kode#; }',
											'data-type' => 'pixel'
											
										),
										'logo-height' => array(
											'title' => esc_html__('Logo Height', 'university-education'),
											'type' => 'text',
											'description' => 'Please enter the height of the logo in Numbers E.g 20',
											'default' => '0',
											//'selector' => '.kode-logo{ margin-bottom: #kode#; }',
											'data-type' => 'pixel'
										),											
										'logo-top-margin' => array(
											'title' => esc_html__('Logo Top Margin', 'university-education'),
											'type' => 'text',
											'description' => 'Please enter the Top Margin of the logo in Numbers E.g 20',
											'default' => '0',
											'selector' => '.kode-logo{ margin-top: #kode#; }',
											'data-type' => 'pixel'
										),
										'logo-bottom-margin' => array(
											'title' => esc_html__('Logo Bottom Margin', 'university-education'),
											'type' => 'text',
											'description' => 'Please enter the Bottom Margin of the logo in Numbers E.g 20',
											'default' => '0',
											'selector' => '.kode-logo{ margin-bottom: #kode#; }',
											'data-type' => 'pixel'
										),	
										'favicon-id' => array(
											'title' => esc_html__('Upload Favicon ( .ico file )', 'university-education'),
											'button' => esc_html__('Select Icon', 'university-education'),
											'type' => 'upload',
											'description' => 'You can upload the favicon icon from here.',
										),	
										
									)
								),
								'header-section' => array(
									'title' => esc_html__('Header Settings', 'university-education'),
									'options' => array(
										'enable-header-option' => array(
											'title' => esc_attr__('Enable Header All Pages', 'university-education'),
											'type' => 'checkbox',	
											'description'=>esc_attr__('Click here to Turn On Header Style for All pages.', 'university-education'),
											'default' => 'enable'
										),
										'kode-header-style' => array(
											'title' => esc_attr__('Select Header', 'university-education'),
											'type' => 'radioheader',	
											'description'=>esc_attr__('There are 4 Different Header Styles Available here. Click on the menu and select the Header Styles here from of your choice.', 'university-education'),
											'options' => array(
												'header-style-1'=>UOE_PATH . '/framework/include/backend_assets/images/headers/1.jpg',
											),
										),
										'enable-top-bar' => array(
											'title' => esc_html__('Enable Top Bar', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Top Bar from here.',
											'wrapper-class'=> ''											
										),
										'kode-header-navi-sidebar' => array(
											'title' => esc_html__('Side Menu Style', 'university-education'),
											'type' => 'combobox',
											'description' => esc_attr__('You can change your site slide menu layout.','university-education'),	
											'options' => array(
												'slide-left' => esc_html__('Slide left', 'university-education'),
												'push-left' => esc_html__('Push left', 'university-education'),
												'slide-right' => esc_html__('Slide Right', 'university-education'),
												'push-right' => esc_html__('Push Right', 'university-education'),
												'slide-top' => esc_html__('Slide Top', 'university-education'),
												'push-top' => esc_html__('Push Top', 'university-education'),
												'slide-bottom' => esc_html__('Slide Bottom', 'university-education'),
												'push-bottom' => esc_html__('Push Bottom', 'university-education'),
											)
										),
										'enable-top-bar-left' => array(
											'title' => esc_html__('Enable Top Bar Left', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Top Bar left opening time text from here.',
											'wrapper-class'=> '',
										),
										'enable-language' => array(
											'title' => esc_html__('language Switcher', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Top Bar language switcher drop down from here.',
											'wrapper-class'=> '',
										),
										'enable-login' => array(
											'title' => esc_html__('Login / Sign Up', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Top Bar login / sign up button from here.',
											'wrapper-class'=> '',
										),
										'kode-top-bar-trans' => array(
											'title' => esc_attr__('Header Style', 'university-education'),
											'type' => 'combobox',	
											'description'=>esc_attr__('There are 4 Different Header Styles Available here. Click on the Drop Down menu and select the Header Style here from of your choice.', 'university-education'),
											'options' => array(
												'transparent' => esc_attr__('Transparent', 'university-education'),
												'colored' => esc_attr__('Colored', 'university-education'),												
											)
										),
										'top-bar-background-color' => array(
											'title' => esc_html__('Top Bar Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker allows you to change the top bar background color.',
											'wrapper-class' => 'colored-wrapper kode-top-bar-trans-wrapper',
											'default' => '#ffffff',
										),
										'enable-menu-fullwidth' => array(
											'title' => esc_html__('Menu Full Width', 'university-education'),
											'type' => 'checkbox',
											'description' => 'You can Switch On / Off the Menu Full Width From Here, turning it on logo and other content will be removed other than Menu.',
											'default' => 'disable'
										),
										'top-bar-left-text' => array(
											'title' => esc_html__('Top Bar Left Text', 'university-education'),
											'type' => 'text',	
											'description' => 'Please enter the Top Bar left text here.',
											'wrapper-class'=> 'header-style-1-wrapper header-style-2-wrapper kode-header-style-wrapper',
										),	
										'enable-side-nav' => array(
											'title' => esc_html__('Enable Side Navigation', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the header side navigation Menu from here.',
											'default' => 'disable'
										),
										'slide-logo' => array(
											'title' => esc_html__('Side Upload Logo', 'university-education'),
											'button' => esc_html__('Set As Logo', 'university-education'),
											'type' => 'upload',
											'description' => 'Please upload your logo supported for example jpeg, gif, png',
										),
										'slide-logo-width' => array(
											'title' => esc_html__('Side Logo Width', 'university-education'),
											'type' => 'text',
											'description' => 'Please enter the width of the logo in Numbers E.g 20',
											'default' => '0',
											//'selector' => '.kode-logo{ margin-top: #kode#; }',
											'data-type' => 'pixel'
											
										),
										'slide-logo-height' => array(
											'title' => esc_html__('Side Logo Height', 'university-education'),
											'type' => 'text',
											'description' => 'Please enter the height of the logo in Numbers E.g 20',
											'default' => '0',
											//'selector' => '.kode-logo{ margin-bottom: #kode#; }',
											'data-type' => 'pixel'
										),	
										'enable-sticky-menu' => array(
											'title' => esc_html__('Enable Sticky', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Sticky Header Menu.',
											'default' => 'disable'
										),
										'enable-one-page-header-navi' => array(
											'title' => esc_html__('Enable One Page Header Nav', 'university-education'),
											'type' => 'checkbox',
											'description' => 'You can Enable / Disable the One Page Navigation Site option from here. If you want to use Multi Pages site then you need to disable the One Page Navigation option from here.',
											'default' =>'disable'
										),	
										'enable-breadcrumbs' => array(
											'title' => esc_html__('Breadcrumbs', 'university-education'),
											'type' => 'checkbox',	
											'description' => 'You can Enable / Disable the Breadcrumbs from here.',
											'default' => 'disable'
										),
										
										
									)
								),
								'layout-style' => array(
									'title' => esc_html__('Style & Layouts', 'university-education'),
									'options' => array(
										'enable-boxed-style' => array(
											'title' => esc_html__('Website Layout', 'university-education'),
											'type' => 'combobox',
											'description' => 'You can change your site layout to Full Width / Boxed Style',											
											'options' => array(
												'full-style' => esc_html__('Full Style', 'university-education'),
												'boxed-style' => esc_html__('Boxed Style', 'university-education')
											)
										),
										'kode-body-style' => array(
											'title' => esc_html__('Body Background Style', 'university-education'),
											'type' => 'combobox',	
											'description' => 'You have selected the Boxed Layout. Now you can set your Boxed layout style from here.',
											'options' => array(
												'body-color' => esc_html__('Body Background Color', 'university-education'),
												'body-background' => esc_html__('Body Background Image', 'university-education'),
												'body-pattern' => esc_html__('Body Background Pattern', 'university-education'),
											),
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper'
										),	
										'body-bg-color' => array(
											'title' => esc_html__('Body Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description' => 'This Color Picket allows you to change Body Background Color for the Boxed Layout.',
											'default' => '#ffffff',
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-color-wrapper kode-body-style-wrapper',
											'selector'=> ''
										),
										'body-background-image' => array(
											'title' => esc_html__('Background Image', 'university-education'),
											'type' => 'upload',
											'description' => 'You can upload the background image for your Boxed Layout.',
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-background-wrapper kode-body-style-wrapper'
										),	
										'body-background-pattern' => array(
											'title' => esc_html__('Background Pattern', 'university-education'),
											'type' => 'radioimage',
											'description' => 'There are 18 Built in Background patterns available in the theme. Just Select the pattern and save the Changes.',
											'options' => array(
												'1'=>UOE_PATH . '/images/pattern/pattern_1.png',
												'2'=>UOE_PATH . '/images/pattern/pattern_2.png', 
												'3'=>UOE_PATH . '/images/pattern/pattern_3.png',
												'4'=>UOE_PATH . '/images/pattern/pattern_4.png',
												'5'=>UOE_PATH . '/images/pattern/pattern_5.png',
												'6'=>UOE_PATH . '/images/pattern/pattern_6.png',
												'7'=>UOE_PATH . '/images/pattern/pattern_7.png',
												'8'=>UOE_PATH . '/images/pattern/pattern_8.png',
												'9'=>UOE_PATH . '/images/pattern/pattern_9.png',
												'10'=>UOE_PATH . '/images/pattern/pattern_10.png', 
												'11'=>UOE_PATH . '/images/pattern/pattern_11.png',
												'12'=>UOE_PATH . '/images/pattern/pattern_12.png',
												'13'=>UOE_PATH . '/images/pattern/pattern_13.png',
												'14'=>UOE_PATH . '/images/pattern/pattern_14.png',
												'15'=>UOE_PATH . '/images/pattern/pattern_15.png',
												'16'=>UOE_PATH . '/images/pattern/pattern_16.png',
												'17'=>UOE_PATH . '/images/pattern/pattern_17.png',
												'18'=>UOE_PATH . '/images/pattern/pattern_18.png'
											),
											'wrapper-class' => 'boxed-style-wrapper enable-boxed-style-wrapper pattern-size-wrap body-pattern-wrapper kode-body-style-wrapper',
											'default' => '1'
										),	
										'kode-body-position' => array(
											'title' => esc_html__('Body Background Position', 'university-education'),
											'type' => 'combobox',	
											'description' => 'Select the Body Background Position you want to have for your site.',
											'options' => array(
												'body-scroll' => esc_html__('Scroll', 'university-education'),
												'body-fixed' => esc_html__('Fixed', 'university-education')												
											),
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-background-wrapper kode-body-style-wrapper'
										),	
										'enable-nice-scroll' => array(
											'title' => esc_html__('Nice Scroll', 'university-education'),
											'type' => 'checkbox',
											'description' => 'You can Enable / Disable the Nice Scroll from here.',											
											'default' => 'enable'
										),
										'nice-scroll-color' => array(
											'title' => esc_html__('Nice Scroll Color', 'university-education'),
											'type' => 'colorpicker',
											'description' => esc_html('Nice Scroll Color','university-education'),
											'default' => '#ffffff',
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
											'selector'=> ''
										),
										'nice-scroll-radius' => array(
											'title' => esc_html__('Scroll Radius', 'university-education'),
											'type' => 'text',				
											'default'=>'5px',
											'description' => esc_html('nice scroll radius','university-education'),
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
										),	
										'nice-scroll-width' => array(
											'title' => esc_html__('Scroll Size', 'university-education'),
											'type' => 'text',				
											'default'=>'12px',
											'description' => esc_html('nice scroll width','university-education'),
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
										),	
										'nice-scroll-touch' => array(
											'title' => esc_html__('Nice Scroll Touch Behavior', 'university-education'),
											'type' => 'combobox',	
											'description' => esc_html('nice scroll width','university-education'),
											'options' => array(
												'true' => esc_html__('True', 'university-education'),
												'false' => esc_html__('False', 'university-education')
											),
											'wrapper-class'=> 'enable-nice-scroll-wrapper enable-nice-scroll-enable',
										),	
										'enable-rtl-layout' => array(
											'title' => esc_html__('Enable RTL', 'university-education'),
											'type' => 'checkbox',
											'description' => 'You can Enable / Disable the RTL Layout of your site.',											
											'default' => 'disable'
										),
										'enable-responsive-mode' => array(
											'title' => esc_html__('Enable Responsive', 'university-education'),
											'description' => 'You can Enable / Disable the Responsive Layout of your site.',
											'type' => 'checkbox',	
											'default' => 'enable'
										),		
										'video-ratio' => array(
											'title' => esc_html__('Default Video Ratio', 'university-education'),
											'type' => 'text',				
											'default'=>'16/9',
											'description'=>esc_html__('Please only fill number/number as default video ratio', 'university-education')
										),										
									)
								),	
								'footer-style' => array(
									'title' => esc_html__('Footer Settings', 'university-education'),
									'options' => array(
										'show-footer' => array(
											'title' => esc_html__('Show Footer', 'university-education'),
											'type' => 'checkbox',
											'description' => 'You can Switch On / Off the Footer From here.',
											'default' => 'enable'
										),
										'kode-footer-style' => array(
											'title' => esc_attr__('Select Footer', 'university-education'),
											'type' => 'radioheader',	
											'description'=>esc_attr__('There is currently one Footer Style Available here.', 'university-education'),
											'options' => array(
												'footer-style-1'=>UOE_PATH . '/framework/include/backend_assets/images/footer/footer-1.jpg',
											),
										),
										'footer-background-color' => array(
											'title' => esc_html__('Footer Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker Allows you to change the Footer Background Color.',
											'default' => '#000',											
										),
										'footer-background-opacity'=> array(
											'title'=> esc_html__('Footer Background Opacity' ,'university-education'),
											'type'=> 'text',
											'description' => 'You can Control the footer background image opacity from here.',
											'default' => '0.6',
											'description'=> esc_html__('Adjust footer background opacity from 0 to 1.', 'university-education')
										),
										'footer-background-image' => array(
											'title' => esc_html__('Footer Background Image', 'university-education'),
											'button' => esc_html__('Upload', 'university-education'),
											'type' => 'upload',
											'description' => 'You can Upload the footer Background Image from here.',
										),
										'footer-layout' => array(
											'title' => esc_html__('Footer Column Layout', 'university-education'),
											'type' => 'radioimage',
											'description' => 'There are 3 Footer Column Styles available in the theme. You can select any of the style you wish to have.',
											'options' => array(
												'1'=>	UOE_PATH . '/framework/include/backend_assets/images/footer-2-col.jpg',
												'2'=>	UOE_PATH . '/framework/include/backend_assets/images/footer-3-col.jpg',
												'3'=>	UOE_PATH . '/framework/include/backend_assets/images/footer-4-col.jpg',
											),
											'default' => '2',	
											'wrapper-class' => 'kode-footer-style-wrapper footer-style-1-wrapper footer-style-3-wrapper',
										),
										'show-newsletter' => array(
											'title' => esc_html__('Show Newsletter', 'university-education'),
											'type' => 'checkbox',
											'wrapper-class' => '',
											'description' => 'Use this option to Enable or Disable the Newsletter area for your site.',
											'default' => 'enable'
										),
										'newsletter-title' => array(
											'title' => esc_html__('Newsletter Title', 'university-education'),
											'type' => 'text',
											'default' => 'Subscribe weekly newsletter',
											'description' => 'Use this field to enter your Footer title over there.',	
											'class' => 'full-width',
										),	
										'footer-logo-image' => array(
											'title' => esc_html__('Footer Logo Image', 'university-education'),
											'button' => esc_html__('Upload', 'university-education'),
											'type' => 'upload',
											'description' => 'You can Upload the footer logo Image from here.',
										),
										'show-cards' => array(
											'title' => esc_html__('Show Payment Cards', 'university-education'),
											'type' => 'checkbox',
											'wrapper-class' => '',
											'description' => 'Use this option to Enable or Disable the footer payment cards.',
											'default' => 'enable'
										),
										'show-copyright' => array(
											'title' => esc_html__('Show Copyright', 'university-education'),
											'type' => 'checkbox',
											'wrapper-class' => '',
											'description' => 'Use this option to Enable or Disable the Copyright Text for your site.',
											'default' => 'enable'
										),
										'kode-copyright-text' => array(
											'title' => esc_html__('Copyright Text', 'university-education'),
											'type' => 'text',
											'description' => 'Use this field to enter your Footer Copyright text here. Note : This field will not render the HTML tags due to the XSS Security Implemented in the theme.',	
											'class' => 'full-width',
										),	
										'kode-back-top' => array(
											'title' => esc_html__('Back To Top', 'university-education'),
											'type' => 'checkbox',
											'description' => 'Use this option to Enable or Disable the Back to Top button.',
											'default' => 'enable',
											'wrapper-class' => 'kode-footer-style-wrapper footer-style-2-wrapper',
										),
									)
								),									
								'page-style' => array(
									'title' => esc_html__('Sub Header', 'university-education'),
									'options' => array(
										'subheader-background-color' => array(
											'title' => esc_html__('Sub Header Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description' => 'This Color Picker Allows you to change the Sub Header Background Color.',
											'default' => '#000',											
										),
										'subheader-background-opacity'=> array(
											'title'=> esc_html__('Sub Header Background Opacity' ,'university-education'),
											'type'=> 'text',
											'description' => 'You can Control the Sub Header background image opacity from here.',
											'default' => '0.6',
											'description'=> esc_html__('Adjust footer background opacity from 0 to 1.', 'university-education')
										),
										'subheader-bg-position' => array(
											'title' => esc_html__('Sub Header Background Position', 'university-education'),
											'type' => 'combobox',	
											'description' => 'Select the Sub Header Background Position you want to have for your site.',
											'options' => array(
												'body-scroll' => esc_html__('Scroll', 'university-education'),
												'body-fixed' => esc_html__('Fixed', 'university-education')												
											),
											'wrapper-class'=> 'boxed-style-wrapper enable-boxed-style-wrapper body-background-wrapper kode-body-style-wrapper'
										),	
										'default-page-title' => array(
											'title' => esc_html__('Default Page Title Background', 'university-education'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default Page Title Background from here.',
											'selector' => '.kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),	
										'default-post-title-background' => array(
											'title' => esc_html__('Default Post Title Background', 'university-education'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default Post Title Background from here.',
											'selector' => 'body.single .kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),
										'default-search-archive-title-background' => array(
											'title' => esc_html__('Default Search Archive Title Background', 'university-education'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default Search / Archive Page Title Background from here.',
											'selector' => 'body.archive .kode-subheader, body.search .kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),
										'default-404-title-background' => array(
											'title' => esc_html__('Default 404 Title Background', 'university-education'),
											'type' => 'upload',	
											'description' => 'You can Upload the Default 404 Page Title Background from here.',
											'selector' => 'body.error404 .kode-subheader { background-image: url(\'#kode#\'); }',
											'data-type' => 'upload'
										),										
									)
								),	
	
								'server-configuration' => array(
									'title' => esc_html__('System Diagnostic Information', 'university-education'),
									'options' => array(
										'server-config' => array(
											'title' => esc_html__('Server Configuration', 'university-education'),
											'type' => 'server-config',
											'wrapper-class' => 'server-config',
											'description' => 'Above information is useful to diagnose some of the possible reasons to malfunctions, performance issues or any errors. You can faciliate the process of support by providing below information to our support staff.',
										),
									)
								),

								'import-export-option' => array(
									'title' => esc_html__('Import/Export Option', 'university-education'),
									'options' => array(
										'export-option' => array(
											'title' => esc_html__('Export Option', 'university-education'),
											'type' => 'custom',
											'description'=> esc_html__('Here you can copy/download your themes current option settings. Keep this safe as you can use it as a backup should anything go wrong. Or you can use it to restore your settings on this site (or any other site). You also have the handy option to copy the link to yours sites settings. Which you can then use to duplicate on another site.', 'university-education'),
											'option' => 
												'<input type="button" id="kode-export" class="kdf-button" value="' . esc_html__('Export', 'university-education') . '" />' .
												'<textarea class="full-width"></textarea>'
										),
										'import-option' => array(
											'title' => esc_html__('Import Option', 'university-education'),
											'type' => 'custom',
											'description'=> esc_html__('WARNING! This will overwrite any existing options, please proceed with caution!', 'university-education'),
											'option' => 
												'<input type="button" id="kode-import" class="kdf-button" value="' . esc_html__('Import', 'university-education') . '" />' .
												'<textarea class="full-width"></textarea>'
										),										
									)
								),
							)
						),
						
						'color-settings' =>  array(
							'title' => esc_html__('Color Options', 'university-education'),				
							'icon' => 'fa fa-eyedropper',
						),
						
						'font-settings' => array(
							'title' => esc_html__('Typography Options', 'university-education'),				
							'icon' => 'fa-font',
						),
						
						'blog-style' => array(
							'title' => esc_html__('Blog Settings', 'university-education'),
							'icon' => 'fa fa-cubes',
							'options' => array(
							
								'blog-single' => array(
									'title' => esc_html__('Post Detail - Single', 'university-education'),
									'options' => array(
										'thumbnail-size' => array(
											'title' => esc_html__('Post Detail Thumbnail Size', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'default'=> 'uoe-post-thumbnail-size'
										),
										'post-sidebar-template' => array(
											'title' => esc_html__('Post Detail Default Sidebar', 'university-education'),
											'type' => 'radioimage',
											'options' => array(
												'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
												'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
												'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
												'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
											),
										),
										'post-sidebar-left' => array(
											'title' => esc_html__('Post Detail Default Sidebar Left', 'university-education'),
											'type' => 'combobox_sidebar',
											'wrapper-class' => 'left-sidebar-wrapper both-sidebar-wrapper post-sidebar-template-wrapper',
											'options' => $university_education_theme_option['sidebar-element'],		
										),
										'post-sidebar-right' => array(
											'title' => esc_html__('Post Detail Default Sidebar Right', 'university-education'),
											'type' => 'combobox_sidebar',
											'wrapper-class' => 'right-sidebar-wrapper both-sidebar-wrapper post-sidebar-template-wrapper',
											'options' => $university_education_theme_option['sidebar-element'],
										),	
										'single-post-feature-image' => array(
											'title' => esc_html__('Post Detail Feature Image', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('If you do not want to set a featured image (in case of sound post type : Audio player, in case of video post type : Video Player) kindly disable it here.', 'university-education'),
											'default' => 'enable'
										),
										'single-post-date' => array(
											'title' => esc_html__('Post Detail Date', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Date.', 'university-education'),											
											'default' => 'enable'
										),
										'single-post-author' => array(
											'title' => esc_html__('Post Detail Author', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('You can enable or disable the about author box from here.', 'university-education'),
											'default' => 'enable'											
										),	
										'single-post-comments' => array(
											'title' => esc_html__('Post Detail Comments', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Comments.', 'university-education'),		
											'default' => 'enable'
										),
										'single-post-tags' => array(
											'title' => esc_html__('Post Detail Tags', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Tags.', 'university-education'),
											'default' => 'enable'
										),
										'single-post-category' => array(
											'title' => esc_html__('Post Detail Category', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide extra information about the blog or post,  Category.', 'university-education'),
											'default' => 'enable'
										),
										'single-next-pre' => array(
											'title' => esc_html__('Post Detail Next / Previous Button', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can turn on/off the navigation arrows when viewing the blog single page.', 'university-education'),
											'default' => 'enable'
										),
										'single-recommended-post' => array(
											'title' => esc_html__('Post Detail Recommended Post Button', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can turn on/off the Recommended Posts on the blog single page.', 'university-education'),
											'default' => 'enable'
										),
										'recommended-thumbnail-size' => array(
											'title' => esc_html__('Post Detail Recommended Thumbnail Size', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'default'=> 'uoe-post-thumbnail-size'
										),
									)
								),
								'search-archive-style' => array(
									'title' => esc_html__('Default Pages - Search - Archive', 'university-education'),
									'options' => array(
										
										
										'archive-sidebar-template' => array(
											'title' => esc_html__('Index - Search - Archive Sidebar Template', 'university-education'),
											'type' => 'radioimage',
											'description' => 'You can Select the Search / Archive page sidebar position from here.',
											'options' => array(
												'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
												'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
												'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
												'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
											),
											'default' => 'no-sidebar'							
										),
										'archive-sidebar-left' => array(
											'title' => esc_html__('Search - Archive Sidebar Left', 'university-education'),
											'type' => 'combobox_sidebar',
											'options' => $university_education_theme_option['sidebar-element'],		
											'wrapper-class'=>'left-sidebar-wrapper both-sidebar-wrapper archive-sidebar-template-wrapper',											
										),
										'archive-sidebar-right' => array(
											'title' => esc_html__('Search - Archive Sidebar Right', 'university-education'),
											'type' => 'combobox_sidebar',
											'options' => $university_education_theme_option['sidebar-element'],
											'wrapper-class'=>'right-sidebar-wrapper both-sidebar-wrapper archive-sidebar-template-wrapper',
										),		
										'archive-blog-style' => array(
											'title' => esc_html__('Search - Archive Blog Style', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'blog-grid' => esc_html__('Blog Grid', 'university-education'),
												'blog-full' => esc_html__('Blog Full', 'university-education'),
											),
											'default' => 'blog-full'							
										),	
										'archive-col-size' => array(
											'title' => esc_html__('Search - Archive Blog Style', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'2' => esc_html__('2 Column', 'university-education'),
												'3' => esc_html__('3 Column', 'university-education'),
												'4' => esc_html__('4 Column', 'university-education'),
											),
											'default' => 'blog-full'							
										),
										'archive-col-size'=> array(
											'title'=> esc_html__('Column size' ,'university-education'),
											'type'=> 'text',	
											'default'=> '3',
											'wrapper-class'=>'blog-grid-wrapper archive-blog-style-wrapper',
											'description'=> esc_html__('Select the column width of content.', 'university-education')
										),										
										'archive-num-excerpt'=> array(
											'title'=> esc_html__('Search - Archive Num Excerpt (Word)' ,'university-education'),
											'type'=> 'text',	
											'default'=> '25',
											'wrapper-class'=>'blog-full-wrapper archive-blog-style-wrapper',
											'description'=> esc_html__('This is a number of word (decided by spaces) that you want to show on the post excerpt. Use 0 to hide the excerpt, -1 to show full posts and use the WordPress more tag.', 'university-education')
										),
									)
								),
							),
						),
						
						'event-style' => array(
							'title' => esc_html__('Event Settings', 'university-education'),
							'icon' => 'fa-calendar-check-o',
							'options' => array(
								'Event-single' => array(
									'title' => esc_html__('Event Single', 'university-education'),
									'options' => array(
										'single-event-feature-image' => array(
											'title' => esc_html__('Single Event Feature Image', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the Event Date on Featured Image.', 'university-education'),											
											'default' => 'enable'
										),
										'single-event-feature-size' => array(
											'title' => esc_html__('Single Event Feature Image', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'default'=> 'uoe-post-thumbnail-size'
										),
										'single-event-date' => array(
											'title' => esc_html__('Single Event Image Date', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide extra information about the Event Date on Featured Image.', 'university-education'),											
											'default' => 'enable'
										),
										'single-event-organizer' => array(
											'title' => esc_html__('Single Event Organizer Information', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('You can enable or disable the Event Organizer Information from here.', 'university-education'),
											'default' => 'enable'											
										),	
										'single-event-tags' => array(
											'title' => esc_html__('Single Event Meta Information', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide the Event Detail Tags.', 'university-education'),
											'default' => 'enable'
										),
										'single-event-gallery' => array(
											'title' => esc_html__('Single Event Gallery', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide the Event Detail Gallery.', 'university-education'),
											'default' => 'enable'
										),
										'single-related-events' => array(
											'title' => esc_html__('Single Related Events', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide the Related Events on Event Detail Page.', 'university-education'),		
											'default' => 'enable'
										),
										'single-related-events-meta' => array(
											'title' => esc_html__('Single Related Events Meta', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can show/hide some extra related events meta information on detail page.', 'university-education'),
											'default' => 'enable'
										),
										'single-event-related-size' => array(
											'title' => esc_html__('Single Event Related Image', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'default'=> 'uoe-post-thumbnail-size'
										),
										'single-event-comments' => array(
											'title' => esc_html__('Single Events Comments', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option you can turn on/off the Author Comments Section on the Event Detail Page.', 'university-education'),
											'default' => 'enable'
										),
									)
								),
							),
						),
						
						'teachers-style' => array(
							'title' => esc_html__('Teachers Settings', 'university-education'),
							'icon' => 'fa-user',
							'options' => array(
								'teachers-settings' => array(
									'title' => esc_html__('Teachers Single', 'university-education'),
									'options' => array(
										'single-teachers-feature-image' => array(
											'title' => esc_html__('Single Teachers Feature Image', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('If you do not want to set a featured image on Teachers detail kindly disable it here.', 'university-education'),
											'default' => 'enable',
										),
										'single-teachers-comments' => array(
											'title' => esc_html__('Single Teachers Comments', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide the Teachers Comments on Teachers Detail Page.', 'university-education'),		
											'default' => 'enable'
										),
										'single-teachers-rating' => array(
											'title' => esc_html__('Single Teachers Rating', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option you can show/hide the single Teachers rating starts on Teachers Detail Page.', 'university-education'),		
											'default' => 'enable'
										),
									)
								),
							),
						),
						
						'woocommerce-style' => array(
							'title' => esc_html__('Woo Settings', 'university-education'),
							'icon' => 'fa fa-shopping-cart',
							'options' => array(
								'woocommerce-single' => array(
									'title' => esc_html__('WooCommerce Single', 'university-education'),
									'options' => array(	
										'woo-post-title' => array(
											'title' => esc_html__('Woo Post Title', 'university-education'),
											'type' => 'checkbox',
											'description'=> esc_html__('Using this option to show/hide Woocommerce Product Titles at the Product Details page', 'university-education'),											
											'default' => 'enable'
										),
										'woo-post-price' => array(
											'title' => esc_html__('Woo Post Price', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products Prices at the Product Details page', 'university-education'),
											'default' => 'enable'
										),
										'woo-post-variable-price' => array(
											'title' => esc_html__('Woo Post Variable Price', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Variables Products Prices at the Product Details page', 'university-education'),
											'default' => 'enable'
										),
										'woo-post-related' => array(
											'title' => esc_html__('Woo Post Related', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Related Products at the Product Details page', 'university-education'),
											'default' => 'enable'
										),
										'woo-post-sku' => array(
											'title' => esc_html__('Woo Post SKU', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce SKU Number at the Product Details page', 'university-education'),	
											'default' => 'enable'
										),
										'woo-post-category' => array(
											'title' => esc_html__('Woo Post Category', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products Category at the Product Details page', 'university-education'),	
											'default' => 'enable'
										),
										'woo-post-tags' => array(
											'title' => esc_html__('Woo Post Tags', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products Tags at the Product Details page', 'university-education'),
											'default' => 'enable'
										),
										'woo-post-outofstock' => array(
											'title' => esc_html__('Woo Post Out of Stock Icon', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Out Of Stock text', 'university-education'),
											'default' => 'enable'
										),
										'woo-post-saleicon' => array(
											'title' => esc_html__('Woo Post Sale Icon', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products Sale Icon', 'university-education'),
											'default' => 'enable'
										)
									)
								),	
								'woocommerce-list' => array(
									'title' => esc_html__('WooCommerce Listing', 'university-education'),
									'options' => array(	
										'woo-list-cart-btn' => array(
											'title' => esc_html__('Woo List Cart Button', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products Cart Icon at the listing page', 'university-education'),
											'default' => 'enable'
										),
										'woo-list-title' => array(
											'title' => esc_html__('Woo List Title', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products  Titles at the listing page', 'university-education'),
											'default' => 'enable'
										),
										'woo-list-price' => array(
											'title' => esc_html__('Woo List Price', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products Prices at the listing page', 'university-education'),	
											'default' => 'enable'
										),
										'woo-list-rating' => array(
											'title' => esc_html__('Woo List Rating', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Using this option to show/hide Woocommerce Products rating stars at the listing page.', 'university-education'),	
											'default' => 'enable'
										),
										'all-products-per-row' => array(
											'title' => esc_html__('Products Per Row', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'1'=> '1',
												'2'=> '2',
												'3'=> '3',
												'4'=> '4',
												'5'=> '5'
											),
											'default' => '3'							
										),
										'all-products-sidebar' => array(
											'title' => esc_html__('All Products Sidebar', 'university-education'),
											'type' => 'radioimage',
											'description'=> esc_html__('You can select the Shop layout Side bar layout from here.', 'university-education'),
											'options' => array(
												'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
												'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
												'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
												'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
											),
											'default' => 'no-sidebar'							
										),
										'all-products-sidebar-left' => array(
											'title' => esc_html__('All Products Sidebar Left', 'university-education'),
											'type' => 'combobox_sidebar',
											'description'=> esc_html__('You can select the left side bar for your shop page from here.', 'university-education'),
											'options' => $university_education_theme_option['sidebar-element'],		
											'wrapper-class'=>'left-sidebar-wrapper both-sidebar-wrapper all-products-sidebar-wrapper',											
										),
										'all-products-sidebar-right' => array(
											'title' => esc_html__('All Products Sidebar Right', 'university-education'),
											'type' => 'combobox_sidebar',
											'description'=> esc_html__('You can select the right side bar for your shop page from here.', 'university-education'),
											'options' => $university_education_theme_option['sidebar-element'],
											'wrapper-class'=>'right-sidebar-wrapper both-sidebar-wrapper all-products-sidebar-wrapper',
										)
									)
								),
								
							),
						),

						// overall elements menu
						'overall-elements' => array(
							'title' => esc_html__('Social Settings', 'university-education'),
							'icon' => 'fa fa-share-square-o',
							'options' => array(

								'header-social' => array(),
								
								'social-shares' => array(),
								
							)				
						),
						
						// plugin setting menu
						'plugin-settings' => array(
							'title' => esc_html__('Slider Settings', 'university-education'),
							'icon' => 'fa fa-sliders ',
							'options' => array(
								'bx-slider' => array(
									'title' => esc_html__('BX Slider', 'university-education'),
									'options' => array(		
										'bx-slider-effects' => array(
											'title' => esc_html__('BX Slider Effect', 'university-education'),
											'type' => 'combobox',
											'description'=> esc_html__('You can select the BX slide effect from the dropdown menu.', 'university-education'),
											'options' => array(
												'fade' => esc_html__('Fade', 'university-education'),
												'slide'	=> esc_html__('Slide', 'university-education')
											)
										),
										'bx-min-slide' => array(
											'title' => esc_html__('BX Min Slide', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the minimum number of slides here.', 'university-education'),
											'wrapper-class' => 'bx-slider-effects-wrapper slidee-wrapper',
											'default' => '1'
										),
										'bx-max-slide' => array(
											'title' => esc_html__('BX Max Slide', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the maximum number of slides here.', 'university-education'),
											'wrapper-class' => 'bx-slider-effects-wrapper slidee-wrapper',
											'default' => '1'
										),
										'bx-slide-margin' => array(
											'title' => esc_html__('BX Slide Margin', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the maximum number of slides here.', 'university-education'),
											'wrapper-class' => 'bx-slider-effects-wrapper slidee-wrapper',
											'default' => '10'
										),
										'bx-arrow' => array(
											'title' => esc_html__('Bxslider Arrows', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Turn on or off bx slider arrows from here enabling this option will enable the arrows on home page.', 'university-education'),	
											'default' => 'enable'
										),
										'bx-pause-time' => array(
											'title' => esc_html__('BX Slider Pause Time', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the BX slider pause time you want to have.', 'university-education'),
											'default' => '7000'
										),
										'bx-slide-speed' => array(
											'title' => esc_html__('BX Slider Animation Speed', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the BX slider animation speed you want to have.', 'university-education'),
											'default' => '600'
										),	
									)
								),
								'flex-slider' => array(
									'title' => esc_html__('Flex Slider', 'university-education'),
									'options' => array(		
										'flex-slider-effects' => array(
											'title' => esc_html__('Flex Slider Effect', 'university-education'),
											'type' => 'combobox',
											'description'=> esc_html__('You can select the Flex slide effect from the dropdown menu.', 'university-education'),
											'options' => array(
												'fade' => esc_html__('Fade', 'university-education'),
												'slide'	=> esc_html__('Slide', 'university-education')
											)
										),
										'flex-pause-time' => array(
											'title' => esc_html__('Flex Slider Pause Time', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Flex slider pause time you want to have.', 'university-education'),
											'default' => '7000'
										),
										'flex-slide-speed' => array(
											'title' => esc_html__('Flex Slider Animation Speed', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Flex slider animation speed you want to have.', 'university-education'),
											'default' => '600'
										),	
									)
								),
								
								'nivo-slider' => array(
									'title' => esc_html__('Nivo Slider', 'university-education'),
									'options' => array(		
										'nivo-slider-effects' => array(
											'title' => esc_html__('Nivo Slider Effect', 'university-education'),
											'type' => 'combobox',
											'description'=> esc_html__('You can select the Nivo slide effect from the dropdown menu.', 'university-education'),
											'options' => array(
												'sliceDownRight'	=> esc_html__('sliceDownRight', 'university-education'),
												'sliceDownLeft'		=> esc_html__('sliceDownLeft', 'university-education'),
												'sliceUpRight'		=> esc_html__('sliceUpRight', 'university-education'),
												'sliceUpLeft'		=> esc_html__('sliceUpLeft', 'university-education'),
												'sliceUpDown'		=> esc_html__('sliceUpDown', 'university-education'),
												'sliceUpDownLeft'	=> esc_html__('sliceUpDownLeft', 'university-education'),
												'fold'				=> esc_html__('fold', 'university-education'),
												'fade'				=> esc_html__('fade', 'university-education'),
												'boxRandom'			=> esc_html__('boxRandom', 'university-education'),
												'boxRain'			=> esc_html__('boxRain', 'university-education'),
												'boxRainReverse'	=> esc_html__('boxRainReverse', 'university-education'),
												'boxRainGrow'		=> esc_html__('boxRainGrow', 'university-education'),
												'boxRainGrowReverse'=> esc_html__('boxRainGrowReverse', 'university-education')
											)
										),
										'nivo-pause-time' => array(
											'title' => esc_html__('Nivo Slider Pause Time', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Nivo slider pause time you want to have.', 'university-education'),
											'default' => '7000'
										),
										'nivo-slide-speed' => array(
											'title' => esc_html__('Nivo Slider Animation Speed', 'university-education'),
											'type' => 'text',
											'description'=> esc_html__('Enter the Nivo slider animation speed you want to have.', 'university-education'),
											'default' => '600'
										),	
									)
								),
								'slider-caption' => array(
									'title' => esc_html__('Slider Caption', 'university-education'),
									'options' => array(		
										'caption-title-color' => array(
											'title' => esc_html__('Caption Title Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Title Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-background-color-switch' => array(
											'title' => esc_html__('Caption Background Color Switch', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('if you have transparent background and dont want to use caption bg you can turn it off.', 'university-education'),	
											'default' => 'enable'
										),
										'caption-background-color' => array(
											'title' => esc_html__('Caption Title Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Title Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper caption-background-color-switch-wrapper caption-background-color-switch-enable'
										),
										'title-font-size' => array(
											'title' => esc_html__('Caption Title Size', 'university-education'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can control the Caption Title Size on the slider from here.', 'university-education'),
											'default' => '30',
											'range_start' => '30',
											'range_end' => '30',
											//'selector' => 'h1{ font-size: #kode#; }',
											'data-type' => 'pixel'											
										),
										'caption-desc-color' => array(
											'title' => esc_html__('Caption Description Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Description Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'show-caption-wrapper yes-wrapper'
										),	
										'caption-btn-color-switch' => array(
											'title' => esc_html__('Caption Button Color Switch', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('if you have transparent background and dont want to use caption bg you can turn it off.', 'university-education'),	
											'default' => 'enable'
										),
										'caption-font-size' => array(
											'title' => esc_html__('Caption Description Size', 'university-education'),
											'type' => 'sliderbar',
											'description'=> esc_html__('You can select the Caption Description Font Size on the slider from here.', 'university-education'),
											'default' => '30',
											'range_start' => '30',
											'range_end' => '30',
											//'selector' => 'h1{ font-size: #kode#; }',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
											'data-type' => 'pixel'											
										),
										'caption-btn-color' => array(
											'title' => esc_html__('Button Text Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),
										'caption-btn-color-hover' => array(
											'title' => esc_html__('Button Hover Text Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),	
										'caption-btn-color-border' => array(
											'title' => esc_html__('Button Border Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Border Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),										
										'caption-btn-color-bg' => array(
											'title' => esc_html__('Button Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),	
										'caption-btn-hover-bg' => array(
											'title' => esc_html__('Button Background Hover Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),	
										'caption-btn-arrow-color' => array(
											'title' => esc_html__('Caption Button Arrow Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),	
										'caption-btn-arrow-hover' => array(
											'title' => esc_html__('Caption Button Hover Arrow Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),
										'caption-btn-arrow-bg' => array(
											'title' => esc_html__('Caption Button Arrow BG Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),
										'caption-btn-arrow-hover-bg' => array(
											'title' => esc_html__('Caption Button Arrow Hover BG Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Caption Button Background Color on the slider from here.', 'university-education'),
											'default'=> '#ffffff',
											'wrapper-class'=>'caption-btn-color-switch-wrapper yes-wrapper enable-wrapper',
										),
									) ,
								)
							)					
						),
						'courses-search' => array(
							'title' => esc_html__('Courses Search', 'university-education'),
							'icon' => 'fa fa-cogs',
							'options' => array(							
								'search-settings' => array(
									'title' => esc_html__('Search Settings', 'university-education'),
									'options' => array(
										'courses-search-page' => array(
											'title' => __('Select Search Page', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_post_list_id('page'),
											'description'=> esc_html__('Please select the search page from here.', 'university-education'),
										),
									)
								),	
							),
						),
						
						'sidebar-settings' => array(
							'title' => esc_html__('Sidebar Settings', 'university-education'),
							'icon' => 'fa fa-columns',
							'options' => array(
								'sidebar_element' => array(
									'title' => esc_html__('Sidebar', 'university-education'),
									'options' => array(
										'sidebar-size' => array(
											'title' => __('Sidebar Size', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'2'=>__('16 Percent', 'university-education'),
												'3'=>__('25 Percent', 'university-education'),
												'4'=>__('33 Percent', 'university-education'),
												'5'=>__('41 Percent', 'university-education'),
												'6'=>__('50 Percent', 'university-education')
											),
											'default' => '4',
											'descripton' => '1 column equals to around 80px',
										),		
										'both-sidebar-size' => array(
											'title' => __('Both Sidebar Size', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'3'=>__('25 Percent', 'university-education'),
												'4'=>__('33 Percent', 'university-education')
											),
											'default' => '3',
											'descripton' => '1 column equals to around 80px',
										),	
										'sidebar-tbn' => array(
											'title' => esc_html__('Sidebar Button', 'university-education'),
											'type' => 'checkbox',	
											'description'=> esc_html__('Sidebar button', 'university-education'),
											'default' => 'enable'
										),
										'sidebar-bg-color' => array(
											'title' => esc_html__('Sidebar Background Color', 'university-education'),
											'type' => 'colorpicker',
											'description'=> esc_html__('You can select the Sidebar Background Color.', 'university-education'),
											'default'=> '#ffffff',											
										),
										'sidebar-padding-top' => array(
											'title' => esc_html__('Padding Top', 'university-education'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from top of the sidebar'
										),
										'sidebar-padding-bottom' => array(
											'title' => esc_html__('Padding Bottom', 'university-education'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from Bottom of the sidebar'
										),
										'sidebar-padding-left' => array(
											'title' => esc_html__('Padding Left', 'university-education'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from Left of the sidebar'
										),
										'sidebar-padding-right' => array(
											'title' => esc_html__('Padding Right', 'university-education'),
											'type' => 'text',
											'default' => '30',
											'description' => 'Please add padding from Right of the sidebar'
										),
										'sidebar-element' => array(
											'title' => esc_html__('Sidebar Name', 'university-education'),
											'description'=> esc_html__('Enter a name for new sidebar. It must be a valid name which starts with a letter. Then Click on the Add Button to create the Custom Sidebar', 'university-education'),
											'placeholder' => esc_html__('type sidebar name', 'university-education'),
											'type' => 'sidebar',
											'btn_title' => 'Add Sidebar'
										)										
									)
								),
							)
						),
						'api-settings' => array(
							'title' => esc_html__('API Settings', 'university-education'),
							'icon' => 'fa fa-gear',
							'options' => array(
								'api_configuration' => array(
									'title' => esc_html__('Mail Chimp API', 'university-education'),
									'options' => array(		
										'mail-chimp-api' => array(
											'title' => esc_html__('Mail Chimp API', 'university-education'),
											'type' => 'text',
											'default' => 'API KEY',
											'description' => 'Please add mail chimp API Key here'
										),									
										'mail-chimp-listid' => array(
											'title' => esc_html__('MailChimp List ID', 'university-education'),
											'type' => 'text',
											'description' => 'For getting list id first login to your mail chimp account then click on list > List name and Campaign defaults > you will see list id written on the right side of first section.'
										),
									)
								),
								'g_api_configuration' => array(
									'title' => esc_html__('Google API', 'university-education'),
									'options' => array(		
										'google-public-api' => array(
											'title' => esc_html__('Public Key (Sitekey) - Google Recaptcha', 'university-education'),
											'type' => 'text',
											'default' => 'API KEY',
											'description' => 'Please add mail google public which also known as sitekey here
												<p>you can get API from here, https://www.google.com/recaptcha/admin#site/</p>
												<p>Step 1: Login to Your Gmail Account</p>
												<p>Step 2: It will redirect you back https://www.google.com/recaptcha/admin#site/ Register a new site add Label Anything for example : Kodeforest project </p>
												<p>Step 3: Add your domains for example: kodeforest.com one per line.</p>
												<p>Step 4: Click On Register</p>'
										),									
										'google-secret-api' => array(
											'title' => esc_html__('Secret Key(Secret) - Google Recaptcha', 'university-education'),
											'type' => 'text',
											'description' => 'Please add secret key here 
												<p>you can get API from here, https://www.google.com/recaptcha/admin#site/</p>
												<p>Step 1: Login to Your Gmail Account</p>
												<p>Step 2: It will redirect you back https://www.google.com/recaptcha/admin#site/ Register a new site add Label Anything for example : Kodeforest project </p>
												<p>Step 3: Add your domains for example: kodeforest.com one per line.</p>
												<p>Step 4: Click On Register</p>'
										),
										'google-map-api' => array(
											'title' => esc_html__('Google Map Api', 'university-education'),
											'type' => 'text',
											'description' => 'Please add google map api key here <p>You can get Google Map Api Key from https://developers.google.com/maps/web/.</p>
												<p>Step 1: Login to Your Gmail Account</p>
												<p>Step 2: It will redirect you back https://developers.google.com/maps/web/ Click on Get A Key > Popup Will Appear Click Continue.</p>
												<p>Step 3: It will take some time to take you right place where you should be.</p>
												<p>Step 4: On left of screen a dropdown and button will appear click on dropdown <strong>create new project</strong></p>
												<p>Step 5: Click on Continue After a while it will show you credential tab.</p>
												<p>Step 6: Add Name of Project And Under it add Your Site URl or Add *.</p>
												<p>Step 7: Click on Create and Grab your key and paste in Google MAP api.</p>
												'
										)
									)
								),
								'twitter_api' => array(
									'title' => esc_html__('Twitter API Settings', 'university-education'),
									'options' => array(		
										'twitter-user-name' => array(
											'title' => esc_html__('User Name : For example: KodeForest', 'university-education'),
											'type' => 'text',
											'default' => 'username',
											'description' => 'Please add mail twitter username here'
										),									
										'twitter-consumer-api' => array(
											'title' => esc_html__('Consumer Key (API Key)', 'university-education'),
											'type' => 'text',
											'default' => 'consumer',
											'description' => 'Please add mail consumer api here'
										),									
										'twitter-consumer-secret' => array(
											'title' => esc_html__('Consumer Secret (API Secret)', 'university-education'),
											'type' => 'text',
											'default' => 'consumer secret',
											'description' => 'Please add mail consumer secret here'
										),									
										'twitter-access-token' => array(
											'title' => esc_html__('Access Token', 'university-education'),
											'type' => 'text',
											'default' => 'access token',
											'description' => 'Please add Access Token here'
										),
										'twitter-access-token-secret' => array(
											'title' => esc_html__('Access Token Secret', 'university-education'),
											'type' => 'text',
											'default' => 'access token secret',
											'description' => 'Please add Access Token Secret here'
										),	
										'twitter-show-num' => array(
											'title' => esc_html__('Show Num of Tweets', 'university-education'),
											'type' => 'text',
											'default' => '5',
											'description' => 'Please limit number of tweets here'
										),	
										'twitter-cache' => array(
											'title' => esc_html__('Twitter Cache', 'university-education'),
											'type' => 'text',
											'default' => '1',
											'description' => 'Please twitter cache here for example : 1 hour'
										),										
									)
								),
							)
						),						
					)
				), 
				
				
				
				$university_education_theme_option
			);
			
		}
		
	}

?>