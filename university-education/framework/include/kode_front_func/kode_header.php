<?php
	/*	
	*	Kodeforest Header File
	*	---------------------------------------------------------------------
	*	This file contains utility function in the theme
	*	---------------------------------------------------------------------
	*/
	
	
	if( !function_exists('university_education_get_woo_cart') ){	
		function university_education_get_woo_cart(){
			if(class_exists('Woocommerce')){
				global $post,$post_id,$product,$woocommerce;	
				$currency = get_woocommerce_currency_symbol();
				if($woocommerce->cart->cart_contents_count <> 0){ 
					return $shopping_cart_div = '<a href="#" id="no-active-btn-shopping"><i class="fa fa-shopping-cart"></i></a><div class="widget_shopping_cart_content"></div>';
				}else{ 
					return $shopping_cart_div = '<a href="#" id="no-active-btn-shopping"><i class="fa fa-shopping-cart"></i></a><div class="widget_shopping_cart_content"></div>';
				}
			}
		}
	}
	

	
	if( !function_exists('university_education_get_selected_header') ){	
		function university_education_get_selected_header($university_education_post_option,$university_education_theme_option) {
			if(isset($university_education_theme_option['enable-header-option'])){
				if($university_education_theme_option['enable-header-option'] == 'disable'){
					if(isset($university_education_post_option['kode-header-style'])){
						$university_education_theme_option['kode-header-style'] = $university_education_post_option['kode-header-style'];
						university_education_get_header($university_education_theme_option);	
					}else{
						university_education_get_header($university_education_theme_option);	
					}
				}else{
				
					university_education_get_header($university_education_theme_option);	
				}
			}else{
				if(isset($university_education_post_option['kode-header-style'])){
					$university_education_theme_option['kode-header-style'] = $university_education_post_option['kode-header-style'];
					university_education_get_header($university_education_theme_option);						
				}else{
					university_education_get_header($university_education_theme_option);	
				}
			}
		}
	}	
	
	if( !function_exists('university_education_get_selected_header_class') ){	
		function university_education_get_selected_header_class($university_education_post_option,$university_education_theme_option) {	
			if(isset($university_education_theme_option['enable-header-option'])){
				if($university_education_theme_option['enable-header-option'] == 'disable'){
					if(isset($university_education_post_option['kode-header-style'])){
						$university_education_theme_option['kode-header-style'] = $university_education_post_option['kode-header-style'];
						return $university_education_theme_option['kode-header-style'];
					}else{						
						return $university_education_theme_option['kode-header-style'];
					}
				}else{
					return $university_education_theme_option['kode-header-style'];
				}
			}else{
				if(isset($university_education_post_option['kode-header-style'])){
					$university_education_theme_option['kode-header-style'] = $university_education_post_option['kode-header-style'];
					return $university_education_theme_option['kode-header-style'];
				}else{
					//return $university_education_theme_option['kode-header-style'];
					return 'default';
				}
			}
		}
	}
	
	
	if( !function_exists('university_education_get_header') ){	
		function university_education_get_header ($university_education_theme_option) {
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_plugin_option = get_option('university_education_plugin_option');
			if($university_education_theme_option['kode-header-style'] == 'header-style-1'){ ?>
				<!--HEADER START-->
				<header id="header_2">
					<?php if(isset($university_education_theme_option['enable-top-bar']) && $university_education_theme_option['enable-top-bar'] == 'enable'){ ?>	
					<!--kode top bar start-->
					<div class="top_bar_2">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<div class="pull-left">
										<?php 
											if( !empty($university_education_theme_option['top-bar-left-text']) ) {
												echo do_shortcode($university_education_theme_option['top-bar-left-text']); 
											}	
										?>
									</div>
								</div>
								<div class="col-md-8">
								<?php if(isset($university_education_theme_option['enable-language']) && $university_education_theme_option['enable-language'] == 'enable'){ ?>
									<div class="lng_wrap">
										<div class="dropdown">
											<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="fa fa-globe"></i><?php echo esc_attr__('Language','university-education'); ?>
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
												<li><a href="#"><i><img src="<?php echo UOE_PATH;?>/images/english.jpg" alt=""></i><?php echo esc_attr__('English','university-education'); ?></a></li>
												<li><a href="#"><i><img src="<?php echo UOE_PATH;?>/images/german.jpg" alt=""></i><?php echo esc_attr__('German','university-education'); ?></a></li> 
											</ul>
										</div>
									</div>
								<?php } ?>	
								<?php if(isset($university_education_theme_option['enable-login']) && $university_education_theme_option['enable-login'] == 'enable'){ ?>
									<ul class="login_wrap">
										<li><a href="#" data-toggle="modal" data-target="#reg-box"><i class="fa fa-user"></i><?php echo esc_attr__('Register','university-education'); ?></a></li>
										<li><a href="#" data-toggle="modal" data-target="#signin-box"><i class="fa fa-sign-in"></i><?php echo esc_attr__('Sign In','university-education'); ?></a></li>
									</ul>	
									<?php echo university_education_signin_form(); ?>
									<?php echo university_education_signup_form(); ?>
								<?php } ?>			
									<?php if( has_nav_menu('top_navigation') ){ ?>
									<div class="top_nav">
										<?php 
										wp_nav_menu( array(
											'theme_location'=>'top_navigation', 
											'container'=> '', 
											'menu_class'=> 'kode-top-main-menu'
										) );
										?>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
					<!--kode top bar end-->
					<?php }?>
					<!--kode navigation start-->
					<div class="kode_navigation">
						<div class="container">
							<div class="row">
								<div class="col-md-2">
									<div class="logo_wrap">
										<a class="kode-logo logo" href="<?php echo esc_url(home_url('/')); ?>" >
											<?php 
												if(empty($university_education_theme_option['logo'])){ 
													echo university_education_get_image(UOE_PATH . '/images/logo.png');
												}else{
													if(university_education_get_image($university_education_theme_option['logo']) <> ''){
														echo university_education_get_image($university_education_theme_option['logo']);	
													}else{
														echo university_education_get_image(UOE_PATH . '/images/logo.png');
													}
												}
											?>						
										</a>
									</div>
								</div>
								<div class="col-md-10">
									<!--kode nav_2 start-->
									<div class="nav_2" id="navigation">
										<?php get_template_part( 'header', 'nav' ); ?>	
										<!--DL Menu Start-->
										<?php
										// mobile navigation
										if( class_exists('university_education_dlmenu_walker') && has_nav_menu('main_menu_responsive') &&
											( empty($university_education_theme_option['enable-responsive-mode']) || $university_education_theme_option['enable-responsive-mode'] == 'enable' ) ){
											echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
											echo '<button class="dl-trigger">'.esc_attr__('Open Menu','university-education').'</button>';
											wp_nav_menu( array(
												'theme_location'=>'main_menu_responsive', 
												'container'=> '', 
												'menu_class'=> 'dl-menu kode-main-mobile-menu',
												'walker'=> new university_education_dlmenu_walker() 
											) );						
											echo '</div>';
										}
										?>
										<!--DL Menu END-->
										<?php if(isset($university_education_theme_option['enable-side-nav']) && $university_education_theme_option['enable-side-nav'] == 'enable'){ ?>
										<?php
										$footer_item_class = 'slide-left';
										if(isset($university_education_theme_option['kode-header-navi-sidebar']) && $university_education_theme_option['kode-header-navi-sidebar'] <> ''){
											$footer_item_class = $university_education_theme_option['kode-header-navi-sidebar'];
										}
										?>
										<!--kode nav_2 end-->
										<a id="c-button--<?php echo esc_attr($footer_item_class)?>" class="c-button" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>	
										<?php } ?>
									</div>
									<!--kode nav_2 end-->
									
								</div>
								
							</div>
						</div>
					</div>
					<!--kode navigation end-->
				</header>
				<!--HEADER END-->
			<?php
			}else{ ?>
				<!--HEADER START-->
				<header id="header_2">
					<?php if(isset($university_education_theme_option['enable-top-bar']) && $university_education_theme_option['enable-top-bar'] == 'enable'){ ?>
					<!--kode top bar start-->
					<div class="top_bar_2">
						<div class="container">
							<div class="row">
								<div class="col-md-5">
									<div class="pull-left">
										<?php 
											if( !empty($university_education_theme_option['top-bar-left-text']) ) {
												echo do_shortcode($university_education_theme_option['top-bar-left-text']); 
											}	
										?>
									</div>
								</div>
								<div class="col-md-7">
									<div class="lng_wrap">
										<div class="dropdown">
											<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="fa fa-globe"></i><?php echo esc_attr__('Language','university-education'); ?>
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
												<li><a href="#"><i><img src="images/english.jpg" alt=""></i><?php echo esc_attr__('English','university-education'); ?></a></li>
												<li><a href="#"><i><img src="images/german.jpg" alt=""></i><?php echo esc_attr__('German','university-education'); ?></a></li> 
											</ul>
										</div>
									</div>
									<ul class="login_wrap">
										<li><a href="#" data-toggle="modal" data-target="#reg-box"><i class="fa fa-user"></i><?php echo esc_attr__('Register','university-education'); ?></a></li>
										<li><a href="#" data-toggle="modal" data-target="#signin-box"><i class="fa fa-sign-in"></i><?php echo esc_attr__('Sign In','university-education'); ?></a></li>
									</ul>	    					
									<?php if( has_nav_menu('top_navigation') ){ ?>
									<ul class="top_nav">
										<?php 
										wp_nav_menu( array(
											'theme_location'=>'top_navigation', 
											'container'=> '', 
											'menu_class'=> 'kode-top-main-menu'
										) );
										?>
									</ul>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
					<!--kode top bar end-->
					<?php }?>
					<!--kode navigation start-->
					<div class="kode_navigation">
						<div id="mobile-header">
							<a id="responsive-menu-button" href="#sidr-main"><i class="fa fa-bars"></i></a>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-md-2">
									<div class="logo_wrap">
										<a class="kode-logo logo" href="<?php echo esc_url(home_url('/')); ?>" >
											<?php 
												if(empty($university_education_theme_option['logo'])){ 
													echo university_education_get_image(UOE_PATH . '/images/logo.png');
												}else{
													if(university_education_get_image($university_education_theme_option['logo']) <> ''){
														echo university_education_get_image($university_education_theme_option['logo']);	
													}else{
														echo university_education_get_image(UOE_PATH . '/images/logo.png');
													}
												}
											?>						
										</a>
									</div>
								</div>
								<div class="col-md-10">
									<!--kode nav_2 start-->
									<div class="nav_2" id="navigation">
										<?php get_template_part( 'header', 'nav' ); ?>	
										<!--DL Menu Start-->
										<?php
										// mobile navigation
										if( class_exists('university_education_dlmenu_walker') && has_nav_menu('main_menu_responsive') &&
											( empty($university_education_theme_option['enable-responsive-mode']) || $university_education_theme_option['enable-responsive-mode'] == 'enable' ) ){
											echo '<div class="kode-responsive-navigation dl-menuwrapper" id="kode-responsive-navigation" >';
											echo '<button class="dl-trigger">'.esc_attr__('Open Menu','university-education').'</button>';
											wp_nav_menu( array(
												'theme_location'=>'main_menu_responsive', 
												'container'=> '', 
												'menu_class'=> 'dl-menu kode-main-mobile-menu',
												'walker'=> new university_education_dlmenu_walker() 
											) );						
											echo '</div>';
										}
										?>
										<!--DL Menu END-->
									</div>
									<?php
									$footer_item_class = 'slide-left';
									if(isset($university_education_theme_option['kode-header-navi-sidebar']) && $university_education_theme_option['kode-header-navi-sidebar'] <> ''){
										$footer_item_class = $university_education_theme_option['kode-header-navi-sidebar'];
									}
									?>
									<!--kode nav_2 end-->
									<a id="c-button--<?php echo esc_attr($footer_item_class)?>" class="c-button" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
								</div>
								
							</div>
						</div>
					</div>
					<!--kode navigation end-->
				</header>
				<!--HEADER END-->
				
			<?php
			}
		}
	}

	