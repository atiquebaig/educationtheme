<?php
	/*	
	*	Kodeforest Event Option file
	*	---------------------------------------------------------------------
	*	This file creates all post options to the post page
	*	---------------------------------------------------------------------
	*/
	
	// add a post admin option
	// add a post option to post page
	if( is_admin() ){ add_action('init', 'university_education_create_event_options'); }
	if( !function_exists('university_education_create_event_options') ){
	
		function university_education_create_event_options(){
			global $university_education_theme_option;
			
			if( !class_exists('university_education_page_options') ) return;
			new university_education_page_options( 
				
				
					  
				// page option settings
				array(
					'page-layout' => array(
						'title' => esc_attr__('Page Layout', 'university-education'),
						'options' => array(
							'sidebar' => array(
								'title' => esc_attr__('Sidebar Template' , 'university-education'),
								'type' => 'radioimage',
								'options' => array(
									'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
									'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
									'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
									'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
								),
								'default' => 'default-sidebar'
							),	
							'left-sidebar' => array(
								'title' => esc_attr__('Left Sidebar' , 'university-education'),
								'type' => 'combobox_sidebar',
								'options' => $university_education_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
							),
							'right-sidebar' => array(
								'title' => esc_attr__('Right Sidebar' , 'university-education'),
								'type' => 'combobox_sidebar',
								'options' => $university_education_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
							),						
						)
					),
					
					'page-option' => array(
						'title' => esc_attr__('Page Option', 'university-education'),
						'options' => array(
							'page-title' => array(
								'title' => esc_attr__('Post Title' , 'university-education'),
								'type' => 'text',
								'description' => esc_attr__('Leave this field blank to use the default title from admin panel > general > blog style section.', 'university-education')
							),
							'page-caption' => array(
								'title' => esc_attr__('Post Caption' , 'university-education'),
								'type' => 'textarea'
							),
							'team_member' => array(
								'title' => esc_attr__('Team Member' , 'university-education'),
								'type' => 'multi-combobox',
								'options' => university_education_get_post_list_id('teacher'),
								'description'=> esc_attr__('Select Team Member.', 'university-education')
							),	
							
							'slider'=> array(	
								'title' => esc_html__('Event Gallery', 'university-education'),
								'overlay'=> 'false',
								'caption'=> 'false',
								'type'=> 'slider',								
							),
							'thumbnail-size' => array(
								'title' => esc_html__('Gallery Size', 'university-education'),
								'type'=> 'combobox',
								'options'=> university_education_get_thumbnail_list(),
								'default'=> 'uoe-post-thumbnail-size'								
							),	
						)
					),

				),
				
				// page option attribute
				array(
					'post_type' => array('event'),
					'meta_title' => esc_attr__('Kodeforest Event Option', 'university-education'),
					'meta_slug' => 'kodeforest-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}	
	
	
	// add work in page builder area
	add_filter('university_education_page_builder_option', 'university_education_register_event_item');
	if( !function_exists('university_education_register_event_item') ){
		function university_education_register_event_item( $page_builder = array() ){
			global $university_education_spaces;
			$page_builder['content-item']['options']['events'] = array(
				'title'=> esc_attr__('Events', 'university-education'), 
				'icon'=>'fa-calendar',
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'university-education')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'university-education')
					),
					'title-num-excerpt'=> array(
						'title'=> esc_attr__('Title Num Length (Word)' ,'university-education'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the event title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the WordPress more tag</strong>.', 'university-education')
					),	
					'category'=> array(
						'title'=> esc_attr__('Category' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('event-categories'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'tag'=> array(
						'title'=> esc_attr__('Tag' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('event-tags'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'num-excerpt'=> array(
						'title'=> esc_attr__('Num Excerpt (Word)' ,'university-education'),
						'type'=> 'text',	
						'default'=> '25',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the post excerpt. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'university-education')
					),	
					'num-fetch'=> array(
						'title'=> esc_attr__('Num Fetch' ,'university-education'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> esc_attr__('Specify the number of posts you want to pull out.', 'university-education')
					),	
					'thumbnail-size' => array(
						'title' => esc_html__('Event List Thumbnail Size', 'university-education'),
						'type'=> 'combobox',
						'options'=> university_education_get_thumbnail_list(),
						'default'=> ''
					),						
					'event-style'=> array(
						'title'=> esc_attr__('Event Style' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'event-simple-view' => esc_attr__('Event Simple View', 'university-education'),							
							'event-modern-view' => esc_attr__('Event Modern View', 'university-education'),
							'event-grid-view' => esc_attr__('Event Grid View', 'university-education'),
							'event-medium-view' => esc_attr__('Event Medium View', 'university-education'),
							'event-full-view' => esc_attr__('Event Full Normal', 'university-education'),
							'event-calendar-view' => esc_attr__('Event Calendar View', 'university-education'),
							'event-marker-view' => esc_attr__('Event Google Markers View', 'university-education'),
						),
						'default'=>'event-grid-simple'
					),	
					'event-size'=> array(
						'title'=> esc_html__('Event Size' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'1' => esc_html__('1 Column', 'university-education'),
							'2' => esc_html__('2 Column', 'university-education'),
							'3' => esc_html__('3 Column', 'university-education'),
							'4' => esc_html__('4 Column', 'university-education'),
						),						
						'default'=>'3'
					),					
					'scope'=> array(
						'title'=> esc_attr__('Event Scope' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'past' => esc_attr__('Past Events', 'university-education'), 
							'future' => esc_attr__('Upcoming Events', 'university-education'), 
							'all' => esc_attr__('All Events', 'university-education'), 
						)
					),
					'order'=> array(
						'title'=> esc_attr__('Order' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>esc_attr__('Descending Order', 'university-education'), 
							'asc'=> esc_attr__('Ascending Order', 'university-education'), 
						)
					),									
					// 'pagination'=> array(
						// 'title'=> esc_attr__('Enable Pagination' ,'university-education'),
						// 'type'=> 'checkbox'
					// ),										
					'margin-bottom' => array(
						'title' => esc_attr__('Margin Bottom', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_attr__('Spaces after ending of this item', 'university-education')
					),					
				)
			);
			
			$page_builder['content-item']['options']['upcoming-event'] = array(
				'title'=> esc_attr__('Upcoming Event', 'university-education'), 
				'icon'=>'fa-calendar-check-o',
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'university-education')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'university-education')
					),
					'heading'=> array(
						'title'=> esc_attr__('Element Heading' ,'university-education'),
						'type'=> 'text',	
						'default'=> '',
						'description'=> esc_attr__('Please add element heading text which will be shown above the element.', 'university-education')
					),	
					'caption'=> array(
						'title'=> esc_attr__('Element Caption' ,'university-education'),
						'type'=> 'text',	
						'default'=> '',
						'description'=> esc_attr__('Please add Element Caption text here which will be shwon above the element.', 'university-education')
					),	
					'title-num-excerpt'=> array(
						'title'=> esc_attr__('Title Num Length (Word)' ,'university-education'),
						'type'=> 'text',	
						'default'=> '15',
						'description'=> esc_attr__('This is a number of word (decided by spaces) that you want to show on the event title. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the WordPress more tag</strong>.', 'university-education')
					),
					'event-upload-image' => array(
						'title' => esc_html__('Upload Image', 'university-education'),
						'button' => esc_html__('Select Image', 'university-education'),
						'type' => 'upload',
						'description' => 'You can upload the Image from here.',
					),						
					'category'=> array(
						'title'=> esc_attr__('Category' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('event-categories'),
						'description'=> esc_attr__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'margin-bottom' => array(
						'title' => esc_attr__('Margin Bottom', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_attr__('Spaces after ending of this item', 'university-education')
					),					
				)
			);
			
			
			
			return $page_builder;
		}
	}
	
	if( !function_exists('university_education_get_event_info') ){
		function university_education_get_event_info( $event_id='', $array = array(), $wrapper = true, $sep = '',$div_wrap = 'div' ){
			global $university_education_theme_option; $ret = '';
			if( empty($array) ) return $ret;
			//$exclude_meta = empty($university_education_theme_option['post-meta-data'])? array(): esc_attr($university_education_theme_option['post-meta-data']);
			
			foreach($array as $post_info){
				
				if( !empty($sep) ) $ret .= $sep;

				switch( $post_info ){
					case 'date':
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-date"><i class="fa fa-clock-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))) . '">';
						$ret .= esc_attr(get_the_time());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($div_wrap).'>';
						break;
					case 'tag':
						$tag = get_the_term_list($event_id, 'event-tags', '', '<span class="sep">,</span> ' , '' );
						if(empty($tag)) break;					
						
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-tags"><i class="fa fa-tag"></i>';
						$ret .= $tag;						
						$ret .= '</'.esc_attr($div_wrap).'>';					
						break;
					case 'category':
						$category = get_the_term_list($event_id, 'event-categories', '', '<span class="sep">,</span> ' , '' );
						if(empty($category)) break;
						
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-category"><i class="fa fa-list"></i>';
						$ret .= $category;					
						$ret .= '</'.esc_attr($div_wrap).'>';			
						break;
					case 'comment':
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-comment"><i class="fa fa-comment-o"></i>';
						$ret .= '<a href="' . esc_url(get_permalink($event_id)) . '#respond" >' . esc_attr(get_comments_number()) . ' Comments</a>';						
						$ret .= '</'.esc_attr($div_wrap).'>';					
						break;
					case 'author':
						ob_start();
						the_author_posts_link();
						$author = ob_get_contents();
						ob_end_clean();
						
						$ret .= '<'.esc_attr($div_wrap).' class="event-info event-author"><i class="fa fa-user"></i>';
						$ret .= $author;
						$ret .= '</'.esc_attr($div_wrap).'>';			
						break;						
				}
			}
			
			
			if($wrapper && !empty($ret)){
				return '<div class="kode-event-info kode-info">' . $ret . '<div class="clear"></div></div>';
			}else if( !empty($ret) ){
				return $ret;
			}
			return '';			
		}
	}
	
	//Event Listing
	if( !function_exists('university_education_get_events_item') ){
		function university_education_get_events_item( $settings ){
			global $university_education_spaces,$university_education_theme_option;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$settings['element-item-class'] = empty($settings['element-item-class'])? '' : $settings['element-item-class'];
			$evn = '<div '.$margin_style.' '.$item_id.' class="'.esc_attr($settings['element-item-class']).' kode_event_upnext_new">';
			// $settings['category'];
			// $settings['tag'];
			// $settings['num-excerpt'];
			// $settings['num-fetch'];
			// $settings['event-style'];
			// $settings['scope'];
			// $settings['order'];
			// $settings['margin-bottom'];			
			$order = 'DESC';
			$limit = 10;//Default limit
			$offset = '';		
			$rowno = 0;
			$event_count = 0;
			$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>$settings['scope'], 'limit' => $settings['num-fetch'], 'order' => $settings['order']) );
			$events_count = count ( $EM_Events );	
			$current_size = 0;
			$size = 1;
			// $evn = '<div class="event-listing">';
			
			$settings['thumbnail-size'];
			$settings['event-size'];
			if($settings['event-style'] == 'event-simple-view'){
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-simple-view col-md-12"><div class="row">';
			}else if($settings['event-style'] == 'event-modern-view'){
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-modern-view col-md-12"><div class="row">';
			}else if($settings['event-style'] == 'event-grid-view'){
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-grid-view col-md-12"><div class="row">';
			}else if($settings['event-style'] == 'event-medium-view'){
				if($settings['event-size'] == 3){
					$size = 2;
				}else if($size == 4){
					$size = 2;
				}else{
					$size = $settings['event-size'];	
				}
				$evn = '<div class="event-listing event-medium-view col-md-12"><div class="row">';
			}else if($settings['event-style'] == 'event-full-view'){
				if($settings['event-size'] == 3){
					$size = 2;
				}else if($size == 4){
					$size = 2;
				}else{
					$size = $settings['event-size'];	
				}
				$evn = '<div class="event-listing event-full-view col-md-12"><div class="row">';
			}else{
				$size = $settings['event-size'];
				$evn = '<div class="event-listing event-simple-view col-md-12"><div class="row">';
			}
			if($settings['event-style'] == 'event-calendar-view'){
			$current_date = date('Y-d-m');
			$evn = '
			<script type="text/javascript">
				jQuery(document).ready(function($){
					$("#calendar").fullCalendar({
						defaultDate: "2015-02-12",
						editable: true,
						eventLimit: true, // allow "more" link when too many events
						events: [';
			}
			if($settings['event-style'] == 'event-marker-view'){
				
				$script_html = '
				var icons = [ ';
					foreach ( $EM_Events as $event ) {
						$event_year = date('Y',$event->start);
						$event_month = date('m',$event->start);
						$event_day = date('d',$event->start);
						$event_start_time = date("G,i,s", strtotime($event->start_time));
						$location = esc_attr($event->get_location()->name);
						global $post;
							$script_html .= "'".UOE_PATH.'/images/map-icon-2.png'."',";
					} 
					$script_html .= '
				];';
				
				$evn = '
				
				<script type="text/javascript">
					var geocoder = new google.maps.Geocoder();
					var iconURLPrefix = "images";
					'.$script_html.'
					function university_education_initialize(){
						
						var MY_MAPTYPE_ID = "custom_style";
						var icons_length = icons.length;
						var shadow = {
						  anchor: new google.maps.Point(16,16),
						  url: iconURLPrefix + "msmarker.shadow.png"
						};
						var featureOpts = [
							{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}

						];
						var myOptions = {
						  center: new google.maps.LatLng(16,18),
						  mapTypeId: MY_MAPTYPE_ID,
						  mapTypeControl: true,
						  streetViewControl: true,
						  panControl: true,
						  scrollwheel: false,
						  draggable: true,	  
						  zoom: 3,
						}
						var map = new google.maps.Map(document.getElementById("kode_map_canv"), myOptions);
						var styledMapOptions = {
							name: "Custom Style"
						};

						var customMapType = new google.maps.StyledMapType(
							[
							  {
								stylers: [
								  {hue: "'.esc_attr($university_education_theme_option['color-scheme-one']).'"},
								  {visibility: "simplified"},
								  {gamma: 0.5},
								  {weight: 0.5}
								]
							  },
							  {
								elementType: "labels",
								stylers: [{visibility: "off"}]
							  },
							  {
								featureType: "water",
								stylers: [{color: "'.esc_attr($university_education_theme_option['color-scheme-one']).'"}]
							  }
							], {
							  name: "Custom Style"
						  }
						);

						map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
						
						var infowindow = new google.maps.InfoWindow({
						  maxWidth: 350,
						});
						var marker;
						var markers = new Array();
						var iconCounter = 0;';				
			}
			$settings['title-num-excerpt'] = (empty($settings['title-num-excerpt']))? '15': $settings['title-num-excerpt'];
			$modern_layout = $settings['event-style'];
			$current_size = 0;
			foreach ( $EM_Events as $event ) {
				$event_year = date('Y',$event->start);
				$event_month = date('M',$event->start);
				$event_day = date('d',$event->start);
				$event_start_time = date("G,i,s", strtotime($event->start_time));
				$location = esc_attr($event->get_location()->name);
				if($settings['event-style'] <> 'event-calendar-view'){
					if($settings['event-style'] <> 'event-marker-view'){
						if( $current_size % $size == 0 ){
							$evn .= '<div class="clear clearfix"></div>';
						}
					}
				}
				if($settings['event-style'] == 'event-simple-view'){
					$evn .= '
					<div class="kode-ux ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
						if($current_size % 2 == 0 ){
						$evn .= '
						<div class="edu2_new_des">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="edu2_event_des">
										<h4>'.esc_attr($event_month).'</h4>
										<p>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</p>
										<ul class="post-option">
											'.university_education_get_event_info($event->post_id,array('comment','author','date'),'','','li').'
										</ul>
										<a href="'.esc_url($event->guid).'" class="readmore">'.esc_attr__('read more','university-education').'<i class="fa fa-long-arrow-right"></i></a>
										<span>'.esc_attr($event_day).'</span>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 thumb">
									<figure><a href="'.esc_url($event->guid).'">'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'</a>
										<figcaption><a href="'.esc_url($event->guid).'"><i class="fa fa-plus"></i></a></figcaption>
									</figure>
								</div>
							</div>
						</div>';
						}else{
							$evn .= '
							<div class="edu2_new_des">
								<div class="row">
									<div class="col-md-6 col-sm-6 thumb">
										<figure><a href="'.esc_url($event->guid).'">'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'</a>
											<figcaption><a href="'.esc_url($event->guid).'"><i class="fa fa-plus"></i></a></figcaption>
										</figure>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="edu2_event_des text-right">
											<h4>'.esc_attr($event_month).'</h4>
											<p>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</p>
											<ul class="post-option">
												'.university_education_get_event_info($event->post_id,array('comment','author','date'),'','','li').'
											</ul>
											<a href="'.esc_url($event->guid).'" class="readmore">'.esc_attr__('read more','university-education').'<i class="fa fa-long-arrow-right"></i></a>
											<span>'.esc_attr($event_day).'</span>
										</div>
									</div>
								</div>
							</div>';
						}
					$evn .= '	
					</div>';
				}else if($settings['event-style'] == 'event-modern-view'){
					$evn .= '
					<div class="kode-ux ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
						if($current_size % 2 == 0 ){
						$evn .= '
						<div class="edu2_event_wrap">
							<div class="edu2_event_des">
								<h4>'.esc_attr($event_month).'</h4>
								<p>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</p>
								<ul class="post-option">
										'.university_education_get_event_info($event->post_id,array('comment','author','date'),'','','li').'
								</ul>
								<a href="'.esc_url($event->guid).'" class="readmore">'.esc_attr__('read more','university-education').'<i class="fa fa-long-arrow-right"></i></a>
								<span>'.esc_attr($event_day).'</span>
							</div>
								
							<figure>'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
								<figcaption><a href="'.esc_url($event->guid).'"><i class="fa fa-plus"></i></a></figcaption>
							</figure>
						</div>';
						}else{
							$evn .= '
							<div class="edu2_event_wrap side_change">
								<div class="edu2_event_des">
									<h4>'.esc_attr($event_month).'</h4>
									<p>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</p>
									<ul class="post-option">
											'.university_education_get_event_info($event->post_id,array('comment','author','date'),'','','li').'
									</ul>
									<a href="'.esc_url($event->guid).'" class="readmore">'.esc_attr__('read more','university-education').'<i class="fa fa-long-arrow-right"></i></a>
									<span>'.esc_attr($event_day).'</span>
								</div>
									
								<figure>'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
									<figcaption><a href="'.esc_url($event->guid).'"><i class="fa fa-plus"></i></a></figcaption>
								</figure>
							</div>';
						}
					$evn .= '	
					</div>';
				}else if($settings['event-style'] == 'event-grid-view'){
					 if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<p class="location"><i class="fa fa-location-arrow"></i>
								'.$location.'
							</p>';
						}
					}else{
						$event_html = '';				
					}
					$evn .= '
					<div class="kode-ux  event-grid-view ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">
						<div class="kode-blog-list option-2">
							<div class="kode-thumb">
								<a href="#">'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'</a>
								<div class="blog-date">
									<p>15</p>
									<span>August</span>
								</div>
							</div>
							<div class="kode-text">
								<h2>'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</h2>
								'.$event_html.'								
							</div>
						</div>
					</div>';				
				}else if($settings['event-style'] == 'event-medium-view'){
					 if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<p>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</p>';
						}
					}else{
						$event_html = '';				
					}
					$evn .= '
					<div class="kode-ux ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">			
						<div class="edu2_new_des">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="edu2_event_des">
										<h4>'.esc_attr($event->start_date).'</h4>
										<p>'.esc_attr(strip_tags(substr($event->post_excerpt,0,$settings['num-excerpt']))).'</p>
										<ul class="post-option">
											'.university_education_get_event_info($event->post_id,array('comment','author','date'),'','','li').'
										</ul>
										<a href="'.esc_url($event->guid).'" class="readmore">read more<i class="fa fa-long-arrow-right"></i></a>
										<span>'.esc_attr($event->start_date).'</span>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 thumb">
									<figure>
										'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
										<figcaption><a href="'.esc_url($event->guid).'"><i class="fa fa-plus"></i></a></figcaption>
									</figure>
								</div>
							</div>
						</div>
					</div>';
				}else if($settings['event-style'] == 'event-full-view'){
                  if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<span>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</span>';
						}
					}else{
						$event_html = '';				
					}
					$evn .= '
					<!--EVENT LIST Wrap Start-->
					<div class="kf_event_list_wrap">
						<div class="row">
							<div class="col-lg-6 col-md-5 col-sm-5">
								<!--EVENT LIST THUMB Start-->
								<div class="kf_event_list_thumb">
									<figure>
										'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'
										<div data-year="'.esc_attr($event_year).'" data-month="'.esc_attr($event_month).'" data-day="'.esc_attr($event_day).'" data-time="'.esc_attr($event_start_time).'" class="countdown"></div>
									</figure>
								</div>
								<!--EVENT LIST THUMB END-->
							</div>

							<div class="col-lg-6 col-md-7 col-sm-7">
								<!--EVENT LIST DES Start-->
								<div class="kf_event_list_des">
									<h4><a href="'.esc_url($event->guid).'">'.esc_attr($event->post_title).'</a></h4>
									<p>'.esc_attr(substr($event->post_content,0,$settings['num-excerpt'])).'</p>
									<ul class="kf_event_list_links">
										'.university_education_get_event_info($event->post_id,array('comment','author','date'),'','','li').'
									</ul>
								</div>
								<!--EVENT LIST DES END-->
							</div>
						</div>
					</div>
					<!--EVENT LIST Wrap END-->';
				}else if($settings['event-style'] == 'event-calendar-view'){
                  if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_html = '<span>
								<i class="fa fa-map-marker"></i>
								'.$location.'
							</span>';
						}
					}else{
						$event_html = '';				
					}
				  $evn .= '
					{
						id: '.esc_attr($event->post_id).',
						title: "'.esc_attr($event->post_title).'",
						start: "'.esc_attr(date('Y-d-m',$event->start)).'T'.esc_attr(date('H:i:s',strtotime($event->start_time))).'",
						end: "'.esc_attr(date('Y-d-m',$event->end)).'",
						url: "'.$event->guid.'",
					},';
				}else if($settings['event-style'] == 'event-marker-view'){
                  if(isset($event->get_location()->location_address)){
						$location = esc_attr($event->get_location()->location_address);	
						if($location <> ''){
							$event_loc = $location;
						}
					}else{
						$event_loc = '';				
					}
					if($event_loc <> ''){
					$html_ev = '<div class="col-md-12"><div class="kode-event-list-map ">'.get_the_post_thumbnail($event->post_id, $settings['thumbnail-size']).'<div class="kode-event-caption"><h2><a href="'.esc_url($event->guid).'">'.esc_attr(substr($event->post_title,0,$settings['title-num-excerpt'])).'</a></h2><p>'.esc_attr(date('D M d Y',$event->start)).'</p><a class="btn-borderd" href="'.esc_url($event->guid).'">'.esc_attr__('Read More','university-education').'</a></div></div></div>';
					
					$evn .= "
						var i = ".esc_attr($current_size).";
						geocoder.geocode( { 'address': '".esc_attr($event_loc)."'}, function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {

								marker = new google.maps.Marker({
									position: results[0].geometry.location,
									map: map,
									icon : icons[iconCounter],
									shadow: shadow
								});

							markers.push(marker);
							google.maps.event.addListener(marker, 'click', (function(marker, i) {
								return function() {
									infowindow.setContent('".$html_ev."');
									infowindow.open(map, marker);
								}
							})(marker, i));
							  
							  iconCounter++;
							  // We only have a limited number of possible icon colors, so we may have to restart the counter
							  if(iconCounter >= icons_length){
								iconCounter = 0;
							  }
							}
							
						});";
					}	
				}else{
				
				}
				$current_size++;
			}
			
			if($settings['event-style'] == 'event-marker-view'){
				$evn .= '}
						google.maps.event.addDomListener(window, "load", university_education_initialize);
				</script><div class="row"><div class="ev_map_canvas" id="kode_map_canv"></div></div>';
			}
			if($settings['event-style'] == 'event-calendar-view'){
				$evn .= '
							]
						});
					})
				</script>
				<div id="calendar"></div>
				';
			}
			
			if($modern_layout == 'event-grid-modern'){
				$evn .= '</div></div>';	
			}else if($modern_layout == 'event-grid-simple'){
				$evn .= '</ul></div>';
			}else if($modern_layout == 'event-grid-view'){
				$evn .= '</div></div>';
			}else if($settings['event-style'] == 'event-marker-view'){
				$evn .= '';
			}else if($settings['event-style'] == 'event-calendar-view'){
				$evn .= '';
			}else{
				$evn .= '</div></div>';
			}
		return $evn;
		}
	}
	
	function university_education_get_upnext_event( $settings ){
		
		global $university_education_spaces;
		$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
		$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
		$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
		$settings['element-item-class'] = empty($settings['element-item-class'])? '' : $settings['element-item-class'];
		$evn = '<div '.$margin_style.' '.$item_id.' class="'.esc_attr($settings['element-item-class']).' kode_event_upnext_new">';
		
		$settings['heading'];
		$settings['caption'];
		$settings['category'];
		$settings['title-num-excerpt'];		
		$order = 'DESC';
		$limit = 10;//Default limit
		$offset = '';		
		$rowno = 0;
		$event_count = 0;
		$EM_Events = EM_Events::get( array('category'=>$settings['category'], 'group'=>'this','scope'=>'future', 'limit' => '1', 'order' => 'DESC') );
		$events_count = count ( $EM_Events );	
		foreach ( $EM_Events as $event ) {
			$event_year = date('Y',$event->start);
			$event_month = date('m',$event->start);
			$event_day = date('d',$event->start);
			$event_start_time = date("G:i:s", strtotime($event->start_time));
			$location = esc_attr($event->get_location()->name);
			$thumbnail_id = get_post_thumbnail_id( $settings['event-upload-image'] );
			$thumbnail = wp_get_attachment_image_src( $settings['event-upload-image'] , 'large' );
			$evn .= '
			<!--TRAINING WRAP START-->
			<div class="edu2_tarining_bg">
				<div class="col-md-4">
					<div class="kf_edu2_training_des">
						<figure>
							<img src="'.esc_url($thumbnail[0]).'" alt=""/>
						</figure>
					</div>
				</div>

				<div class="col-md-8">
					<div class="edu2_training_wrap">
						<h2>'.esc_attr($settings['heading']).'</h2>
						<h3>'.esc_attr($settings['caption']).'</h3>
						<!--COUNTDOWN START-->
						<ul data-year="'.esc_attr($event_year).'" data-month="'.esc_attr($event_month).'" data-day="'.esc_attr($event_day).'" data-time="'.esc_attr($event_start_time).'" class="downcount">
							<li>
								<span class="days">00</span>
								<p class="days_ref">'.esc_attr__('Week','university-education').'</p>
							</li>
							<li>
								<span class="hours">00</span>
								<p class="hours_ref">'.esc_attr__('Days','university-education').'</p>
							</li>
							<li>
								<span class="minutes">00</span>
								<p class="minutes_ref">'.esc_attr__('Hours','university-education').'</p>
							</li>
							<li>
								<span class="seconds last">00</span>
								<p class="seconds_ref">'.esc_attr__('Second','university-education').'</p>
							</li>
						</ul>
						<!--COUNTDOWN END-->
						<strong>'.esc_attr__('It’s limited Seating! Hurry up','university-education').'</strong>
						<a href="'.esc_url(get_permalink()).'" class="btn-1">'.esc_attr__('REGISTER NOW','university-education').'</a>
					</div>

				</div>
			</div>
			<!--TRAINING WRAP END-->';
			
		}	
		$evn .= '</div>';
		return $evn;
		
		
	}
	
	
	
?>