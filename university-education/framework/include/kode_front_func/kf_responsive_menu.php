<?php
	/*	
	*	Kodeforest Menu Management File
	*	---------------------------------------------------------------------
	*	This file use to include a necessary script / function for the 
	* 	navigation area
	*	---------------------------------------------------------------------
	*/
	
	// add action to enqueue superfish menu
	add_filter('university_education_enqueue_scripts', 'university_education_register_dlmenu');
	if( !function_exists('university_education_register_dlmenu') ){
		function university_education_register_dlmenu($array){	
			$array['style']['dlmenu'] = UOE_PATH . '/framework/include/frontend_assets/dl-menu/component.css';
			
			$array['script']['modernizr'] = UOE_PATH . '/framework/include/frontend_assets/dl-menu/modernizr.custom.js';
			$array['script']['dlmenu'] = UOE_PATH . '/framework/include/frontend_assets/dl-menu/jquery.dlmenu.js';

			return $array;
		}
	}
	
	// creating the class for outputing the custom navigation menu
	if( !class_exists('university_education_dlmenu_walker') ){
		
		// from wp-includes/nav-menu-template.php file
		class university_education_dlmenu_walker extends Walker_Nav_Menu{		

			function start_lvl( &$output, $depth = 0, $args = array() ) {
				$indent = str_repeat("\t", $depth);
				$output .= "\n$indent<ul class=\"dl-submenu\">\n";
			}

		}
		
	}

?>