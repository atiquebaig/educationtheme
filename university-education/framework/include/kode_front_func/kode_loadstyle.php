<?php
	/*	
	*	Kodeforest Load Style
	*	---------------------------------------------------------------------
	*	This file create the custom style
	*	For Color Scheme, and Typography Options
	*	---------------------------------------------------------------------
	*/	
		
		
		$font_options_slug = 'font_option_slug';
		add_action('university_education_save_' + $font_options_slug, 'university_education_save_font_options');
		if( !function_exists('university_education_save_font_options') ){
			function university_education_save_font_options($options){
				
				// for multisite
				$file_url = get_template_directory() . '/css/style-custom.css';
				if( is_multisite() && get_current_blog_id() > 1 ){
					$file_url = get_template_directory() . '/css/style-custom' . get_current_blog_id() . '.css';
				}
				
				// write file content
				$university_education_theme_option = get_option('university_education_admin_option', array());
				
				// for updating google font list to use on front end
				global $university_education_font_controller; $google_font_list = array(); 
				
				foreach( $options as $menu_key => $menu ){
					if(!empty($menu['options'])){
						foreach( $menu['options'] as $submenu_key => $submenu ){
							if( !empty($submenu['options']) ){
								foreach( $submenu['options'] as $option_slug => $option ){
									if( !empty($option['selector']) ){
										// prevents warning message
										$option['data-type'] = (empty($option['data-type']))? 'color': $option['data-type'];
										
										if( !empty($university_education_theme_option[$option_slug]) ){
											$value = university_education_check_option_data_type($university_education_theme_option[$option_slug], $option['data-type']);
										}else{
											$value = '';
										}
										if($value){
											$university_education_font = str_replace('#kode#', $value, $option['selector']) . "\r\n";
										}
										
										// updating google font list
										if( $menu_key == 'font-settings' ){
											if( !empty($university_education_font_controller->google_font_list[$university_education_theme_option[$option_slug]]) ){
												$google_font_list[$university_education_theme_option[$option_slug]] = $university_education_font_controller->google_font_list[$university_education_theme_option[$option_slug]];
											}
										}
									}
								}
							}
						}
					}
				}
				
				// update google font list
				update_option('university_education_google_font_list', $google_font_list);	

				$render_style = university_education_generate_style_custom($university_education_theme_option,$page_options='no');
				
				$current_page =  wp_nonce_url(admin_url('admin.php?page=uoe'),'university-education');
				if( !university_education_write_filesystem($current_page, $file_url, $render_style) ){
					$ret = array(
						'status'=>'failed', 
						'message'=> '<span class="head">' . esc_html__('Cannot write style-custom.css file', 'university-education') . '</span> ' .
							esc_html__('Please contact the administrator.' ,'university-education')
					);	
					
					die(json_encode($ret));		
				}
				
			}
		}
		
		
		if( !function_exists('university_education_generate_style_custom') ){
			function university_education_generate_style_custom( $university_education_theme_options,$page_options= "" ){				
				// write file content
				if($page_options == 'Yes'){
					$university_education_theme_option = $university_education_theme_options;
					$k_option = 'custom_style';
					if(isset($_GET['layout']) && $_GET['layout'] == 'boxed'){
						$university_education_theme_option['enable-boxed-style'] = 'boxed-style';
					}else{
						$university_education_theme_option['enable-boxed-style'] = 'full-style';
					}
				}else{
					$university_education_theme_option = get_option('university_education_admin_option', array());
					$k_option = 'default_stylesheet';
				}
				
				$style = '';
				
				
				if(!empty($university_education_theme_option['logo-width'])){
					if($university_education_theme_option['logo-width'] <> ''){
						if($university_education_theme_option['logo-width'] <> 0){
							$style .= '.logo img{';
							$style .= 'width:'.esc_attr($university_education_theme_option['logo-width']) .'px';
							$style .= '}' . "\r\n";
						}
						
					}
				}

				
				if(!empty($university_education_theme_option['logo-height'])){
					if($university_education_theme_option['logo-height'] <> ''){
						if($university_education_theme_option['logo-height'] <> 0){
							$style .= '.logo img{';
							$style .= 'height:'.esc_attr($university_education_theme_option['logo-height']) .'px';
							$style .= '}' . "\r\n";
						}
					}
				}
				
				
				//Side bar Area Logo Width Height
				if(!empty($university_education_theme_option['slide-logo-width'])){
					if($university_education_theme_option['slide-logo-width'] <> ''){
						if($university_education_theme_option['slide-logo-width'] <> 0){
							$style .= '.side-logo-widget img{';
							$style .= 'width:'.esc_attr($university_education_theme_option['slide-logo-width']) .'px';
							$style .= '}' . "\r\n";
						}
						
					}
				}

				
				if(!empty($university_education_theme_option['slide-logo-height'])){
					if($university_education_theme_option['slide-logo-height'] <> ''){
						if($university_education_theme_option['slide-logo-height'] <> 0){
							$style .= '.side-logo-widget img{';
							$style .= 'height:'.esc_attr($university_education_theme_option['slide-logo-height']) .'px';
							$style .= '}' . "\r\n";
						}
					}
				}
				
				if(!empty($university_education_theme_option['navi-font-family'])){
					if($university_education_theme_option['navi-font-family'] <> ''){
						$style .= '
						.kode-navigation-wrapper,
						.kode-navigation-wrapper ul li,
						.kode-navigation-wrapper ul li a{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['navi-font-family']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['heading-font-family'])){
					if($university_education_theme_option['heading-font-family'] <> ''){
						$style .= '.kode-caption-title, .kode-caption-text, h1, h2, h3, h4, h5, h6{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['heading-font-family']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['navigation-font-family'])){
					if($university_education_theme_option['navigation-font-family'] <> ''){
						$style .= '.kode-navigation-wrapper ul li a{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['navigation-font-family']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['navigation-font-size'])){
					if($university_education_theme_option['navigation-font-size'] <> ''){
						$style .= '.kode-navigation-wrapper ul li a{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['navigation-font-size']) .'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['navigation-line-height'])){
					if($university_education_theme_option['navigation-line-height'] <> ''){
						$style .= '.kode-navigation-wrapper ul li a{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['navigation-line-height']) .'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['navigation-font-weight'])){
					if($university_education_theme_option['navigation-font-weight'] <> ''){
						$style .= '.kode-navigation-wrapper ul li a{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['navigation-font-weight']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['navigation-letter-spacing'])){
					if($university_education_theme_option['navigation-letter-spacing'] <> ''){
						$style .= '.kode-navigation-wrapper ul li a{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['navigation-letter-spacing']) .'px';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['navigation-text-transform'])){
					if($university_education_theme_option['navigation-text-transform'] <> ''){
						$style .= '.kode-navigation-wrapper ul li a{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['navigation-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//heading h1 typo
				
				if(!empty($university_education_theme_option['h1-font-family'])){
					if($university_education_theme_option['h1-font-family'] <> ''){
						$style .= 'body h1{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['h1-font-family']) .'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h1-font-size'])){
					if($university_education_theme_option['h1-font-size'] <> ''){
						$style .= 'body h1{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['h1-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h1-line-height'])){
					if($university_education_theme_option['h1-line-height'] <> ''){
						$style .= 'body h1{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['h1-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h1-font-weight'])){
					if($university_education_theme_option['h1-font-weight'] <> ''){
						$style .= 'body h1{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['h1-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h1-letter-spacing'])){
					if($university_education_theme_option['h1-letter-spacing'] <> ''){
						$style .= 'body h1{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['h1-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h1-text-transform'])){
					if($university_education_theme_option['h1-text-transform'] <> ''){
						$style .= 'body h1{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['h1-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end heading h1 typo
				
				//heading h2 typo
				
				if(!empty($university_education_theme_option['h2-font-family'])){
					if($university_education_theme_option['h2-font-family'] <> ''){
						$style .= 'body h2{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['h2-font-family']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h2-font-size'])){
					if($university_education_theme_option['h2-font-size'] <> ''){
						$style .= 'body h2{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['h2-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h2-line-height'])){
					if($university_education_theme_option['h2-line-height'] <> ''){
						$style .= 'body h2{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['h2-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h2-font-weight'])){
					if($university_education_theme_option['h2-font-weight'] <> ''){
						$style .= 'body h2{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['h2-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h2-letter-spacing'])){
					if($university_education_theme_option['h2-letter-spacing'] <> ''){
						$style .= 'body h2{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['h2-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h2-text-transform'])){
					if($university_education_theme_option['h2-text-transform'] <> ''){
						$style .= 'body h2{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['h2-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end heading h2 typo
				
				//heading h3 typo
				
				if(!empty($university_education_theme_option['h3-font-family'])){
					if($university_education_theme_option['h3-font-family'] <> ''){
						$style .= 'body h3{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['h3-font-family']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h3-font-size'])){
					if($university_education_theme_option['h3-font-size'] <> ''){
						$style .= 'body h3{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['h3-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h3-line-height'])){
					if($university_education_theme_option['h3-line-height'] <> ''){
						$style .= 'body h3{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['h3-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h3-font-weight'])){
					if($university_education_theme_option['h3-font-weight'] <> ''){
						$style .= 'body h3{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['h3-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h3-letter-spacing'])){
					if($university_education_theme_option['h3-letter-spacing'] <> ''){
						$style .= 'body h3{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['h3-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h3-text-transform'])){
					if($university_education_theme_option['h3-text-transform'] <> ''){
						$style .= 'body h3{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['h3-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end heading h3 typo
				
				//heading h4 typo
				
				if(!empty($university_education_theme_option['h4-font-family'])){
					if($university_education_theme_option['h4-font-family'] <> ''){
						$style .= 'body h4{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['h4-font-family']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h4-font-size'])){
					if($university_education_theme_option['h4-font-size'] <> ''){
						$style .= 'body h4{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['h4-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h4-line-height'])){
					if($university_education_theme_option['h4-line-height'] <> ''){
						$style .= 'body h4{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['h4-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h4-font-weight'])){
					if($university_education_theme_option['h4-font-weight'] <> ''){
						$style .= 'body h4{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['h4-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h4-letter-spacing'])){
					if($university_education_theme_option['h4-letter-spacing'] <> ''){
						$style .= 'body h4{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['h4-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h4-text-transform'])){
					if($university_education_theme_option['h4-text-transform'] <> ''){
						$style .= 'body h4{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['h4-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end heading h4 typo
				
				//heading h5 typo
				
				if(!empty($university_education_theme_option['h5-font-family'])){
					if($university_education_theme_option['h5-font-family'] <> ''){
						$style .= 'body h5{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['h5-font-family']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h5-font-size'])){
					if($university_education_theme_option['h5-font-size'] <> ''){
						$style .= 'body h5{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['h5-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h5-line-height'])){
					if($university_education_theme_option['h5-line-height'] <> ''){
						$style .= 'body h5{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['h5-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h5-font-weight'])){
					if($university_education_theme_option['h5-font-weight'] <> ''){
						$style .= 'body h5{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['h5-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h5-letter-spacing'])){
					if($university_education_theme_option['h5-letter-spacing'] <> ''){
						$style .= 'body h5{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['h5-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h5-text-transform'])){
					if($university_education_theme_option['h5-text-transform'] <> ''){
						$style .= 'body h5{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['h5-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end heading h5 typo
				
				//heading h6 typo
				
				if(!empty($university_education_theme_option['h6-font-family'])){
					if($university_education_theme_option['h6-font-family'] <> ''){
						$style .= 'body h6{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['h6-font-family']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h6-font-size'])){
					if($university_education_theme_option['h6-font-size'] <> ''){
						$style .= 'body h6{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['h6-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h6-line-height'])){
					if($university_education_theme_option['h6-line-height'] <> ''){
						$style .= 'body h6{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['h6-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h6-font-weight'])){
					if($university_education_theme_option['h6-font-weight'] <> ''){
						$style .= 'body h6{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['h6-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h6-letter-spacing'])){
					if($university_education_theme_option['h6-letter-spacing'] <> ''){
						$style .= 'body h6{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['h6-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['h6-text-transform'])){
					if($university_education_theme_option['h6-text-transform'] <> ''){
						$style .= 'body h6{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['h6-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end heading h6 typo
				
				//Paragraph Typo
				
				if(!empty($university_education_theme_option['body-font-family'])){
					if($university_education_theme_option['body-font-family'] <> ''){
						$style .= 'body, p{';
						$style .= 'font-family:'.esc_attr($university_education_theme_option['body-font-family']).'';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['body-font-size'])){
					if($university_education_theme_option['body-font-size'] <> ''){
						$style .= 'body, p{';
						$style .= 'font-size:'.esc_attr($university_education_theme_option['body-font-size']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['body-line-height'])){
					if($university_education_theme_option['body-line-height'] <> ''){
						$style .= 'body, p{';
						$style .= 'line-height:'.esc_attr($university_education_theme_option['body-line-height']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['body-font-weight'])){
					if($university_education_theme_option['body-font-weight'] <> ''){
						$style .= 'body, p{';
						$style .= 'font-weight:'.esc_attr($university_education_theme_option['body-font-weight']) .' ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['body-letter-spacing'])){
					if($university_education_theme_option['body-letter-spacing'] <> ''){
						$style .= 'body, p{';
						$style .= 'letter-spacing:'.esc_attr($university_education_theme_option['body-letter-spacing']) .'px ';
						$style .= '}' . "\r\n";
						
					}
				}
				
				if(!empty($university_education_theme_option['body-text-transform'])){
					if($university_education_theme_option['body-text-transform'] <> ''){
						$style .= 'body, p{';
						$style .= 'text-transform:'.esc_attr($university_education_theme_option['body-text-transform']) .' !important';
						$style .= '}' . "\r\n";
						
					}
				}
				
				//end Paragraph Typo
				
				
					
					if(!empty($university_education_theme_option['navi-color'])){
						if($university_education_theme_option['navi-color'] <> ''){
							$style .= '.navigation ul > li > a, .navbar-nav > li > a{';
							$style .= 'color:'.esc_attr($university_education_theme_option['navi-color']).' !important';
							$style .= '}' . "\r\n";
							
						}
					}
					
					
					
					
					if(!empty($university_education_theme_option['navi-hover-bg'])){
						if($university_education_theme_option['navi-hover-bg'] <> ''){
							$style .= '.navigation ul > li:hover > a{';
							$style .= 'background-color:'.esc_attr($university_education_theme_option['navi-hover-bg']).' !important';
							$style .= '}' . "\r\n";
						}
					}
					
					if(!empty($university_education_theme_option['navi-dropdown-hover-bg'])){
						if($university_education_theme_option['navi-dropdown-hover-bg'] <> ''){
							$style .= '.navigation ul.sub-menu > li:hover > a{';
							$style .= 'background-color:'.esc_attr($university_education_theme_option['navi-dropdown-hover-bg']).' !important';
							$style .= '}' . "\r\n";
						}
					}
					
					if(!empty($university_education_theme_option['navi-hover-color'])){
						if($university_education_theme_option['navi-hover-color'] <> ''){
							$style .= '.navigation ul > li:hover > a, .navbar-nav > li:hover{';
							$style .= 'color:'.esc_attr($university_education_theme_option['navi-hover-color']).' !important';
							$style .= '}' . "\r\n";
							
						}
					}
					
					if(!empty($university_education_theme_option['navi-dropdown-bg'])){
						if($university_education_theme_option['navi-dropdown-bg'] <> ''){
							$style .= '.navigation .sub-menu, .navigation .children, .navbar-nav .children{';
							$style .= 'background:'.esc_attr($university_education_theme_option['navi-dropdown-bg']).' !important';
							$style .= '}' . "\r\n";
							
						}
					}
					if(!empty($university_education_theme_option['navi-dropdown-hover'])){
						if($university_education_theme_option['navi-dropdown-hover'] <> ''){
							$style .= '.navigation .menu .sub-menu li:hover a:before, .navigation .sub-menu li:hover a:before, .navbar-nav .children li:hover a:before{ ';
							$style .= 'color:'.esc_attr($university_education_theme_option['navi-dropdown-hover']).' !important';
							$style .= ' }' . "\r\n";
							
						}
					}

					if(!empty($university_education_theme_option['navi-dropdown-hover'])){
						if($university_education_theme_option['navi-dropdown-hover'] <> ''){
							$style .= '.navigation ul li ul > li:hover > a, .navbar-nav li ul > li:hover > a{ ';
							$style .= 'color:'.esc_attr($university_education_theme_option['navi-dropdown-hover']).' !important';
							$style .= ' }' . "\r\n";
							
						}
					}
					
					if(!empty($university_education_theme_option['navi-dropdown-color'])){
						if($university_education_theme_option['navi-dropdown-color'] <> ''){
							$style .= '.navigation .sub-menu li a, .navigation .children li a, .navbar-nav .children li a{ ';
							$style .= 'color:'.esc_attr($university_education_theme_option['navi-dropdown-color']).' !important';
							$style .= ' }' . "\r\n";
							
						}
					}	
					
					//Top Bar Text Color
					if(isset($university_education_theme_option['main-menu-text'])){
						if($university_education_theme_option['main-menu-text'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation > .menu > ul > li a, .kode-navigation-wrapper .navigation ul.menu > li a{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['main-menu-text']) . ';  }' . "\r\n";
						}
					}
					 
					//Top Bar Text Color
					if(isset($university_education_theme_option['main-menu-active-link'])){
						if($university_education_theme_option['main-menu-active-link'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li.current_page_item a, .kode-navigation-wrapper .navigation ul.menu > li.current-menu-item.current_page_item a{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['main-menu-active-link']) . ';  }' . "\r\n";
						}
					}
					
					//Top Bar Text Color
					if(isset($university_education_theme_option['main-menu-active-link-bg'])){
						if($university_education_theme_option['main-menu-active-link-bg'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li.current_page_item a, .kode-navigation-wrapper .navigation ul.menu > li.current-menu-item.current_page_item a{';						
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['main-menu-active-link-bg']) . ';  }' . "\r\n";
						}
					}
					
					//Top Bar Text Color
					if(isset($university_education_theme_option['main-menu-active-link'])){
						if($university_education_theme_option['main-menu-active-link'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li.current_page_ancestor > a, .kode-navigation-wrapper .navigation ul.menu > li.current_page_ancestor > a{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['main-menu-active-link']) . ';  }' . "\r\n";
						}
					}
					
					//Top Bar Text Color
					if(isset($university_education_theme_option['main-menu-active-link-bg'])){
						if($university_education_theme_option['main-menu-active-link-bg'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation ul.menu > li.current_page_ancestor > a{';						
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['main-menu-active-link-bg']) . ';  }' . "\r\n";
						}
					}
					
					 
					//Navigation Link Bg Color
					if(isset($university_education_theme_option['main-menu-link-background'])){
						if($university_education_theme_option['main-menu-link-background'] <> ''){						
							$style .= '.kode-navigation-wrapper .navigation .menu > li a, .kode-navigation-wrapper .navigation ul.menu > li a{';
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['main-menu-link-background']) . ';  }' . "\r\n";
						}
					}
					
					//Navigation Link Bg Hover Color
					if(isset($university_education_theme_option['main-menu-link-background-on-hover'])){
						if($university_education_theme_option['main-menu-link-background-on-hover'] <> ''){
							$style .= '
							.navigation ul > li:hover a,
							.kode-navigation-wrapper .navigation .menu > li:hover a, 
										.kode-navigation-wrapper .navigation ul.menu > li:hover a{';
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['main-menu-link-background-on-hover']) . ' !important;  }' . "\r\n";
						}
					}
					
					//Navigation Link Bg Hover Color
					if(isset($university_education_theme_option['main-menu-link-color-on-hover'])){
						if($university_education_theme_option['main-menu-link-color-on-hover'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation ul.menu > li:hover > a{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['main-menu-link-color-on-hover']) . ' !important;  }' . "\r\n";
						}
					}
					
					
					//Navigation Link Bg Hover Color
					if(isset($university_education_theme_option['main-menu-link-icon-color-on-hover'])){
						if($university_education_theme_option['main-menu-link-icon-color-on-hover'] <> ''){
							$style .= '.kode-navigation-wrapper ul li:before{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['main-menu-link-icon-color-on-hover']) . ';  }' . "\r\n";
						}
					}
					
					
					//Sub Menu Configuration Bg Color
					if(isset($university_education_theme_option['submenu-background'])){
						if($university_education_theme_option['submenu-background'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li > ul.children li a, .kode-navigation-wrapper .navigation ul > li > ul.children li a,.kode-navigation-wrapper .navigation ul > li > ul.sub-menu li a,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li a,.kode-navigation-wrapper .navigation ul > li ul.sub-menu li,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li{';
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['submenu-background']) . ' !important;  }' . "\r\n";
						}
					}
					
					
					//Sub Menu Configuration Bg Color
					if(isset($university_education_theme_option['submenu-bottom-border-color'])){
						if($university_education_theme_option['submenu-bottom-border-color'] <> ''){
							$style .= '.sub-menu, .children{';
							$style .= 'border-color: ' . esc_attr($university_education_theme_option['submenu-bottom-border-color']) . ' !important;  }' . "\r\n";
						}
					}
					
					
					
					if(isset($university_education_theme_option['submenu-link-color'])){
						if($university_education_theme_option['submenu-link-color'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li ul.children li a,.kode-navigation-wrapper .navigation ul > li ul.children li a,.kode-navigation-wrapper .navigation ul > li ul.sub-menu li a,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li a,.kode-navigation-wrapper .navigation ul > li ul.sub-menu li,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['submenu-link-color']) . ' !important;  }' . "\r\n";
						}
					}
					
					if(isset($university_education_theme_option['submenu-hover-link-color'])){
						if($university_education_theme_option['submenu-hover-link-color'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li ul.children li:hover a,.kode-navigation-wrapper .navigation ul > li ul.children li:hover a,.kode-navigation-wrapper .navigation ul > li ul.sub-menu li:hover a,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li:hover a, header .kf_logo_nav_wrap .kode-navigation-wrapper .navigation ul > li ul.sub-menu li:hover,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li:hover{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['submenu-hover-link-color']) . ' !important;  }' . "\r\n";
						}
					}
					
					if(isset($university_education_theme_option['submenu-hover-link-hover-bg'])){
						if($university_education_theme_option['submenu-hover-link-hover-bg'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li ul.children li:hover a,.kode-navigation-wrapper .navigation ul > li ul.children li:hover a, .kode-navigation-wrapper .navigation ul > li ul.sub-menu li:hover a,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li:hover a,.kode-navigation-wrapper .navigation > ul > li ul.sub-menu li:hover,.kode-navigation-wrapper .navigation ul.menu > li ul.sub-menu li:hover{';
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['submenu-hover-link-hover-bg']) . ' !important;  }' . "\r\n";
						}
					}
					
					
					//Sub Main Menu Background Color on Active Ancestor
					if(isset($university_education_theme_option['submenu-link-active-bg-color'])){
						if($university_education_theme_option['submenu-link-active-bg-color'] <> ''){
							$style .= '
							.kode-navigation-wrapper .navigation .menu > li.current_page_ancestor ul.children li.current_page_item a, 
							.kode-navigation-wrapper .navigation .menu > li.current_page_ancestor li.current_page_item a, 
							.kode-navigation-wrapper .navigation ul.menu > li.current_page_ancestor li.current_page_item a{';
							$style .= 'background-color: ' . esc_attr($university_education_theme_option['submenu-link-active-bg-color']) . ' !important;  }' . "\r\n";
						}
					}
					
					
					//Page Sub Menu Text Color on Active Ancestor
					if(isset($university_education_theme_option['submenu-link-active-color'])){
						if($university_education_theme_option['submenu-link-active-color'] <> ''){
							$style .= '.kode-navigation-wrapper .navigation .menu > li.current_page_ancestor ul.children li.current_page_item a, .kode-navigation-wrapper .navigation ul.menu > li.current-menu-ancestor.current_page_ancestor li.current-menu-item.current_page_item a{';
							$style .= 'color: ' . esc_attr($university_education_theme_option['submenu-link-active-color']) . ' !important;  }' . "\r\n";
						}
					}
				
				
				// Slider Caption Styling Starts
				if(isset($university_education_theme_option['caption-background-color-switch']) && $university_education_theme_option['caption-background-color-switch'] == 'enable'){
					if(isset($university_education_theme_option['caption-background-color'])){
						$style .= '.kode-caption .kode-caption-title:before,.kode-caption .kode-caption-title::after{';
						$style .= 'border-bottom-color:'.esc_attr($university_education_theme_option['caption-background-color']).' !important';
						$style .= '}' . "\r\n";				
						
						$style .= '.kode-caption .kode-caption-title{';
						$style .= 'background-color:'.esc_attr($university_education_theme_option['caption-background-color']).' !important';
						$style .= '}' . "\r\n";				
					}
				}
					
				if(isset($university_education_theme_option['caption-btn-color-border'])){
					$style .= '.kode-linksection, .kode-modren-btn{';
					$style .= 'border-color:'.esc_attr($university_education_theme_option['caption-btn-color-border']).' !important';
					$style .= '}' . "\r\n";		
				}
				
				if(isset($university_education_theme_option['caption-btn-color-bg'])){
					$style .= '.kode-linksection, .kode-modren-btn{';
					$style .= 'background:'.esc_attr($university_education_theme_option['caption-btn-color-bg']).' !important';
					$style .= '}' . "\r\n";		
				}
				// Slider Caption Styling Ends
				
				if(!empty($university_education_theme_option['header-background-opacity'])){
					if($university_education_theme_option['header-background-opacity'] <> ''){
						$style .= '#header-style-3 .kode_top_strip:before, .kode_header_7 .kode_top_eng:before{';
						$style .= 'opacity:'.esc_attr($university_education_theme_option['header-background-opacity']).' !important';
						$style .= '}' . "\r\n";
					}
				}
				
				if(!empty($university_education_theme_option['header-background-opacity'])){
					if($university_education_theme_option['header-background-opacity'] <> ''){
						$style .= '#header-style-3 .kode_top_strip:before, .kode_header_7 .kode_top_eng:before{';
						$style .= 'opacity:'.esc_attr($university_education_theme_option['header-background-opacity']).' !important';
						$style .= '}' . "\r\n";
					}
				}
				
				
				//Background As Pattern
				if(isset($university_education_theme_option['header-background-image'])){
					if($university_education_theme_option['header-background-image'] <> ''){
						if( !empty($university_education_theme_option['header-background-image']) && is_numeric($university_education_theme_option['header-background-image']) ){
							$alt_text = get_post_meta($university_education_theme_option['header-background-image'] , '_wp_attachment_image_alt', true);	
							$image_src = wp_get_attachment_image_src($university_education_theme_option['header-background-image'], 'full');
							
							$style .= '.kode_header_7 .kode_top_eng, .kode_top_strip{background:url(' . esc_url($image_src[0]).')}';
						}else if( !empty($university_education_theme_option['body-background-pattern']) ){
							$style .= '.kode_header_7 .kode_top_eng, .kode_top_strip{background:url(' . $university_education_theme_option['header-background-image'].')}';
						}
					}
				}
				
				//Background As Pattern
				if(isset($university_education_theme_option['kode-body-style'])){
					if($university_education_theme_option['kode-body-style'] == 'body-pattern'){
						if($university_education_theme_option['body-background-pattern'] <> ''){
							if( !empty($university_education_theme_option['body-background-pattern']) && is_numeric($university_education_theme_option['body-background-pattern']) ){
								// $alt_text = get_post_meta($university_education_theme_option['body-background-pattern'] , '_wp_attachment_image_alt', true);	
								// $image_src = wp_get_attachment_image_src($university_education_theme_option['body-background-image'], 'full');
								$style .= 'body{background:url(' . esc_url(UOE_PATH . '/images/pattern/pattern_'.$university_education_theme_option['body-background-pattern'].'.png') . ')}';
							}else if( !empty($university_education_theme_option['body-background-pattern']) ){
								$style .= 'body{background:url(' . esc_url(UOE_PATH . '/images/pattern/pattern_'.$university_education_theme_option['body-background-pattern'].'.png') . ')}';
							}
						}
					}
				}
				
				//Background As Image
				if(isset($university_education_theme_option['kode-body-style'])){
					if($university_education_theme_option['kode-body-style'] == 'body-background'){
						if($university_education_theme_option['body-background-image'] <> ''){
							if( !empty($university_education_theme_option['body-background-image']) && is_numeric($university_education_theme_option['body-background-image']) ){
								$alt_text = get_post_meta($university_education_theme_option['body-background-image'] , '_wp_attachment_image_alt', true);	
								$image_src = wp_get_attachment_image_src($university_education_theme_option['body-background-image'], 'full');
								$style .= 'body{background:url(' . esc_url($image_src[0]) . ')}';
								if($university_education_theme_option['kode-body-position'] == 'body-scroll'){
									$style .= 'body{background-attachment:scroll !important}';
								}else{
									$style .= 'body{background-attachment:fixed !important}';
								}
							}else if( !empty($university_education_theme_option['body-background-image']) ){
								$style .= 'body{background:url(' . esc_url($university_education_theme_option['body-background-image']) . ')}';
								if($university_education_theme_option['kode-body-position'] == 'body-scroll'){
									$style .= 'body{background-attachment:scroll !important}';
								}else{
									$style .= 'body{background-attachment:fixed !important}';
								}
							}
						}
					}
				}
				
				//Background As Color
				if(isset($university_education_theme_option['body-bg-color'])){
					if($university_education_theme_option['body-bg-color'] <> ''){
						$style .= 'body { ';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['body-bg-color']) . ';  }' . "\r\n";
					}
				}
				

				if(!empty($university_education_theme_option['enable-boxed-style'])){
					if($university_education_theme_option['enable-boxed-style'] == 'boxed-style'){
						if( !empty($university_education_theme_option['boxed-background-image']) && is_numeric($university_education_theme_option['boxed-background-image']) ){
							$alt_text = get_post_meta($university_education_theme_option['boxed-background-image'] , '_wp_attachment_image_alt', true);	
							$image_src = wp_get_attachment_image_src($university_education_theme_option['boxed-background-image'], 'full');
							$style .= 'body{background:url(' . esc_url($image_src[0]) . ')}';
						}else if( !empty($university_education_theme_option['boxed-background-image']) ){
							$style .= 'body{background:url(' . esc_url($university_education_theme_option['boxed-background-image']) . ')}';
						}
						
						$style .= '.logged-in.admin-bar .body-wrapper .kode-header-1{';
						$style .= 'margin-top:0px !important;';
						$style .= '}' . "\r\n";
						$style .= '.body-wrapper .kode-topbar:before{width:25%;}';
						$style .= '.body-wrapper #footer-widget .kode-widget-bg-footer:before{width:23em;}';
						$style .= '.body-wrapper .eccaption{top:40%;}';
						$style .= '.body-wrapper .main-content{background:#fff;}';
						$style .= '.body-wrapper {';
						$style .= 'background:#fff;width: 1200px;overflow:hidden; margin: 0 auto; margin-top: 40px; margin-bottom: 40px; box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.2);position:relative;';
						$style .= '}' . "\r\n";
					}
				}
				
				
				if(isset($university_education_theme_option['footer-background-opacity'])){
					if($university_education_theme_option['footer-background-opacity'] <> ''){
						$style .= 'footer:before{ ';
						$style .= 'opacity: ' . esc_attr($university_education_theme_option['footer-background-opacity']) . ';  }' . "\r\n";
					}
				}
				
				
				
				//Background As Color
				if(isset($university_education_theme_option['footer-background-color'])){
					if($university_education_theme_option['footer-background-color'] <> ''){
						$style .= '.kf_footer_bg{ ';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['footer-background-color'] ). ' !important;  }' . "\r\n";
					}
				}
				
				if(!empty($university_education_theme_option['footer-background-image'])){
					if( !empty($university_education_theme_option['footer-background-image']) && is_numeric($university_education_theme_option['footer-background-image']) ){
						$alt_text = get_post_meta($university_education_theme_option['footer-background-image'] , '_wp_attachment_image_alt', true);	
						$image_src = wp_get_attachment_image_src($university_education_theme_option['footer-background-image'], 'full');
						$style .= '.footer{background:url(' . esc_url($image_src[0]) . ');background-size: cover;float: left;position: relative;width: 100%;}';
						$style .= '.kf_footer_bg:before{background-image:url(' . esc_url($image_src[0]) . ') !important;}';
					}else if( !empty($university_education_theme_option['footer-background-image']) ){
						$style .= '.footer{background:url(' . esc_url($university_education_theme_option['footer-background-image']) . ');background-size: cover;float: left;position: relative;width: 100%;}';
						$style .= '.kf_footer_bg:before{background-image:url(' . esc_url($university_education_theme_option['footer-background-image']) . ') !important;}';
					}
				}
				
				if(isset($university_education_theme_option['logo-bottom-margin'])){
					if($university_education_theme_option['logo-bottom-margin'] <> ''){
						$style .= 'a.logo{';
						$style .= 'margin-bottom: ' . esc_attr($university_education_theme_option['logo-bottom-margin']) . 'px;  }' . "\r\n";
					}
				}
				
				if(isset($university_education_theme_option['logo-top-margin'])){
					if($university_education_theme_option['logo-top-margin'] <> ''){
						$style .= 'a.logo{';
						$style .= 'margin-top: ' . esc_attr($university_education_theme_option['logo-top-margin']) . 'px;  }' . "\r\n";
					}
				}
				
				if(isset($university_education_theme_option['kode-top-bar-trans'])){
					if($university_education_theme_option['kode-top-bar-trans'] == 'colored'){
						if(isset($university_education_theme_option['top-bar-background-color'])){
							if($university_education_theme_option['top-bar-background-color'] <> ''){
								$style .= '.top-strip{';
								$style .= 'background-color: ' . esc_attr($university_education_theme_option['top-bar-background-color']) . ';  }' . "\r\n";
							}
						}
					}
				}
				
				//Top Bar Background Color
				if(isset($university_education_theme_option['top-bar-background'])){
					if($university_education_theme_option['top-bar-background'] <> ''){
						$style .= '.kf_top_bar{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['top-bar-background']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-content'])){
					if($university_education_theme_option['top-bar-content'] <> ''){
						$style .= '.kf_top_bar, .kf_top_bar p, .kf_top_bar a,.kf_opening_time{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['top-bar-content']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Link Color
				if(isset($university_education_theme_option['top-bar-links'])){
					if($university_education_theme_option['top-bar-links'] <> ''){
						$style .= '.kf_top_bar .kf_top_social_icon li a{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['top-bar-links']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-links-hover-color'])){
					if($university_education_theme_option['top-bar-links-hover-color'] <> ''){
						$style .= '.kf_top_bar .kf_top_social_icon li a:hover{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['top-bar-links-hover-color']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-links-bgcolor'])){
					if($university_education_theme_option['top-bar-links-bgcolor'] <> ''){
						$style .= '.kf_top_bar .kf_top_social_icon li a{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['top-bar-links-bgcolor']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-links-hover-bgcolor'])){
					if($university_education_theme_option['top-bar-links-hover-bgcolor'] <> ''){
						$style .= '.kf_top_bar .kf_top_social_icon li a:hover{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['top-bar-links-hover-bgcolor']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-left-submit-btn-text-color'])){
					if($university_education_theme_option['top-bar-left-submit-btn-text-color'] <> ''){
						$style .= '.kf_list_free a{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['top-bar-left-submit-btn-text-color']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-left-submit-btn-bg-color'])){
					if($university_education_theme_option['top-bar-left-submit-btn-bg-color'] <> ''){
						$style .= '.kf_list_free{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['top-bar-left-submit-btn-bg-color']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-left-submit-btn-text-color-hover'])){
					if($university_education_theme_option['top-bar-left-submit-btn-text-color-hover'] <> ''){
						$style .= '.kf_list_free:hover, .kf_list_free:hover a{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['top-bar-left-submit-btn-text-color-hover']) . ';  }' . "\r\n";
					}
				}
				
				//Top Bar Text Color
				if(isset($university_education_theme_option['top-bar-left-submit-btn-text-bg-color-hover'])){
					if($university_education_theme_option['top-bar-left-submit-btn-text-bg-color-hover'] <> ''){
						$style .= '.kf_list_free:hover{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['top-bar-left-submit-btn-text-bg-color-hover']) . ';  }' . "\r\n";
					}
				}
				
				
				
				
				//Sub Header
				if(isset($university_education_theme_option['subheader-background'])){
					if($university_education_theme_option['subheader-background'] <> ''){
						$style .= '.kf_property_sub_banner{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['subheader-background']) . ' !important;  }' . "\r\n";
					}
				}
				
				//Sub Header Title Color
				if(isset($university_education_theme_option['subheader-title-color'])){
					if($university_education_theme_option['subheader-title-color'] <> ''){
						$style .= '.kf_sub_banner_hdg h3{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['subheader-title-color']) . ' !important;  }' . "\r\n";
					}
				}
				
				//Sub Header Title Color
				if(isset($university_education_theme_option['subheader-breadcrumb-bgcolor'])){
					if($university_education_theme_option['subheader-breadcrumb-bgcolor'] <> ''){
						$style .= '.kf_property_breadcrumb ul{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['subheader-breadcrumb-bgcolor']) . ' !important;  }' . "\r\n";
					}
				}
				
				//Sub Header Title Color
				if(isset($university_education_theme_option['subheader-breadcrumb-color'])){
					if($university_education_theme_option['subheader-breadcrumb-color'] <> ''){
						$style .= '.breadcrumb > li + li::before, .kf_property_breadcrumb ul li a, .kf_property_breadcrumb ul li strong{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['subheader-breadcrumb-color']) . ' !important;  }' . "\r\n";
					}
				}
				
				//Sub Header Title Color
				if(isset($university_education_theme_option['subheader-breadcrumb-hover-color'])){
					if($university_education_theme_option['subheader-breadcrumb-hover-color'] <> ''){
						$style .= '.kf_property_breadcrumb ul li a:hover{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['subheader-breadcrumb-hover-color']) . ' !important;  }' . "\r\n";
					}
				}
				
				
				if(isset($university_education_theme_option['nav-area-background-color'])){
					if($university_education_theme_option['nav-area-background-color'] <> ''){
						$style .= '.kode-navigation-wrapper{ ';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['nav-area-background-color']) . ' !important;  }' . "\r\n";
					}
				}
				
				
				if(isset($university_education_theme_option['text-color'])){
					if($university_education_theme_option['text-color'] <> ''){
						$style .= '.kode-blog-content p{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['text-color']) . ' !important;  }' . "\r\n";
					}
				}
				
				
				
				if(isset($university_education_theme_option['color-scheme-one'])){
					if(empty($university_education_theme_option['color-scheme-one'])){
						$university_education_theme_option['color-scheme-one'] = '#03AF14';
					}
				}
				
				
				if(isset($university_education_theme_option['color-scheme-one'])){
					if($university_education_theme_option['color-scheme-one'] <> ''){
						
						$style .= '
						.widget-skill .widget-radio-btn input[type="radio"]:checked ~ label,
						.kf_grid-column:hover .kf_edu_grid_captions > h5,
						.social-icons li:hover a,
						.comments_section_02 .rateing li span,
						.your-rateing_btn input[type="radio"]:checked ~ label span,
						.your-rateing_btn input[type="radio"]:checked ~ label b,
						.course_tabs_contents ul li:hover a,
						.comment-rateing ul li span,
						.blog_3_des h5:hover a,
						.about-autor_des h6 a:hover,
						.detail_wrap_final .edu2_cur_ftr_strip h6:hover a,
						.edu2_blogpg_des h5 a:hover,
						.ue_style_widget .caregorys li a:hover i,
						.ue_style_widget .caregorys li a:hover,
						.kf_blog_social_icon a:hover,
						.kf_blog_detail_tag a:hover,
						.close:hover, .close:focus,
						.woocommerce #respond input#submit, 
						.woocommerce a.button,
						.woocommerce ul.products li.product .price,
						.woocommerce div.product form.cart .button,
						.product_meta .tagged_as a, .product_meta a:hover,
						.woocommerce div.product p.price, .woocommerce div.product span.price,
						.price del, .price ins,
						.woocommerce button.button,
						.woocommerce p.stars a:hover,
						.woocommerce .woocommerce-message:before,
						.woocommerce .woocommerce-error:before, .woocommerce .woocommerce-info:before,
						.woocommerce .woocommerce-message:before,
						.books-listing-4:hover a.add-to-cart,
						.woocommerce .cart .button, .woocommerce .cart input.button,
						.woocommerce .coupon .submit,
						.woocommerce a.remove,
						.woocommerce input.button,
						.comment-reply-link:hover,
						.widget_archive ul li:hover,
						.nav_des li a:hover, .kf_courses_wrap:hover .courses_des_hding1 h5, .kf_courses_des > span > small, .kf_inr_breadcrumb ul li > a:hover, .kf_intro_des:hover .kf_intro_des_caption a, .kf_intro_des:hover span, .kf_intro_des:hover h6, .kf_edu2_tab_wrap .nav-tabs > li > a:hover, .kf_edu2_tab_wrap .nav-tabs .active a, .edu2_cur_wrap:hover .edu2_cur_des > h5 a, .edu2_cur_wrap:hover .edu2_cur_des > span, .kf_edu2_tab_des .browse_all, .kf_edu2_tab_des .customNavigation .btn:hover, .edu2_gallery_des .active, .edu2_gallery_des a:hover, .edu2_gallery_des > figure > figcaption > a, .loadmore a, .edu2_faculty_des2 h6 a, .edu2_faculty_des figcaption a, .edu2_event_des h4, .edu2_event_des > a, .post-option > li > a:hover, .edu2_pricing_des ul li a:hover, .edu2_pricing_des:hover .subscribe, .edu_testemonial_wrap > a span, .rpost_thumb_des span i, .sidebar_archive_des > li a:hover, .sidebar_rpost_des li:hover h6 a, .sdbar_categories_des > li a:hover,  .edu2_techer_des h6 a, .edu2_techer_wrap figcaption a,  .edu2_blogpg_des ul li a:hover, .edu2_blogpg_des:hover ul li i, .edu2_blogpg_des .blog-readmore,  .abt_courses_des:hover i, .abt_courses_des:hover h6, .abt_courses_des a:hover, .std_name_des > a:hover,  .kf_event_list_des h4 a, .kf_event_list_links li i,  .edu2_col_4_des span,  .video_link_wrap a, .edu2_col_3_ftr a:hover,  .blog_3_des ul li a,  .edu2_footer_contant_des > ul > li > a:hover, .readmore, .readmore:hover,  .teacher_bio_des ul li i, .kf_training_wrap ul li:after, .kf_training_date, .kf_training_des span, .kf_training_des h6 a:hover,  .kf_convocation_wrap h4 span, .heading_5 h4 span, .kf_event_speakers_des h5 a, .blog_detail_meta li i, .comment_reply:hover, .autor_social li a:hover, .blog_detail_meta li a:hover, .kf_courses_tabs .nav-tabs > li.active > a, .course_detail_meta li a:hover, .course_detail_meta li:hover, .teacher_des small, .teacher_meta li a:hover, .inputs_des span i, .contact_meta li i, .location_des > h6, .contact_meta li a:hover, .location_meta li i, .location_meta li a:hover, .location_des > a:hover, .location_des > a:hover i, .contact_2_headung h3,  .error_des span, .error_des h3, .btn-3, #filterable-item-filter-1 > li .active, .kf_edu2_heading1 h5, .widget-links ul li a:hover:before, .widget-links ul li a:hover
						{';
						$style .= 'color: ' . esc_attr($university_education_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";

						$style .= '
						/*
							  ============================================================
								   Background Color
							  ============================================================
						*/
						.search-menu button,
						.filter_priceing,
						.tabs-controls ul li.active a,
						.tabs-controls ul li.active a:focus,
						.tabs-controls ul li.active a:hover,
						.kf_grid-column:hover > figure:before,
						.kf_edu_grid_captions .rating,
						.btn_edu,
						.title-table,
						.contact_des > p input[type="submit"],
						.kode_btn_store_1:hover,
						.rateing_box,
						.contact_2 input[type="submit"],
						.price-velue-btn,
						.widget.bg-color h5,
						.brake-time-view li:last-child:before,
						.widget-radio-btn > span:before,
						.blog_pg_form button,
						.nav_2 .dl-menuwrapper button,
						.nav_2 .dl-menuwrapper .dl-menu,
						.edu2_faculty_des:hover figure,
						.edu2_faculty_des:hover figure:before,
						.nav_2 .dl-menuwrapper .dl-menu .dl-submenu,
						.kf_edu2_search_wrap p input[type="submit"],
						.c-button:hover,
						.edu2_faculty_des figure:before,
						.edu2_testemonial_slider_wrap .owl-dots.active span,
						.edu2_faculty_des:hover figure:before,
						.edu2_testemonial_slider_wrap  .owl-dots .active span,
						.kf_edu2_logo, .kf_topbar_wrap, .nav_des > li:before, .nav_des a:after, .nav_des a:before,  .btn-1:hover,  .kf_edu2_search_wrap form > button, .kf_edu2_search_wrap .dropdown-menu > li > a:focus, .kf_edu2_search_wrap .dropdown-menu > li > a:hover, .kf_intro_des figure:before, .kf_edu2_tab_wrap .nav-tabs > li > a:hover:before, .kf_edu2_tab_wrap .nav-tabs > li.active > a:before, .kf_edu2_tab_wrap .nav-tabs > li.active > a:focus:before, .kf_edu2_tab_wrap .nav-tabs > li.active > a:hover:before, figure:before, .edu2_pricing_des:hover span, .edu_testemonial_wrap:before, .edu2_testemonial_slider_wrap .owl-page.active,  .kf_courses_wrap figure:before, .kf_courses_des > a:hover, .sdb_tag_cloud_des li a:hover,  .edu2_blogpg_des .blog-readmore:hover, .kf_edu_pagination_wrap .pagination > .active > a, .kf_edu_pagination_wrap .pagination > .active > a:focus, .kf_edu_pagination_wrap .pagination > .active > a:hover, .kf_edu_pagination_wrap .pagination > .active > span, .kf_edu_pagination_wrap .pagination > .active > span:focus, .kf_edu_pagination_wrap .pagination > .active > span:hover, .btn-6:hover, .inr_pg_search_wrap, .edu2_col_3_wrap figure figcaption a:hover, .blog_3_wrap .blog_3_sidebar li:hover, .edu2_ft_topbar_wrap, .edu2_col_4_wrap:hover .btn-2, .teacher_bio_logo span, .teacher_thumb figcaption a, .progress_heading:before, .bar, .training_heading:before, .kf_training_outer_des:hover .kf_training_date > span, .kf_convocation_des a, .convocation_link, .kf_event speakers h5, .event_link:hover, .blog_detail_thumbnail figure figcaption a, .blog_pg_form form button, .course_detail_thumbnail figure figcaption a, .kf_courses_tabs .nav-tabs > li > a::before, .apply, .teacher_outer_wrap span,  .error_thumb figure a:before, .contact_des button:hover, .filterable_heading .dropdown-menu > li > a:hover, .filterable_thumb figure:before, .edu_masonery_thumb figure figcaption, #filterable-item-filter-1 a, .pagination > li > a:hover, #header_2 .top_bar_2, #header_2 .nav_2 ul li a:before, #header_2 .top_nav li a:before, .nav_2 ul ul li a:hover, .edu2_navigation ul ul li a:hover, .kf_topbar_wrap .pull-right button, .contact_des button, .contact_2 button, .selectric-items li.selected, .selectric-items li:hover, .get-admition, 
						.widget-categories ul li a:hover,
						.view-all .btn-3:hover,	
						.comment-form .form-submit .submit,
						.inner-post:hover,
						.woocommerce #respond input#submit:hover,
						.woocommerce a.button:hover,
						.woocommerce button.button:hover,
						.woocommerce span.onsale,
						.woocommerce input.button:hover,
						.contact_des .des_2 > input[type="submit"],
						.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
						.kode-pagination .page-numbers.current,
						.woocommerce div.product form.cart .button:hover,
						.kode-pagination .page-numbers:hover,
						.woocommerce a.remove:hover,
						.books-listing-4 .book-price,
						.widget-courses-list figure a, .widget-tag-cloud ul li a:hover, #mobile-header, .sidr ul li:hover > a, .sidr ul li:hover > span, .sidr ul li.active > a, .sidr ul li.active > span, .sidr ul li.sidr-class-active > a, .sidr ul li.sidr-class-active > span, 
						.btn-style, .cont_socil_meta li a:hover, .edu_masonery_thumb .caption,.abt_univ_des .btn-3:hover,
						.lng_wrap button,
						.kode-search label input,
						.kode-center-content-tf figure::before,
						.kode-search label:hover input,
						.widget_pages ul li:hover,
						.kode-search label{';
						$style .= 'background-color: ' . esc_attr($university_education_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						
						$style .= '
						/*
						============================================================
						Border Color
						============================================================
						*/
						.kode_btn_store_1:hover,
						.view-all .btn-3:hover,
						.kode-pagination .page-numbers.current,
						.kode-pagination .page-numbers:hover,
						.kf_grid-column > figure,
						.grid-option_3:before,
						.social-icons li:hover a,
						.woocommerce #respond input#submit, 
						.woocommerce a.button,
						.woocommerce button.button,
						.woocommerce input.button,
						.woocommerce-message,
						.flex-control-paging li a.flex-active,
						.woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce,
						.woocommerce-message,
						.course_tabs_contents ul li a:before,						
						.nav_des li,  .btn-1:hover, .edu2_serc_des input[type="text"]:focus, .edu2_serc_des .btn.btn-default.dropdown-toggle:focus, .kf_courses_des > a:hover, .kf_edu2_tab_des .browse_all, .kf_edu2_tab_des .customNavigation .btn:hover, .loadmore a, .edu2_faculty_des:hover .edu2_faculty_des2, .sdb_tag_cloud_des li a:hover, .kf_sidebar_srch_wrap form > input[type="search"]:focus,  .edu2_techer_wrap:hover .edu2_techer_des,  .edu2_blogpg_des .blog-readmore,  .edu2_col_4_wrap:hover .edu2_col_4_des figure, .btn-6:hover, .teacher_bio_logo, .teacher_thumb figure:after, .kf_training_date, .convocation_timing, .event_link:hover, .kf_blog_detail_des blockquote, .blog_pg_form textarea:focus, .blog_pg_form input[type="e-mail"]:focus, .blog_pg_form input[type="text"]:focus, /*course detail start*/ .teacher_meta li a:hover,  .cont_socil_meta li a:hover, .error_thumb, .error_thumb figure a, input[type="text"]:focus, input[type="email"]:focus, textarea:focus, .btn-3, #filterable-item-filter-1 a, #filterable-item-filter-1 > li .active, .pagination > li > a:hover, .owl-page.active > span, .widget-search input[type="search"]:focus, .widget-tag-cloud ul li a:hover 
						{';
						$style .= 'border-color: ' . esc_attr($university_education_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
					}						
				}
						
						
						
						$style .= '.header_3 .kode_top_bar_3{';
						$style .= 'border-top-color: ' . esc_attr($university_education_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						
						$style .= '.kf_login_colum{';
						$style .= 'border-bottom-color: ' . esc_attr($university_education_theme_option['color-scheme-one']) . ' !important;  }' . "\r\n";
						
						$style .= '.kode_location_property_4{';
						$style .= 'border-bottom-color: #fff !important;  }' . "\r\n";
						
						$style .= '.kf_login_1::before{';
						$style .= 'border-color: ' . esc_attr($university_education_theme_option['color-scheme-one']) . ' transparent transparent !important;  }' . "\r\n";
						
						
						if(isset($university_education_theme_option['color-scheme-one']) && $university_education_theme_option['color-scheme-one'] <> ''){
							$style .= '.kode_pet_navigation ul.nav li a.current{';
							$style .= 'border-top-color: ' . esc_attr($university_education_theme_option['color-scheme-one'] ). ' !important;  }' . "\r\n";
						}
						
						if(isset($settings['caption-btn-color-switch']) && $settings['caption-btn-color-switch'] == 'enable'){
							if(isset($university_education_theme_option['caption-btn-color-hover']) && $university_education_theme_option['caption-btn-color-hover'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover{';
								$style .= 'color: ' . esc_attr($university_education_theme_option['caption-btn-color-hover']) . ' !important;  }' . "\r\n";
							}
							if(isset($university_education_theme_option['caption-btn-color']) && $university_education_theme_option['caption-btn-color'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out{';
								$style .= 'color: ' . esc_attr($university_education_theme_option['caption-btn-color']) . ' !important;  }' . "\r\n";
							}
							
							if(isset($university_education_theme_option['caption-btn-color-border']) && $university_education_theme_option['caption-btn-color-border'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out{';
								$style .= 'outline-color: ' . esc_attr($university_education_theme_option['caption-btn-color-border']) . ' !important;  }' . "\r\n";
							}
							
							if(isset($university_education_theme_option['caption-btn-color-bg']) && $university_education_theme_option['caption-btn-color-bg'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out{';
								$style .= 'background-color: ' . esc_attr($university_education_theme_option['caption-btn-color-bg']) . ' !important;  }' . "\r\n";
							}
							
							if(isset($university_education_theme_option['caption-btn-hover-bg']) && $university_education_theme_option['caption-btn-hover-bg'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover{';
								$style .= 'background-color: ' . esc_attr($university_education_theme_option['caption-btn-hover-bg']) . ' !important;  }' . "\r\n";		
							}

							if(isset($university_education_theme_option['caption-btn-arrow-color']) && $university_education_theme_option['caption-btn-arrow-color'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out i{';
								$style .= 'color: ' . esc_attr($university_education_theme_option['caption-btn-arrow-color']) . ' !important;  }' . "\r\n";		
							}
							
							if(isset($university_education_theme_option['caption-btn-arrow-hover']) && $university_education_theme_option['caption-btn-arrow-hover'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover i{';
								$style .= 'color: ' . esc_attr($university_education_theme_option['caption-btn-arrow-hover']) . ' !important;  }' . "\r\n";		
							}
							
							if(isset($university_education_theme_option['bx-arrow']) && $university_education_theme_option['bx-arrow'] == 'disable'){
								$style .= '.kode-item.kode-slider-item .kode-bxslider .bx-controls-direction{';
								$style .= 'display:none !important;  }' . "\r\n";		
							}
							
							if(isset($university_education_theme_option['caption-btn-arrow-bg']) && $university_education_theme_option['caption-btn-arrow-bg'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out:before{';
								$style .= 'background-color: ' . esc_attr($university_education_theme_option['caption-btn-arrow-bg']) . ' !important;  }' . "\r\n";		
							}
							
							if(isset($university_education_theme_option['caption-btn-arrow-hover-bg']) && $university_education_theme_option['caption-btn-arrow-hover-bg'] <> ''){
								$style .= '.kode_link_1.kode-linksection.hvr-radial-out:hover:before{';
								$style .= 'background-color: ' . esc_attr($university_education_theme_option['caption-btn-arrow-hover-bg']) . ' !important;  }' . "\r\n";	
							}
						}
						if(isset($university_education_theme_option['sidebar-tbn']) && $university_education_theme_option['sidebar-tbn'] == 'enable'){
							if(isset($university_education_theme_option['sidebar-bg-color'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'background-color: ' . esc_attr($university_education_theme_option['sidebar-bg-color']) . ' !important;float:left;width:100%;}' . "\r\n";
							}
							
							if(isset($university_education_theme_option['sidebar-padding-top'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-top: ' . esc_attr($university_education_theme_option['sidebar-padding-top']) . 'px !important;}' . "\r\n";
							}
							if(isset($university_education_theme_option['sidebar-padding-bottom'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-bottom: ' . esc_attr($university_education_theme_option['sidebar-padding-bottom']) . 'px !important;}' . "\r\n";
							}
							if(isset($university_education_theme_option['sidebar-padding-left'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-left: ' . esc_attr($university_education_theme_option['sidebar-padding-left']) . 'px !important;}' . "\r\n";
							}
							if(isset($university_education_theme_option['sidebar-padding-right'])){
								$style .= '
								.kode-sidebar.kode-left-sidebar,
								.kode-sidebar.kode-right-sidebar,
								.kode-widget.kode-sidebar-element{';
								$style .= 'padding-right: ' . esc_attr($university_education_theme_option['sidebar-padding-right']) . 'px !important;}' . "\r\n";
							}
						}
						if(isset($university_education_theme_option['woo-post-title']) && $university_education_theme_option['woo-post-title'] == 'disable'){
							$style .= '.woocommerce-content-item .product .product_title.entry-title{ display:none;}';							
						}
						
						if(isset($university_education_theme_option['woo-post-price']) && $university_education_theme_option['woo-post-price'] == 'disable'){
							$style .= '.woocommerce-content-item div[itemprop="offers"]{ display:none;}';
						}
						
						if(isset($university_education_theme_option['woo-post-variable-price']) && $university_education_theme_option['woo-post-price'] == 'disable'){
							$style .= '.woocommerce-content-item div[itemprop="offers"]{ display:none;}';
						}
						
						if(isset($university_education_theme_option['woo-post-related']) && $university_education_theme_option['woo-post-related'] == 'disable'){
							$style .= '.woocommerce-content-item .product .related.products{ display:none;}';
						}
						
						if(isset($university_education_theme_option['woo-post-sku']) && $university_education_theme_option['woo-post-sku'] == 'disable'){
							$style .= '.woocommerce-content-item .product .sku_wrapper{ display:none;}';
						}
						
						if(isset($university_education_theme_option['woo-post-category']) && $university_education_theme_option['woo-post-category'] == 'disable'){
							$style .= '.woocommerce-content-item .product .posted_in{ display:none; !important;}';
						}
						
						if(isset($university_education_theme_option['woo-post-tags']) && $university_education_theme_option['woo-post-tags'] == 'disable'){
							$style .= '.woocommerce-content-item .product .tagged_as{ display:none !important;}';
						}
						
						if(isset($university_education_theme_option['woo-post-outofstock']) && $university_education_theme_option['woo-post-outofstock'] == 'disable'){
							
						}
						
						if(isset($university_education_theme_option['woo-post-saleicon']) && $university_education_theme_option['woo-post-saleicon'] == 'disable'){
							$style .= '.woocommerce-content-item .product .onsale{ display:none;}';
						}

					
				return $style;
				
			}
		}