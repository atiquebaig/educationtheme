<?php
	/*	
	*	Kodeforest Woo Option file
	*	---------------------------------------------------------------------
	*	This file creates all post options to the post page
	*	---------------------------------------------------------------------
	*/
	
	//Woo Listing
	if( !function_exists('university_education_get_woo_item_slider') ){
		function university_education_get_woo_item_slider( $settings ){
			
			// query posts section
			$args = array('post_type' => 'product', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : 1;
			
			
			
			$settings['woo-column-size'] = (empty($settings['woo-column-size']))? '3': $settings['woo-column-size'];
			//$settings['woo-style'];
			if( !empty($settings['category']) || (!empty($settings['tag'])) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'product_cat', 'field'=>'slug'));
				}
				// if( !empty($settings['tag'])){
					// array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'product_tag', 'field'=>'slug'));
				// }
			}			
			$query = new WP_Query( $args );

			// create the woo filter
			
			$settings['num-title-fetch'] = empty($settings['num-title-fetch'])? '25': $settings['num-title-fetch'];
			
			$size = 4;
			$university_education_woo_new  = '<div class="kode-best-slider">';
			$university_education_woo_new .= '<h2>New Product</h2>';
            $university_education_woo_new .= '<ul class="bxslider" data-min="2" data-max="2" data-mode="vertical" data-margin="10" >';
                        	
			$current_size = 0;
			while($query->have_posts()){
				$query->the_post();
				global $university_education_post_option,$post,$university_education_post_settings,$product;	
				$university_education_option = json_decode(university_education_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true)), true);
				$prices_precision = wc_get_price_decimals();
				$price = wc_format_decimal( $product->get_price(), $prices_precision );
				$price_regular = wc_format_decimal( $product->get_regular_price(), $prices_precision );
				$price_sale = $product->get_sale_price() ? floatval(wc_format_decimal( $product->get_sale_price(), $prices_precision )) : null;
				if(empty($price_sale)){
					$price_sale = $price_regular;
				}
				$rating_count = $product->get_rating_count();
				$review_count = $product->get_review_count();
				$average      = $product->get_average_rating();
				$rating_html      = $product->get_rating_html();
				
				// if( $current_size % $size == 0 ){
					// $woo .= '<li class="clear"></li>';
				// }	
				$university_education_woo_new .= '
				<li>				 
				<div class="kode-best-pro-seller">
					<div class="kode-thumb">
						<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, 'uoe-post-thumbnail-size').'</a>
					</div>
					<div class="kode-text">
						<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,18)).'</a></h4>
						<p>Creative Living</p>
						<div class="rating">
							'.$rating_html.'
						</div>
						<p class="price">$'.esc_attr($price_sale).'</p>
						<div class="clear"></div>';
						$university_education_woo_new .= apply_filters( 'woocommerce_loop_add_to_cart_link',
							sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="add-to-cart %s product_type_%s"><i class="fa fa-shopping-cart"></i> '.esc_html__('Add To Cart','university-education').'</a>',
								esc_url( $product->add_to_cart_url() ),
								esc_attr( $product->id ),
								esc_attr( $product->get_sku() ),
								$product->is_purchasable() ? 'add_to_cart_button' : '',
								esc_attr( $product->product_type ),
								esc_html( $product->add_to_cart_text() )
							),
						$product ); 
						$university_education_woo_new .= '						
					</div>
				</div>
				</li>';
			
				$current_size++;
			}

			$university_education_woo_new .= '</ul></div>';
			
			return $university_education_woo_new;
		}
	}
	
	
	
	//Woo Listing
	if( !function_exists('university_education_get_woo_item_tab_slider') ){
		function university_education_get_woo_item_tab_slider( $settings ){
			
			// query posts section
			$args = array('post_type' => 'product', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '6': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];			
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : 1;
						
			$settings['woo-column-size'] = (empty($settings['woo-column-size']))? '3': $settings['woo-column-size'];			
			// $settings['num-fetch'] = (empty($settings['num-fetch']))? '6': $settings['num-fetch'];
			$settings['num-desc-fetch'] = (empty($settings['num-desc-fetch']))? 250: $settings['num-desc-fetch'];
			$settings['num-title-fetch'] = empty($settings['num-title-fetch'])? '15': $settings['num-title-fetch'];
			//$settings['woo-style'];
			if( !empty($settings['category']) || (!empty($settings['tag'])) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'product_cat', 'field'=>'slug'));
				}
				if( !empty($settings['tag'])){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'product_tag', 'field'=>'slug'));
				}
			}			
			$query = new WP_Query( $args );
			
			
			$size = 4;
			
			
			$current_size = 0;
			$item_class_new = '';
			$university_education_woo = '<div class="lib-papular-books">';
			$university_education_woo .= '<ul class="nav nav-tabs" role="tablist">';
			while($query->have_posts()){ 
				$query->the_post();
				global $university_education_post_option,$post,$university_education_post_settings,$product;	
				$university_education_option = json_decode(university_education_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true)), true);
				$prices_precision = wc_get_price_decimals();
				$price = wc_format_decimal( $product->get_price(), $prices_precision );
				$price_regular = wc_format_decimal( $product->get_regular_price(), $prices_precision );
				$price_sale = $product->get_sale_price() ? floatval(wc_format_decimal( $product->get_sale_price(), $prices_precision )) : null;
				if(empty($price_sale)){
					$price_sale = $price_regular;
				}
				$rating_count = $product->get_rating_count();
				$review_count = $product->get_review_count();
				$average      = $product->get_average_rating();
				$rating_html      = $product->get_rating_html();
				// if( $current_size % $size == 0 ){
					// $woo .= '<li class="clear"></li>';
				// }	
				if($current_size == 0){$item_class_new	=	'active';}else{$item_class_new='';}
				$university_education_woo .= '
				<li role="presentation" class="col-md-4 col-sm-3 '.esc_attr($item_class_new).'">
					<a href="#shop-'.esc_attr($post->ID).'" role="tab" data-toggle="tab">
						<div class="lib-papular-thumb">
							'.get_the_post_thumbnail($post->ID, 'uoe-small-serv-size').'
						</div>
					</a>
				</li>';
			
				$current_size++;
			}

			$university_education_woo .= '</ul>
			<div class="tab-content">';
			$current_size = 0;		
			$item_class	= '';
			while($query->have_posts()){ 
				$query->the_post();
				global $university_education_post_option,$post,$university_education_post_settings,$product;	
				$university_education_option = json_decode(university_education_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true)), true);
				$prices_precision = wc_get_price_decimals();
				$price = wc_format_decimal( $product->get_price(), $prices_precision );
				$price_regular = wc_format_decimal( $product->get_regular_price(), $prices_precision );
				$price_sale = $product->get_sale_price() ? floatval(wc_format_decimal( $product->get_sale_price(), $prices_precision )) : null;
				if(empty($price_sale)){
					$price_sale = $price_regular;
				}
				$rating_count = $product->get_rating_count();
				$review_count = $product->get_review_count();
				$average      = $product->get_average_rating();
				// if( $current_size % $size == 0 ){
					// $woo .= '<li class="clear"></li>';
				// }	
				if($current_size == 0){$item_class	=	'active in';}else{$item_class='';}
				$university_education_woo .= '<div role="tabpanel" class="tab-pane fade '.esc_attr($item_class).'" id="shop-'.esc_attr($post->ID).'">
					<div class="lib-papular">
						<div class="kode-thumb">
							'.get_the_post_thumbnail($post->ID, 'uoe-post-thumbnail-size').'
						</div>
						<div class="kode-text">
							<h2>'.esc_attr(substr(get_the_title(),0,$settings['num-title-fetch'])).'</h2>
							<h4>John M Fluid</h4>
							<div class="rating">
								'.$rating_html.'
							</div>
							<p>'.substr(get_the_content(),0,$settings['num-desc-fetch']).'</p>
							<div class="lib-price">
								<h3>$'.esc_attr($price_sale).'</h3>
								<a href="'.esc_url(get_permalink()).'">See More</a>
							</div>
						</div>
					</div>
				</div>';
				$current_size++;
			}
			
			$university_education_woo .= '</div>';
			$university_education_woo .= '</div>';
			
			return $university_education_woo;
		}
	}
	
	

	if( !function_exists('university_education_get_woo_item') ){
		function university_education_get_woo_item($settings = array()){
			global $university_education_counter;
			$args = array('post_type' => 'product', 'suppress_filters' => false);
			if($settings['woo-layout'] == 'simple-layout'){
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			}else{
				$args['posts_per_page'] = -1;
			}
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : 1;
			
			$settings['woo-column-size'] = (empty($settings['woo-column-size']))? '3': $settings['woo-column-size'];
			//$settings['woo-style'];
			if( !empty($settings['category']) || (!empty($settings['tag'])) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'product_cat', 'field'=>'slug'));
				}
				// if( !empty($settings['tag'])){
					// array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'product_tag', 'field'=>'slug'));
				// }
			}			
			$query = new WP_Query( $args );
			$university_education_woo = '<div class="col-md-12">';
			$size = 3;
				$size = $settings['woo-column-size'];
				if($settings['woo-layout'] == 'simple-layout'){
					$university_education_woo  .= '<ul class="portfolio-section row '.esc_attr($settings['woo-padding']).'">';
				}else{
					$padding_sec = '';
					if(isset($settings['woo-padding']) && $settings['woo-padding'] == 'with-padding'){
						$padding_sec = '10';	
					}else{
						$padding_sec = '0';
					}
					$university_education_woo  .= '<ul class="bxslider kode-woo-com" data-move="'.esc_attr($settings['woo-move']).'" data-min="2" data-width="'.esc_attr($settings['woo-width']).'" data-max="'.esc_attr($settings['woo-column-size']).'" data-margin="'.esc_attr($padding_sec).'" data-mode="'.esc_attr($settings['woo-mode']).'" >';
				}
			
			$current_size = 0;
			while($query->have_posts()){ $query->the_post();
				global $university_education_post_option,$post,$university_education_post_settings,$product,$university_education_theme_option;
				// if( $current_size % $size == 0 ){
					// $university_education_woo .= '<li class="clear"></li>';
				// }
				
				// print_r($product);
				$university_education_option = json_decode(university_education_decode_stopbackslashes(get_post_meta($post->ID, 'post-option', true)), true);
				$prices_precision = wc_get_price_decimals();
				$price = wc_format_decimal( $product->get_price(), $prices_precision );
				$price_regular = wc_format_decimal( $product->get_regular_price(), $prices_precision );
				$price_sale = $product->get_sale_price() ? floatval(wc_format_decimal( $product->get_sale_price(), $prices_precision )) : null;
				if(empty($price_sale)){
					$price_sale = $price_regular;
				}
				$rating_count = $product->get_rating_count();
				$review_count = $product->get_review_count();
				$average      = $product->get_average_rating();
				$rating_html      = $product->get_rating_html();
				$university_education_theme_option['woo-list-cart-btn'];
				$university_education_theme_option['woo-list-price'];
				$university_education_theme_option['woo-list-rating'];
				$university_education_theme_option['woo-list-title'];
					
					if($settings['woo-layout'] == 'simple-layout'){
						if( $current_size % $size == 0 ){
							$university_education_woo .= '<li class="clear"></li>';
						}
						$university_education_woo .= '<li class="col-sm-6 ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
					}else{
						$university_education_woo .= '<li style="margin-right:'.esc_attr($padding_sec).'px" class="kode-'.esc_attr($settings['woo-mode']).'">';
					}	
				if($settings['woo-style'] == 'woo-style-1'){
					$university_education_woo .= '
						<div class="books-listing-4">
							<div class="kode-thumb">
								<a class="shop-list-thumb" href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
							</div>
							<div class="kode-text">
								<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h3>
								<p>'.esc_attr(substr(get_the_content(),0,$settings['num-excerpt'])).'</p>
							</div>
							<div class="book-price">';
							if(isset($university_education_theme_option['woo-list-price']) && $university_education_theme_option['woo-list-price'] == 'enable'){
								$university_education_woo .= '<p>$'.esc_attr($price_sale).'</p>';
							}	
							$university_education_woo .= '
								<div class="rating">
									'.$rating_html.'
								</div>
							</div>';
							if(isset($university_education_theme_option['woo-list-cart-btn']) && $university_education_theme_option['woo-list-cart-btn'] == 'enable'){
								$university_education_woo .= apply_filters( 'woocommerce_loop_add_to_cart_link',
									sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="add-to-cart %s product_type_%s">'.esc_html__('Add To Cart','university-education').'</a>',
										esc_url( $product->add_to_cart_url() ),
										esc_attr( $product->id ),
										esc_attr( $product->get_sku() ),
										$product->is_purchasable() ? 'add_to_cart_button' : '',
										esc_attr( $product->product_type ),
										esc_html( $product->add_to_cart_text() )
									),
								$product ); 									
							}
							$university_education_woo .= '							
						</div>';
				}else if($settings['woo-style'] == 'woo-style-2'){
					$university_education_woo .= '
						<div class="best-seller-pro">
							<figure>
								<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
							</figure>
							<div class="kode-text">
								<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h3>
							</div>
							<div class="kode-caption">
								<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h3>
								<div class="rating">
									'.$rating_html.'
								</div>
								<p>'.esc_attr(substr(get_the_content(),0,$settings['num-excerpt'])).'</p>
								<p class="price">$'.esc_attr($price_sale).'</p>';
								if(isset($university_education_theme_option['woo-list-cart-btn']) && $university_education_theme_option['woo-list-cart-btn'] == 'enable'){
									$university_education_woo .= apply_filters( 'woocommerce_loop_add_to_cart_link',
										sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="add-to-cart %s product_type_%s">'.esc_html__('Add To Cart','university-education').'</a>',
											esc_url( $product->add_to_cart_url() ),
											esc_attr( $product->id ),
											esc_attr( $product->get_sku() ),
											$product->is_purchasable() ? 'add_to_cart_button' : '',
											esc_attr( $product->product_type ),
											esc_html( $product->add_to_cart_text() )
										),
									$product ); 									
								}
								$university_education_woo .= '
							</div>
						</div>					
					<!--PRODUCT LIST ITEM END-->';
				}else if($settings['woo-style'] == 'woo-style-3'){
					$university_education_woo .= '					
						<div class="books-listing-3">
							<div class="kode-thumb">
								<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
							</div>
							<div class="kode-text">
								<p class="price">$'.esc_attr($price_sale).'<span>'.esc_attr($price_regular).'$</span></p>
								<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h3>';
								$university_education_woo .= '
								<div class="kode-caption">
									<p>'.esc_attr(substr(get_the_content(),0,$settings['num-excerpt'])).'</p>
									<a href="'.esc_url(get_permalink()).'"><i class="fa fa-heart"></i></a>
									<a href="'.esc_url(get_permalink()).'"><i class="fa fa-cart-plus"></i></a>';
									if(isset($university_education_theme_option['woo-list-cart-btn']) && $university_education_theme_option['woo-list-cart-btn'] == 'enable'){
										$university_education_woo .= apply_filters( 'woocommerce_loop_add_to_cart_link',
											sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="%s product_type_%s"><i class="fa fa-cart-plus"></i></a>',
												esc_url( $product->add_to_cart_url() ),
												esc_attr( $product->id ),
												esc_attr( $product->get_sku() ),
												$product->is_purchasable() ? 'add_to_cart_button' : '',
												esc_attr( $product->product_type ),
												esc_html( $product->add_to_cart_text() )
											),
										$product ); 									
									}
								$university_education_woo .= '	
								</div>
							</div>
						</div>';
				}else if($settings['woo-style'] == 'woo-style-4'){
					$university_education_woo .= '
							<div class="book-released">
								<div class="kode-thumb">
									<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, $settings['thumbnail-size']).'</a>
									<div class="cart-btns">
										<a title="" data-toggle="tooltip" href="#" data-original-title="582"><i class="fa fa-eye"></i></a>';
										$university_education_woo .= apply_filters( 'woocommerce_loop_add_to_cart_link',
											sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="cart button %s product_type_%s"><i class="fa fa-shopping-cart"></i></a>',
												esc_url( $product->add_to_cart_url() ),
												esc_attr( $product->id ),
												esc_attr( $product->get_sku() ),
												$product->is_purchasable() ? 'add_to_cart_button' : '',
												esc_attr( $product->product_type ),
												esc_html( $product->add_to_cart_text() )
											),
										$product );		
										$university_education_woo .= '
										<a title="" data-toggle="tooltip" href="#" data-original-title="Add To Wishlist"><i class="fa fa-heart-o"></i></a>
									</div>
								</div>
								<div class="kode-text">
									<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h3>
									<div class="rating">
										'.$rating_html.'
									</div>
								</div>
							</div>';
				}else if($settings['woo-style'] == 'woo-style-5'){
					$university_education_woo .= '<li class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">
							<div class="kf-products ">
								<div class="kode-ux">
									<div class="thumb">
										<div class="new-tag">
											'.esc_html__('New','university-education').'
										</div>
										'.get_the_post_thumbnail($post->ID, array(350,350)).'
										<div class="caption">
											<p>'.esc_attr(substr(get_the_content(),0,25)).'</p>
											<h3>Discount Upto</h3>
											<h3>50%</h3>
											<a class="zoom" href="'.esc_url(get_permalink()).'"><i class="fa fa-search"></i></a>
										</div>
									</div>
									<div class="text">
										<div class="kf-price">
											<div>
												<i class="fa fa-shopping-cart"></i>
											   $'.esc_attr($price_sale).'
											</div>
										</div>
										<h3><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h3>
										<p>'.esc_attr(substr(get_the_content(),0,10)).'</p>
									</div>
								</div>	
							</div>
						</li>';
				}else if($settings['woo-style'] == 'woo-style-6'){
					$university_education_woo .= '					
						<div class="all-product-container kode-ux">
							<div class="thumb">
								<div class="new-tag">'.esc_html__('New','university-education').'</div>
								<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, array(350,350)).'</a>
								<div class="btn-container">
									<div class="compare">';
										$university_education_woo .= university_education_pro_favorite($post);
									$university_education_woo .= '</div>
									<span class="price">$'.$price_sale.'</span>';
									$university_education_woo .= apply_filters( 'woocommerce_loop_add_to_cart_link',
										sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="cart button %s product_type_%s"><i class="fa fa-shopping-cart"></i></a>',
											esc_url( $product->add_to_cart_url() ),
											esc_attr( $product->id ),
											esc_attr( $product->get_sku() ),
											$product->is_purchasable() ? 'add_to_cart_button' : '',
											esc_attr( $product->product_type ),
											esc_html( $product->add_to_cart_text() )
										),
									$product );                                    
							   $university_education_woo .= ' </div>
							</div>
							<div class="text">
								<h2><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h2>
								<p>'.esc_attr(substr(get_the_content(),0,10)).'</p>
								<a class="detail" href="'.esc_url(get_permalink()).'"><i class="fa fa-file-text-o"></i></a>
							</div>
						</div>';
				}else{
					
				}
				
				
				$university_education_woo .= '</li>';
				
				$current_size++;
			} wp_reset_postdata();						
			
			if( $settings['pagination'] == 'enable' ){
				$university_education_woo .= university_education_get_pagination($query->max_num_pages, $args['paged']);
			}			
				$university_education_woo .= '</ul>';			
			$university_education_woo .= '</div>';
			wp_reset_postdata();	
			return $university_education_woo;
		}
	}	
	
	
	if( !function_exists('university_education_wish_products') ){
		function university_education_wish_products($post){
			$university_education_woo = '';
			$university_education_woo .= '
			<script>
			jQuery(document).ready(function($){
			"use strict";
				$("a#add-to-wish-'.esc_js($post->ID).'").click(function(e){
					e.preventDefault(); //
					var $star = $(this).find("i");
					var add_to_fav_opptions = {
						target:        "#fav_target-wish-'.esc_js($post->ID).'",   // target element(s) to be updated with server response
						beforeSubmit:  function(){
							$star.addClass("fa-spin");
						},  // pre-submit callback
						success:       function(){
							$star.removeClass("fa-spin");
							$("#add-to-wish-'.esc_js($post->ID).'").hide(0,function(){
								$("#fav_output-wish-'.esc_js($post->ID).'").delay(200).show();
							});
						}
					};
					$("#add-to-wish-form-'.esc_js($post->ID).'").ajaxSubmit( add_to_fav_opptions );
				});
			});	
			</script>';
			$university_education_woo .= '<!-- Add to wish -->
			<span class="add-to-fav">';
				if( is_user_logged_in() ){
					$user_id = get_current_user_id();
					$k_product_id = $post->ID;
					if ( university_education_is_added_to_wish_products( $user_id, $k_product_id ) ) {													
						$university_education_woo .= '<div id="fav_output-wish-'.esc_attr($post->ID).'" class="fav_output show k_hide_fav"><i class="fa fa-eye dim"></i></div>';
					} else {
						$university_education_woo .= '
						<form class="k_hide_fav" action="'.esc_url(admin_url('admin-ajax.php')).'" method="post" id="add-to-wish-form-'.esc_attr($post->ID).'">
							<input type="hidden" name="user_id" value="'.esc_attr($user_id).'" />
							<input type="hidden" name="k_product_id" value="'.esc_attr($k_product_id).'" />
							<input type="hidden" name="action" value="add_to_wish_products" />
						</form>
						<div id="fav_output-wish-'.esc_attr($post->ID).'" class="k_hide_fav fav_output"><span id="fav_target-wish-'.esc_attr($post->ID).'" class="dim fav_target"><i class="fa fa-eye dim"></i></span></div>
						<a id="add-to-wish-'.esc_attr($post->ID).'" href="#"><i class="fa fa-eye"></i></a>';
					}
				}else{
					$university_education_woo .= '<a href="#login-modal" data-toggle="modal"><i class="fa fa-eye"></i></a>';
				}
			return $university_education_woo .= '</span>';
		}
	}
	
	if( !function_exists( 'university_education_remove_wish_products' ) ){
		function university_education_remove_wish_products($post){
			$user_id = get_current_user_id();
			$html_remove = '<a class="remove-from-wish" data-product-id="'.esc_attr($post->ID).'" data-user-id="'.esc_attr($user_id).'" href="'.esc_url(admin_url('admin-ajax.php')).'" title="'. esc_html__('Remove from Wishlist','university-education').'"><i class="fa fa-trash-o"></i></a>';
			$html_remove .=  '<span class="loader"><i class="fa fa-spinner fa-spin"></i></span><div class="ajax-response"></div>';
			return $html_remove;
		}
	}
	
	/*	Add to favourite */
	add_action('wp_ajax_add_to_wish_products', 'university_education_add_to_wish_products');
	if( !function_exists( 'university_education_add_to_wish_products' ) ){
		function university_education_add_to_wish_products(){
			if( isset($_POST['k_product_id']) && isset($_POST['user_id']) ){
				$k_product_id = intval($_POST['k_product_id']);
				$user_id = intval($_POST['user_id']);
				if( $k_product_id > 0 && $user_id > 0 ){
					//$prev_value = get_user_meta($user_id,'wish_products', $k_product_id );
					if( add_user_meta($user_id,'wish_products', $k_product_id ) ){
						//update_user_meta( $user_id, 'wish_products', $k_product_id, $prev_value );
						echo '<i class="fa fa-eye"><i>';
					}else{
						echo '<i class="fa fa-arrow yellow"><i>';
					}
				}
			}else{
				esc_html_e('Invalid Paramenters!', 'university-education');
			}
			die;
		}
	}

	/*	Already added to favourite	*/
	if( !function_exists( 'university_education_is_added_to_wish_products' ) ){
		function university_education_is_added_to_wish_products( $user_id, $k_product_id ){
			
		}
	}

	// Remove from favourites
	add_action( 'wp_ajax_remove_from_wish', 'university_education_remove_from_wish' );
	if( !function_exists( 'university_education_remove_from_wish' ) ){
		function university_education_remove_from_wish(){
			if( isset($_POST['product_id']) && isset($_POST['user_id']) ){
				$product_id = intval($_POST['product_id']);
				$user_id = intval($_POST['user_id']);
				if( $product_id > 0 && $user_id > 0 ){
					if( delete_user_meta( $user_id, 'wish_products', $product_id ) ){
						echo 3;
						/* Removed successfully! */
					}else{
						echo 2;
						/* Failed to remove! */
					}
				}else{
					echo 1;
					/* Invalid parameters! */
				}
			}else{
				echo 1;
				/* Invalid parameters! */
			}
			die;
		}
	}
	
	// Remove from favourites
	add_action( 'wp_ajax_remove_from_favorites', 'university_education_remove_from_favorites' );
	if( !function_exists( 'university_education_remove_from_favorites' ) ){
		function university_education_remove_from_favorites(){
			if( isset($_POST['product_id']) && isset($_POST['user_id']) ){
				$product_id = intval($_POST['product_id']);
				$user_id = intval($_POST['user_id']);
				if( $product_id > 0 && $user_id > 0 ){
					if( delete_user_meta( $user_id, 'favorite_products', $product_id ) ){
						echo 3;
						/* Removed successfully! */
					}else{
						echo 2;
						/* Failed to remove! */
					}
				}else{
					echo 1;
					/* Invalid parameters! */
				}
			}else{
				echo 1;
				/* Invalid parameters! */
			}
			die;
		}
	}
	
	//Product Favourite
	if( !function_exists('university_education_pro_favorite') ){
		function university_education_pro_favorite($post){
			$university_education_woo = '';
			$university_education_woo .= '
			<script>
			jQuery(document).ready(function($){
			"use strict";
				$("a#add-to-favorite-'.esc_js($post->ID).'").click(function(e){
					e.preventDefault(); //
					var $star = $(this).find("i");
					var add_to_fav_opptions = {
						target:        "#fav_target-'.esc_js($post->ID).'",   // target element(s) to be updated with server response
						beforeSubmit:  function(){
							$star.addClass("fa-spin");
						},  // pre-submit callback
						success:       function(){
							$star.removeClass("fa-spin");
							$("#add-to-favorite-'.esc_js($post->ID).'").hide(0,function(){
								$("#fav_output-'.esc_js($post->ID).'").delay(200).show();
							});
						}
					};
					$("#add-to-favorite-form-'.esc_js($post->ID).'").ajaxSubmit( add_to_fav_opptions );
				});
			});	
			</script>';
			$university_education_woo .= '<!-- Add to favorite -->
			<span class="add-to-fav">';
				if( is_user_logged_in() ){
					$user_id = get_current_user_id();
					$k_product_id = $post->ID;
					if ( university_education_is_added_to_favorite( $user_id, $k_product_id ) ) {													
						$university_education_woo .= '<div id="fav_output-'.esc_attr($post->ID).'" class="fav_output show k_hide_fav"><i class="fa fa-heart dim"></i></div>';
					} else {
						$university_education_woo .= '
						<form class="k_hide_fav" action="'.esc_url(admin_url('admin-ajax.php')).'" method="post" id="add-to-favorite-form-'.esc_attr($post->ID).'">
							<input type="hidden" name="user_id" value="'.esc_attr($user_id).'" />
							<input type="hidden" name="k_product_id" value="'.esc_attr($k_product_id).'" />
							<input type="hidden" name="action" value="add_to_favorite" />
						</form>
						<div id="fav_output-'.esc_attr($post->ID).'" class="k_hide_fav fav_output"><span id="fav_target-'.esc_attr($post->ID).'" class="dim fav_target"><i class="fa fa-star dim"></i></span></div>
						<a id="add-to-favorite-'.esc_attr($post->ID).'" href="#"><i class="fa fa-heart"></i></a>';
					}
				}else{
					$university_education_woo .= '<a href="#login-modal" data-toggle="modal"><i class="fa fa-heart"></i></a>';
				}
			return $university_education_woo .= '</span>';
		}
	}
	
	//Remove from Favourite
	if( !function_exists( 'university_education_remove_favorite' ) ){
		function university_education_remove_favorite($post){
			$user_id = get_current_user_id();
			$html_remove = '<a class="remove-from-favorite" data-product-id="'.$post->ID.'" data-user-id="'.$user_id.'" href="'.admin_url('admin-ajax.php').'" title="'. esc_html__('Remove from favorties','university-education').'"><i class="fa fa-trash-o"></i></a>';
			$html_remove .=  '<span class="loader"><i class="fa fa-spinner fa-spin"></i></span><div class="ajax-response"></div>';
			return $html_remove;
		}
	}
	
	/*	Add to favourite */
	add_action('wp_ajax_add_to_favorite', 'university_education_add_to_favorite');
	if( !function_exists( 'university_education_add_to_favorite' ) ){
		function university_education_add_to_favorite(){
			if( isset($_POST['k_product_id']) && isset($_POST['user_id']) ){
				$k_product_id = intval($_POST['k_product_id']);
				$user_id = intval($_POST['user_id']);
				if( $k_product_id > 0 && $user_id > 0 ){
					if( add_user_meta($user_id,'favorite_products', $k_product_id ) ){
						echo '<i class="fa fa-heart"><i>';
					}else{
						echo '<i class="fa fa-arrow yellow"><i>';
					}
				}
			}else{
				esc_html_e('Invalid Paramenters!', 'university-education');
			}
			die;
		}
	}

	/*	Already added to favourite	*/
	if( !function_exists( 'university_education_is_added_to_favorite' ) ){
		function university_education_is_added_to_favorite( $user_id, $k_product_id ){
			
		}
	}
	
	// Remove from favourites
	add_action( 'wp_ajax_remove_from_favorites', 'university_education_remove_from_favorites' );
	if( !function_exists( 'university_education_remove_from_favorites' ) ){
		function university_education_remove_from_favorites(){
			if( isset($_POST['product_id']) && isset($_POST['user_id']) ){
				$product_id = intval($_POST['product_id']);
				$user_id = intval($_POST['user_id']);
				if( $product_id > 0 && $user_id > 0 ){
					if( delete_user_meta( $user_id, 'favorite_products', $product_id ) ){
						echo 3;
						/* Removed successfully! */
					}else{
						echo 2;
						/* Failed to remove! */
					}
				}else{
					echo 1;
					/* Invalid parameters! */
				}
			}else{
				echo 1;
				/* Invalid parameters! */
			}
			die;
		}
	}
	
	// use for printing product slider
	if( !function_exists('university_education_get_product_slider_item') ){
		function university_education_get_product_slider_item( $settings ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			$slide_order = array();
			$slide_data = array();
			
			// query posts section
			$args = array('post_type' => 'product', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['ignore_sticky_posts'] = 1;

			if( is_numeric($settings['category']) ){
				$args['category'] = (empty($settings['category']))? '': $settings['category'];	
			}else{ 
				if( !empty($settings['category']) || !empty($settings['tag']) ){
					$args['tax_query'] = array('relation' => 'OR');
					
					if( !empty($settings['category']) ){
						array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'product_cat', 'field'=>'slug'));
					}
					// if( !empty($settings['tag']) ){
						// array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'product_tag', 'field'=>'slug'));
					// }				
				}	
			}
			$query = new WP_Query( $args );	
			
			// set the excerpt length
			global $university_education_theme_option, $university_education_excerpt_length, $university_education_excerpt_read_more; 
			$university_education_excerpt_read_more = false;
			$university_education_excerpt_length = $settings['num-excerpt'];
			$ret = '<div class="owl-banner owl-theme owl-slider">';
			global $post;
			while($query->have_posts()){ $query->the_post();
				$image_id = get_post_thumbnail_id();
				
				if( !empty($image_id) ){
				$ret .= '
					<!--BANNER ITEM START-->
					<div class="item">
						<div class="banner-slide">
							<a href="'.esc_url(get_permalink()).'">'.get_the_post_thumbnail($post->ID, 'product-size').'</a>
							<div class="banner-caption">
								<h3>20% Discount on</h3>
								<h2>'.esc_attr(get_the_title()).'</h2>
							</div>
						</div>
					</div>
					<!--BANNER ITEM END-->';
				}
			}	
			$ret .= '</div>';
		
			return $ret;
		}
	}
	
	
	if( !function_exists('university_education_track_order_detail') ){
		function university_education_track_order_detail($settings){
		
			if(class_exists('Woocommerce')){
				if(!isset($_GET['order_number']) && $_GET['order_number'] == ''){
					echo '<div class="order-track-submit">';
						echo '<form method="GET" id="track_order">';
						echo '<h2>Track Order</h2>';
							echo '<input type="text" name="order_number" value="" id="order_number" />';							
							echo '<input type="submit" value="Submit" class="btn_submit">';
						echo '</form>';
					echo '</div>';
				}else{
					$settings['order-status-pending'] = (empty($settings['order-status-pending']))? 'Order is Pending!': $settings['order-status-pending'];
					$settings['order-status-processing'] = (empty($settings['order-status-processing']))? 'Order is processing!': $settings['order-status-processing'];
					$settings['order-status-on-hold'] = (empty($settings['order-status-on-hold']))? 'Order is on-hold!': $settings['order-status-on-hold'];
					$settings['order-status-completed'] = (empty($settings['order-status-completed']))? 'Order is completed!': $settings['order-status-completed'];
					$settings['order-status-cancelled'] = (empty($settings['order-status-cancelled']))? 'Order is cancelled!': $settings['order-status-cancelled'];
					$settings['order-status-refunded'] = (empty($settings['order-status-refunded']))? 'Order is refunded!': $settings['order-status-refunded'];
					$settings['order-status-failed'] = (empty($settings['order-status-failed']))? 'Order is failed!': $settings['order-status-failed'];
					
					$order_id = esc_attr($_GET['order_number']);
					$order = new WC_Order( $order_id );
					$order_status = '';
					//Order Condition
					if($order->post_status == 'wc-pending'){
						$order_status = $settings['order-status-pending'];
					}else if($order->post_status == 'wc-processing'){
						$order_status = $settings['order-status-processing'];
					}else if($order->post_status == 'wc-on-hold'){
						$order_status = $settings['order-status-on-hold'];
					}else if($order->post_status == 'wc-completed'){
						$order_status = $settings['order-status-completed'];
					}else if($order->post_status == 'wc-cancelled'){
						$order_status = $settings['order-status-cancelled'];
					}else if($order->post_status == 'wc-refunded'){
						$order_status = $settings['order-status-refunded'];
					}else if($order->post_status == 'wc-failed'){
						$order_status = $settings['order-status-failed'];
					}else{
						$order_status = 'No order match with your request!';
					}	
					//echo '<div class="kd-table">';
						echo '<h2>Order Number: #'.esc_attr($order_id).'</h2>';
						if(!empty($order->post_status)){
							echo '
							<table class="table_head">
								<tr>
									<th>Product Name</th>
									<th>Price</th>
									<th>Quality</th>
									<th>Status</th>
								</tr>';
								if(!empty($order)){
									$items = $order->get_items();
									foreach ( $items as $item ) {
										echo '<tr class="table_body">';
											echo '<td><a href="'.get_permalink($item['product_id']).'">'.$item['name'].'</a></td>';
											echo '<td>'.esc_attr($item['line_total']).'</td>';
											echo '<td>'.esc_attr($item['qty']).'</td>';
											echo '<td>'.esc_attr($order_status).'</td>';
										echo '</tr>';
									}
								}
							echo '</table>';
						}else{
							echo '<h3>'.esc_attr($order_status).'</h3>';
						}
						echo '<div class="order-track-submit">';
							echo '<form method="GET" id="track_order">';
							echo '<h2>Track Order</h2>';
								echo '<input type="text" name="order_number" value="'.esc_attr($order_id).'" id="order_number" />';							
								echo '<input type="submit" value="Submit" class="btn_submit">';
							echo '</form>';
						echo '</div>';
					//echo '</div>';
				}
			}
			//order_tracking
		}
	}
	
	
	// add work in page builder area
	add_filter('university_education_page_builder_option', 'university_education_register_woo_item');
	if( !function_exists('university_education_register_woo_item') ){
		function university_education_register_woo_item( $page_builder = array() ){
			global $university_education_spaces;
			$page_builder['content-item']['options']['woo'] = array(
				'title'=> esc_html__('Woo Element', 'university-education'), 
				'icon'=> 'fa-shopping-cart', 
				'type'=>'item',
				'options'=> array(					
					'element-item-id' => array(
						'title' => esc_html__('Page Item ID', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item id.', 'university-education')
					),
					'element-item-class' => array(
						'title' => esc_html__('Page Item Class', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('please add the page item class.', 'university-education')
					),	
					
					'category'=> array(
						'title'=> esc_html__('Category' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('product_cat'),
						'description'=> esc_html__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'tag'=> array(
						'title'=> esc_html__('Tag' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('product_tag'),
						'description'=> esc_html__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),
					'woo-style'=> array(
						'title'=> esc_html__('Style' ,'university-education'),
						'type'=> 'styles',
						'wrapper-class'=> 'kode-custom-styles',
						'options'=> array(
							'woo-style-1'=>UOE_PATH . '/framework/include/backend_assets/images/woo/woo-1.jpg',
							//'woo-style-2'=>UOE_PATH . '/framework/include/backend_assets/images/woo/woo-2.jpg',
							//'woo-style-3'=>UOE_PATH . '/framework/include/backend_assets/images/woo/woo-3.jpg',
							//'woo-style-4'=>UOE_PATH . '/framework/include/backend_assets/images/woo/woo-4.jpg',							
						)
					),
					'woo-column-size'=> array(
						'title'=> esc_html__('Woo Column Size' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'2' => esc_html__('Column 2', 'university-education'),
							'3' => esc_html__('Column 3', 'university-education'),
							'4' => esc_html__('Column 4', 'university-education'),
						),
						'default'=>'woo-full'
					),	
					'title-num-fetch'=> array(
						'title'=> esc_html__('Title Num Words' ,'university-education'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> esc_html__('Specify the number of words you want to pull out for title.', 'university-education')
					),						
					'woo-layout'=> array(
						'title'=> esc_html__('Woo Layout' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'simple-layout' => esc_html__('Simple Layout', 'university-education'),
							//'slider-layout' => esc_html__('Slider Layout', 'university-education'),
						),
						'default'=>'simple-layout'
					),
					// 'woo-mode'=> array(
						// 'title'=> esc_html__('Slide Mode' ,'university-education'),
						// 'type'=> 'combobox',
						// 'options'=> array(
							// 'fade' => esc_html__('fade', 'university-education'),
							// 'horizontal' => esc_html__('slide', 'university-education'),
							// 'vertical' => esc_html__('vertical', 'university-education'),
						// ),
						// 'wrapper-class'=>'woo-layout-wrapper slider-layout-wrapper',
						// 'description'=> esc_html__('In fade case only one slide will be shown.', 'university-education'),
						// 'default'=>'2'
					// ),
					// 'woo-move'=> array(
						// 'title'=> esc_html__('Slide Move' ,'university-education'),
						// 'type'=> 'combobox',
						// 'options'=> array(
							// '1' => esc_html__('1 Slide', 'university-education'),
							// '2' => esc_html__('2 Slide', 'university-education'),
							// '3' => esc_html__('3 Slide', 'university-education'),
							// '4' => esc_html__('4 Slide', 'university-education'),
						// ),
						// 'wrapper-class'=>'woo-layout-wrapper slider-layout-wrapper',
						// 'default'=>'2'
					// ),
					// 'woo-width'=> array(
						// 'title'=> esc_html__('Slide Width' ,'university-education'),
						// 'type'=> 'text',	
						// 'default'=> '275',
						// 'wrapper-class'=>'woo-layout-wrapper slider-layout-wrapper',
						// 'description'=> esc_html__('Specify the width of slide without px.', 'university-education')
					// ),
					
					'woo-padding'=> array(
						'title'=> esc_html__('Woo Padding' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'with-padding' => esc_html__('With Padding', 'university-education'),
							'without-padding' => esc_html__('Without Padding', 'university-education'),
						),
						'default'=>'with-padding'
					),
					// 'woo-filterable'=> array(
						// 'title'=> esc_html__('Woo Filterable' ,'university-education'),
						// 'type'=> 'combobox',
						// 'options'=> array(
							// 'enable' => esc_html__('Enable', 'university-education'),
							// 'disable' => esc_html__('Dsiable', 'university-education'),
						// ),
						// 'default'=>'woo-layout-shop'
					// ),
					'thumbnail-size' => array(
						'title' => esc_html__('Thumbnail Size', 'university-education'),
						'type'=> 'combobox',
						'options'=> university_education_get_thumbnail_list(),
						'default'=> 'uoe-post-thumbnail-size'
					),	
					
					'num-excerpt'=> array(
						'title'=> esc_html__('Num Excerpt (Word)' ,'university-education'),
						'type'=> 'text',	
						'default'=> '25',
						'description'=> esc_html__('This is a number of word (decided by spaces) that you want to show on the post excerpt. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the WordPress more tag</strong>.', 'university-education')
					),	
					'num-fetch'=> array(
						'title'=> esc_html__('Num Fetch' ,'university-education'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> esc_html__('Specify the number of posts you want to pull out.', 'university-education')
					),	
					'order'=> array(
						'title'=> esc_html__('Order' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>esc_html__('Descending Order', 'university-education'), 
							'asc'=> esc_html__('Ascending Order', 'university-education'), 
						)
					),									
					'pagination'=> array(
						'title'=> esc_html__('Enable Pagination' ,'university-education'),
						'type'=> 'checkbox'
					),										
					'margin-bottom' => array(
						'title' => esc_html__('Margin Bottom', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('Spaces after ending of this item', 'university-education')
					),					
				)
			);
			
			$page_builder['content-item']['options']['woo-slider'] = array(
				'title'=> esc_html__('Woo Slider', 'university-education'), 
				'icon'=>'fa-opencart',
				'type'=>'item',
				'options'=>array(					
					'category'=> array(
						'title'=> esc_html__('Category' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list_detail('product_cat'),
						'description'=> esc_html__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'tag'=> array(
						'title'=> esc_html__('Tag' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list_detail('product_tag'),
						'description'=> esc_html__('Will be ignored when the woo filter option is enabled.', 'university-education')
					),					
					'num-title-fetch'=> array(
						'title'=> esc_html__('Num Fetch' ,'university-education'),
						'type'=> 'text',	
						'default'=> '25',
						'description'=> esc_html__('Specify the number of title word you want to pull out.', 'university-education')
					),	
					'orderby'=> array(
						'title'=> esc_html__('Order By' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'date' => esc_html__('Publish Date', 'university-education'), 
							'title' => esc_html__('Title', 'university-education'), 
							'rand' => esc_html__('Random', 'university-education'), 
						)
					),
					'order'=> array(
						'title'=> esc_html__('Order' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>esc_html__('Descending Order', 'university-education'), 
							'asc'=> esc_html__('Ascending Order', 'university-education'), 
						)
					),
					'margin-bottom' => array(
						'title' => esc_html__('Margin Bottom', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('Spaces after ending of this item', 'university-education')
					),				
				)
			);
			
			
			
			return $page_builder;
		}
	}
	
	if( is_admin() ){ add_action('after_setup_theme', 'university_education_create_woo'); }
	if( !function_exists('university_education_create_woo') ){
	
		function university_education_create_woo(){
			global $university_education_theme_option;
			if(!isset($university_education_theme_option['sidebar-element'])){$university_education_theme_option['sidebar-element'] = array('blog','contact');}
			if( !class_exists('university_education_page_options') ) return;
			new university_education_page_options( 
				
				// page option settings
				array(
					'page-layout' => array(
						'title' => __('Page Layout', 'university-education'),
						'options' => array(
								'sidebar' => array(
									'type' => 'radioimage',
									'description' => 'Please select the side bar position from here.',
									'options' => array(
										'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
										'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
										'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
										'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
									),
									'default' => 'no-sidebar'
								),	
								'left-sidebar' => array(
									'title' => __('Left Sidebar' , 'university-education'),
									'type' => 'combobox_sidebar',
									'options' => $university_education_theme_option['sidebar-element'],
									'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
								),
								'right-sidebar' => array(
									'title' => __('Right Sidebar' , 'university-education'),
									'type' => 'combobox_sidebar',
									'options' => $university_education_theme_option['sidebar-element'],
									'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
								),						
						)
					),
					
					'page-option' => array(
						'title' => __('Page Option', 'university-education'),
						'options' => array(
							'page-caption' => array(
								'title' => __('Page Caption' , 'university-education'),
								'type' => 'textarea',
								'description' => 'Please enter the Caption for the page here.',
							),							
							'header-background' => array(
								'title' => __('Header Background Image' , 'university-education'),
								'button' => __('Upload', 'university-education'),
								'type' => 'upload',
								'description' => 'Click here to Upload the Header Background Image.',
							),	
								
						)
					),

				),
				// page option attribute
				array(
					'post_type' => array('product'),
					'meta_title' => __('Woo Option', 'university-education'),
					'meta_slug' => 'woo-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}	
	
	add_action('pre_post_update', 'kode_save_woo_meta_option');
	if( !function_exists('kode_save_woo_meta_option') ){
	function kode_save_woo_meta_option( $post_id ){
			if( get_post_type() == 'product' && isset($_POST['post-option']) ){
				$post_option = university_education_stopbackslashes(university_education_stripslashes($_POST['post-option']));
				$post_option = json_decode(university_education_decode_stopbackslashes($post_option), true);
				
				if(!empty($post_option['condition'])){
					update_post_meta($post_id, 'condition', esc_attr($_POST['condition']));
				}else{
					delete_post_meta($post_id, 'condition');
				}
				if(!empty($post_option['publish-date'])){
					update_post_meta($post_id, 'publish-date', esc_attr($_POST['publish-date']));
				}else{
					delete_post_meta($post_id, 'publish-date');
				}
				if(!empty($post_option['publisher'])){
					update_post_meta($post_id, 'publisher', esc_attr($_POST['publisher']));
				}else{
					delete_post_meta($post_id, 'publisher');
				}
				if(!empty($post_option['language'])){
					update_post_meta($post_id, 'language', esc_attr($_POST['language']));
				}else{
					delete_post_meta($post_id, 'language');
				}
			}
		}
	}
	
	
	
	
	
?>