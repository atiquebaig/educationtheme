<?php
	/*	
	*	Kodeforest Event Option file
	*	---------------------------------------------------------------------
	*	This file creates all post options to the post page
	*	---------------------------------------------------------------------
	*/

	// add work in page builder area
	add_filter('university_education_page_builder_option', 'university_education_register_igni_item');
	if( !function_exists('university_education_register_igni_item') ){
		function university_education_register_igni_item( $page_builder = array() ){
			global $university_education_spaces;
			$page_builder['content-item']['options']['ignitiondeck'] = array(
				'title'=> esc_html__('Crowd Funding', 'university-education'), 
				'icon'=> 'fa fa-usd',
				'type'=>'item',
				'options'=> array(					
					'title-num-fetch'=> array(
						'title'=> esc_html__('Title Character Fetch' ,'university-education'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> esc_html__('Specify the number of title character you want to pull out.', 'university-education')
					),
					'category'=> array(
						'title'=> esc_html__('Category' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('project_category'),
						'description'=> esc_html__('You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'tag'=> array(
						'title'=> esc_html__('Project Types' ,'university-education'),
						'type'=> 'multi-combobox',
						'options'=> university_education_get_term_list('project_type'),
						'description'=> esc_html__('You can use Ctrl/Command button to select multiple project types or remove the selected type. <br><br> Leave this field blank to select all categories.', 'university-education')
					),	
					'igni-style'=> array(
						'title'=> esc_html__('Listing View' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'simple-view' => esc_html__('Simple View', 'university-education'),
							'advance-view' => esc_html__('Advance View', 'university-education'),
							'advance-view-list' => esc_html__('Advance View List', 'university-education'),
						),
						'default'=>'3'
					),	
					'num-excerpt'=> array(
						'title'=> esc_html__('Num Excerpt (Word)' ,'university-education'),
						'type'=> 'text',	
						'default'=> '0',
						'wrapper-class' => 'igni-style-wrapper advance-view-wrapper advance-view-list-wrapper',
						'description'=> esc_html__('This is a number of word (decided by spaces) that you want to show on the post excerpt. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the WordPress more tag</strong>.', 'university-education')
					),	
					'num-fetch'=> array(
						'title'=> esc_html__('Num Fetch' ,'university-education'),
						'type'=> 'text',	
						'default'=> '8',
						'description'=> esc_html__('Specify the number of posts you want to pull out.', 'university-education')
					),	
									
					'project-view'=> array(
						'title'=> esc_html__('Project View' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'2' => esc_html__('2 Column Grid', 'university-education'),
							'3' => esc_html__('3 Column Grid', 'university-education'),
							'4' => esc_html__('4 Column Grid', 'university-education'),
						),
						'wrapper-class' => 'igni-style-wrapper advance-view-list-wrapper',
						'default'=>'3'
					),		
					'order'=> array(
						'title'=> esc_html__('Order' ,'university-education'),
						'type'=> 'combobox',
						'options'=> array(
							'desc'=>esc_html__('Descending Order', 'university-education'), 
							'asc'=> esc_html__('Ascending Order', 'university-education'), 
						)
					),									
					'pagination'=> array(
						'title'=> esc_html__('Enable Pagination' ,'university-education'),
						'type'=> 'checkbox'
					),								
					'margin-bottom' => array(
						'title' => esc_html__('Margin Bottom', 'university-education'),
						'type' => 'text',
						'default' => '',
						'description' => esc_html__('Spaces after ending of this item', 'university-education')
					),					
				)
			);
			return $page_builder;
		}
	}
	
	
	// add a post option to post page
	if( is_admin() ){ add_action('init', 'university_education_create_igni_options'); }
	if( !function_exists('university_education_create_igni_options') ){
	
		function university_education_create_igni_options(){
			global $university_education_theme_option;
			
			if( !class_exists('university_education_page_options') ) return;
			new university_education_page_options( 
				
				
					  
				// page option settings
				array(
					'page-layout' => array(
						'title' => esc_html__('Page Layout', 'university-education'),
						'options' => array(
								'sidebar' => array(
									'title' => esc_html__('Sidebar Template' , 'university-education'),
									'type' => 'radioimage',
									'options' => array(
										'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
										'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
										'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
										'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
									),
									'default' => 'default-sidebar'
								),	
								'left-sidebar' => array(
									'title' => esc_html__('Left Sidebar' , 'university-education'),
									'type' => 'combobox_sidebar',
									'options' => $university_education_theme_option['sidebar-element'],
									'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
								),
								'right-sidebar' => array(
									'title' => esc_html__('Right Sidebar' , 'university-education'),
									'type' => 'combobox_sidebar',
									'options' => $university_education_theme_option['sidebar-element'],
									'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
								),						
						)
					),
					
					'page-option' => array(
						'title' => esc_html__('Page Option', 'university-education'),
						'options' => array(
							'page-title' => array(
								'title' => esc_html__('Post Title' , 'university-education'),
								'type' => 'text',
								'description' => esc_html__('Leave this field blank to use the default title from admin panel > general > blog style section.', 'university-education')
							),
							'page-caption' => array(
								'title' => esc_html__('Post Caption' , 'university-education'),
								'type' => 'textarea'
							),
							'header-background' => array(
								'title' => esc_html__('Header Background Image' , 'university-education'),
								'button' => esc_html__('Upload', 'university-education'),
								'type' => 'upload',
							),	
						)
					),
										
				),
				
				// page option attribute
				array(
					'post_type' => array('ignition_product'),
					'meta_title' => esc_html__('Kodeforest Post Option', 'university-education'),
					'meta_slug' => 'kodeforest-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);
			
		}
	}
	
	//Crowd Funding Listing
	if( !function_exists('university_education_get_crowdfunding_item') ){
		function university_education_get_crowdfunding_item( $settings ){
						
			$item_id = empty($settings['page-item-id'])? '': ' id="' .esc_attr( $settings['page-item-id'] ). '" ';
		
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-blog-item'])? 'margin-bottom: '.esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$ret  = '';	
			
			
			
			
			
			// query section
			// query post and sticky post
			$args = array('post_type' => 'ignition_product', 'suppress_filters' => false);
			if( !empty($settings['category']) || !empty($settings['tag']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'project_category', 'field'=>'slug'));
				}
				if( !empty($settings['tag']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'project_type', 'field'=>'slug'));
				}				
			}
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
			$args['paged'] = empty($args['paged'])? 1: $args['paged'];
			$args['offset'] = (empty($settings['offset']))? 0: $settings['offset'];			
			$query = new WP_Query( $args );
			$settings['project-view'] = (empty($settings['project-view']))? '3': $settings['project-view'];
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '15': $settings['title-num-fetch'];
			$settings['num-excerpt'] = (empty($settings['num-excerpt']))? '15': $settings['num-excerpt'];
			
			$html_excerpt = '';
			
			
			$size = 3;
			if($settings['igni-style'] == 'advance-view-list'){
				$ret .= '<div class="kode-causes-list kode-causes-box row " ' . $item_id . $margin_style . '>'; 	
				$size = 1;
			}else{
				$ret .= '<div class="kode-causes-list kode-causes-box row" ' . $item_id . $margin_style . '>'; 	
				$size = $settings['project-view'];
			}
			
			$current_size = '';
			
			while($query->have_posts()){ $query->the_post();
				global $post;
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}
				if($settings['num-excerpt'] <> 0){
					$html_excerpt = '<p>'.esc_attr(substr(get_the_content(),0,$settings['num-excerpt'])).'</p>';
				}
				$university_education_ign_fund_end = get_post_meta($post->ID, 'ign_fund_end', true);
				$university_education_ignition_datee = date('d-m-Y h:i:s',strtotime($university_education_ign_fund_end));
				$university_education_ign_project_id = get_post_meta($post->ID, 'ign_project_id', true);
				$university_education_ign_fund_goal = get_post_meta($post->ID, 'ign_fund_goal', true);
				$university_education_ign_product_image1 = get_post_meta($post->ID, 'ign_product_image1', true);
				$university_education_thumbnail_id = get_post_thumbnail_id( $post->ID, 'ign_project_id', true );
				$university_education_total_raised = getTotalProductFund($university_education_ign_project_id);
				$university_education_supporter = getPledge($university_education_ign_project_id);				
				$university_education_current_date = date('d-m-Y h:i:s');
				$university_education_project_date = new DateTime($university_education_ignition_datee);
				$university_education_current = new DateTime($university_education_current_date);
				$university_education_days = round(($university_education_project_date->format('U') - $university_education_current->format('U')) / (60*60*24));
				$thumbnail = wp_get_attachment_image_src( $university_education_thumbnail_id , 'uoe-small-grid-size' );
				
					if($settings['igni-style'] == 'simple-view'){
						$ret .= '
						<div class="col-sm-4 university_education_crowd ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">
							<div class="kode-ux">
								<figure>
									<a class="kode-causes-thumb" href="'.esc_url(get_permalink()).'">
										<img alt="'.esc_attr(get_the_title()).'" src="'.esc_url($thumbnail[0]).'">
									</a>
									<figcaption>
										<div class="kode-causes-info">
											<div class="custom-skills">
												<div class="progress">
													<div style="background-color: rgb(255, 255, 255); width: '.esc_attr(getPercentRaised($university_education_ign_project_id)).'%;" data-transitiongoal="'.esc_attr(getPercentRaised($university_education_ign_project_id)).'" role="progressbar" class="progress-bar" aria-valuenow="'.esc_attr(getPercentRaised($university_education_ign_project_id)).'">'.esc_attr(getPercentRaised($university_education_ign_project_id)).'%</div>
												</div>
											</div>
										</div>
										<div class="cause-inner-caption">
											<h2><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title($post->ID),0,$settings['title-num-fetch'])).'</a></h2>
										</div>
									</figcaption>
								</figure>
							</div>	
						</div>
						';
					}else{
						$item_class = '';
						if($settings['igni-style'] == 'advance-view-list'){
							$item_class = 'kode-list-view'; 
							$thumbnail = wp_get_attachment_image_src( $university_education_thumbnail_id , 'uoe-causes-small-size' );
						}else if($settings['igni-style'] == 'advance-view'){							
							$thumbnail = wp_get_attachment_image_src( $university_education_thumbnail_id , 'uoe-post-thumbnail-size' );
						}else{
							$thumbnail = wp_get_attachment_image_src( $university_education_thumbnail_id , 'uoe-small-grid-size' );
						}
					$ret .= '
					<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">
						<div class="kode-cause-list kode-item '.esc_attr($item_class).'">
                            <div class="kode-thumb kode-ux">
                                <a href="'.esc_url(get_permalink()).'"><img alt="'.esc_attr(get_the_title()).'" src="'.esc_url($thumbnail[0]).'"></a>
                            </div>
                            <div class="kode-cause-content">
                                <div class="kode-text">
                                    <h2>'.esc_attr(get_the_title()).'</h2>
                                    <!--COUSES META START-->
                                    <div class="kode-cause-meta">
                                        <ul>                                            
											'.university_education_get_igni_info(array( 'author','comment') ,false,'' ,'li').'
                                        </ul>
                                    </div>
                                    <!--COUSES META END-->
									'.$html_excerpt.'
									'.idcf_level_select_lb($university_education_ign_project_id).'
                                </div>
                                <!--FUND RAISING START-->
                                <div class="kode-fund-raised">
                                    <!--SUPPORTERS START-->
                                    <div class="raised-text">
                                        <h6>'.esc_attr($university_education_total_raised).' '.esc_html__('Raised','university-education').'</h6>
                                        <h6 class="pull-right">'.esc_attr($university_education_supporter[0]->p_number).' Supporters</h6>
                                    </div>
                                    <!--SUPPORTERS END-->
                                    <!--PROGRESS BAR START-->
                                    <div class="progress">
                                        <div style="width: '.esc_attr(getPercentRaised($university_education_ign_project_id)).'%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar">
                                           '.esc_attr(getPercentRaised($university_education_ign_project_id)).'%
                                        </div>
                                    </div>
                                    <!--PROGRESS BAR END-->
                                    <!--GOAL START-->
                                    <div class="raised-text">
                                        <h6>'.esc_html__('Goal','university-education').'</h6>
                                        <h6 class="pull-right">'.esc_attr($university_education_ign_fund_goal).'</h6>
                                    </div>
                                    <!--GOAL END-->
                                    <a class="btn-filled-rounded" href="'.esc_url(get_permalink()).'">'.esc_html__('Donate Now','university-education').'</a>
                                    <div class="fund-meta">
                                        <ul>
                                            <li><a href="'.esc_url(get_permalink()).'"><i class="fa fa-clock-o"></i> '.esc_attr($university_education_days).' Days</a></li>
                                            <li><a href="'.esc_url(get_permalink()).'"><i class="fa fa-eye"></i>'. esc_attr(university_education_get_post_views($post->ID)).'</a></li>
											<li><a href="'.esc_url(get_permalink()).'"><i class="fa fa-share-square-o"></i> Share</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--FUND RAISING END-->
                            </div>
                        </div>
					</div>';
					}					
				$current_size++;				
			}	
			
			if( $settings['pagination'] == 'enable' ){
				$ret .= '<div class="kode_pagi col-md-12">';
				$ret .= university_education_get_pagination($query->max_num_pages, $args['paged']);
				$ret .=  '</div>';
			}
			$ret .= '</div>';			
			
			return $ret;
		}
	}
	
	
	//Crowd Funding Listing
	if( !function_exists('university_education_get_crowdfunding_item') ){
		function university_education_get_cf_call_item( $settings ){
						
			$item_id = empty($settings['page-item-id'])? '': ' id="' .esc_attr( $settings['page-item-id'] ). '" ';
		
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-blog-item'])? 'margin-bottom: '.esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';

			$ret  = '';	
			$ret .= '<div class="kode-causes-list kode-causes-box" ' . $item_id . $margin_style . '>'; 
			
			// query section
			// query post and sticky post
			$args = array('post_type' => 'ignition_product', 'suppress_filters' => false);
			if( !empty($settings['category']) || !empty($settings['tag']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'project_category', 'field'=>'slug'));
				}
				if( !empty($settings['tag']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'project_type', 'field'=>'slug'));
				}				
			}
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
			$args['paged'] = empty($args['paged'])? 1: $args['paged'];
			$args['offset'] = (empty($settings['offset']))? 0: $settings['offset'];			
			$query = new WP_Query( $args );
			$settings['project-view'] = (empty($settings['project-view']))? '3': $settings['project-view'];
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '15': $settings['title-num-fetch'];
			
			
			
			
			$size = $settings['project-view'];
			$current_size = '';
			while($query->have_posts()){ $query->the_post();
				global $post;
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}
				
				$university_education_ign_fund_end = get_post_meta($post->ID, 'ign_fund_end', true);
				$university_education_ignition_datee = date('d-m-Y h:i:s',strtotime($university_education_ign_fund_end));
				$university_education_ign_project_id = get_post_meta($post->ID, 'ign_project_id', true);
				$university_education_ign_fund_goal = get_post_meta($post->ID, 'ign_fund_goal', true);
				$university_education_ign_product_image1 = get_post_meta($post->ID, 'ign_product_image1', true);
				$university_education_thumbnail_id = get_post_thumbnail_id( $post->ID, 'ign_project_id', true );
				$university_education_getPledge_cp = getPledge_cp($university_education_ign_project_id);
				$university_education_current_date = date('d-m-Y h:i:s');
				$university_education_project_date = new DateTime($university_education_ignition_datee);
				$university_education_current = new DateTime($university_education_current_date);
				$university_education_days = round(($university_education_project_date->format('U') - $university_education_current->format('U')) / (60*60*24));
				$thumbnail = wp_get_attachment_image_src( $university_education_thumbnail_id , 'kode-small-grid-size' );
					$ret .= '
						<div class="col-sm-4 university_education_crowd ' . esc_attr(university_education_get_column_class('1/' . $size)) . '">
							<div class="kode-ux">
								<figure>
									<a class="kode-causes-thumb" href="'.esc_url(get_permalink()).'">
										<img alt="" src="'.esc_url($thumbnail[0]).'">
									</a>
									<figcaption>
										<div class="kode-causes-info">
											<div class="custom-skills">
												<div class="progress">
													<div style="background-color: rgb(255, 255, 255); width: '.esc_attr(getPercentRaised($university_education_ign_project_id)).'%;" data-transitiongoal="'.esc_attr(getPercentRaised($university_education_ign_project_id)).'" role="progressbar" class="progress-bar" aria-valuenow="'.esc_attr(getPercentRaised($university_education_ign_project_id)).'">'.esc_attr(getPercentRaised($university_education_ign_project_id)).'%</div>
												</div>
											</div>
										</div>
										<div class="cause-inner-caption">
											<h2><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title($post->ID),0,$settings['title-num-fetch'])).'</a></h2>
										</div>
									</figcaption>
								</figure>
							</div>	
						</div>
						';
				$current_size++;				
			}	
			
			if( $settings['pagination'] == 'enable' ){
				$ret .= '<div class="kode_pagi col-md-12">';
				$ret .= university_education_get_pagination($query->max_num_pages, $args['paged']);
				$ret .=  '</div>';
			}
			$ret .= '</div>';			
			
			return $ret;
		}
	}
	
	if( !function_exists('university_education_get_igni_info') ){
		function university_education_get_igni_info( $array = array(), $wrapper = true, $sep = '',$custom_wrap='div' ){
			global $university_education_theme_option; $ret = '';
			if( empty($array) ) return $ret;
			$exclude_meta = empty($university_education_theme_option['post-meta-data'])? array(): esc_attr($university_education_theme_option['post-meta-data']);
			
			foreach($array as $post_info){
				if( in_array($post_info, $exclude_meta) ) continue;
				if( !empty($sep) ) $ret .= $sep;
				
				switch( $post_info ){
					case 'date':
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-date"><i class="fa fa-clock-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( esc_attr(get_the_time('Y')), esc_attr(get_the_time('m')), esc_attr(get_the_time('d')))) . '">';
						$ret .= esc_attr(get_the_time());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';	
						break;
					case 'tag':
						$tag = get_the_term_list(get_the_ID(), 'project_type', '', '<span class="sep">,</span> ' , '' );
						if(empty($tag)) break;					
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-tag"><i class="fa fa-tag"></i>';
						$ret .= $tag;						
						$ret .= '</'.esc_attr($custom_wrap).'>';						
						break;
					case 'category':
						$category = get_the_term_list(get_the_ID(), 'project_category', '', '<span class="sep">,</span> ' , '' );
						if(empty($category)) break;
						
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-category"><i class="fa fa-list"></i>';
						$ret .= $category;					
						$ret .= '</'.esc_attr($custom_wrap).'>';					
						break;
					case 'comment':
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-comment"><i class="fa fa-comment-o"></i>';
						$ret .= '<a href="' . esc_url(get_permalink()) . '#respond" >' . esc_attr(get_comments_number()) . ' ' . esc_html__('Comment','university-education').'</a>';						
						$ret .= '</'.esc_attr($custom_wrap).'>';							
						break;
					case 'author':
						ob_start();
						the_author_posts_link();
						$author = ob_get_contents();
						ob_end_clean();
						
						$ret .= '<'.esc_attr($custom_wrap).' class="blog-info blog-author"><i class="fa fa-user"></i>';
						$ret .= $author;
						$ret .= '</'.esc_attr($custom_wrap).'>';						
						break;						
				}
			}
			
			
			if($wrapper && !empty($ret)){
				return '<div class="kode-blog-info kode-info">' . $ret . '<div class="clear"></div></div>';
			}else if( !empty($ret) ){
				return $ret;
			}
			return '';
		}
	}
	
	
	if( !function_exists('getPledge') ){
		function getPledge($project_id) {
			
		}
	}
	
?>