<?php
	/*	
	*	Kodeforest Post Option file
	*	---------------------------------------------------------------------
	*	This file creates all post options to the post page
	*	---------------------------------------------------------------------
	*/
	
	// Generate Options in theme Option Panel
	// add_filter('university_education_themeoption_panel', 'university_education_register_post_themeoption');
	if( !function_exists('university_education_register_post_themeoption') ){
		function university_education_register_post_themeoption( $array ){		
			global $university_education_theme_option;
			if(!isset($university_education_theme_option['sidebar-element'])){$university_education_theme_option['sidebar-element'] = array('blog','contact');}
			//if empty
			if( empty($array['general']['options']) ){
				return $array;
			}
			//Blog options
			$post_themeoption = array(
				'title' => esc_html__('Blog Settings', 'university-education'),
				'options' => array(
					// 'post-title' => array(
						// 'title' => 'Sub Header Post Title',
						// 'type' => 'text',	
						// 'description' => 'Sub Header Post Title'
					// ),
					// 'post-caption' => array(
						// 'title' => 'Sub Header Post Caption',
						// 'type' => 'textarea',
						// 'description' => 'Add Sub Header Post Caption'
					// ),
					'post-thumbnail-size' => array(
						'title' => esc_html__('Single Post Thumbnail Size', 'university-education'),
						'type'=> 'combobox',
						'options'=> university_education_get_thumbnail_list(),
						'default'=> 'uoe-post-thumbnail-size'
					),
					'post-sidebar-template' => array(
						'title' => esc_html__('Single Default Sidebar', 'university-education'),
						'type' => 'radioimage',
						'options' => array(
							'no-sidebar'=>		UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
							'both-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
							'right-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
							'left-sidebar'=>	UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
						),
					),
					'post-sidebar-left' => array(
						'title' => esc_html__('Single Default Sidebar Left', 'university-education'),
						'type' => 'combobox_sidebar',
						'wrapper-class' => 'left-sidebar-wrapper both-sidebar-wrapper post-sidebar-template-wrapper',
						'options' => $university_education_theme_option['sidebar-element'],		
					),
					'post-sidebar-right' => array(
						'title' => esc_html__('Single Default Sidebar Right', 'university-education'),
						'type' => 'combobox_sidebar',
						'wrapper-class' => 'right-sidebar-wrapper both-sidebar-wrapper post-sidebar-template-wrapper',
						'options' => $university_education_theme_option['sidebar-element'],
					),	
					'single-post-author' => array(
						'title' => esc_html__('Single Post Author', 'university-education'),
						'type' => 'checkbox',							
					),	
					// 'blog-post-meta-seo' => array(
						// 'title' => esc_html__('Theme SEO', 'university-education'),
						// 'type' => 'checkbox',	
						// 'default' => 'enable'
					// ),
					// 'post-seo-meta-title' => array(
						// 'title' => 'SEO Post Title',
						// 'type' => 'text',	
						// 'description' => 'Please add post meta title.'
					// ),
					// 'post-seo-meta-description' => array(
						// 'title' => 'SEO Post Description',
						// 'type' => 'textarea',
						// 'description' => 'Please add post meta descriotion here.'
					// ),
					// 'post-seo-meta-keyword' => array(
						// 'title' => 'SEO Post Keyword',
						// 'type' => 'textarea',
						// 'description' => 'Please add Post keyword quoma seperated for example: food, charity, news'
					// ),				
				)
			);
			
			
			$array['general']['options']['blog-style'] = $post_themeoption;
			return $array;
		}
	}		

	// add a post option to post page
	if( is_admin() ){
		add_action('init', 'university_education_create_post_options');
	}
	if( !function_exists('university_education_create_post_options') ){
	
		function university_education_create_post_options(){
			global $university_education_theme_option;
			if( !class_exists('university_education_page_options') ) return;
			new university_education_page_options( 
				
				// page option settings
				array(
					// 'seo-option' => array(
						// 'title' => esc_html__('Seo Post Option', 'university-education'),
						// 'options' => array(								
							// 'post-seo-meta-title' => array(
								// 'title' => 'SEO Default Title',
								// 'type' => 'text',	
								// 'wrapper-class' => 'default-post-meta-button-wrapper enable-wrapper',
								// 'description' => 'Please add Default meta title.'
							// ),
							// 'post-seo-meta-description' => array(
								// 'title' => 'SEO Default Description',
								// 'type' => 'textarea',
								// 'wrapper-class' => 'default-post-meta-button-wrapper enable-wrapper',
								// 'description' => 'Please add Default meta descriotion here.'
							// ),
							// 'post-seo-meta-keyword' => array(
								// 'title' => 'SEO Default Keyword',
								// 'type' => 'textarea',
								// 'wrapper-class' => 'default-post-meta-button-wrapper enable-wrapper',
								// 'description' => 'Please add Default keyword quoma seperated for example: food, charity, news'
							// ),							
						// )
					// ),
					'page-layout' => array(
						'title' => esc_html__('Post Sidebar Layout', 'university-education'),
						'options' => array(
							'sidebar' => array(
								'title' => esc_html__('Sidebar Template' , 'university-education'),
								'type' => 'radioimage',
								'options' => array(
									//'default-sidebar'=>UOE_PATH . '/framework/include/backend_assets/images/default-sidebar-2.png',
									'no-sidebar'=>UOE_PATH . '/framework/include/backend_assets/images/no-sidebar.png',
									'both-sidebar'=>UOE_PATH . '/framework/include/backend_assets/images/both-sidebar.png', 
									'right-sidebar'=>UOE_PATH . '/framework/include/backend_assets/images/right-sidebar.png',
									'left-sidebar'=>UOE_PATH . '/framework/include/backend_assets/images/left-sidebar.png'
								),
								'default' => 'default-sidebar'
							),	
							'left-sidebar' => array(
								'title' => esc_html__('Left Sidebar' , 'university-education'),
								'type' => 'combobox_sidebar',
								'options' => $university_education_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper left-sidebar-wrapper both-sidebar-wrapper'
							),
							'right-sidebar' => array(
								'title' => esc_html__('Right Sidebar' , 'university-education'),
								'type' => 'combobox_sidebar',
								'options' => $university_education_theme_option['sidebar-element'],
								'wrapper-class' => 'sidebar-wrapper right-sidebar-wrapper both-sidebar-wrapper'
							),						
						)
					),
					
					'page-option' => array(
						'title' => esc_html__('Post Option', 'university-education'),
						'options' => array(
							'page-title' => array(
								'title' => esc_html__('Post Title' , 'university-education'),
								'type' => 'text',
								'description' => esc_html__('Post title', 'university-education')
							),
							'page-caption' => array(
								'title' => esc_html__('Post Caption' , 'university-education'),
								'type' => 'textarea'
							),
							'post_media_type' => array(
								'title' => esc_html__('Select Post Media' , 'university-education'),
								'type' => 'combobox',
								'options' => array(
									'audio'=>	esc_html__('Audio URL' , 'university-education'),
									'video'=>	esc_html__('Video URL' , 'university-education'),
									'featured_image'=>	esc_html__('Feature Image' , 'university-education'),
									'slider'=>	esc_html__('Slider' , 'university-education'),
								),
								'description'=> esc_html__('Select post media type.', 'university-education')
							),	
							'university_education_audio' => array(
								'title' => esc_html__('Audio URL' , 'university-education'),
								'type' => 'text',
								'wrapper-class' => 'audio-wrapper post_media_type-wrapper',
								'description' => esc_html__('Add audio url, it could be uploaded mp3 , wav or add soundcloud track or profile url.', 'university-education')
							),		
							'university_education_video' => array(
								'title' => esc_html__('Video URL' , 'university-education'),
								'type' => 'text',
								'wrapper-class' => 'video-wrapper post_media_type-wrapper',
								'description' => esc_html__('Add video url, it could be uploaded video track or youtube, vimeo.', 'university-education')
							),	
							'thumbnail-size' => array(
								'title' => esc_html__('Thumbnail Size', 'university-education'),
								'type'=> 'combobox',
								'options'=> university_education_get_thumbnail_list(),
								'default'=> 'uoe-post-thumbnail-size'								
							),
							'slider'=> array(	
								'overlay'=> 'false',
								'caption'=> 'false',
								'type'=> 'slider',
								'wrapper-class' => 'slider-wrapper post_media_type-wrapper',
							),
						),
					),
					

				),
				// page option attribute
				array(
					'post_type' => array('post'),
					'meta_title' => esc_html__('Kodeforest Post Option', 'university-education'),
					'meta_slug' => 'kodeforest-page-option',
					'option_name' => 'post-option',
					'position' => 'normal',
					'priority' => 'high',
				)
			);	
		}
	}
	
	add_action('pre_post_update', 'university_education_save_post_meta_option');
	if( !function_exists('university_education_save_post_meta_option') ){
		function university_education_save_post_meta_option( $post_id ){
			if( get_post_type() == 'post' && isset($_POST['post-option']) ){
				$post_option = university_education_stopbackslashes(university_education_stripslashes($_POST['post-option']));
				$post_option = json_decode(university_education_decode_stopbackslashes($post_option), true);
				
				if(!empty($post_option['rating'])){
					update_post_meta($post_id, 'kode-post-rating', floatval($post_option['rating']) * 100);
				}else{
					delete_post_meta($post_id, 'kode-post-rating');
				}
			}
		}
	}
	
?>