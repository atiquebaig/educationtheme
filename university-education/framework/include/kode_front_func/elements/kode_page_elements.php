<?php
	/*	
	*	University Of Education Theme File
	*	---------------------------------------------------------------------
	*	This file contains the function use to print the elements of the theme
	*	---------------------------------------------------------------------
	*/
	
	// print title
	if( !function_exists('university_education_get_item_title') ){
		function university_education_get_item_title( $atts ){
			$ret = '';
			
			$atts['title-type'] = (empty($atts['title-type']))? $atts['type']: $atts['title-type'];
		
			if( !empty($atts['title-type']) && $atts['title-type'] != 'none' ){
				$item_class  = 'pos-' . str_replace('-divider', '', $atts['title-type']);
				$item_class .= (!empty($atts['carousel']))? ' kode-nav-container': '';
				
				$ret .= '<div class="kode-item-title-wrapper kode-item ' . esc_attr($item_class) . ' ">';
				
				$ret .= '<div class="kode-item-title-head">';
				$ret .= !empty($atts['carousel'])? '<i class="icon-angle-left kode-flex-prev"></i>': '';
				if(!empty($atts['title'])){
					$ret .= '<h3 class="kode-item-title kode-skin-title kode-skin-border">' . esc_attr($atts['title']) . '</h3>';
				}
				$ret .= !empty($atts['carousel'])? '<i class="icon-angle-right kode-flex-next"></i>': '';
				$ret .= '<div class="clear"></div>';
				$ret .= '</div>';
				
				$ret .= (strpos($atts['title-type'], 'divider') > 0)? '<div class="kode-item-title-divider"></div>': '';
				
				if(!empty($atts['caption'])){
					$ret .= '<div class="kode-item-title-caption kode-skin-info">' . esc_attr($atts['caption']) . '</div>';
				}
				
				if(!empty($atts['right-text']) && !empty($atts['right-text-link'])){
					$ret .= '<a class="kode-item-title-link" href="' . esc_url($atts['right-text-link']) . '" >' . esc_attr($atts['right-text']) . '</a>';
				}
				
				$ret .= '</div>'; // kode-item-title-wrapper
			}
			return $ret;
		}
	}

	// title item
	if( !function_exists('university_education_get_title_item') ){
		function university_education_get_title_item( $settings ){	
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';		
			
			$ret  = '<div class="kode-title-item" ' . $item_id . $margin_style . ' >';
			//$ret .= university_education_get_item_title($settings);			
			$ret .= '</div>';
			return $ret;
		}
	}
	
	// sidebar item
	if( !function_exists('university_education_get_sidebar_item') ){
		function university_education_get_sidebar_item( $settings ){
			$ret = '';
			return dynamic_sidebar($settings['widget']);			
		}
	}	
	
	// accordion item
	if( !function_exists('university_education_get_accordion_item') ){
		function university_education_get_accordion_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$accordion = is_array($settings['accordion'])? esc_attr($settings['accordion']): json_decode($settings['accordion'], true);

			//$ret  = university_education_get_item_title($settings);	
			$ret = '';
			$ret .= '<div class="kode-item kd-accordion kode-accordion-item" ' . $item_id . $margin_style . ' >';
			$ret .= '
			<script type="text/javascript">
			jQuery(document).ready(function($){
				/* ---------------------------------------------------------------------- */
				/*	Accordion Script
				/* ---------------------------------------------------------------------- */
				if($(".accordion").length){
					//custom animation for open/close
					$.fn.slideFadeToggle = function(speed, easing, callback) {
					  return this.animate({opacity: "toggle", height: "toggle"}, speed, easing, callback);
					};

					$(".accordion").accordion({
					  defaultOpen: "section1",
					  cookieName: "nav",
					  speed: "slow",
					  animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
						elem.next().stop(true, true).slideFadeToggle(opts.speed);
					  },
					  animateClose: function (elem, opts) { //replace the standard slideDown with custom function
						elem.next().stop(true, true).slideFadeToggle(opts.speed);
					  }
					});
				}
			});
			</script>
			';
		
			$current_tab = 0;
			foreach( $accordion as $tab ){  $current_tab++;
				$ret .= '<div class="kode_goal_wrap">';
				$ret .= '<div id="';
				$ret .= ($current_tab == intval($settings['initial-state']))? 'section1"': 'section_'.$current_tab.'"';
				$ret .= ' class="goal_heading accordion" ';
				//$ret .= empty($tab['kdf-tab-title-id'])? '': 'id="' . esc_attr($tab['kdf-tab-title-id']) . '" ';
				$ret .= '><span class="fa ';
				$ret .= ($current_tab == intval($settings['initial-state']))? 'fa-minus': 'fa-plus';
				$ret .= '"></span>' . university_education_text_filter($tab['kdf-tab-title']) . '</div>';
				$ret .= '<div class="goal_des accordion-content">' . university_education_content_filter($tab['kdf-tab-content']) . '</div></div>';			
			}
			$ret .= '</div>';
			
			return $ret;
		}
	}	
	
	// Heading item
	if( !function_exists('university_education_get_headings_item') ){
		function university_education_get_headings_item( $settings ){
			global $university_education_counter;
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$settings['element-item-class'] = (empty($settings['element-item-class']))? '': $settings['element-item-class'];
			$settings['element-style'] = (empty($settings['element-style']))? '': $settings['element-style'];
			$settings['title'] = (empty($settings['title']))? '': $settings['title'];
			$settings['caption'] = (empty($settings['caption']))? '': $settings['caption'];
			$settings['title-color'] = (empty($settings['title-color']))? '': $settings['title-color'];
			$settings['line-color'] = (empty($settings['line-color']))? '': $settings['line-color'];
			$settings['caption-color'] = (empty($settings['caption-color']))? '': $settings['caption-color'];
			$settings['alignment'] = (empty($settings['alignment']))? 'center': $settings['alignment'];
			
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$ret = '<div class="kode-simple-heading text-'.esc_attr($settings['alignment']).' '.esc_attr($settings['element-item-class']).'" '.$margin_style.'>';
			if($settings['element-style'] == 'style-1'){
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '
					<div class="col-md-12">
						<div class="contact_2_headung text-'.esc_attr($settings['alignment']).'">
							<h3 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h3>
							<p style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</p>
						</div>
					</div>';
				}
			}else if($settings['element-style'] == 'style-2'){
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '
					<style scoped>
					.kf_edu2_heading_'.esc_attr($university_education_counter).' h3:before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div class="col-md-12">
						<div class="kf_edu2_heading1 text-'.esc_attr($settings['alignment']).' kf_edu2_heading_'.esc_attr($university_education_counter).'">
							<h3 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h3>
						</div>
					</div>';
				}
			}else if($settings['element-style'] == 'style-3'){
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '			
					<style scoped>
					.kf_edu2_heading2'.esc_attr($university_education_counter).' h3:before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div class="col-md-12">
						<div class="kf_edu2_heading1 text-'.esc_attr($settings['alignment']).' kf_edu2_heading2'.esc_attr($university_education_counter).'">
							<h3 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h3>
							<p style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</p>
							
						</div>
					</div>';
				}
			}else if($settings['element-style'] == 'style-4'){
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '			
					<style scoped>
					.training_heading_'.esc_attr($university_education_counter).':before{
						background-color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div class="col-md-12">
						<div class="training_heading training_heading_'.esc_attr($university_education_counter).' text-'.esc_attr($settings['alignment']).'">
							<h4 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h4>
						</div>
					</div>';
				}
			}else{
				if(isset($settings['title']) && $settings['title'] <> ''){
					$ret .= '
					<style scoped>
					#kode-heading-'.esc_attr($university_education_counter).' .kf_property_line:before{
						color:'.esc_attr($settings['line-color']).' !important;
					}
					</style>
					<div class="col-md-12">
						<div id="kode-heading-'.esc_attr($university_education_counter).'" class="kf_heading_1 kode-align-'.esc_attr($settings['alignment']).'">
							<h3 style="color:'.esc_attr($settings['title-color']).'">'.esc_attr($settings['title']).'</h3>
							<p style="color:'.esc_attr($settings['caption-color']).'">'.esc_attr($settings['caption']).'</p>
							<span class="kf_property_line"></span>
						</div>
					</div>';
				}
			}
			
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	
	if( !function_exists('university_education_get_call_to_action') ){
		function university_education_get_call_to_action($settings){
			$ret = '<div class="lib-video-section">				
				<a style="background:'.esc_attr($settings['icon-bg-color']).';color:'.esc_attr($settings['icon-color']).' " class="fa '.esc_attr($settings['icon']).' video-play" href="'.esc_url($settings['link']).'"></a>
				<h2 style="color:'.esc_attr($settings['title-color']).' ">'.esc_attr($settings['title']).' </h2>				
				<h4 style="color:'.esc_attr($settings['sub-title-color']).' ">'.esc_attr($settings['sub-title']).'</h4>
				<p style="color:'.esc_attr($settings['sub-title-small-color']).' " >'.esc_attr($settings['sub-title-small']).'</p>				
			</div>';
			return $ret;
		}
	}
	
	// Blog Slider item
	if( !function_exists('university_education_get_blog_slider_item') ){
		function university_education_get_blog_slider_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}			
			}
			$ret = '';
			$slider_class = '';
			$settings['thumbnail-size'];
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			
			$settings['num-fetch'] = (empty($settings['num-fetch']))? '20': $settings['num-fetch'];
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];
			$settings['heading'] = (empty($settings['heading']))? '20': $settings['heading'];
			$settings['caption'] = (empty($settings['caption']))? '20': $settings['caption'];
			$settings['element-style'] = (empty($settings['element-style']))? 'style-1': $settings['element-style'];
			$settings['element-type'] = (empty($settings['element-type']))? 'slider': $settings['element-type'];
			if($settings['element-type'] == 'simple-post'){$slider_class = '';$args['posts_per_page'] = 1;}else{$slider_class = 'bxslider';}
			
			$query = new WP_Query( $args );
			if($settings['element-style'] == 'style-1'){
				$ret .= '
					<div '.$margin_style.' class="kode_latst_post_lst">                	
                    	<ul data-mode="fade" class="post_bxslider '.esc_attr($slider_class).'">';
                            while($query->have_posts()){ $query->the_post();global $post;
							global $post;							
							$ret .= '
							<li class="kode_latest_blog kode-item">
                                <figure class="kode-ux">
									'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
                                </figure>
                                <h6>'.university_education_get_blog_info(array('category'), false, '','span').'</h6>
                                <div class="kode_latest_blog_des">
                                    <h6> <a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h6>
                                    <ul>
										'.university_education_get_blog_info(array('comment','views','author'), false, '','li').'
                                    </ul>
                                </div>
                            </li> ';
							}
                        $ret .= '    
                        </ul>
                    </div>';
			}else if($settings['element-style'] == 'style-2'){
				$ret .= '
				<div '.$margin_style.' data-mode="fade" class="kode-item-post-slider '.esc_attr($slider_class).'">';
				while($query->have_posts()){ $query->the_post();global $post;
					$ret .= '
					<div class="kode_achment_wrap kode-item">
						<figure class="kode-ux">
							'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
							<figcaption>
								<h6>'.esc_attr(get_the_date('D M, Y')).'</h6>
							</figcaption>
						</figure>
						<div class="kode_achment_des">
							<h5><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h5>
						</div>
					</div>';
				}
				$ret .= '</div>';
			}else if($settings['element-style'] == 'style-3'){
				$ret .= '<div class="kode_latest_blog_wrap">
				<ul '.$margin_style.' data-margin="30" data-width="570" data-min="2" data-max="4" data-mode="horizontal" class="'.esc_attr($slider_class).'">';
				while($query->have_posts()){ $query->the_post();global $post;				
				$ret .= '
					<li>
						<div class="kode_blog">
							<figure class="kode-ux">
								'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
							</figure>
							<div class="kode-caption">
								<div class="kode-user">
									<div class="kode-thumb">
										'.get_avatar(get_the_author_meta('ID'), 90).'									
									</div>
									<div class="kode_blog_posting">
										<h6>'.esc_attr(get_the_date()).'</h6>
										<h5>John Doe</h5>
									</div>
								</div>
								<div class="kode-text">
									<h4><a href="'.esc_url(get_permalink()).'">'.esc_attr(substr(get_the_title(),0,$settings['title-num-fetch'])).'</a></h4>
									<p>'.substr(get_the_content(),0,50).'</p>
									<a class="view" href="'.esc_url(get_permalink()).'">'.esc_attr__('View Blog','university-education').'</a>
								</div>
							</div>
						</div>						
					</li>';
				}
				$ret .= '</ul></div>';
				
			}else{}
					wp_reset_postdata();
			return $ret;
		}
	}	

	// Simple Column item
	if( !function_exists('university_education_get_simple_column_item') ){
		function university_education_get_simple_column_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			$settings['element-item-class'] = (empty($settings['element-item-class']))? '': $settings['element-item-class'];

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$ret  = '<div '.$item_id.' class="col-md-12 simple-column '.esc_attr($settings['element-item-class']).' " '.$margin_style.'>'.university_education_content_filter($settings['content']).'</div>';
			return $ret;
		}
	}	
	
	// column service item
	if( !function_exists('university_education_get_column_service_item') ){
		function university_education_get_column_service_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' . esc_attr($settings['element-item-id']) . '" ';
			$item_class = empty($settings['element-item-class'])? '': ' ' . esc_attr($settings['element-item-class']) . '';
			
			global $university_education_spaces, $custom_counter_service;
			$margin = (!empty($settings['margin-bottom']) && 
			$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$settings['style'] = empty($settings['style'])? 'type-1': $settings['style'];
			$settings['link'] = (empty($settings['link']))? '': $settings['link'];
			$settings['link-text'] = (empty($settings['link-text']))? '': $settings['link-text'];
			$settings['icon_type'] = empty($settings['icon_type'])? 'fa fa-lock': $settings['icon_type'];
			$settings['background-hover-color'] = empty($settings['background-hover-color'])? '': $settings['background-hover-color'];
			$settings['foreground-color'] = empty($settings['foreground-color'])? '': $settings['foreground-color'];
			
			$thumbnail = wp_get_attachment_image_src( $settings['service-image-box'] , $settings['thumbnail-size'] );
			if($settings['style'] == 'type-1'){
				$ret = '
				<style scoped>
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).' > span{color:'.esc_attr($settings['foreground-color']).'}
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).':before{background-color:'.esc_attr($settings['foreground-color']).'}
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).':hover{background-color:'.esc_attr($settings['background-hover-color']).'}
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).':hover:before{background-color:'.esc_attr($settings['foreground-color']).'}
				</style>
				<div class="col-md-12">
					<div '.$item_id.' class="kf_cur_catg_des color-'.esc_attr($custom_counter_service).' '.esc_attr($item_class).' kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>';
						if($settings['icon_type'] <> ''){
							$ret .= '<span><i class="' . esc_attr($settings['icon_type']) . '"></i></span>';
						}
						$ret .= '
						<div class="kf_cur_catg_capstion">
							<h5><a href="'.esc_url($settings['link']).'">' . university_education_text_filter($settings['title']) . '</a></h5>
							<p>' . university_education_text_filter(strip_tags(substr($settings['content'],0,150))) . '</p>
						</div>
					</div>
				</div>';
				$custom_counter_service++;
			}else if($settings['style'] == 'type-2'){
				$ret = '
				<div class="'.esc_attr($item_class).' ">
					<div '.$item_id.' class="kf_services_style_2 kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>
						<div class="kf_intro_des">
							<div class="kf_intro_des_caption">';
								if($settings['icon_type'] <> ''){
									$ret .= '<span><i class="' . esc_attr($settings['icon_type']) . '"></i></span>';
								}
								$ret .= '						
								<h6>' . university_education_text_filter($settings['title']) . '</h6>
								<p>' . university_education_text_filter(strip_tags(substr($settings['content'],0,150))) . '</p>
								<a class="kf-intro-view-all" href="'.esc_url($settings['link']).'">'.esc_attr__('See More','university-education').'</a>
							</div>
							<figure>
								<img src="'.esc_url($thumbnail[0]).'" alt="">
								<figcaption><a href="'.esc_url($settings['link']).'">' . university_education_text_filter($settings['link-text']) . '</a></figcaption>
							</figure>
						</div>
					</div>
				</div>';
			}else if($settings['style'] == 'type-3'){
				$ret = '
				<div class="'.esc_attr($item_class).' ">
					<div '.$item_id.' class="kf_services_style_3 kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>
						<div class="kf_intro_des">
							<div class="kf_intro_des_caption">';
							if($settings['icon_type'] <> ''){
								$ret .= '<span><i class=" '.esc_attr($settings['icon_type']).' "></i></span>';
							}
							$ret .= '
								<h6>' . university_education_text_filter($settings['title']) . '</h6>
								<p>' . university_education_text_filter(strip_tags(substr($settings['content'],0,150))) . '</p>
								<a class="kf-intro-view-all" href="'.esc_url($settings['link']).'">'.esc_attr__('View More','university-education').'</a>
							</div>
						</div>
					</div>
				</div>';
			}else{
				$ret = '
				<style scoped>
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).' > span{color:'.esc_attr($settings['foreground-color']).'}
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).':before{background-color:'.esc_attr($settings['foreground-color']).'}
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).':hover{background-color:'.esc_attr($settings['background-hover-color']).'}
					.kf_cur_catg_des.color-'.esc_attr($custom_counter_service).':hover:before{background-color:'.esc_attr($settings['foreground-color']).'}
				</style>
				<div class="col-md-12">
				<div '.$item_id.' class="kf_cur_catg_des color-'.esc_attr($custom_counter_service).' '.esc_attr($item_class).' kode-' . esc_attr($settings['style']) . '" '.$margin_style.'>';
					if($settings['icon_type'] <> ''){
						$ret .= '<span><i class="' . esc_attr($settings['icon_type']) . '"></i></span>';
					}
					$ret .= '
					<div class="kf_cur_catg_capstion">
						<h5>' . university_education_text_filter($settings['title']) . '</h5>
						<p>' . university_education_text_filter(strip_tags(substr($settings['content'],0,150))) . '</p>
					</div>
				</div>
				</div>';
				$custom_counter_service++;
			}
			return $ret;
		}
	}
	
	// content item
	if( !function_exists('university_education_get_content_item') ){
		function university_education_get_content_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			$ret = '<div '.$item_id.' class="k-content-container" '.$margin_style.'>';
			while ( have_posts() ){ the_post();
				$content = university_education_content_filter(get_the_content(), true); 
				
				//$ret .= '<div class="container">';
					//Show Title
					//Show Title
					if( empty($settings['show-title']) || $settings['show-title'] != 'disable' ){
						$ret .= '<div class="kode-item k-title"><h2>';
							$ret .= get_the_title();
						$ret .= '</h2></div>';
					}
					//Show Content
					if( empty($settings['show-content']) || $settings['show-content'] != 'disable' ){
						if(!empty($content)){
							$ret .= '<div class="kode-item k-content kode_news_detail">';
								$ret .= $content;
							$ret .= '</div>';
						}
					}
					ob_start();
				
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					
					$ret .= ob_get_contents();
					ob_end_clean();
					
				//$ret .= '</div>'; // Grid Container
			} // WHile Loop End
			$ret .= '</div>'; // End Element Container
			return $ret;
		}
	}	
	
	// content item
	if( !function_exists('university_education_get_default_content_item') ){
		function university_education_get_default_content_item( $settings ){			
			$item_id = empty($settings['page-item-id'])? '': ' id="' .esc_attr( $settings['page-item-id'] ). '" ';
			?>
			<div <?php echo esc_attr($item_id);?> class="k-content-container">
			<?php
			while ( have_posts() ){ the_post();
				$content = university_education_content_filter(get_the_content(), true); 
					//Show Title
					if(isset($university_education_post_option['enable-title-top']) && $university_education_post_option['enable-title-top'] == 'enable'){ ?>
						<div class="kode-item k-title">
							<h2><?php echo esc_attr(get_the_title());?></h2>
						</div>
					<?php }
					//Show Content
					if( empty($settings['show-content']) || $settings['show-content'] != 'disable' ){
						if(!empty($content)){ ?>
							<div class="kode-item k-content kode_news_detail">
								<?php echo university_education_content_filter(get_the_content(), true); ?>
							</div>
							<?php
						}
					} ?>
					<?php if(isset($university_education_post_option['show-author-section']) && $university_education_post_option['show-author-section'] == 'enable'){ ?>
					<div class="kode-single-detail">
						<div class="kode-admin-post">
							<figure><a href="#"><?php echo get_avatar(get_the_author_meta('ID'), 90); ?></a></figure>
							<div class="admin-info">
								<h4><?php the_author_posts_link(); ?></h4>
								<p><?php echo esc_attr(get_the_author_meta('description')); ?></p>
							</div>
						</div>
					</div>
					<?php }
					
					// if(isset($university_education_post_option['show-comment-section']) && $university_education_post_option['show-comment-section'] == 'enable'){
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					// }
				// Grid Container
			} // WHile Loop End
			?>
			</div>
		<?php
		}
	}

	// tab item
	if( !function_exists('university_education_get_tab_item') ){
		function university_education_get_tab_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr( $settings['margin-bottom'] ) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			$tabs = is_array($settings['tab'])? $settings['tab']: json_decode($settings['tab'], true);			
			$current_tab = 0;
			$ret = '';
			//$ret  = university_education_get_item_title($settings);	
			$ret .= '<div class="kode-item kode-tab-item '  . esc_attr($settings['style']) . '" ' . $item_id . $margin_style . '>';
			$ret .= '<div class="tab-title-wrapper" >';
			foreach( $tabs as $tab ){  $current_tab++;
				$ret .= '<h4 class="tab-title';
				$ret .= ($current_tab == intval($settings['initial-state']))? ' active" ': '" ';
				$ret .= empty($tab['kdf-tab-title-id'])? '>': 'id="' . esc_attr($tab['kdf-tab-title-id']) . '" >';
				$ret .= empty($tab['kdf-tab-icon-title'])? '': '<i class="' . esc_attr($tab['kdf-tab-icon-title']) . '" ></i>';				
				$ret .= '<span>' . university_education_text_filter($tab['kdf-tab-title']) . '</span></h4>';				
			}
			$ret .= '</div>';
			
			$current_tab = 0;
			$ret .= '<div class="tab-content-wrapper" >';
			foreach( $tabs as $tab ){  $current_tab++;
				$ret .= '<div class="tab-content';
				$ret .= ($current_tab == intval($settings['initial-state']))? ' active" >': '" >';
				$ret .= university_education_content_filter($tab['kdf-tab-content']) . '</div>';
							
			}	
			$ret .= '</div>';	
			$ret .= '<div class="clear"></div>';
			$ret .= '</div>'; // kode-tab-item 
			
			return $ret;
		}
	}		

	if( !function_exists('university_education_get_divider_item') ){
		function university_education_get_divider_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
			
			global $university_education_spaces;
			
			$margin = (!empty($settings['margin-bottom']))? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			//$style = empty($settings['size'])? '': ' style="width: ' . $settings['size'] . ';" ';
			$ret  = '<div class="clear"></div>';
			$ret .= '<div class="kode-item kode-divider-item" ' . $item_id . $margin_style . ' >';
			$ret .= '<div class="kode-divider"></div>';
			$ret .= '</div>';					
			
			return $ret;
		}
	}
	
	//project facts
	if( !function_exists('university_education_get_project_facts') ){
		function university_education_get_project_facts( $settings ){
		global $data;
		wp_register_script('kode-waypoints-min', UOE_PATH.'/framework/include/frontend_assets/default/js/waypoints-min.js', false, '1.0', true);
		wp_enqueue_script('kode-waypoints-min');
		
		static $university_educationpro_id = 1;
		$university_educationpro_id++;

		$html = '';
		$html .= '<script type="text/javascript">
		jQuery(document).ready(function($){
			/* ---------------------------------------------------------------------- */
			/*	Counter Functions
			/* ---------------------------------------------------------------------- */
			if($("#old-count-'.esc_js($university_educationpro_id).'").length){
				$("#old-count-'.esc_js($university_educationpro_id).'").counterUp({
					delay: 10,
					time: 1000
				});
			}
			
			/* ---------------------------------------------------------------------- */
			/*	Counter Functions
			/* ---------------------------------------------------------------------- */
			if($("#new-count-'.esc_js($university_educationpro_id).'").length){
				$("#new-count-'.esc_js($university_educationpro_id).'").counterUp({
					delay: 10,
					time: 1000
				});
			}
		});
		</script>';
		if($settings['style'] == 'style-1'){
			$html .= '
			<div class="edu2_counter_des">
				<span><i class="'.esc_attr($settings['icon']).'"></i></span>
				<h3 id="new-count-'.esc_attr($university_educationpro_id).'" class="counter">'.esc_attr($settings['value']).'</span>
				<h5>'.esc_attr($settings['sub-text']).'</h5>
			</div>';
		}else{
			$html .= '
			<div class="edu2_counter_des">
				<span><i class="'.esc_attr($settings['icon']).'"></i></span>
				<h3 id="new-count-'.esc_attr($university_educationpro_id).'" class="counter">'.esc_attr($settings['value']).'</span>
				<h5>'.esc_attr($settings['sub-text']).'</h5>
			</div>';
		}
		return $html;
		}
	}	
	
	