<?php
	/*	
	*	Kodeforest Woo Books Item Management File
	*	---------------------------------------------------------------------
	*	This file contains functions that help you get woo item
	*	---------------------------------------------------------------------
	*/
	
		
	if( !function_exists('university_education_get_woo_item') ){
		function university_education_get_woo_item( $settings = array() ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-woo-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			//$ret  = university_education_get_item_title($settings);
			$ret = '';
			$ret .= '<div class="woo-item-wrapper"  ' . $item_id . $margin_style . '>';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) || !empty($settings['tag']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}
				if( !empty($settings['tag']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'post_tag', 'field'=>'slug'));
				}				
			}

			// if( $settings['enable-sticky'] == 'enable' ){
				// if( get_query_var('paged') <= 1 ){
					// $sticky_args = $args;
					// $sticky_args['post__in'] = get_option('sticky_posts');
					// if( !empty($sticky_args['post__in']) ){
						// $sticky_query = new WP_Query($sticky_args);	
					// }
				// }
				// $args['post__not_in'] = get_option('sticky_posts', '');
			// }else{
				// $args['ignore_sticky_posts'] = 1;
			// }
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['paged'] = (get_query_var('paged'))? get_query_var('paged') : get_query_var('page');
			$args['paged'] = empty($args['paged'])? 1: $args['paged'];
			$query = new WP_Query( $args );
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];

			// merge query
			// if( !empty($sticky_query) ){
				// $query->posts = array_merge($sticky_query->posts, $query->posts);
				// $query->post_count = $sticky_query->post_count + $query->post_count;
			// }

			// set the excerpt length
			if( !empty($settings['num-excerpt']) ){
				global $university_education_excerpt_length; $university_education_excerpt_length = $settings['num-excerpt'];
				add_filter('excerpt_length', 'university_education_set_excerpt_length');
			} 
			
			// get woo by the woo style
			global $university_education_post_settings, $university_education_lightbox_id;
			$university_education_lightbox_id++;
			$university_education_post_settings['excerpt'] = intval($settings['num-excerpt']);
			$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];	
			$university_education_post_settings['woo-style'] = $settings['woo-style'];	
			
			$ret .= '<div class="woo-item-holder">';
			if($settings['woo-style'] == 'woo-full'){
				$ret .= '<div class="kode-woo-list-full kode-large-woo row">';
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= university_education_get_woo_full($query);
				$ret .= '</div>';
			}else if($settings['woo-style'] == 'woo-modern'){
				$ret .= '<div class="kode-woo-list-modern kode-list-woo row">';
				$settings['woo-size'] = 1;
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= university_education_get_woo_modern_full($query);			
				$ret .= '</div>';
			}else if($settings['woo-style'] == 'woo-list'){
				$ret .= '<div class="kode-woo-list-list kode-list-woo row">';
				$settings['woo-size'] = 1;
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= university_education_get_woo_list($query);			
				$ret .= '</div>';
			}else if(strpos($settings['woo-style'], 'woo-grid') !== false){
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$woo_size = $settings['woo-size'];	
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-woo-list-grid kode-grid-woo row">';
				$ret .= university_education_get_woo_grid($query, $woo_size, $settings['woo-slider']);			
				$ret .= '</div>';
			}else if(strpos($settings['woo-style'], 'woo-press') !== false){
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$woo_size = $settings['woo-size'];
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-woo-list-press kode-press-woo row">';
				$ret .= university_education_get_woo_press($query, $woo_size, '');			
				$ret .= '</div>';		
			}else if(strpos($settings['woo-style'], 'woo-grid-small') !== false){
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$woo_size = $settings['woo-size'];
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-woo-list-small kode-box-woo row">';
				$ret .= university_education_get_woo_grid_small($query, $woo_size, $settings['woo-slider']);
				$ret .= '</div>';		
			}else{
				$university_education_post_settings['thumbnail-size'] = $settings['uoe-woo-thumbnail-size'];
				$woo_size = $settings['woo-size'];
				$university_education_post_settings['title-num-fetch'] = $settings['title-num-fetch'];
				$ret .= '<div class="kode-woo-list-dd kode-grid-woo row">';
				$ret .= university_education_get_woo_grid($query, $woo_size, '');			
				$ret .= '</div>';
			}
			$ret .= '</div>';
			
			if( $settings['pagination'] == 'enable' ){
				$ret .= university_education_get_pagination($query->max_num_pages, $args['paged']);
			}
			$ret .= '</div>'; // woo-item-wrapper
			
			remove_filter('excerpt_length', 'university_education_set_excerpt_length');
			return $ret;
		}
	}	

	if( !function_exists('university_education_get_woo_info') ){
		function university_education_get_woo_info( $array = array(), $wrapper = true, $sep = '',$custom_wrap='div' ){
			global $university_education_theme_option; $ret = '';
			if( empty($array) ) return $ret;
			$exclude_meta = empty($university_education_theme_option['post-meta-data'])? array(): esc_attr($university_education_theme_option['post-meta-data']);
			
			foreach($array as $post_info){
				if( in_array($post_info, $exclude_meta) ) continue;
				if( !empty($sep) ) $ret .= $sep;
				
				switch( $post_info ){
					case 'time':
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-time"><i class="fa fa-clock-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))) . '">';
						$ret .= esc_attr(get_the_time());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';	
						break;
					case 'date':
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-date"><i class="fa fa-calendar-o"></i>';
						$ret .= '<a href="' . esc_url(get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))) . '">';
						$ret .= esc_attr(get_the_date());
						$ret .= '</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';	
						break;
					case 'tag':
						$tag = get_the_term_list(get_the_ID(), 'post_tag', '', '<span class="sep">,</span> ' , '' );
						if(empty($tag)) break;					
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-tag"><i class="fa fa-tag"></i>';
						$ret .= $tag;						
						$ret .= '</'.esc_attr($custom_wrap).'>';						
						break;
					case 'category':
						$category = get_the_term_list(get_the_ID(), 'category', '', '<span class="sep">,</span> ' , '' );
						if(empty($category)) break;
						
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-category"><i class="fa fa-list"></i>';
						$ret .= $category;					
						$ret .= '</'.esc_attr($custom_wrap).'>';					
						break;
					case 'comment':
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-comment"><i class="fa fa-comment-o"></i>';
						$ret .= '<a href="' . esc_url(get_permalink()) . '#respond" >' . esc_attr(get_comments_number()) . ' ' . esc_html__('Comment','university-education').'</a>';						
						$ret .= '</'.esc_attr($custom_wrap).'>';							
						break;
					case 'views':
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-views"><i class="fa fa-eye"></i>';
						$ret .= '<a href="' . esc_url(get_permalink()) . '" >' . esc_attr(university_education_get_post_views(get_the_ID())) . ' ' . esc_html__('Views','university-education').'</a>';
						$ret .= '</'.esc_attr($custom_wrap).'>';							
						break;	
					case 'author':
						ob_start();
						the_author_posts_link();
						$author = ob_get_contents();
						ob_end_clean();
						
						$ret .= '<'.esc_attr($custom_wrap).' class="woo-info woo-author"><i class="fa fa-user"></i>';
						$ret .= $author;
						$ret .= '</'.esc_attr($custom_wrap).'>';						
						break;						
				}
			}
			
			
			if($wrapper && !empty($ret)){
				return '<div class="kode-woo-info kode-info">' . $ret . '<div class="clear"></div></div>';
			}else if( !empty($ret) ){
				return $ret;
			}
			return '';
		}
	}
	
	
	if( !function_exists('university_education_get_woo_modern_full') ){
		function university_education_get_woo_modern_full($query){
			$ret = ''; $current_size = 0;
			$size = 1;
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-woo-full-modern">';
				$ret .= '<div class="kode-ux kode-woo-full-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}
	
	
	if( !function_exists('university_education_get_woo_simple') ){
		function university_education_get_woo_simple($query, $size){
			$ret = ''; $current_size = 0;
			
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-woo-simple">';
				$ret .= '<div class="kode-ux kode-woo-simple-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}	
	
	if( !function_exists('university_education_get_woo_grid_small') ){
		function university_education_get_woo_grid_small($query, $size,$woo_layout){
			$ret = ''; $current_size = 0;
			if($woo_layout == 'carousel'){ return university_education_get_woo_grid_carousel($query, $size); }
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-woo-grid-small">';
				$ret .= '<div class="kode-ux kode-woo-grid-small-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
			
		}
	}
	

	if( !function_exists('university_education_get_woo_widget') ){
		function university_education_get_woo_widget($query, $size){
			$ret = ''; $current_size = 0;
			// if($woo_layout == 'carousel'){ return university_education_get_woo_grid_carousel($query, $size); }
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-woo-widget">';
				$ret .= '<div class="kode-ux kode-woo-widget-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item			
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
		}
	}
	

	
	
	if( !function_exists('university_education_get_woo_grid') ){
		function university_education_get_woo_grid($query, $size, $woo_layout){
			if($woo_layout == 'carousel'){ return university_education_get_woo_grid_carousel($query, $size); }
		
			$ret = ''; $current_size = 0;			
			//$ret .= '<div class="kode-isotope" data-type="woo" data-layout="' . $woo_layout  . '" >';
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clearfix clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-ux kode-woo-grid-ux">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux				
				$ret .= '</div>'; // column_class
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			//$ret .= '</div>'; // close the kode-isotope
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	if( !function_exists('university_education_get_woo_grid_carousel') ){
		function university_education_get_woo_grid_carousel($query, $size){
			$ret = ''; 			
			$ret .= '<div class="owl-carousel owl-theme" data-slide="'.esc_attr($size).'" >';			
			while($query->have_posts()){ $query->the_post();
				$ret .= '<div class="item">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();					
				$ret .= '</div>'; // kode-item
			}
			$ret .= '</div>';
			$ret .= '<div class="clear"></div>';			
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	if( !function_exists('university_education_get_woo_list') ){
		function university_education_get_woo_list($query){
			$ret = '';$current_size = 0;
			$size = 1;
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}

				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-woo-medium">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux			
				$ret .= '</div>'; // kode-item
				$current_size ++;
			}
			wp_reset_postdata();
			
			return $ret;
		}
	}		
	
	if( !function_exists('university_education_get_woo_full') ){
		function university_education_get_woo_full($query){
			$ret = '';$current_size = 0;
		
			$size = 1;
			while($query->have_posts()){ $query->the_post();
				if( $current_size % $size == 0 ){
					$ret .= '<div class="clear"></div>';
				}
				$ret .= '<div class="' . esc_attr(university_education_get_column_class('1/' . $size)) . '">';
				$ret .= '<div class="kode-item kode-woo-single-full ">';
				ob_start();
				
				get_template_part('single/content');
				$ret .= ob_get_contents();
				
				ob_end_clean();			
				$ret .= '</div>'; // kode-ux
				$ret .= '</div>'; // kode-item
				$current_size++;
			}
			wp_reset_postdata();
			
			return $ret;
		}
	}	

?>