<?php
	/*	
	*	Kodeforest Media Center File
	*	---------------------------------------------------------------------
	*	This file contains functions that manage the media in the theme
	*	---------------------------------------------------------------------
	*/	
	
		
	if( !function_exists('university_education_get_social_shares') ){
		function university_education_get_social_shares(){	
			global $university_education_theme_option;
			$thumbnail = array();
			$page_title = rawurlencode(get_the_title());
			$current_url = KODE_HTTP . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];?>
			<ul class="kf_blog_social_icon">
				<?php if(isset($university_education_theme_option['facebook-share']) && $university_education_theme_option['facebook-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-facebook" href="http://www.facebook.com/share.php?u=<?php echo esc_url($current_url); ?>" data-original-title="Facebook"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['digg-share']) && $university_education_theme_option['digg-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-digg" href="http://digg.com/submit?url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Digg"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['google-plus-share']) && $university_education_theme_option['google-plus-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-google-plus" href="https://plus.google.com/share?url=<?php echo esc_url($current_url); ?>" data-original-title="Google PLus"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['linkedin-share']) && $university_education_theme_option['linkedin-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&#038;url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Linkedin"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['my-space-share']) && $university_education_theme_option['my-space-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-steam" href="http://www.myspace.com/Modules/PostTo/Pages/?u=<?php echo esc_url($current_url); ?>" data-original-title="MySpace"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['pinterest-share']) && $university_education_theme_option['pinterest-share'] == 'enable'){ $thumbnail_id = get_post_thumbnail_id( get_the_ID() );$thumbnail = wp_get_attachment_image_src( $thumbnail_id , 'large' ); ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url($current_url); ?>&media=<?php echo esc_url($thumbnail[0]); ?>" data-original-title="Pinterest"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['reddit-share']) && $university_education_theme_option['reddit-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-reddit" href="http://reddit.com/submit?url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Reddit"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['stumble-upon-share']) && $university_education_theme_option['stumble-upon-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-stumbleupon" href="http://www.stumbleupon.com/submit?url=<?php echo esc_url($current_url); ?>&#038;title=<?php echo esc_attr($page_title); ?>" data-original-title="Stumble"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['twitter-share']) && $university_education_theme_option['twitter-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-twitter" href="http://twitter.com/home?status=<?php echo esc_attr(str_replace('%26%23038%3B', '%26', $page_title)) . ' - ' . esc_url($current_url); ?>" data-original-title="Twitter"></a></li><?php }?>
				<?php if(isset($university_education_theme_option['instagram-share']) && $university_education_theme_option['instagram-share'] == 'enable'){ ?><li><a title="" data-placement="top" data-toggle="tooltip" class="thbg-colorhover fa fa-instagram" href="http://instagram.com/home?status=<?php echo esc_attr(str_replace('%26%23038%3B', '%26', $page_title)) . ' - ' . esc_url($current_url); ?>" data-original-title="Instagram"></a></li><?php }?>
			</ul>
			<?php 
		}
	}
	
	$university_education_header_social_icon = array(
		'delicious'		=> esc_html__('Delicius','university-education'), 
		'digg'			=> esc_html__('Digg','university-education'),
		'facebook'		=> esc_html__('Facebook','university-education'), 
		'flickr'		=> esc_html__('Flickr','university-education'),
		'google-plus' 	=> esc_html__('Google Plus','university-education'),						
		'linkedin' 		=> esc_html__('Linkedin','university-education'),		
		'pinterest' 	=> esc_html__('Pinterest','university-education'),		
		'skype'			=> esc_html__('Skype','university-education'),
		'stumble-upon' 	=> esc_html__('Stumble Upon','university-education'),
		'tumblr' 		=> esc_html__('Tumblr','university-education'),
		'twitter' 		=> esc_html__('Twitter','university-education'),
		'vimeo' 		=> esc_html__('Vimeo','university-education'),
		'youtube' 		=> esc_html__('Youtube','university-education'),
		'instagram' 		=> esc_html__('Instagram','university-education'),
	);	
	
	add_filter('university_education_themeoption_panel', 'university_education_register_header_social_option');
	if( !function_exists('university_education_register_header_social_option') ){
		function university_education_register_header_social_option( $array ){		
			if( empty($array['overall-elements']['options']) ) return $array;
			
			global $university_education_header_social_icon;
			$header_social = array( 									
				'title' => esc_html__('Header Social', 'university-education'),
				'options' => array(
				)
			);
				
			foreach( $university_education_header_social_icon as $social_slug => $social_name ){
				$header_social['options'][$social_slug . '-header-social'] = array(
					'title' => $social_name . ' ' . esc_html__('Header Social', 'university-education'),
					'type' => 'text',
					'description' => 'Enter URL of your social profile here.'										
				);
				
			}
			
			$array['overall-elements']['options']['header-social'] = $header_social;
			return $array;
		}
	}
	
	
	
	if( !function_exists('university_education_print_header_social') ){
		function university_education_print_header_social(){
			global $university_education_header_social_icon, $university_education_theme_option;
			$type = empty($university_education_theme_option['header-social-type'])? 'dark': $university_education_theme_option['header-social-type'];
			
			foreach( $university_education_header_social_icon as $social_slug => $social_name ){
				if( !empty($university_education_theme_option[$social_slug . '-header-social']) ){ ?>
				<div class="social-icon">
					<a href="<?php echo esc_attr($university_education_theme_option[$social_slug . '-header-social']); ?>" target="_blank" >
						<img width="32" height="32" src="<?php echo UOE_PATH . '/images/' . $type . '/social-icon/' . $social_slug . '.png'; ?>" alt="<?php echo esc_attr($social_name); ?>" />
					</a>
				</div>
				<?php				
				}
			}
			echo '<div class="clear"></div>';
		}
	}	
	
	if( !function_exists('university_education_print_header_social_icon') ){
		function university_education_print_header_social_icon($class=''){
			global $university_education_header_social_icon, $university_education_theme_option;
			$type = empty($university_education_theme_option['header-social-type'])? 'dark': esc_attr($university_education_theme_option['header-social-type']); ?>
			<ul class="<?php echo esc_attr($type).' '.$class;?> kode-team-network ">
				<?php if(isset($university_education_theme_option['delicious-header-social']) && $university_education_theme_option['delicious-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['delicious-header-social']);?>"><i class="fa fa-delicious"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['digg-header-social']) && $university_education_theme_option['digg-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['digg-header-social']);?>"><i class="fa fa-digg"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['facebook-header-social']) && $university_education_theme_option['facebook-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['facebook-header-social']);?>"><i class="fa fa-facebook"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['flickr-header-social']) && $university_education_theme_option['flickr-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['flickr-header-social']);?>"><i class="fa fa-flickr"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['google-plus-header-social']) && $university_education_theme_option['google-plus-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['google-plus-header-social']);?>"><i class="fa fa-google-plus"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['linkedin-header-social']) && $university_education_theme_option['linkedin-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['linkedin-header-social']);?>"><i class="fa fa-linkedin"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['pinterest-header-social']) && $university_education_theme_option['pinterest-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['pinterest-header-social']);?>"><i class="fa fa-pinterest"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['skype-header-social']) && $university_education_theme_option['skype-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['skype-header-social']);?>"><i class="fa fa-skype"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['stumble-header-social']) && $university_education_theme_option['stumble-upon-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['stumble-upon-header-social']);?>"><i class="fa fa-stumbleupon"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['tumblr-header-social']) && $university_education_theme_option['tumblr-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['tumblr-header-social']);?>"><i class="fa fa-tumblr"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['twitter-header-social']) && $university_education_theme_option['twitter-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['twitter-header-social']);?>"><i class="fa fa-twitter"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['vimeo-header-social']) && $university_education_theme_option['vimeo-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['vimeo-header-social']);?>"><i class="fa fa-vimeo-square"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['youtube-header-social']) && $university_education_theme_option['youtube-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['youtube-header-social']);?>"><i class="fa fa-youtube"></i></a></li><?php }?>
				<?php if(isset($university_education_theme_option['instagram-header-social']) && $university_education_theme_option['instagram-header-social'] <> ''){ ?><li><a target="_blank" href="<?php echo esc_url($university_education_theme_option['instagram-header-social']);?>"><i class="fa fa-instagram"></i></a></li><?php }?>
			</ul>	
			<?php				
		}
	}
	
	if( !function_exists('university_education_print_header_social_icon_unechod') ){
		function university_education_print_header_social_icon_unechod($class=''){
			global $university_education_header_social_icon, $university_education_theme_option;
			$type = empty($university_education_theme_option['header-social-type'])? 'dark': esc_attr($university_education_theme_option['header-social-type']); 
			$html_social = '<ul class="'.esc_attr($type).' '.$class.' kode-team-network ">';
				if($university_education_theme_option['delicious-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['delicious-header-social']).'"><i class="fa fa-delicious"></i></a></li>';
				}
				if($university_education_theme_option['digg-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['digg-header-social']).'"><i class="fa fa-digg"></i></a></li>';
				}
				if($university_education_theme_option['flickr-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['flickr-header-social']).'"><i class="fa fa-flickr"></i></a></li>';
				}
				if($university_education_theme_option['google-plus-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['google-plus-header-social']).'"><i class="fa fa-google-plus"></i></a></li>';
				}
				if($university_education_theme_option['facebook-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['facebook-header-social']).'"><i class="fa fa-facebook"></i></a></li>';
				}
				if($university_education_theme_option['linkedin-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['linkedin-header-social']).'"><i class="fa fa-linkedin"></i></a></li>';
				}
				if($university_education_theme_option['pinterest-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['pinterest-header-social']).'"><i class="fa fa-pinterest"></i></a></li>';
				}
				if($university_education_theme_option['skype-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['skype-header-social']).'"><i class="fa fa-skype"></i></a></li>';
				}
				if($university_education_theme_option['stumble-upon-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['stumble-upon-header-social']).'"><i class="fa fa-stumble-upon"></i></a></li>';
				}
				if($university_education_theme_option['tumblr-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['tumblr-header-social']).'"><i class="fa fa-tumblr"></i></a></li>';
				}
				if($university_education_theme_option['twitter-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['twitter-header-social']).'"><i class="fa fa-twitter"></i></a></li>';
				}
				if($university_education_theme_option['vimeo-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['vimeo-header-social']).'"><i class="fa fa-vimeo"></i></a></li>';
				}
				if($university_education_theme_option['youtube-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['youtube-header-social']).'"><i class="fa fa-youtube"></i></a></li>';
				}		
				if($university_education_theme_option['instagram-header-social'] <> ''){
					$html_social .= '<li><a href="'.esc_url($university_education_theme_option['instagram-header-social']).'"><i class="fa fa-instagram"></i></a></li>';
				}						
			$html_social .= '</ul>';
			
			return $html_social;
			
		}
	}
	
	add_filter('university_education_themeoption_panel', 'university_education_register_social_shares_option');
	if( !function_exists('university_education_register_social_shares_option') ){
		function university_education_register_social_shares_option( $array ){	
			if( empty($array['overall-elements']['options']) ) return $array;
			
			$university_education_social_shares = array(
				'digg'			=> esc_html__('Digg','university-education'),			
				'facebook'		=> esc_html__('Facebook','university-education'), 
				'google-plus' 	=> esc_html__('Google Plus','university-education'),	
				'linkedin' 		=> esc_html__('Linkedin','university-education'),
				'my-space' 		=> esc_html__('My Space','university-education'),
				'pinterest' 	=> esc_html__('Pinterest','university-education'),
				'reddit' 		=> esc_html__('Reddit','university-education'),
				'stumble-upon' 	=> esc_html__('Stumble Upon','university-education'),
				'twitter' 		=> esc_html__('Twitter','university-education'),
				'instagram' 		=> esc_html__('Instagram','university-education'),
			);	
			$header_social = array( 									
				'title' => esc_html__('Social Shares', 'university-education'),
				'options' => array(
					'enable-social-share'=> array(
						'title' => esc_html__('Enable Social Share' ,'university-education'),
						'type' => 'checkbox',
						'description' => 'Enable this option to show the social shares below each post'
					)
				)
			);
				
			foreach( $university_education_social_shares as $social_slug => $social_name ){
				$header_social['options'][$social_slug . '-share'] = array(
					'title' => $social_name,
					'type' => 'checkbox',
					'description' => 'Enable this option to show the social Icon under post.'					
				);
			}
			
			$array['overall-elements']['options']['social-shares'] = $header_social;
			return $array;
		}
	}	
	
	if( !function_exists('university_education_get_video') ){
		function university_education_get_video($video, $size = 'full'){
			if( empty($video) ) return '';
			
			$video_size = university_education_get_video_size($size);
			$width = $video_size['width']; 
			$height = $video_size['height']; 

			// video shortcode
			if(preg_match('#^\[video\s.+\[/video\]#', $video, $match)){ 
				return do_shortcode($match[0]);
				
			// embed shortcode
			}else if(preg_match('#^\[embed.+\[/embed\]#', $video, $match)){ 
				global $wp_embed; 
				return $wp_embed->run_shortcode($match[0]);
				
			// youtube link
			}else if(strpos($video, 'youtube') !== false){
				preg_match('#[?&]v=([^&]+)(&.+)?#', $video, $id);
				$id[2] = empty($id[2])? '': $id[2];
				return '<iframe src="http://www.youtube.com/embed/' . esc_attr($id[1]) . '?wmode=transparent' . esc_attr($id[2]) . '" width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" ></iframe>';
			
			// youtu.be link
			}else if(strpos($video, 'youtu.be') !== false){
				preg_match('#youtu.be\/([^?&]+)#', $video, $id);
				return '<iframe src="http://www.youtube.com/embed/' . esc_attr($id[1]) . '?wmode=transparent" width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" ></iframe>';
			
			// vimeo link
			}else if(strpos($video, 'vimeo') !== false){
				preg_match('#https?:\/\/vimeo.com\/(\d+)#', $video, $id);
				return '<iframe src="http://player.vimeo.com/video/' . esc_attr($id[1]) . '?title=0&amp;byline=0&amp;portrait=0" width="' . esc_attr($width) . '" height="' . esc_attr($height) . '"></iframe>';
			
			// another link
			}else if(preg_match('#^https?://\S+#', $video, $match)){ 	
				$path_parts = pathinfo($match[0]);
				if( !empty($path_parts['extension']) ){
					return do_shortcode('[video width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" src="' . esc_url($match[0]) . '" ][/video]');
				}else{
					global $wp_embed; 
					$video_embed = '[embed width="' . esc_attr($width) . '" height="' . esc_attr($height) . '" ]' . esc_url($match[0]) . '[/embed]';
					return $wp_embed->run_shortcode($video_embed);
				}				
			}
			return '';
		}
	}	
	
	// use for printing the image from  image id
	if( !function_exists('university_education_get_image') ){
		function university_education_get_image($image, $size = 'full', $link = array(), $attr = ''){
			if( empty($image) ) return '';
		
			if( is_numeric($image) ){
				$alt_text = get_post_meta($image , '_wp_attachment_image_alt', true);	
				$image_src = wp_get_attachment_image_src($image, $size);	
				if( empty($image_src) ) return '';
				
				if( $link === true ){ 
					$image_full = wp_get_attachment_image_src($image, 'full');
					$link = array('url'=>esc_url($image_full[0]));
				}else if( !empty($link) && empty($link['url']) ){
					$image_full = wp_get_attachment_image_src($image, 'full');
					$link['url'] = esc_url($image_full[0]);				
				}
				$ret = '<img src="' . esc_url($image_src[0]) . '" alt="' . esc_attr($alt_text) . '" width="' . esc_attr($image_src[1]) .'" height="' . esc_attr($image_src[2]) . '" ' . $attr . '/>';
			}else{
				if( $link === true ){ 
					$link = array('url'=>esc_url($image)); 
				}else if( !empty($link) && empty($link['url']) ){
					$link['url'] = esc_url($image);		
				}
				$ret = '<img src="' . esc_url($image) . '" alt="" ' . $attr . ' />';
			}
			
			if( !empty($link) ){
				$pretty_photo  = '<a href="' . esc_url($link['url']) . '" ';
				$pretty_photo .= (empty($link['id']))? '': 'data-pretty-group="kode-gal-' . $link['id'] . '" ';
				$pretty_photo .= (!empty($link['type']) && $link['type'] == 'link')? '': 'data-rel="prettyphoto[]" ';
				$pretty_photo .= (!empty($link['type']) && $link['type'] == 'video')? 'data-pretty-type="iframe" ': '';
				$pretty_photo .= (!empty($link['new-tab']) && $link['new-tab'] == 'enable')? 'target="_blank" ': '';
				$pretty_photo .= '>' . $ret;
				$pretty_photo .= (!empty($link['close-tag']))? '': '</a>';
				return $pretty_photo;
			}
			return $ret;
		}
	}
	
	if( !function_exists('university_education_get_attachment_info') ){
		function university_education_get_attachment_info($attachment_id, $type = '') {
			$attachment = get_post($attachment_id);
			if( !empty($attachment) ){
				$ret = array(
					'caption' => $attachment->post_excerpt,
					'description' => $attachment->post_content,
					'title' => $attachment->post_title
				);
				
				if( !empty($type) ) return $ret[$type];
				return $ret;
			}
			return array();
		}	
	}	
	
	// use for printing slider
	if( !function_exists('university_education_get_slider_item') ){
		function university_education_get_slider_item( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' . esc_attr($settings['element-item-id']) . '" ';
			$settings['slider-style'] = (empty($settings['slider-style']))? '5': $settings['slider-style'];
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			$settings['thumbnail-size'] = 'full';
			$ret  = '<div class="kode-item '.esc_attr($settings['slider-style']).' kode-slider-item" ' . $item_id . $margin_style . ' >';
			$ret .= university_education_get_slider($settings['slider'], $settings['thumbnail-size'], $settings['slider-type']);
			$ret .= '</div>';
			return $ret;
		}
	}
	
	// use for printing post slider
	if( !function_exists('university_education_get_post_slider_item') ){
		function university_education_get_post_slider_item( $settings ){
			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			$slide_order = array();
			$slide_data = array();
			
			// query posts section
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			$args['ignore_sticky_posts'] = 1;

			if( is_numeric($settings['category']) ){
				$args['category'] = (empty($settings['category']))? '': $settings['category'];	
			}else{ 
				if( !empty($settings['category']) || !empty($settings['tag']) ){
					$args['tax_query'] = array('relation' => 'OR');
					
					if( !empty($settings['category']) ){
						array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
					}
					if( !empty($settings['tag']) ){
						array_push($args['tax_query'], array('terms'=>explode(',', $settings['tag']), 'taxonomy'=>'post_tag', 'field'=>'slug'));
					}				
				}	
			}
			$query = new WP_Query( $args );	
			
			// set the excerpt length
			global $university_education_theme_option, $university_education_excerpt_length, $university_education_excerpt_read_more; 
			$university_education_excerpt_read_more = false;
			$university_education_excerpt_length = $settings['num-excerpt'];
			add_filter('excerpt_length', 'university_education_set_excerpt_length');

			global $post;
			while($query->have_posts()){ $query->the_post();
				$image_id = get_post_thumbnail_id();
				
				if( !empty($image_id) ){
					$slide_order[] = $image_id;
					$slide_data[$image_id] = array(	
						'title'=> esc_attr(get_the_title()),
						'slide-link'=> 'url',
						'url'=> esc_url(get_permalink()),
						'new-tab'=> 'disable',
						'caption-position'=>$settings['caption-style']
					);
					
					if( $settings['style'] == 'no-excerpt' ){
						$slide_data[$image_id]['caption']  = '<div class="kode-caption-date" >';
						$slide_data[$image_id]['caption'] .= '<i class="fa fa-calendar"></i>';
						$slide_data[$image_id]['caption'] .= esc_attr(get_the_time(get_option('date_format')));				
						$slide_data[$image_id]['caption'] .= '</div>';				
						
						$slide_data[$image_id]['caption'] .= '<div class="kode-title-link" >';
						$slide_data[$image_id]['caption'] .= '<i class="fa fa-angle-right" ></i>';
						$slide_data[$image_id]['caption'] .= '</div>';		
					}else{
						$slide_data[$image_id]['caption']  = '<div class="blog-info blog-date"><i class="fa fa-calendar"></i>';
						$slide_data[$image_id]['caption'] .= esc_attr(get_the_time(get_option('date_format')));		
						$slide_data[$image_id]['caption'] .= '</div>';
						$slide_data[$image_id]['caption'] .= '<div class="blog-info blog-comment"><i class="fa fa-comment"></i>';
						$slide_data[$image_id]['caption'] .= esc_attr(get_comments_number());
						$slide_data[$image_id]['caption'] .= '</div>';					
						$slide_data[$image_id]['caption'] .= '<div class="clear"></div>';					
						$slide_data[$image_id]['caption'] .= '<p>'.esc_attr(get_the_excerpt()).'</p>';
					}
				}
			}	
			
			$university_education_excerpt_read_more = true;
			remove_filter('excerpt_length', 'university_education_set_excerpt_length');
			
			if( $settings['style'] == 'no-excerpt' ){
				$settings['caption-style'] = 'no-excerpt';
			}
			
			$ret  = '<div class="kode-item kode-post-slider-item style-' . $settings['caption-style'] . '" ' . $item_id . $margin_style . ' >';
			$ret .= university_education_get_slider(array($slide_order, $slide_data), $settings['thumbnail-size'], 'bxslider');
			$ret .= '</div>';
			return $ret;
		}
	}	
	
	// use for printing slider
	if( !function_exists('university_education_get_slider') ){
		function university_education_get_slider( $slider_data, $thumbnail_size, $slider_type = 'flexslider' ){
			if( is_array($slider_data) ){
				$slide_order = $slider_data[0];
				$slide_data = $slider_data[1];
			}else{
				$slider_option = json_decode($slider_data, true);
				$slide_order = $slider_option[0];
				$slide_data = $slider_option[1];			
			}
			
			$slides = array();
			$slide_order = empty($slide_order)? array(): $slide_order;
			foreach($slide_order as $slide){
				$slides[$slide] = $slide_data[$slide];
			}
				
			if($slider_type == 'flexslider'){
				return university_education_get_flex_slider($slides, array('size'=> $thumbnail_size));
			}else if($slider_type == 'nivoslider'){
				return university_education_get_nivo_slider($slides, array('size'=> $thumbnail_size));
			}else if($slider_type == 'bxslider'){
				return university_education_get_bx_slider($slides, array('size'=> $thumbnail_size));
			}else{
				return 'slider is not defined';
			}
			
		}
	}	
	
	
	
	
	// use for printing flex slider
	if( !function_exists('university_education_get_flex_slider') ){
		function university_education_get_flex_slider($slides, $settings = array()){
			global $university_education_theme_option, $university_education_gallery_id; $university_education_gallery_id++;
			
			$ret  = '<div class="flexslider" ';
			$ret .= empty($settings['pausetime'])? 'data-pausetime="' . esc_attr($university_education_theme_option['flex-pause-time']) . '" ': 
						'data-pausetime="' . esc_attr($settings['pausetime']) . '" ';
			$ret .= empty($settings['slidespeed'])? 'data-slidespeed="' . esc_attr($university_education_theme_option['flex-slide-speed']) . '" ': 
						'data-slidespeed="' . esc_attr($settings['slidespeed']) . '" ';			
			$ret .= empty($settings['effect'])? 'data-effect="' . esc_attr($university_education_theme_option['flex-slider-effects']) . '" ': 
						'data-effect="' . esc_attr($settings['effect']) . '" ';	
						
			$ret .= empty($settings['columns'])? '': 'data-columns="' . esc_attr($settings['columns']) . '" ';
			$ret .= empty($settings['carousel'])? '': 'data-type="carousel" ';
			$ret .= empty($settings['nav-container'])? '': 'data-nav-container="' . esc_attr($settings['nav-container']) . '" ';
			$ret .= '>';
			$ret .= '<ul class="slides" >';
			$title_font_size = '';
			if(isset($university_education_theme_option['title-font-size'])){
				if($university_education_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($university_education_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($university_education_theme_option['caption-font-size'])){
				if($university_education_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($university_education_theme_option['caption-font-size']).'px';
				}
			}
			$btn_bg_color_style = '';
			if(isset($university_education_theme_option['caption-btn-color-switch']) && $university_education_theme_option['caption-btn-color-switch'] == 'enable'){
				$btn_bg_color_style = 'style="color:'.esc_attr($university_education_theme_option['caption-btn-color']).';background:'.esc_attr($university_education_theme_option['caption-btn-color-bg']).'"';
			}
			
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){
				$ret .= '<li>';
				
				if( is_array($slide) ){

					// flex slider caption
					$caption = '';
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
					
						$caption .= '<div class="kode-caption-wrapper position-' . esc_attr($slide['caption-position']) . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($university_education_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($university_education_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div '.$btn_bg_color_style.' class="kode_btn_store_1">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-slider-caption
						$caption .= '</div>'; // kode-slider-caption-wrapper
						$caption .= '</div>';
					}				
				
					// flex slider link
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size'])) . $caption;
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab']), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}
				}else{
					$ret .= university_education_get_image(esc_attr($slide), esc_attr($settings['size']), array('id'=>esc_attr($university_education_gallery_id)));
				}
				$ret .= '</li>';
			}
			$ret .= '</ul>';
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	
	
	// breaking news item
	if( !function_exists('university_education_get_wp_post_slider') ){
		function university_education_get_wp_post_slider( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' .esc_attr( $settings['element-item-id'] ). '" ';
	
			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' .esc_attr( $settings['margin-bottom'] ). 'px;': '';
			$margin_style = (!empty($margin))? ' style="' .esc_attr( $margin ). '" ': '';
			
			// query post and sticky post
			$args = array('post_type' => 'post', 'suppress_filters' => false);
			if( !empty($settings['category']) ){
				$args['tax_query'] = array('relation' => 'OR');
				
				if( !empty($settings['category']) ){
					array_push($args['tax_query'], array('terms'=>explode(',', $settings['category']), 'taxonomy'=>'category', 'field'=>'slug'));
				}			
			}
			$ret = '';
			$slider_class = '';
			$settings['thumbnail-size'];
			$args['posts_per_page'] = (empty($settings['num-fetch']))? '5': $settings['num-fetch'];
			$args['orderby'] = (empty($settings['orderby']))? 'post_date': $settings['orderby'];
			$args['order'] = (empty($settings['order']))? 'desc': $settings['order'];
			
			$settings['num-fetch'] = (empty($settings['num-fetch']))? '20': $settings['num-fetch'];
			$settings['title-num-fetch'] = (empty($settings['title-num-fetch']))? '20': $settings['title-num-fetch'];
			
			$query = new WP_Query( $args );
	
			$ret .= '
			<div '.$margin_style.' class="kode_banner kode_post_slider kode-slider-item">                	
				<ul data-mode="fade" data-min="1" data-max="1" class="bxslider '.esc_attr($slider_class).'">';
					while($query->have_posts()){
					$query->the_post();
					global $post;							
					$ret .= '
					<li>
					'.get_the_post_thumbnail( $post->ID, $settings['thumbnail-size']).'
					<div class="kode-caption">
						'.university_education_get_blog_info(array('category'), false, '','h6').'
						<h2 class="kode-caption-title"><a href="'.esc_url(get_permalink()).'">'.esc_attr(get_the_title()).'</a></h2>
						<p class="kode-caption-text">'.esc_attr(substr(get_the_content(),0,100)).'</p>
						'.do_shortcode('[button class="kode-linksection hvr-radial-out" style="type-1" icon="fa fa-angle-right" iconcolor="#000000" iconbgcolor="#dd9933" url="'.esc_url(get_permalink()).'" target="_blank" color="#ffffff" bgcolor="#dd3333" size="small" ]Read More[/button]').'
					</div>
					</li>';
					}
				$ret .= '    
				</ul>
			</div>';
			
			wp_reset_postdata();
			
			return $ret;
		}
	}	
	
	// use for printing flex slider
	if( !function_exists('university_education_get_wp_post_slider') ){
		function university_education_get_wp_post_slider($settings = array()){
			global $university_education_theme_option, $university_education_gallery_id; $university_education_gallery_id++;
			
			
			$ret .= '<ul class="slides" >';
			$title_font_size = '';
			if(isset($university_education_theme_option['title-font-size'])){
				if($university_education_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($university_education_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($university_education_theme_option['caption-font-size'])){
				if($university_education_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($university_education_theme_option['caption-font-size']).'px';
				}
			}
			$btn_bg_color_style = '';
			if(isset($university_education_theme_option['caption-btn-color-switch']) && $university_education_theme_option['caption-btn-color-switch'] == 'enable'){
				$btn_bg_color_style = 'style="color:'.esc_attr($university_education_theme_option['caption-btn-color']).';background:'.esc_attr($university_education_theme_option['caption-btn-color-bg']).'"';
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){
				$ret .= '<li>';
				
				if( is_array($slide) ){

					// flex slider caption
					$caption = '';
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
					
						$caption .= '<div class="kode-caption-wrapper position-' . esc_attr($slide['caption-position']) . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($university_education_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($university_education_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div '.$btn_bg_color_style.' class="kode_btn_store_1">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-slider-caption
						$caption .= '</div>'; // kode-slider-caption-wrapper
						$caption .= '</div>';
					}				
				
					// flex slider link
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size'])) . $caption;
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab']), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}
				}else{
					$ret .= university_education_get_image(esc_attr($slide), esc_attr($settings['size']), array('id'=>esc_attr($university_education_gallery_id)));
				}
				$ret .= '</li>';
			}
			$ret .= '</ul>';
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	
	// use for printing bx slider
	if( !function_exists('university_education_get_bx_slider') ){
		function university_education_get_bx_slider($slides, $settings = array()){
			global $university_education_theme_option, $university_education_gallery_id; $university_education_gallery_id++;
			
			$ret  = '<div class="kode-bxslider" ';
			$ret .= empty($settings['pausetime'])? 'data-pausetime="' . esc_attr($university_education_theme_option['bx-pause-time']) . '" ': 
						'data-pausetime="' . esc_attr($settings['pausetime']) . '" ';
			$ret .= empty($settings['slidespeed'])? 'data-slidespeed="' . esc_attr($university_education_theme_option['bx-slide-speed']) . '" ': 
						'data-slidespeed="' . esc_attr($settings['slidespeed']) . '" ';			
			$ret .= empty($settings['effect'])? 'data-effect="' . esc_attr($university_education_theme_option['bx-slider-effects']) . '" ': 
						'data-effect="' . esc_attr($settings['effect']) . '" ';	
						
			$ret .= empty($settings['columns'])? '': 'data-columns="' . esc_attr($settings['columns']) . '" ';
			$ret .= empty($settings['carousel'])? '': 'data-type="carousel" ';
			//$ret .= empty($settings['nav-container'])? '': 'data-nav-container="' . $settings['nav-container'] . '" ';
			$ret .= '>';
			if($university_education_theme_option['bx-slider-effects'] == 'slide'){
				$ret .= '<ul data-min="'.esc_attr($university_education_theme_option['bx-min-slide']).'" data-max="'.esc_attr($university_education_theme_option['bx-max-slide']).'" data-margin="'.esc_attr($university_education_theme_option['bx-slide-margin']).'" data-mode="horizontal" class="bxslider" >';
			}else{
				$ret .= '<ul data-mode="fade" class="bxslider">';
			}
			$title_font_size = '';
			if(isset($university_education_theme_option['title-font-size'])){
				if($university_education_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($university_education_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($university_education_theme_option['caption-font-size'])){
				if($university_education_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($university_education_theme_option['caption-font-size']).'px';
				}
			}
			$btn_bg_color_style = '';
			if(isset($university_education_theme_option['caption-btn-color-switch']) && $university_education_theme_option['caption-btn-color-switch'] == 'enable'){
				$btn_bg_color_style = 'style="color:'.esc_attr($university_education_theme_option['caption-btn-color']).';background:'.esc_attr($university_education_theme_option['caption-btn-color-bg']).'"';
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){
				$ret .= '<li>';
				
				if( is_array($slide) ){

					// flex slider caption
					$caption = '';
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
					
						$caption .= '<div class="kode-caption-wrapper position-' . esc_attr($slide['caption-position']) . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($university_education_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($university_education_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div '.$btn_bg_color_style.' class="kode_btn_store_1">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-slider-caption
						$caption .= '</div>'; // kode-slider-caption-wrapper
						$caption .= '</div>';
					}				
				
					// bx slider link
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size'])) . $caption;
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab']), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= university_education_get_image(esc_attr($slide_id), esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($university_education_gallery_id), 'close-tag'=>true));
						$ret .= $caption . '</a>';
					}
				}else{
					$ret .= university_education_get_image($slide, esc_attr($settings['size']), array('id'=>esc_attr($university_education_gallery_id)));
				}
				$ret .= '</li>';
			}
			$ret .= '</ul>';
			$ret .= '</div>';
			
			return $ret;
		}
	}
	
	// use for printing nivo slider
	if( !function_exists('university_education_get_nivo_slider') ){
		function university_education_get_nivo_slider($slides, $settings = array()){
			global $university_education_theme_option, $university_education_gallery_id; $university_education_gallery_id++;
			
			$i = 0; $caption = '';
			$ret  = '<div class="nivoSlider-wrapper">';
			$ret .= '<div class="nivoSlider" ';
			$ret .= empty($settings['pausetime'])? 'data-pausetime="' . esc_attr($university_education_theme_option['nivo-pause-time']) . '" ': 
						'data-pausetime="' . esc_attr($settings['pausetime']) . '" ';
			$ret .= empty($settings['slidespeed'])? 'data-slidespeed="' . esc_attr($university_education_theme_option['nivo-slide-speed']) . '" ': 
						'data-slidespeed="' . esc_attr($settings['slidespeed']) . '" ';			
			$ret .= empty($settings['effect'])? 'data-effect="' . esc_attr($university_education_theme_option['nivo-slider-effects']) . '" ': 
						'data-effect="' . esc_attr($settings['effect']) . '" ';
			$ret .= '>';
			$title_font_size = '';
			if(isset($university_education_theme_option['title-font-size'])){
				if($university_education_theme_option['title-font-size'] == 0){
					$title_font_size = 'font-size:'.esc_attr($university_education_theme_option['title-font-size']).'px';
				}
			}
			$caption_font_size = '';
			if(isset($university_education_theme_option['caption-font-size'])){
				if($university_education_theme_option['caption-font-size'] == 0){
					$caption_font_size = 'font-size:'.esc_attr($university_education_theme_option['caption-font-size']).'px';
				}
			}
			$btn_bg_color_style = '';
			if(isset($university_education_theme_option['caption-btn-color-switch']) && $university_education_theme_option['caption-btn-color-switch'] == 'enable'){
				$btn_bg_color_style = 'style="color:'.esc_attr($university_education_theme_option['caption-btn-color']).';background:'.esc_attr($university_education_theme_option['caption-btn-color-bg']).'"';
			}
			$slides = empty($slides)? array(): $slides;
			foreach($slides as $slide_id => $slide){ 
				if( is_array($slide) ){

					// nivo slider caption
					$id = 'nivo-caption' . $university_education_gallery_id . '-' . $i; $i++;
					if( !empty($slide['title']) || !empty($slide['caption']) ){
						$slide['caption-position'] = empty($slide['caption-position'])? 'left': esc_attr($slide['caption-position']);
						
						$caption .= '<div class="kode-nivo-caption" id="' . $id . '" >';
						$caption .= '<div class="kode-caption-wrapper position-' . $slide['caption-position'] . '">';
						$caption .= '<div class="kode-caption-inner" >';
						$caption .= '<div class="kode-caption">';
						$caption .= empty($slide['title'])? '': '<div style="'.$title_font_size.';color:'.esc_attr($university_education_theme_option['caption-title-color']).'" class="kode-caption-title">' . wp_kses($slide['title'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['caption'])? '': '<div style="'.$caption_font_size.';color:'.esc_attr($university_education_theme_option['caption-desc-color']).'" class="kode-caption-text">' . wp_kses($slide['caption'],array('a'=>array('class'=>array()),'div'=>array('class'=>array()),'span'=>array('class'=>array(),'id'=>array()))). '</div>';
						$caption .= empty($slide['button_txt'])? '': '<div '.$btn_bg_color_style.' class="kode_btn_store_1">' . esc_attr($slide['button_txt']) . ' <i class="fa fa-angle-right"></i></div>';
						$caption .= '</div>'; // kode-caption
						$caption .= '</div>'; // kode-caption-inner
						$caption .= '</div>'; // kode-caption-wrapper
						$caption .= '</div>'; // kode-nivo-caption
					}				
					
					// flex slider link
					$attr = ' title="#' . $id . '" '; 
					if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
						$ret .= university_education_get_image($slide_id, esc_attr($settings['size']), array(), $attr);
					}else if( $slide['slide-link'] == 'url' ){
						$ret .= university_education_get_image($slide_id, esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])), $attr);
					}else if( $slide['slide-link'] == 'current' ){	
						$ret .= university_education_get_image($slide_id, esc_attr($settings['size']), 
							array('id'=>esc_attr($university_education_gallery_id)), $attr);
					}else if( $slide['slide-link'] == 'image' ){
						$ret .= university_education_get_image($slide_id, esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id)), $attr);
					}else if( $slide['slide-link'] == 'video' ){
						$ret .= university_education_get_image($slide_id, esc_attr($settings['size']), 
							array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>esc_attr($university_education_gallery_id)), $attr);
					}
				}else{
					$ret .= university_education_get_image($slide, esc_attr($settings['size']), array('id'=>$university_education_gallery_id), $attr);
				}
			}
			$ret .= '</div>'; // nivoSlider
			$ret .= $caption;
			$ret .= '</div>'; // nivoSlider-wrapper
			
			return $ret;
		}
	}	
	
	if( !function_exists('university_education_get_work_gallery') ){
		function university_education_get_work_gallery($university_education_post_option,$col="",$num_fetch=""){
			global $university_education_gallery_id, $university_education_spaces; $university_education_gallery_id++; 
			$ret = '';
			$slider_option = json_decode($university_education_post_option['image-gallery'], true);
			$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
			$num_page = ceil(sizeof($university_education_post_option['image-gallery']) / $num_fetch);
			$slide_order = $slider_option[0];
			$slide_data = $slider_option[1];					
			
			$slides = array();
			if(!empty($slide_order)){
				foreach( $slide_order as $slide_id ){
					$slides[$slide_id] = $slide_data[$slide_id];
				}
			}
			$university_education_post_option['slider'] = $slides;
			$gallery_print = '';
			if(isset($university_education_post_option['slider']) && $university_education_post_option['slider'] <> ''){
				$gallery_print  = '<div class="kode_project_flick_wrap gallery-item"><ul>';
			
				$current_size = 0;
				foreach($university_education_post_option['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $num_fetch) &&
						($current_size < ($paged) * $num_fetch) ){

						if( !empty($current_size) && ($current_size % $col == 0) ){
							$gallery_print .= '<li class="clear"></li>';
						}			
						$image_src = wp_get_attachment_image_src($slide_id, 'full');	
						$gallery_print .= '
						<li>
							<a href="'.esc_url($image_src[0]).'" data-gal="prettyphoto[]">';
								if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
									$gallery_print .= university_education_get_image($slide_id, esc_attr($university_education_post_option['thumbnail-size']));
								}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
									$gallery_print .= university_education_get_image($slide_id, esc_attr($university_education_post_option['thumbnail-size']), 
										array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
								}else if($slide['slide-link'] == 'current'){
									$gallery_print .= university_education_get_image($slide_id, esc_attr($university_education_post_option['thumbnail-size']), 
										array('id'=>esc_attr($university_education_gallery_id)));
								}else if($slide['slide-link'] == 'image'){
									$gallery_print .= university_education_get_image($slide_id, esc_attr($university_education_post_option['thumbnail-size']), 
										array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id)));
								}else if($slide['slide-link'] == 'video'){
									$gallery_print .= university_education_get_image($slide_id, esc_attr($university_education_post_option['thumbnail-size']), 
										array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$university_education_gallery_id));
								}
								$image_src = wp_get_attachment_image_src($slide_id, 'full');	
								if(!isset($slide['gallery_caption'])){
									$slide['gallery_caption'] = '';
								}
								if(!isset($slide['gallery_title'])){
									$slide['gallery_title'] = '';
								}
							$gallery_print .= '	
							</a>
						</li>';
						
					}
				$current_size ++;
				}					
			$gallery_print .= '</ul></div>  '; // kode-gallery-item
			
			}
			return $gallery_print;
		}
	}
	
	// gallery item
	if( !function_exists('university_education_get_gallery_item') ){
		function university_education_get_gallery_item( $settings ){
			// title section	
			//$ret .= university_education_get_item_title($settings);		
			$ret = '';
			$slider_option = json_decode($settings['slider'], true);
			
			$slide_order = $slider_option[0];
			$slide_data = $slider_option[1];					
			
			$slides = array();
			if(!empty($slide_order)){
				foreach( $slide_order as $slide_id ){
					$slides[$slide_id] = $slide_data[$slide_id];
				}
			}
			$settings['slider'] = $slides;
			
			//if( $settings['gallery-style'] == 'thumbnail' ) return university_education_get_gallery_thumbnail($settings);
			return $ret . university_education_get_gallery($settings);
		}
	}	
	
	
	
	
	// print gallery function
	if( !function_exists('university_education_get_gallery') ){
		function university_education_get_gallery( $settings ){
			global $university_education_gallery_id, $university_education_spaces; $university_education_gallery_id++; 

			$item_id = empty($settings['page-item-id'])? '': ' id="' . $settings['page-item-id'] . '" ';
			$element_item_class = empty($settings['element-item-class'])? '': $settings['element-item-class'];
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . $margin . '" ': '';
			
			// start printing gallery
			$current_size = 0;
			$settings['num-fetch'] = empty($settings['num-fetch'])? 9999: intval($settings['num-fetch']);
			$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
			$num_page = ceil(sizeof($settings['slider']) / $settings['num-fetch']);
			
			$ret  = '<div class="'.esc_attr($settings['element-item-class']).' kode-style-gal-'.esc_attr($settings['style']).' kode-gallery kode-gutter-gallery bottom-spacer">';
			
			if($settings['style'] == 'simple-gallery'){
				if($settings['layout'] == 'without-space'){
					$ret  .= '<ul class="row kode-item kode-padding-free" ' . $item_id . $margin_style . '>';	
				}else{
					$ret  .= '<ul class="row kode-item" ' . $item_id . $margin_style . '>';	
				}
				foreach($settings['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $settings['num-fetch']) &&
						($current_size < ($paged) * $settings['num-fetch']) ){

						if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
							$ret .= '<li class="clear"></li>';
						}			
						$ret .= '<li class="gallery-item col-sm-6 ' . university_education_get_column_class('1/' . $settings['gallery-columns']) . '">';
							$ret .= '
							<div class="edu2_gallery_des">
                                <figure>';
									if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']));
									}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
									}else if($slide['slide-link'] == 'current'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('id'=>esc_attr($university_education_gallery_id)));
									}else if($slide['slide-link'] == 'image'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id)));
									}else if($slide['slide-link'] == 'video'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$university_education_gallery_id));
									}
									$image_src = wp_get_attachment_image_src($slide_id, 'full');
									if(!isset($slide['gallery_caption'])){
										$slide['gallery_caption'] = '';
									}
									if(!isset($slide['gallery_title'])){
										$slide['gallery_title'] = '';										
									}
									$ret .= '
                                    <figcaption>
                                        <a data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'"><i class="fa fa-eye"></i></a>
                                        <a href="'.esc_url($slide['url']).'"><i class="fa fa-link"></i></a>
                                        <h5>' . esc_attr($slide['gallery_title']) . '</h5>
                                        <p>' . esc_attr($slide['gallery_caption']) . '</p>
                                    </figcaption>
                                </figure>
                            </div>';							
						$ret .= '</li>'; // gallery column				
					}
					$current_size ++;
				}
				$ret .= '<li class="clear"></li>';
				$ret .= '</ul>'; // kode-gallery-item
			}else if($settings['style'] == 'modern-gallery'){
				
				$ret  .= '<div ' . $item_id . $margin_style . '>';	
				
				foreach($settings['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $settings['num-fetch']) &&
						($current_size < ($paged) * $settings['num-fetch']) ){
			
						$ret .= '<div class="gallery-item col-sm-6 ' . university_education_get_column_class('1/' . $settings['gallery-columns']) . '">';
							$ret .= '
							<div class="filterable_thumb">
                                <figure>';
									if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']));
									}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
									}else if($slide['slide-link'] == 'current'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('id'=>esc_attr($university_education_gallery_id)));
									}else if($slide['slide-link'] == 'image'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id)));
									}else if($slide['slide-link'] == 'video'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$university_education_gallery_id));
									}
									$image_src = wp_get_attachment_image_src($slide_id, 'full');
									if(!isset($slide['gallery_caption'])){
										$slide['gallery_caption'] = '';
									}
									if(!isset($slide['gallery_title'])){
										$slide['gallery_title'] = '';										
									}
									$ret .= '
									
                                    <figcaption>
										<a data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'"><i class="fa fa-plus"></i></a>
                                    </figcaption>
                                </figure>
                            </div>';							
						$ret .= '</div>'; // gallery column				
					}
					$current_size ++;
				}
				
				$ret .= '</div>'; // kode-gallery-item
			}else if($settings['style'] == 'gallery-slider'){
				if($settings['layout'] == 'without-space'){
					$ret  .= '<div data-slide="'.esc_attr($settings['gallery-columns']).'" data-small-slide="'.esc_attr($settings['gallery-columns']).'" class="owl-no-space owl-theme kode_video_list" ' . $item_id . $margin_style . '>';
				}else{
					$ret  .= '<div data-slide="'.esc_attr($settings['gallery-columns']).'" data-small-slide="'.esc_attr($settings['gallery-columns']).'" class="owl-carousel owl-theme kode_video_list" ' . $item_id . $margin_style . '>';
				}
				foreach($settings['slider'] as $slide_id => $slide){
					if( ($current_size >= ($paged - 1) * $settings['num-fetch']) &&
						($current_size < ($paged) * $settings['num-fetch']) ){

						if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
							$ret .= '';
						}
						$image_src = wp_get_attachment_image_src($slide_id, 'full');	
						$ret .= '<div class="item">
							<div class="edu2_gallery_des">
                                <figure>';
									if( empty($slide['slide-link']) || $slide['slide-link'] == 'none' ){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']));
									}else if($slide['slide-link'] == 'url' || $slide['slide-link'] == 'attachment'){		
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'new-tab'=>esc_attr($slide['new-tab'])));				
									}else if($slide['slide-link'] == 'current'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('id'=>esc_attr($university_education_gallery_id)));
									}else if($slide['slide-link'] == 'image'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'id'=>esc_attr($university_education_gallery_id)));
									}else if($slide['slide-link'] == 'video'){
										$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']), 
											array('url'=>esc_url($slide['url']), 'type'=>'video', 'id'=>$university_education_gallery_id));
									}
									$image_src = wp_get_attachment_image_src($slide_id, 'full');
									if(!isset($slide['gallery_caption'])){
										$slide['gallery_caption'] = '';
									}
									if(!isset($slide['gallery_title'])){
										$slide['gallery_title'] = '';										
									}
									$ret .= '
                                    <figcaption>
                                        <a data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'"><i class="fa fa-eye"></i></a>
                                        <a href="'.esc_url($slide['url']).'"><i class="fa fa-link"></i></a>
                                        <h5>' . esc_attr($slide['gallery_title']) . '</h5>
                                        <p>' . esc_attr($slide['gallery_caption']) . '</p>
                                    </figcaption>
                                </figure>
                            </div>';
						$ret .= '</div>'; // gallery column				
					}
					$current_size ++;
				}
				//$ret .= '<div class="clear"></div>';
				$ret .= '</div>'; // kode-gallery-item
				
			}
			if( $settings['pagination'] == 'enable' ){
				$ret .= university_education_get_pagination($num_page, $paged);
			}
			$ret .= '</div>'; // kode-gallery-item
			
			
			return $ret;
		}
	}
	if( !function_exists('university_education_get_gallery_thumbnail') ){
		function university_education_get_gallery_thumbnail( $settings ){
			$item_id = empty($settings['element-item-id'])? '': ' id="' . esc_attr($settings['element-item-id']) . '" ';

			global $university_education_spaces;
			$margin = (!empty($settings['margin-bottom']) && 
				$settings['margin-bottom'] != $university_education_spaces['bottom-item'])? 'margin-bottom: ' . esc_attr($settings['margin-bottom']) . 'px;': '';
			$margin_style = (!empty($margin))? ' style="' . esc_attr($margin) . '" ': '';			
			
			$ret  = '<div class="kode-gallery-item kode-item kode-gallery-thumbnail" ' . $item_id . $margin_style . '>';
			
			// full image
			$ret .= '<div class="kode-gallery-thumbnail-container">';
			foreach($settings['slider'] as $slide_id => $slide){
				$ret .= '<div class="kode-gallery-thumbnail" data-id="' . $slide_id . '" >';
				$ret .= university_education_get_image($slide_id);
				if($settings['show-caption'] != 'no'){
					$ret .= '<div class="gallery-caption-wrapper">';
					$ret .= '<span class="gallery-caption">';
					$ret .= university_education_get_attachment_info($slide_id, 'caption');
					$ret .= '</span>';
					$ret .= '</div>';
				}
				$ret .= '</div>';
			}
			$ret .= '</div>';
			
			// start printing gallery
			$current_size = 0;
			foreach($settings['slider'] as $slide_id => $slide){
				if( !empty($current_size) && ($current_size % $settings['gallery-columns'] == 0) ){
					$ret .= '<div class="clear"></div>';
				}			
			
				$ret .= '<div class="gallery-column ' . university_education_get_column_class('1/' . $settings['gallery-columns']) . '">';
				$ret .= '<div class="gallery-item" data-id="' . $slide_id . '" >';
				$ret .= university_education_get_image($slide_id, esc_attr($settings['thumbnail-size']));
				$ret .= '</div>'; // gallery item
				$ret .= '</div>'; // gallery column
				$current_size ++;
			}
			$ret .= '<div class="clear"></div>';
			
			$ret .= '</div>'; // kode-gallery-item
			
			return $ret;
		}
	}	
	
	
	
		


?>