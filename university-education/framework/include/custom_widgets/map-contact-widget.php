<?php
/**
 * Plugin Name: Kodeforest Map Contact Widget
 * Plugin URI: http://kodeforest.com/
 * Description: A widget contains the Map contact information( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'university_education_map_contact_widget' );
if( !function_exists('university_education_map_contact_widget') ){
	function university_education_map_contact_widget() {
		register_widget( 'Kodeforest_Map_Contact' );
	}
}

if( !class_exists('Kodeforest_Map_Contact') ){
	class Kodeforest_Map_Contact extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'university_education_map_contact_widget', 
				esc_html__('Kodeforest Map Contact Widget','university-education'), 
				array('description' => esc_html__('A widget that show contact us on map information.', 'university-education')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {			
			
			$title = apply_filters( 'widget_title', $instance['title'] );
			$widget_address = $instance['widget_address'];
			$widget_phone = $instance['widget_phone'];
			$widget_desc = $instance['widget_desc'];
			$widget_email = $instance['widget_email'];
			$btn_text = $instance['btn_text'];
			$btn_url = $instance['btn_url'];

			// Opening of widget
			echo $args['before_widget'];
			
			// Open of title tag
			if( !empty($title) ){
				echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
			}

			?>
				
				<div class="location_des">
					<h6><?php echo esc_attr__('University of Education','university-education'); ?></h6>
					<p><?php echo esc_attr($widget_desc); ?></p>
					<ul class="location_meta">
						<li><i class="fa fa-phone"></i> <a href="#"><?php echo esc_attr($widget_phone); ?></a></li>
						<li><i class="fa fa-map-marker"></i>  <?php echo esc_attr($widget_address); ?></li>
						<li><i class="fa fa-envelope-o"></i>  <a href="#"> <?php echo esc_attr($widget_email); ?></a></li>
					</ul>
					<a href="<?php echo esc_url($btn_url); ?>"><?php echo esc_attr($btn_text); ?> <i class="fa fa-long-arrow-right"></i></a>
				</div>
				
				<!--// TextWidget //-->
<?php
	
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$widget_address = isset($instance['widget_address'])? $instance['widget_address']: '';
			$widget_phone = isset($instance['widget_phone'])? $instance['widget_phone']: '';
			$widget_desc = isset($instance['widget_desc'])? $instance['widget_desc']: '';
			$widget_email = isset($instance['widget_email'])? $instance['widget_email']: '';
			$btn_text = isset($instance['btn_text'])? $instance['btn_text']: '';
			$btn_url = isset($instance['btn_url'])? $instance['btn_url']: '';
			
			?>
			<!-- Title Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'university-education'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>			

			<!-- Description Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>"><?php esc_html_e('Description :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_desc')); ?>" type="text" value="<?php echo esc_attr($widget_desc); ?>" />
			</p>
			
			<!-- Phone Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>"><?php esc_html_e('Phone :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_phone')); ?>" type="text" value="<?php echo esc_attr($widget_phone); ?>" />
			</p>
			
			<!-- Address Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_address')); ?>"><?php esc_html_e('Address :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_address')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_address')); ?>" type="text" value="<?php echo esc_attr($widget_address); ?>" />
			</p>
			
			<!-- Email Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_email')); ?>"><?php esc_html_e('Email :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_email')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_email')); ?>" type="text" value="<?php echo esc_attr($widget_email); ?>" />
			</p>
			
			
			<!-- Button Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('btn_text')); ?>"><?php esc_html_e('Button Text', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('btn_text')); ?>" name="<?php echo esc_attr($this->get_field_name('btn_text')); ?>" type="text" value="<?php echo esc_attr($btn_text); ?>" />
			</p>
			
			<!-- Button URL Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('btn_url')); ?>"><?php esc_html_e('Button URL', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('btn_url')); ?>" name="<?php echo esc_attr($this->get_field_name('btn_url')); ?>" type="text" value="<?php echo esc_attr($btn_url); ?>" />
			</p>
			
		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['widget_address'] = (empty($new_instance['widget_address']))? '': strip_tags($new_instance['widget_address']);
			$instance['widget_desc'] = (empty($new_instance['widget_desc']))? '': strip_tags($new_instance['widget_desc']);
			$instance['widget_phone'] = (empty($new_instance['widget_phone']))? '': strip_tags($new_instance['widget_phone']);
			$instance['btn_text'] = (empty($new_instance['btn_text']))? '': strip_tags($new_instance['btn_text']);
			$instance['btn_url'] = (empty($new_instance['btn_url']))? '': strip_tags($new_instance['btn_url']);
			$instance['widget_email'] = (empty($new_instance['widget_email']))? '': strip_tags($new_instance['widget_email']);
			
			return $instance;
		}	
	}
}
?>