<?php
/**
 * Plugin Name: Kodeforest Featured Teacher
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show Featured Teacher( Specified by Name ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'university_education_featured_teacher_widget' );
if( !function_exists('university_education_featured_teacher_widget') ){
	function university_education_featured_teacher_widget() {
		register_widget( 'Kodeforest_Featured_Teacher_Post' );
	}
}

if( !class_exists('Kodeforest_Featured_Teacher_Post') ){
	class Kodeforest_Featured_Teacher_Post extends WP_Widget{
		// Initialize the widget
		function __construct() {
			parent::__construct(
				'university_education_featured_teacher_widget', 
				esc_html__('Kodeforest Featured Teacher Widget','university-education'), 
				array('description' => esc_html__('A widget that shows Featured Teacher', 'university-education'))			);  
		}
		// Output of the widget
		function widget( $args, $instance ) {
			global $lawbase_theme_option;				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$fetch_member = (empty($instance['fetch_member']))? '': strip_tags($instance['fetch_member']);			
			// Opening of widget
			echo $args['before_widget'];
			// Open of title tag
			if( !empty($title) ){ 
			//	echo $args['before_title'] . $title . $args['after_title']; 
			}
			$get_post = get_post($fetch_member);			$university_education_teacher_option = university_education_decode_stopbackslashes(get_post_meta($get_post->ID, 'post-option', true ));			if( !empty($university_education_teacher_option) ){				$university_education_teacher_option = json_decode( $university_education_teacher_option, true );								}			$designation = empty($university_education_teacher_option['designation'])? '' : $university_education_teacher_option['designation'];			$facebook = empty($university_education_teacher_option['facebook'])? '' : $university_education_teacher_option['facebook'];			$twitter = empty($university_education_teacher_option['twitter'])? '' : $university_education_teacher_option['twitter'];			$linkedin = empty($university_education_teacher_option['linkedin'])? '' : $university_education_teacher_option['linkedin'];			$pinterest = empty($university_education_teacher_option['pinterest'])? '' : $university_education_teacher_option['pinterest'];
			$thumbnail = university_education_get_image(get_post_thumbnail_id($get_post->ID), array(150,150));
			echo '
			    <div class="teacher_outer_wrap">					'.$args['before_title'].' '. esc_attr($title).' '.$args['after_title'].'					<div class="teacher_wrap">						<figure>'.$thumbnail.'</figure>						<div class="teacher_des">							<h4><a href="'.esc_url(get_the_permalink($get_post->ID)).'">' . esc_attr(get_the_title($get_post->ID)) . '</a></h4>							<small>'.esc_attr($designation).'</small>						</div>						<p>'.esc_attr(substr($get_post->post_content,0,150)).'</p>						<ul class="teacher_meta">';						if($facebook <> ''){							echo '<li><a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a></li>';						}						if($twitter <> ''){							echo '<li><a href="'.esc_url($facebook).'"><i class="fa fa-twitter"></i></a></li>';						}						if($linkedin <> ''){							echo '<li><a href="'.esc_url($facebook).'"><i class="fa fa-linkedin"></i></a></li>';						}						if($pinterest <> ''){							echo '<li><a href="'.esc_url($facebook).'"><i class="fa fa-pinterest"></i></a></li>';						}						echo '														</ul>					</div>				</div>';										
			// Closing of widget
			echo $args['after_widget'];	
		}
		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$fetch_member = isset($instance['fetch_member'])? $instance['fetch_member']: '';
			?>
			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'university-education'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>					<!-- Post Category -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('fetch_member')); ?>"><?php esc_html_e('Select Member :', 'university-education'); ?></label>		
				<select class="widefat" name="<?php echo esc_attr($this->get_field_name('fetch_member')); ?>" id="<?php echo esc_attr($this->get_field_id('fetch_member')); ?>">
				<option value="" <?php if(empty($fetch_member)) echo esc_html__(' selected ','university-education'); ?>><?php esc_html_e('Select Teacher', 'university-education') ?></option>
				<?php 	
				$team_list_array = university_education_get_post_list_id('teacher'); 
				foreach($team_list_array as $team_slug => $team_name){ ?>
					<option value="<?php echo esc_attr($team_slug); ?>" <?php if ($fetch_member == $team_slug) echo esc_html__(' selected ','university-education'); ?>><?php echo esc_attr($team_name); ?></option>				
				<?php } ?>	
				</select> 
			</p>

		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['fetch_member'] = (empty($new_instance['fetch_member']))? '': strip_tags($new_instance['fetch_member']);
			return $instance;
		}	
	}
}
?>