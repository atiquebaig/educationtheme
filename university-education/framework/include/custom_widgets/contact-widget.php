<?php
/**
 * Plugin Name: Kodeforest Contact Widget
 * Plugin URI: http://kodeforest.com/
 * Description: A widget contains the contact information( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'university_education_contact_widget' );
if( !function_exists('university_education_contact_widget') ){
	function university_education_contact_widget() {
		register_widget( 'Kodeforest_Contact' );
	}
}

if( !class_exists('Kodeforest_Contact') ){
	class Kodeforest_Contact extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'university_education_contact_widget', 
				esc_html__('Kodeforest Contact Widget','university-education'), 
				array('description' => esc_html__('A widget that show contact us information.', 'university-education')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {			
			
			$title = apply_filters( 'widget_title', $instance['title'] );
			$widget_address = $instance['widget_address'];
			$widget_phone = $instance['widget_phone'];
			$widget_fax = $instance['widget_fax'];
			$widget_email = $instance['widget_email'];

			// Opening of widget
			echo $args['before_widget'];
			
			// Open of title tag
			if( !empty($title) ){ 
				echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
			}

			?>
			
				<div class="widget widget-contact">
					<ul>
						<li><?php echo esc_attr($widget_address); ?></li>
						<li><?php echo esc_attr__('Phone :','university-education'); ?> <a href="#"> <?php echo esc_attr($widget_phone); ?></a></li>
						<li><?php echo esc_attr__('Fax :','university-education'); ?> <a href="#"> <?php echo esc_attr($widget_fax); ?></a></li>
						<li><?php echo esc_attr__('Email :','university-education'); ?> <a href="#"> <?php echo esc_attr($widget_email); ?></a></li>
					</ul>
				</div>
				
				<!--// TextWidget //-->
<?php
	
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$widget_address = isset($instance['widget_address'])? $instance['widget_address']: '';
			$widget_phone = isset($instance['widget_phone'])? $instance['widget_phone']: '';
			$widget_fax = isset($instance['widget_fax'])? $instance['widget_fax']: '';
			$widget_email = isset($instance['widget_email'])? $instance['widget_email']: '';
			
			?>
			<!-- Title Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'university-education'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>			

			<!-- Address Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_address')); ?>"><?php esc_html_e('Address :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_address')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_address')); ?>" type="text" value="<?php echo esc_attr($widget_address); ?>" />
			</p>
			<!-- Phone Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>"><?php esc_html_e('Phone :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_phone')); ?>" type="text" value="<?php echo esc_attr($widget_phone); ?>" />
			</p>
			
			<!-- Fax Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_fax')); ?>"><?php esc_html_e('Fax :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_fax')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_fax')); ?>" type="text" value="<?php echo esc_attr($widget_fax); ?>" />
			</p>
			
			<!-- Email Input --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_email')); ?>"><?php esc_html_e('Email :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_email')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_email')); ?>" type="text" value="<?php echo esc_attr($widget_email); ?>" />
			</p>
			
		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['widget_address'] = (empty($new_instance['widget_address']))? '': strip_tags($new_instance['widget_address']);
			$instance['widget_phone'] = (empty($new_instance['widget_phone']))? '': strip_tags($new_instance['widget_phone']);
			$instance['widget_fax'] = (empty($new_instance['widget_fax']))? '': strip_tags($new_instance['widget_fax']);
			$instance['widget_email'] = (empty($new_instance['widget_email']))? '': strip_tags($new_instance['widget_email']);
			
			return $instance;
		}	
	}
}
?>