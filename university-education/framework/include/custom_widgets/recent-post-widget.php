<?php
/**
 * Plugin Name: Kodeforest Recent Post
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show recent posts( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'university_education_recent_post_widget' );
if( !function_exists('university_education_recent_post_widget') ){
	function university_education_recent_post_widget() {
		register_widget( 'Kodeforest_Recent_Post' );
	}
}

if( !class_exists('Kodeforest_Recent_Post') ){
	class Kodeforest_Recent_Post extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'university_education_recent_post_widget', 
				esc_html__('Kodeforest Recent Post Widget','university-education'), 
				array('description' => esc_html__('A widget that show latest posts', 'university-education')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
			global $university_education_theme_option;	
				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$category = $instance['category'];
			$num_fetch = $instance['num_fetch'];
			
			// Opening of widget
			echo $args['before_widget'];
			
			// Open of title tag
			if( !empty($title) ){ 
				//echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
			}

			// Widget Content
			$current_post = array(get_the_ID());		
			$query_args = array('post_type' => 'post', 'suppress_filters' => false);
			$query_args['posts_per_page'] = $num_fetch;
			$query_args['orderby'] = 'post_date';
			$query_args['order'] = 'asc';
			$query_args['paged'] = 1;
			$query_args['category_name'] = $category;
			$query_args['ignore_sticky_posts'] = 1;
			$query_args['post__not_in'] = array(get_the_ID());
			$query = new WP_Query( $query_args );
			
			if($query->have_posts()){
					$item_class = '';
					
					echo '
					
					<div class="widget-recent-posts">
    						'. $args['before_title'].' '.esc_attr($title).' '.$args['after_title'].'
    					<ul class="sidebar_rpost_des">
                    ';
					while($query->have_posts()){ $query->the_post();
						
					$thumbnail = university_education_get_image(get_post_thumbnail_id(), array(350,350));
					echo '
					<li>
						<figure>
							'.$thumbnail.'
							<figcaption><a href="' . esc_url(get_permalink()) . '"><i class="fa fa-search-plus"></i></a></figcaption>
						</figure>
						<div class="kode-text">
							<h6><a href="' . esc_url(get_permalink()) . '">' . esc_attr(substr(get_the_title(),0,60)) . '</a></h6>
							<span><i class="fa fa-clock-o"></i>'.esc_attr(get_the_date('d')).' '.esc_attr(get_the_date('M')).', '.esc_attr(get_the_date('Y')).'</span>
						</div>
					</li>';
				   
					
					}
				echo '</ul></div>';
			}
			wp_reset_postdata();
					
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';
			$category = isset($instance['category'])? $instance['category']: '';
			$num_fetch = isset($instance['num_fetch'])? $instance['num_fetch']: 3;
			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'university-education'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>		

			<!-- Post Category -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Category :', 'university-education'); ?></label>		
				<select class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>" id="<?php echo esc_attr($this->get_field_id('category')); ?>">
				<option value="" <?php if(empty($category)) echo ' selected '; ?>><?php esc_html_e('All', 'university-education') ?></option>
				<?php 	
				$category_list = university_education_get_term_list('category'); 
				foreach($category_list as $cat_slug => $cat_name){ ?>
					<option value="<?php echo esc_attr($cat_slug); ?>" <?php if ($category == $cat_slug) echo ' selected '; ?>><?php echo esc_attr($cat_name); ?></option>				
				<?php } ?>	
				</select> 
			</p>
				
			<!-- Show Num --> 
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('num_fetch')); ?>"><?php esc_html_e('Num Fetch :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('num_fetch')); ?>" name="<?php echo esc_attr($this->get_field_name('num_fetch')); ?>" type="text" value="<?php echo esc_attr($num_fetch); ?>" />
			</p>

		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['category'] = (empty($new_instance['category']))? '': strip_tags($new_instance['category']);
			$instance['num_fetch'] = (empty($new_instance['num_fetch']))? '': strip_tags($new_instance['num_fetch']);

			return $instance;
		}	
	}
}
?>