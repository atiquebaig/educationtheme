<?php
/**
 * Plugin Name: Kodeforest Recent Post
 * Plugin URI: http://kodeforest.com/
 * Description: A widget that show recent posts( Specified by category ).
 * Version: 1.0
 * Author: Kodeforest
 * Author URI: http://www.kodeforest.com
 *
 */

add_action( 'widgets_init', 'university_education_compact_contact_widget' );
if( !function_exists('university_education_compact_contact_widget') ){
	function university_education_compact_contact_widget() {
		register_widget( 'Kodeforest_Compact_Contact' );
	}
}

if( !class_exists('Kodeforest_Compact_Contact') ){
	class Kodeforest_Compact_Contact extends WP_Widget{

		// Initialize the widget
		function __construct() {
			parent::__construct(
				'university_education_compact_contact_widget', 
				esc_html__('Kodeforest Compact Contact Widget','university-education'), 
				array('description' => esc_html__('A widget that show contact us information.', 'university-education')));  
		}

		// Output of the widget
		function widget( $args, $instance ) {
				
			$title = apply_filters( 'widget_title', $instance['title'] );
			$widget_desc = $instance['widget_desc'];
			$widget_add = $instance['widget_add'];
			$widget_phone = $instance['widget_phone'];
			$widget_email = $instance['widget_email'];
			$message = $instance['message'];
			$skype = $instance['skype'];
			$twitter = $instance['twitter'];

			// Opening of widget
			echo $args['before_widget'];
			
			// Open of title tag
			if( !empty($title) ){ 
				echo $args['before_title'] . esc_attr($title) . $args['after_title']; 
			}
			?>
			
				<div class="contact_heading">
					<p><?php echo esc_attr($widget_desc);?></p>
				</div>
				<ul class="contact_meta">
					<li><i class="fa fa-home"></i><?php echo esc_attr($widget_add);?></li>
					<li><i class="fa fa-phone"></i><a href="#"> <?php echo esc_attr($widget_phone);?></a></li>
					<li><i class="fa fa-envelope-o"></i><a href="#"> <?php echo esc_attr($widget_email);?></a></li>
				</ul>
				<div class="contact_heading social">
					<h4><?php echo esc_attr__('Get Social','university-education'); ?></h4>
				</div>
				<ul class="cont_socil_meta">
					<li><a href="<?php echo esc_url($message);?>"><i class="fa fa-envelope"></i></a></li>
					<li><a href="<?php echo esc_url($skype);?>"><i class="fa fa-skype"></i></a></li>
					<li><a href="<?php echo esc_url($twitter);?>"><i class="fa fa-twitter"></i></a></li>
				</ul>
				
				<!--// TextWidget //-->
<?php
	
			// Closing of widget
			echo $args['after_widget'];	
		}

		// Widget Form
		function form( $instance ) {
			$title = isset($instance['title'])? $instance['title']: '';		
			$widget_desc = isset($instance['widget_desc'])? $instance['widget_desc']: '';
			$widget_add = isset($instance['widget_add'])? $instance['widget_add']: '';
			$widget_phone = isset($instance['widget_phone'])? $instance['widget_phone']: '';
			$widget_email = isset($instance['widget_email'])? $instance['widget_email']: '';
			$message = isset($instance['message'])? $instance['message']: '';
			$skype = isset($instance['skype'])? $instance['skype']: '';
			$twitter = isset($instance['twitter'])? $instance['twitter']: '';			
			
			?>

			<!-- Text Input -->
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title :', 'university-education'); ?></label> 
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>"><?php esc_html_e('Widget Description :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_desc')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_desc')); ?>" type="text" value="<?php echo esc_attr($widget_desc); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_add')); ?>"><?php esc_html_e('Address Info :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_add')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_add')); ?>" type="text" value="<?php echo esc_attr($widget_add); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>"><?php esc_html_e('Phone Number :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_phone')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_phone')); ?>" type="text" value="<?php echo esc_attr($widget_phone); ?>" />
			</p>

			<p>
				<label for="<?php echo esc_attr($this->get_field_id('widget_email')); ?>"><?php esc_html_e('Email ID :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('widget_email')); ?>" name="<?php echo esc_attr($this->get_field_name('widget_email')); ?>" type="text" value="<?php echo esc_attr($widget_email); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('message')); ?>"><?php esc_html_e('Message Us', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('message')); ?>" name="<?php echo esc_attr($this->get_field_name('message')); ?>" type="text" value="<?php echo esc_attr($message); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('skype')); ?>"><?php esc_html_e('Skype ID :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('skype')); ?>" name="<?php echo esc_attr($this->get_field_name('skype')); ?>" type="text" value="<?php echo esc_attr($skype); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('twitter')); ?>"><?php esc_html_e('Twitter Social URL :', 'university-education'); ?></label>
				<input class="widefat" id="<?php echo esc_attr($this->get_field_id('twitter')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter')); ?>" type="text" value="<?php echo esc_attr($twitter); ?>" />
			</p>

			
		<?php
		}
		
		// Update the widget
		function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = (empty($new_instance['title']))? '': strip_tags($new_instance['title']);
			$instance['widget_desc'] = (empty($new_instance['widget_desc']))? '': strip_tags($new_instance['widget_desc']);		
			$instance['widget_add'] = (empty($new_instance['widget_add']))? '': strip_tags($new_instance['widget_add']);		
			$instance['widget_phone'] = (empty($new_instance['widget_phone']))? '': strip_tags($new_instance['widget_phone']);
			$instance['widget_email'] = (empty($new_instance['widget_email']))? '': strip_tags($new_instance['widget_email']);
			$instance['message'] = (empty($new_instance['message']))? '': strip_tags($new_instance['message']);		
			$instance['skype'] = (empty($new_instance['skype']))? '': strip_tags($new_instance['skype']);		
			$instance['twitter'] = (empty($new_instance['twitter']))? '': strip_tags($new_instance['twitter']);			

			return $instance;
		}	
	}
}
?>