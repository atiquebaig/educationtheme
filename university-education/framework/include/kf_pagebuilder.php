<?php
	/*	
	*	Kodeforest Pagebuilder File
	*	---------------------------------------------------------------------
	*	This file contains the page builder settings
	*	---------------------------------------------------------------------
	*/	
	
	// create the page option
	add_action('init', 'university_education_create_page_options');
	if( !function_exists('university_education_create_page_options') ){
	
		function university_education_create_page_options(){
			global $university_education_theme_option;
			if(!isset($university_education_theme_option['sidebar-element'])){$university_education_theme_option['sidebar-element'] = array('blog','contact');}
			new university_education_page_options( 
					  
				// page option settings
				array(
					'page-option' => array(
						'title' => esc_html__('Page Option', 'university-education'),
						'options' => array(
							'show-sub' => array(
								'title' => esc_html__('Show Sub Header' , 'university-education'),
								'type' => 'checkbox',
								'default' => 'enable',
								'wrapper-class' => 'columns kode-btn-icons',
								'description'=> esc_html__('Click here to Turn On / Off the Sub Header from here.', 'university-education')
							),	
							'page-caption' => array(
								'title' => esc_html__('Page Caption' , 'university-education'),
								'type' => 'textarea',
								'wrapper-class' => 'columns kode-txt-area',
								'description'=> esc_html__('Enter the page Caption here that you want to show.', 'university-education')
							),								
							'header-background' => array(
								'title' => esc_html__('Header Background Image' , 'university-education'),
								'button' => esc_html__('Upload', 'university-education'),
								'type' => 'upload',
								'wrapper-class' => 'columns kode-bg-sec',
								'description'=> esc_html__('Please Upload the Header Background Image from here.', 'university-education')
							),
							'enable-title-top' => array(
								'title' => __('Content Title (While Visual Composer)', 'university-education'),
								'type' => 'checkbox',
								'default'=>'enable',
								'wrapper-class' => 'columns kode-bg-sec',
								'description'=> esc_attr__('While Visual Composer need to turn off title of the page which come on top of content.', 'university-education')
							),
							'show-author-section' => array(
								'title' => __('Author Section' , 'university-education'),
								'type' => 'checkbox',
								'description'=> esc_attr__('Click here to turn On / Off the Author Information Box from here.', 'university-education'),
								'default' => 'enable',
								'wrapper-class' => 'columns',
							),
							'show-comment-section' => array(
								'title' => __('Comment Section' , 'university-education'),
								'type' => 'checkbox',
								'description'=> esc_attr__('Click here to turn On / Off the Comment Box from here.', 'university-education'),
								'default' => 'enable',
								'wrapper-class' => 'columns',
							),
						)
					),

				),
				// page option attribute
				array(
					'post_type' => array('page'),
					'meta_title' => esc_html__('Page Option', 'university-education'),
					'meta_slug' => 'kodeforest-page-option',
					'option_name' => 'post-option',
					'position' => 'side',
					'priority' => 'low',
				)
			);
			
		}
	}
	
	// create the page builder
	if( is_admin() ){
		add_action('init', 'university_education_create_page_builder_option');
	}
	if( !function_exists('university_education_create_page_builder_option') ){	
		function university_education_create_page_builder_option(){
			global $university_education_theme_option;
			new university_education_page_builder( 
				
				// page builder option setting
				apply_filters('university_education_page_builder_option',
					array(
						'content-item' => array(
							'title' => esc_html__('Content & Post Options', 'university-education'),
							'blank_option' => esc_html__('- Select Content Element -', 'university-education'),
							'options' => array(
								'full-size-wrapper' => array(
									'title'=> esc_html__('Section', 'university-education'), 
									'type'=>'wrapper',
									'icon'=>'fa-circle-o-notch',
									'options'=>array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'type' => array(
											'title' => esc_html__('Type', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'image'=> esc_html__('Background Image', 'university-education'),
												'pattern'=> esc_html__('Predefined Pattern', 'university-education'),
												'video'=> esc_html__('Video Background', 'university-education'),
												'color'=> esc_html__('Color Background', 'university-education'),
												'map'=> esc_html__('Google Map', 'university-education'),
											),
											'default'=>'color'
										),	
										
										'container' => array(
											'title' => esc_html__('Container', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'full-width'=> esc_html__('Full Width', 'university-education'),
												'container-width'=> esc_html__('Container (inside 1170px)', 'university-education'),
											),
											'description' => esc_html__('Select container width, container full width section will be full width.', 'university-education'),
											'default'=>'container-width'
										),
										'map_shortcode' => array(
											'title' => esc_html__('Google Map Shortcode', 'university-education'),
											'type' => 'text',
											'default' => '',
											'wrapper-class'=>'type-wrapper map-wrapper',
											'description' => esc_html__('Add google map shortcode to add background.', 'university-education')
										),	
										'video_url' => array(
											'title' => esc_html__('Video URL', 'university-education'),
											'type' => 'text',
											'default' => '',
											'wrapper-class'=>'type-wrapper video-wrapper',
											'description' => esc_html__('add video url for the parallax video background for mp4', 'university-education')
										),	
										'video_type' => array(
											'title' => esc_html__('Video Type', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'mp4'=> esc_html__('Video Type Mp4 ', 'university-education'),
												'ogg'=> esc_html__('Video Type Ogg ', 'university-education'),
												'webm'=> esc_html__('Video Type Webm ', 'university-education'),
											),
											'wrapper-class'=>'type-wrapper video-wrapper',
											'description' => esc_html__('Select video background type.', 'university-education'),
											'default'=>'mp4'
										),	
										'background-hook'=> array(
											'title'=> esc_html__('Background Image/Video on ::Before' ,'university-education'),
											'type'=> 'checkbox',
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper'
										),
										'background-size' => array(
											'title' => esc_html__('Background Size', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'cover'=> esc_html__('Cover', 'university-education'),
												'contain'=> esc_html__('Contain', 'university-education'),
												'auto'=> esc_html__('Auto', 'university-education'),
											),
											'wrapper-class' => 'type-wrapper image-wrapper',
											'description' => esc_html__('Background Size.', 'university-education'),
											'default'=>'auto'
										),
										'background-position' => array(
											'title' => esc_html__('Background Position', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'cover'=> esc_html__('Cover', 'university-education'),
												'auto'=> esc_html__('Auto', 'university-education'),
												'left top'=> esc_html__('left top', 'university-education'),
												'left center'=> esc_html__('left center', 'university-education'),
												'left bottom'=> esc_html__('left bottom', 'university-education'),
												'right top'=> esc_html__('right top', 'university-education'),
												'right center'=> esc_html__('right center', 'university-education'),
												'right bottom'=> esc_html__('right bottom', 'university-education'),
												'center top'=> esc_html__('center top', 'university-education'),
												'center center'=> esc_html__('center center', 'university-education'),
												'center bottom'=> esc_html__('center bottom', 'university-education'),
											),
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper background-hook-wrapper background-hook-enable',
											'description' => esc_html__('Background Size.', 'university-education'),
											'default'=>'auto'
										),
										'background-attachment' => array(
											'title' => esc_html__('Background Attachment', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'fixed'=> esc_html__('Fixed', 'university-education'),
												'scroll'=> esc_html__('Scroll', 'university-education'),
												'local'=> esc_html__('local', 'university-education'),
												'initial'=> esc_html__('initial', 'university-education'),
											),
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper background-hook-wrapper background-hook-enable',
											'description' => esc_html__('Background Attachment.', 'university-education'),
											'default'=>'auto'
										),
										'background-width'=> array(
											'title'=> esc_html__('Background Area Width' ,'university-education'),
											'type' => 'text',
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper background-hook-wrapper background-hook-enable'
										),
										'background-image-align' => array(
											'title' => esc_html__('Background Image Alignment', 'university-education'),
											'type' => 'combobox',
											'options' => array(
												'left'=> esc_html__('Left', 'university-education'),
												'right'=> esc_html__('Right', 'university-education'),
											),
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper background-hook-wrapper background-hook-enable',
											'description' => esc_html__('Background Image alignment.', 'university-education'),
											'default'=>'left'
										),
										'background' => array(
											'title' => esc_html__('Background Image', 'university-education'),
											'button' => esc_html__('Upload', 'university-education'),
											'type' => 'upload',
											'wrapper-class' => 'type-wrapper image-wrapper'
										),	
										'trans-background'=> array(
											'title'=> esc_html__('Transparent Background' ,'university-education'),
											'type'=> 'checkbox',
											'wrapper-class' => 'type-wrapper image-wrapper video-wrapper'
										),
										'horizontal-background'=> array(
											'title'=> esc_html__('Horizontal Moving Background' ,'university-education'),
											'type'=> 'checkbox',
											'wrapper-class' => 'type-wrapper image-wrapper'
										),
										'background-color' => array(
											'title' => esc_html__('Overlay Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',
											'wrapper-class'=>'type-wrapper image-wrapper color-wrapper video-wrapper'
										),												
										'opacity' => array(
											'title' => esc_html__('Opacity', 'university-education'),
											'type' => 'text',
											'default' => '0.03',
											'wrapper-class'=>'type-wrapper image-wrapper video-wrapper',
											'description' => esc_html__('add opacity to the background image. for example from .01 to 1', 'university-education')
										),	
										'background-speed' => array(
											'title' => esc_html__('Background Speed', 'university-education'),
											'type' => 'text',
											'default' => '0',
											'wrapper-class' => 'type-wrapper image-wrapper',
											'description' => esc_html__('Fill 0 if you don\'t want the background to scroll and 1 when you want the background to have the same speed as the scroll bar', 'university-education') .
												'<br><br><strong>' . esc_html__('*** only allow the number between -1 to 1', 'university-education') . '</strong>'
										),
										'padding-top' => array(
											'title' => esc_html__('Padding Top', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Spaces before starting any content in this section', 'university-education')
										),	
										'padding-bottom' => array(
											'title' => esc_html__('Padding Bottom', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of the content in this section', 'university-education')
										),
									)
								),
								'column1-1' => array(
									'title'=> esc_html__('Column', 'university-education'),
									'type'=>'wrapper',
									'icon'=>'fa-columns',
									'size'=>'1/1',
									'options'=>array(	
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),
									)
								),
								'simple-column' => array(
									'title'=> esc_html__('Simple Column', 'university-education'), 
									'icon'=>'fa-sticky-note-o',
									'type'=>'item',
									'options'=>array(	
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),										
										'content'=> array(
											'title'=> esc_html__('Content Text' ,'university-education'),
											'type'=> 'textarea',						
										),
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),	 
									)
								),		
								'accordion' => array(
									'title'=> esc_html__('Accordion', 'university-education'), 
									'icon'=>'fa-server',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'accordion'=> array(
											'type'=> 'tab',
											'default-title'=> esc_html__('Accordion' ,'university-education')											
										),
										'initial-state'=> array(
											'title'=> esc_html__('On Load Open', 'university-education'),
											'type'=> 'text',
											'default'=> 1,
											'description'=> esc_html__('0 will close all tab as an initial state, 1 will open the first tab and so on.', 'university-education')						
										),												
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),
									)
								),
								'call-to-action' => array(
									'title'=> esc_html__('Call To Action', 'university-education'), 
									'icon'=>'fa-dot-circle-o',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'link' => array(
											'title' => esc_html__('Link', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add External Link Here.', 'university-education')
										),
										'link-source'=> array(
											'title'=> esc_html__('Link Source' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'_self'=>esc_html__('_Self', 'university-education'), 
												'_blank'=> esc_html__('_Blank', 'university-education'), 
												'prettyphoto'=> esc_html__('PrettyPhoto', 'university-education'), 
											)
										),	
										'icon' => array(
											'title' => esc_html__('Icon', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add font awesome icon class here for example: fa fa-lock.', 'university-education')
										),
										'icon-color' => array(
											'title' => esc_html__('Icon Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),
										'icon-bg-color' => array(
											'title' => esc_html__('Icon BG Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),
										'title' => array(
											'title' => esc_html__('Title', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add title of call to action here.', 'university-education')
										),
										'title-color' => array(
											'title' => esc_html__('title Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),										
										'sub-title' => array(
											'title' => esc_html__('Sub Title', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add sub title of call to action here.', 'university-education')
										),
										'sub-title-color' => array(
											'title' => esc_html__('Sub Title Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),										
										'sub-title-small' => array(
											'title' => esc_html__('Sub Title Small', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add sub title small of call to action here.', 'university-education')
										),	
										'sub-title-small-color' => array(
											'title' => esc_html__('Sub Title Small Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),										
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),	
									)
								),
								
								'project-facts' => array(
									'title'=> esc_html__('Project Facts', 'university-education'), 
									'icon'=>'fa-bar-chart',
									'type'=>'item',
									'options'=> array(
										'style'=> array(
											'title'=> esc_html__('Project Facts Style' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'style-1' => esc_html__('Style 1', 'university-education'),
											)
										),
										'icon' => array(
											'title' => esc_html__('Icon', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add font awesome icon class here for example: fa fa-lock.', 'university-education'),
											'wrapper-class' => 'style-wrapper style-1-wrapper style-2-wrapper style-4-wrapper',
										),
										'color' => array(
											'title' => esc_html__('Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',
										),
										'value' => array(
											'title' => esc_html__('Value', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add value here.', 'university-education')
										),
										'sub-text' => array(
											'title' => esc_html__('Sub Text', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('Add sub text of project facts here.', 'university-education')
										)	
									)
								),
								'sidebar' => array(
									'title'=> esc_html__('Sidebar', 'university-education'), 
									'icon'=>'fa-th',
									'type'=>'item',
									'options'=> 
									array(
										'widget'=> array(
											'title'=> esc_html__('Select Widget Area' ,'university-education'),
											'type'=> 'combobox_sidebar',
											'options'=> $university_education_theme_option['sidebar-element'],
											'description'=> esc_html__('You can select Widget Area of your choice.', 'university-education')
										),	
									)
								),
								'blog' => array(
									'title'=> esc_html__('Blog', 'university-education'), 
									'icon'=>'fa-cube',
									'type'=>'item',
									'options'=>  array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),									
										'category'=> array(
											'title'=> esc_html__('Category' ,'university-education'),
											'type'=> 'multi-combobox',
											'options'=> university_education_get_term_list('category'),
											'description'=> esc_html__('Select categories to fetch its posts.', 'university-education')
										),	
										'tag'=> array(
											'title'=> esc_html__('Tag' ,'university-education'),
											'type'=> 'multi-combobox',
											'options'=> university_education_get_term_list('post_tag'),
											'description'=> esc_html__('Select tags to fetch its posts.', 'university-education')
										),	
										'kode-blog-thumbnail-size' => array(
											'title' => esc_html__('Single Post Thumbnail Size', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'default'=> 'uoe-post-thumbnail-size'
										),
										'blog-style'=> array(
											'title'=> esc_html__('Blog Style' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'blog-full' => esc_html__('Blog Full', 'university-education'),
												'blog-grid' => esc_html__('Blog Grid', 'university-education'),
											),
											'default'=>'blog-full'
										),	
										'blog-slider'=> array(
											'title'=> esc_html__('Blog Slider' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'carousel' => esc_html__('Carousel', 'university-education'),
												'bxslider' => esc_html__('BX Slider', 'university-education'),
												'simple-post' => esc_html__('Simple List (No Slider)', 'university-education'),
											),
											'default'=>'blog-full',
											'wrapper-class' => 'blog-style-wrapper blog-small-wrapper blog-grid-wrapper',
										),
										'blog-size'=> array(
											'title'=> esc_html__('Blog Size' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'2' => esc_html__('2 Column', 'university-education'),
												'3' => esc_html__('3 Column', 'university-education'),
												'4' => esc_html__('4 Column', 'university-education'),
												'5' => esc_html__('5 Column', 'university-education'),
												'6' => esc_html__('6 Column', 'university-education'),
											),
											'wrapper-class' => 'blog-grid-wrapper blog-grid-small-wrapper blog-style-wrapper',
											'default'=>'blog-full'
										),	
										'title-num-fetch'=> array(
											'title'=> esc_html__('Num Title (Character)' ,'university-education'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post title.', 'university-education')
										),	
										'num-excerpt'=> array(
											'title'=> esc_html__('Num Excerpt (Word)' ,'university-education'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post description.', 'university-education')
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch' ,'university-education'),
											'type'=> 'text',	
											'default'=> '8',
											'description'=> esc_html__('Specify the number of posts you want to pull out.', 'university-education')
										),										
										'orderby'=> array(
											'title'=> esc_html__('Order By' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'date' => esc_html__('Publish Date', 'university-education'), 
												'title' => esc_html__('Title', 'university-education'), 
												'rand' => esc_html__('Random', 'university-education'), 
											)
										),
										'order'=> array(
											'title'=> esc_html__('Order' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'desc'=>esc_html__('Descending Order', 'university-education'), 
												'asc'=> esc_html__('Ascending Order', 'university-education'), 
											)
										),	
										'pagination'=> array(
											'title'=> esc_html__('Enable Pagination' ,'university-education'),
											'type'=> 'checkbox'
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),										
									)
								),
								'blog-slider' => array(
									'title'=> esc_html__('Blog Slider', 'university-education'), 
									'icon'=>'fa-rocket',
									'type'=>'item',
									'options'=>  array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'element-style'=> array(
											'title'=> esc_html__('Element Style' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'style-1'=>esc_html__('Style 1', 'university-education'), 
												'style-2'=>esc_html__('Style 2', 'university-education'), 
												'style-3'=>esc_html__('Style 3', 'university-education'), 
											)
										),	
										'category'=> array(
											'title'=> esc_html__('Category' ,'university-education'),
											'type'=> 'multi-combobox',
											'options'=> university_education_get_term_list('category'),
											'description'=> esc_html__('Select categories to fetch its posts.', 'university-education')
										),	
										'element-type'=> array(
											'title'=> esc_html__('Element Type' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'slider'=>esc_html__('Slider', 'university-education'), 
												'simple-post'=> esc_html__('Simple Post', 'university-education'), 
											)
										),	
										'thumbnail-size' => array(
											'title' => esc_html__('Thumbnail Size', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'default'=> 'thumbnail-size'
										),
										'title-num-fetch'=> array(
											'title'=> esc_html__('Num Title (Character)' ,'university-education'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post title.', 'university-education')
										),	
										'num-excerpt'=> array(
											'title'=> esc_html__('Num Excerpt (Word)' ,'university-education'),
											'type'=> 'text',	
											'default'=> '25',
											'description'=> esc_html__('This is a number of characters that you want to show on the post description.', 'university-education')
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch' ,'university-education'),
											'type'=> 'text',	
											'default'=> '8',
											'description'=> esc_html__('Specify the number of posts you want to pull out.', 'university-education')
										),										
										'orderby'=> array(
											'title'=> esc_html__('Order By' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'date' => esc_html__('Publish Date', 'university-education'), 
												'title' => esc_html__('Title', 'university-education'), 
												'rand' => esc_html__('Random', 'university-education'), 
											)
										),
										'order'=> array(
											'title'=> esc_html__('Order' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'desc'=>esc_html__('Descending Order', 'university-education'), 
												'asc'=> esc_html__('Ascending Order', 'university-education'), 
											)
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),										
									)
								),
								'headings' => array(
									'title'=> esc_html__('Fancy Heading', 'university-education'), 
									'icon'=>'fa-header',
									'type'=>'item',
									'options'=>  array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),
										'alignment'=> array(
											'title'=> esc_html__('Alignment' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'left'=>esc_html__('Left', 'university-education'), 
												'right'=>esc_html__('Right', 'university-education'), 
												'center'=>esc_html__('Center', 'university-education'), 
											)
										),										
										'element-style'=> array(
											'title'=> esc_html__('Heading Style' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'style-1'=>esc_html__('Style 1', 'university-education'), 
												'style-2'=>esc_html__('Style 2', 'university-education'), 
												'style-3'=>esc_html__('Style 3', 'university-education'), 
												'style-4'=>esc_html__('Style 4', 'university-education'), 
											)
										),	
										'title'=> array(
											'title'=> esc_html__('Title' ,'university-education'),
											'type'=> 'text',	
											'default'=> '',
											'description'=> esc_html__('Add heading title here.', 'university-education')
										),	
										'title-color' => array(
											'title' => esc_html__('Title Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',											
										),	
										'caption'=> array(
											'title'=> esc_html__('Caption' ,'university-education'),
											'type'=> 'text',	
											'default'=> '',
											'wrapper-class'=>'element-style-wrapper style-1-wrapper style-2-wrapper style-3-wrapper'
										),
										'caption-color' => array(
											'title' => esc_html__('Caption Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',
											'wrapper-class'=>'element-style-wrapper style-1-wrapper style-2-wrapper style-3-wrapper'
										),	
										'line-color' => array(
											'title' => esc_html__('Line Color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',
											'wrapper-class'=>'element-style-wrapper style-2-wrapper style-3-wrapper style-4-wrapper'
										),										
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),										
									)
								),
								
								'column-service' => array(
									'title'=> esc_html__('Services', 'university-education'), 
									'icon'=>'fa-gg',
									'type'=>'item',
									'options'=>array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'title'=> array(
											'title'=> esc_html__('Title' ,'university-education'),
											'type'=> 'text',						
										),											
										'style'=> array(
											'title'=> esc_html__('Item Style' ,'university-education'),
											'type'=> 'styles',
											'wrapper-class'=> 'kode-custom-styles',
											'options'=> array(
												'type-1'=>UOE_PATH . '/framework/include/backend_assets/images/services/services-1.jpg',
												'type-2'=>UOE_PATH . '/framework/include/backend_assets/images/services/services-2.jpg',
												'type-3'=>UOE_PATH . '/framework/include/backend_assets/images/services/services-3.jpg',
											)
										),	
										'thumbnail-size' => array(
											'title' => esc_html__('Thumbnail Size', 'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list(),
											'wrapper-class' => 'style-wrapper type-2-wrapper',
											'default'=> 'kode-post-thumbnail-size'
										),
										'background-hover-color' => array(
											'title' => esc_html__('Select Service background color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',	
											'wrapper-class' => 'style-wrapper type-1-wrapper',
										),
										'foreground-color' => array(
											'title' => esc_html__('Select Service forground color', 'university-education'),
											'type' => 'colorpicker',
											'default'=> '#ffffff',	
											'wrapper-class' => 'style-wrapper type-1-wrapper',
										),
										'icon_type'=> array(
											'title'=> esc_html__('Icon Class' ,'university-education'),
											'type'=> 'text',						
											'wrapper-class' => '',
										),	
										'service-image-box' => array(
											'title' => esc_html__('Services Image' , 'university-education'),
											'button' => esc_html__('Upload', 'university-education'),
											'type' => 'upload',
											'wrapper-class' => 'style-wrapper type-2-wrapper',
										),										
										'content'=> array(
											'title'=> esc_html__('Content Text' ,'university-education'),
											'type'=> 'textarea',
											'type'=> 'tinymce',						
										),
										'link' => array(
											'title' => esc_html__('Link', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Please add link here for services.', 'university-education')
										),
										'link-text' => array(
											'title' => esc_html__('Link Text', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Please add link text here for services.', 'university-education')
										),											
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),	 
									)
								),
									
								'content' => array(
									'title'=> esc_html__('Content', 'university-education'), 
									'icon'=>'fa-contao',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'show-title' => array(
											'title' => esc_html__('Show Title' , 'university-education'),
											'type' => 'checkbox',
											'default' => 'enable',
										),						
										'page-caption' => array(
											'title' => esc_html__('Page Caption' , 'university-education'),
											'type' => 'textarea'
										),		
										'show-content' => array(
											'title' => esc_html__('Show Content (From Default Editor)' , 'university-education'),
											'type' => 'checkbox',
											'default' => 'enable',
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),														
									)
								), 	

								'divider' => array(
									'title'=> esc_html__('Divider', 'university-education'), 
									'icon'=>'fa-minus',
									'type'=>'item',
									'options'=>array(									
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),										
									)
								),
								
								// 'portfolio' => array(),
								
								'gallery' => array(
									'title'=> esc_html__('Gallery', 'university-education'), 
									'icon'=>'fa-houzz',
									'type'=>'item',
									'options'=> array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'slider'=> array(	
											'overlay'=> 'false',
											'caption'=> 'false',
											'type'=> 'gallery',
										),				
										'thumbnail-size'=> array(
											'title'=> esc_html__('Thumbnail Size' ,'university-education'),
											'type'=> 'combobox',
											'options'=> university_education_get_thumbnail_list()
										),
										'style'=> array(
											'title'=> esc_html__('Gallery Style' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'gallery-slider'=>esc_html__('Gallery Slider', 'university-education'),
												'simple-gallery'=>esc_html__('Simple Gallery', 'university-education'),
												'modern-gallery'=>esc_html__('Modern Gallery', 'university-education'),
											)
										),
										'layout'=> array(
											'title'=> esc_html__('Gallery Layout' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'with-space'=>esc_html__('With Padding', 'university-education'),
												'without-space'=>esc_html__('Without Padding', 'university-education'),
											)
										),
										'gallery-columns'=> array(
											'title'=> esc_html__('Gallery Image Columns' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array('2'=>'2', '3'=>'3', '4'=>'4'),
											'default'=> '4'
										),	
										'num-fetch'=> array(
											'title'=> esc_html__('Num Fetch (Per Page)' ,'university-education'),
											'type'=> 'text',
											'description'=> esc_html__('Leave this field blank to fetch all image without pagination.', 'university-education'),
											'wrapper-class'=>'gallery-style-wrapper grid-wrapper'
										),
										'show-caption'=> array(
											'title'=> esc_html__('Show Caption' ,'university-education'),
											'type'=> 'combobox',
											'options'=> array('yes'=>'Yes', 'no'=>'No')
										),			
										'pagination'=> array(
											'title'=> esc_html__('Enable Pagination' ,'university-education'),
											'type'=> 'checkbox'
										),	
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),	
									)	
								),		
								// 'post-slider' => array(
									// 'title'=> esc_html__('Post Slider', 'university-education'), 
									// 'icon'=>'fa-file-o',
									// 'type'=>'item',
									// 'options'=>array(
										// 'element-item-id' => array(
											// 'title' => esc_html__('Page Item ID', 'university-education'),
											// 'type' => 'text',
											// 'default' => '',
											// 'description' => esc_html__('please add the page item id.', 'university-education')
										// ),
										// 'element-item-class' => array(
											// 'title' => esc_html__('Page Item Class', 'university-education'),
											// 'type' => 'text',
											// 'default' => '',
											// 'description' => esc_html__('please add the page item class.', 'university-education')
										// ),	
										// 'category'=> array(
											// 'title'=> esc_html__('Category' ,'university-education'),
											// 'type'=> 'combobox',
											// 'options'=> university_education_get_term_list('category'),
											// 'description'=> esc_html__('Select categories to fetch its posts.', 'university-education')
										// ),	
										// 'num-excerpt'=> array(
											// 'title'=> esc_html__('Num Excerpt (Word)' ,'university-education'),
											// 'type'=> 'text',	
											// 'default'=> '25',
											// 'description'=> esc_html__('This is a number of word (decided by spaces) that you want to show on the post excerpt. <strong>Use 0 to hide the excerpt, -1 to show full posts and use the wordpress more tag</strong>.', 'university-education')
										// ),	
										// 'num-fetch'=> array(
											// 'title'=> esc_html__('Num Fetch' ,'university-education'),
											// 'type'=> 'text',	
											// 'default'=> '8',
											// 'description'=> esc_html__('Specify the number of posts you want to pull out.', 'university-education')
										// ),										
										// 'thumbnail-size'=> array(
											// 'title'=> esc_html__('Thumbnail Size' ,'university-education'),
											// 'type'=> 'combobox',
											// 'options'=> university_education_get_thumbnail_list()
										// ),	
										// 'style'=> array(
											// 'title'=> esc_html__('Style' ,'university-education'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'no-excerpt'=>esc_html__('No Excerpt', 'university-education'),
												// 'with-excerpt'=>esc_html__('With Excerpt', 'university-education'),
											// )
										// ),
										// 'caption-style'=> array(
											// 'title'=> esc_html__('Caption Style' ,'university-education'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'post-bottom post-slider'=>esc_html__('Bottom Caption', 'university-education'),
												// 'post-right post-slider'=>esc_html__('Right Caption', 'university-education'),
												// 'post-left post-slider'=>esc_html__('Left Caption', 'university-education')
											// ),
											// 'wrapper-class' => 'style-wrapper with-excerpt-wrapper'
										// ),											
										// 'orderby'=> array(
											// 'title'=> esc_html__('Order By' ,'university-education'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'date' => esc_html__('Publish Date', 'university-education'), 
												// 'title' => esc_html__('Title', 'university-education'), 
												// 'rand' => esc_html__('Random', 'university-education'), 
											// )
										// ),
										// 'order'=> array(
											// 'title'=> esc_html__('Order' ,'university-education'),
											// 'type'=> 'combobox',
											// 'options'=> array(
												// 'desc'=>esc_html__('Descending Order', 'university-education'), 
												// 'asc'=> esc_html__('Ascending Order', 'university-education'), 
											// )
										// ),			
										// 'margin-bottom' => array(
											// 'title' => esc_html__('Margin Bottom', 'university-education'),
											// 'type' => 'text',
											// 'description' => esc_html__('Spaces after ending of this item', 'university-education')
										// ),											
									// )
								// ),
								
								'slider' => array(
									'title'=> esc_html__('Slider', 'university-education'), 
									'icon'=>'fa-sliders',
									'type'=>'item',
									'options'=>array(
										'element-item-id' => array(
											'title' => esc_html__('Page Item ID', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item id.', 'university-education')
										),
										'element-item-class' => array(
											'title' => esc_html__('Page Item Class', 'university-education'),
											'type' => 'text',
											'default' => '',
											'description' => esc_html__('please add the page item class.', 'university-education')
										),	
										'slider-style'=> array(
											'title'=> esc_html__('Slider Style', 'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'style-1' => esc_html__('Style 1', 'university-education'),
												'style-2' => esc_html__('Style 2', 'university-education'),
												'style-3' => esc_html__('Style 3', 'university-education')
											)
										),	
										'slider'=> array(	
											'overlay'=> 'false',
											'caption'=> 'true',
											'type'=> 'slider'						
										),	
										'slider-type'=> array(
											'title'=> esc_html__('Slider Type', 'university-education'),
											'type'=> 'combobox',
											'options'=> array(
												'flexslider' => esc_html__('Flex slider', 'university-education'),
												'bxslider' => esc_html__('BX Slider', 'university-education'),
												'nivoslider' => esc_html__('Nivo Slider', 'university-education')
											)
										),		
										'margin-bottom' => array(
											'title' => esc_html__('Margin Bottom', 'university-education'),
											'type' => 'text',											
											'description' => esc_html__('Spaces after ending of this item', 'university-education')
										),											
									)
								),	
								 					
							)
						),
					)
				),
				// page builder option attribute
				array(
					'post_type' => array('page'),
					'title' => 'Page Options',
					'meta_title' => esc_html__('Page Builder Options', 'university-education'),
				)
			);
			
		}
		
	}
	
	
	// show the pagebuilder item
	if( !function_exists('university_education_show_page_builder') ){
		function university_education_show_page_builder($content, $full_width = true){
			global $university_education_counter, $custom_counter_service;
			$section = array(0, false); // (size, independent)
			$custom_counter_service = 0;
			foreach( $content as $item ){			
				// determine the current item size
				$current_size = 1;
				if( !empty($item['size']) ){
					$current_size = university_education_item_size_to_num($item['size']);
				}
				
				// print each section
				if( $item['type'] == 'color-wrapper' || $item['type'] == 'parallax-bg-wrapper' ||
					$item['type'] == 'full-size-wrapper' ){
					$section = university_education_show_section($section, $current_size, true);	
				}else{
					$section = university_education_show_section($section, $current_size, false);	
				}
				
				// start printing item
				if( $item['item-type'] == 'wrapper' ){
					if( $item['type'] == 'color-wrapper' ){
						university_education_show_color_wrapper( $item );
					}else if(  $item['type'] == 'parallax-bg-wrapper'){
						university_education_show_parallax_wrapper( $item );
					}else if(  $item['type'] == 'full-size-wrapper'){
						university_education_show_full_size_wrapper( $item );
					}else{
						university_education_show_column_wrapper( $item );
					}
				}else{
					university_education_show_element( $item );
				}
				$university_education_counter++;
			}
			
			echo '<div class="clear"></div>';
			
			if( !$section[1] ){
				echo '</div>';
				echo '</div>'; 
			} // close container of dependent section
			echo '</div>'; // close the last opened section
			
		}
	}
	
	// print each section
	if( !function_exists('university_education_show_section') ){
		function university_education_show_section( $section, $size, $independent = false ){
			global $university_education_section_id;
			if( empty($university_education_section_id) ){ $university_education_section_id = 1; }

			if( $section[0] == 0 ){ // starting section
				echo '<div id="content-section-' . $university_education_section_id . '" >';
				if( !$independent ){ echo '<div class="section-container container"><div class="row">'; } // open container
				
				$section = array($size, $independent);
				$university_education_section_id ++;
			}else{

				if( $independent || $section[1] ){ // current or previous section is independent
				
					echo '<div class="clear"></div>';
					if( !$section[1] ){ echo '</div></div>'; } // close container of dependent section
					echo '</div>';
					
					echo '<div id="content-section-' . $university_education_section_id . '" >';		
					if( !$independent ){ echo '<div class="section-container container"><div class="row">'; } // open container
					
					$section[0] = ceil($section[0]) + $size; $section[1] = $independent;
					$university_education_section_id ++;
				}else{

					if( abs((float)$section[0] - floor($section[0])) < 0.01 || 	// is integer or
						(floor($section[0]) < floor($section[0] + $size - 0.01)) ){ 	// exceeding current line
						echo '<div class="clear"></div>';
					}
					if( $size == 1 ){
						echo '<div class="clear"></div>';
						$section[0] = ceil($section[0]) + $size; $section[1] = $independent;
					}else{
						$section[0] += $size; $section[1] = $independent;
					}
				}
			}
			
			return $section;
		}
	}	

	
	
	// print color wrapper
	if( !function_exists('university_education_show_color_wrapper') ){
		function university_education_show_color_wrapper( $content ){
			$item_id = empty($content['option']['element-item-id'])? '': ' id="' . esc_attr($content['option']['element-item-id']) . '" ';
			
			global $university_education_spaces;
			$padding  = (!empty($content['option']['padding-top']) && 
				($university_education_spaces['top-wrapper'] != $content['option']['padding-top']))? 
				'padding-top: ' . esc_attr($content['option']['padding-top']) . 'px; ': '';
			$padding .= (!empty($content['option']['padding-bottom']) && 
				($university_education_spaces['bottom-wrapper'] != $content['option']['padding-bottom']))? 
				'padding-bottom: ' . esc_attr($content['option']['padding-bottom']) . 'px; ': '';
				
			$border = '';
			if( !empty($content['option']['border']) && $content['option']['border'] != 'none' ){
				if($content['option']['border'] == 'top' || $content['option']['border'] == 'both'){
					$border .= ' border-top: 4px solid '. esc_attr($content['option']['border-top-color']) . '; ';
				}
				if($content['option']['border'] == 'bottom' || $content['option']['border'] == 'both'){
					$border .= ' border-bottom: 4px solid '. esc_attr($content['option']['border-bottom-color']) . '; ';
				}
			}

			$content['option']['show-section'] = !empty($content['option']['show-section'])? $content['option']['show-section']: '';
			echo '<div class="kode-color-wrapper ' . ' ' . esc_attr($content['option']['show-section']) . '" ' . $item_id;
			if( !empty($content['option']['background']) || !empty($padding) ){
				echo 'style="';
				if( empty($content['option']['background-type']) || $content['option']['background-type'] == 'color' ){
					echo !empty($content['option']['background'])? 'background-color: ' . esc_attr($content['option']['background']) . '; ': '';
				}
				echo esc_attr($border);
				echo esc_attr($padding);
				echo '" ';
			}
			echo '>';
			echo '<div class="container"><div class="row">';
		
			foreach( $content['items'] as $item ){	
				if( $item['item-type'] == 'wrapper' ){
					university_education_show_column_wrapper( $item );
				}else{
					university_education_show_element( $item );
				}
			}	
			
			echo '<div class="clear"></div>';
			echo '</div></div>'; // close container
			echo '</div>'; // close wrapper
		}
	}	
	
	// show parallax wrapper
	if( !function_exists('university_education_show_parallax_wrapper') ){
		function university_education_show_parallax_wrapper( $content ){
			global $parallax_wrapper_id;
			$parallax_wrapper_id = empty($parallax_wrapper_id)? 1: $parallax_wrapper_id;
			if( empty($content['option']['element-item-id']) ){
				$content['option']['element-item-id'] = 'kode-parallax-wrapper-' . $parallax_wrapper_id;
				$parallax_wrapper_id++;
			}
			$item_id = ' id="' . esc_attr($content['option']['element-item-id']) . '" ';

			global $university_education_spaces;
			$padding  = (!empty($content['option']['padding-top']) && 
				($university_education_spaces['top-wrapper'] != $content['option']['padding-top']))? 
				'padding-top: ' . esc_attr($content['option']['padding-top']) . 'px; ': '';
			$padding .= (!empty($content['option']['padding-bottom']) && 
				($university_education_spaces['bottom-wrapper'] != $content['option']['padding-bottom']))? 
				'padding-bottom: ' . esc_attr($content['option']['padding-bottom']) . 'px; ': '';

			$border = '';

			echo '<div class="kode-parallax-wrapper kode-background-' . esc_attr($content['option']['type']) . '" ' . $item_id;
			
			// background parallax
			if( !empty($content['option']['background']) && $content['option']['type'] == 'image' ){
				if( !empty($content['option']['background-speed']) ){
					echo 'data-bgspeed="' . esc_attr($content['option']['background-speed']) . '" ';
				}else{
					echo 'data-bgspeed="0" ';
				}				
			
				if( is_numeric($content['option']['background']) ){
					$background = wp_get_attachment_image_src($content['option']['background'], 'full');
					$background = esc_url($background[0]);
				}else{
					$background = esc_url($content['option']['background']);
				}
				if(empty($content['option']['opacity']) || $content['option']['opacity'] == ''){ $content['option']['opacity'] = '0.03';}
				if(empty($content['option']['background-color']) || $content['option']['background-color'] == ''){ $content['option']['background-color'] = '#000';}
				echo 'style="background-color:'.esc_attr($content['option']['background-color']).';background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';			
				echo '<style scoped type="text/css">';
				echo '#' . esc_attr($content['option']['element-item-id']) . '{';
				echo ' position:relative;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
				echo ' position:relative;z-index:99999;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
				echo 'opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:100%;';
				echo '}';
				echo '</style>';
				if( !empty($content['option']['background-mobile']) ){
					if( is_numeric($content['option']['background-mobile']) ){
						$background = wp_get_attachment_image_src($content['option']['background-mobile'], 'full');
						$background = esc_url($background[0]);
					}else{
						$background = esc_url($content['option']['background-mobile']);
					}				
				
					echo '<style type="text/css">@media only screen and (max-width: 767px){ ';
					echo '#' . esc_attr($content['option']['element-item-id']) . '{';
					echo ' background-image: url(\'' . esc_url($background) . '\') !important;';
					echo '}';
					echo '}</style>';
				}
				
			// background pattern 
			}else if($content['option']['type'] == 'pattern'){
				$background = UOE_PATH . '/images/pattern/pattern-' . esc_attr($content['option']['pattern']) . '.png';
				echo 'style="background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';
			
			// background video
			}else if( $content['option']['type'] == 'video' ){
				echo 'style="' . $padding . $border . '" >';
				
				global $university_education_gallery_id; $university_education_gallery_id++;
				$overlay_opacity = (empty($content['option']['video-overlay']))? 0: floatval($content['option']['video-overlay']);
				
				echo '<div id="kode-player-' . esc_attr($university_education_gallery_id) . '" class="kode-bg-player" data-property="';
				echo '{videoURL:\'' . esc_attr($content['option']['video']) . '\',containment:\'#kode-player-' . esc_attr($university_education_gallery_id) . '\',';
				echo 'startAt:0,mute:true,autoPlay:true,loop:true,printUrl:false,realfullscreen:false,quality:\'hd720\'';
				echo (!empty($content['option']['video-player']) && $content['option']['video-player'] == 'disable')? ',showControls:false':'';
				echo '}"><div class="kode-player-overlay" ';
				echo 'style="opacity: ' . esc_attr($overlay_opacity) . '; filter: alpha(opacity=' . esc_attr($overlay_opacity) * 100 . ');" ';
				echo '></div></div>';

			// background video / none
			}else if($content['option']['type'] == 'map'){
				echo '><span class="footertransparent-bg"></span>';
				echo do_shortcode($content['option']['map_shortcode']);
				echo '';
			}else if(!empty($padding) || !empty($border) ){
				echo 'style="' . $padding . $border . '" >';
			}

			echo '<div class="container"<div class="row">';
		
			foreach( $content['items'] as $item ){
				if( $item['item-type'] == 'wrapper' ){
					university_education_show_column_wrapper( $item );
				}else{
					university_education_show_element( $item );
				}
			}	
			
			echo '<div class="clear"></div>';
			echo '</div>'; // close row
			echo '</div>'; // close container
			echo '</div>'; // close wrapper
		}
	}
	
	// print full size wrapper
	if( !function_exists('university_education_show_full_size_wrapper') ){
		function university_education_show_full_size_wrapper( $content ){
			global $university_education_wrapper_id;
			$university_education_wrapper_id = empty($university_education_wrapper_id)? 1: $university_education_wrapper_id;
			if( empty($content['option']['element-item-id']) ){
				$content['option']['element-item-id'] = 'kode-parallax-wrapper-' . $university_education_wrapper_id;
				$university_education_wrapper_id++;
			}
			
			$university_education_trans_class = '';
			if( !empty($content['option']['trans-background']) ){
				$university_education_trans_class = $content['option']['trans-background'];
			}
			$university_education_wrapper_class = '';
			if( !empty($content['option']['element-item-class']) ){
				$university_education_wrapper_class = $content['option']['element-item-class'];
			}
			$item_id = ' id="' . esc_attr($content['option']['element-item-id']) . '" ';

			global $university_education_spaces;
			$padding  = (!empty($content['option']['padding-top']) && 
				($university_education_spaces['top-wrapper'] != $content['option']['padding-top']))? 
				'padding-top: ' . esc_attr($content['option']['padding-top']) . 'px; ': '';
			$padding .= (!empty($content['option']['padding-bottom']) && 
				($university_education_spaces['bottom-wrapper'] != $content['option']['padding-bottom']))? 
				'padding-bottom: ' . esc_attr($content['option']['padding-bottom']) . 'px; ': '';

			$border = '';
			$content['option']['type'] = (empty($content['option']['type']))? ' ': $content['option']['type'];
			$university_education_trans_bg = '';
			$university_education_solid_bg = '';
			if($university_education_trans_class == 'enable'){
				$university_education_trans_bg = "background-color:".esc_attr($content['option']['background-color'])."";
			}else{
				$university_education_solid_bg = "background-color:".esc_attr($content['option']['background-color'])."";
			}
			if( !empty($content['option']['horizontal-background']) && $content['option']['horizontal-background'] == 'enable'){
				$university_education_wrapper_class .= ' overlay movingbg';
			}
			echo '<div class="'.esc_attr($university_education_wrapper_class).' kode-parallax-wrapper kode-background-' . esc_attr($content['option']['type']) . '" ' . $item_id;
			
			// background parallax
			if( !empty($content['option']['background']) && $content['option']['type'] == 'image' ){
				
				
				if( !empty($content['option']['horizontal-background']) && $content['option']['horizontal-background'] == 'enable'){
					echo 'data-id="customizer" data-title="Theme Customizer" data-direction="horizontal" ';
				}
				if( !empty($content['option']['background-speed']) ){
					echo 'data-bgspeed="' . esc_attr($content['option']['background-speed']) . '" ';
				}else{
					echo 'data-bgspeed="0" ';
				}				
			
				if( is_numeric($content['option']['background']) ){
					$background = wp_get_attachment_image_src($content['option']['background'], 'full');
					$background = esc_url($background[0]);
				}else{
					$background = esc_url($content['option']['background']);
				}
				if(empty($content['option']['opacity']) || $content['option']['opacity'] == ''){
					$content['option']['opacity'] = '0.03';
				}
				
				if(empty($content['option']['background-color']) || $content['option']['background-color'] == ''){
					$content['option']['background-color'] = '#000';
				}
				
				$bg_width = '50%';
				if(isset($content['option']['background-width']) && $content['option']['background-width'] <> ''){
					$bg_width = $content['option']['background-width'];
				}
				$bg_attachement = 'scroll';
				if(isset($content['option']['background-attachment']) && $content['option']['background-attachment'] <> ''){
					$bg_attachement = $content['option']['background-attachment'];
				}
				$bg_size = 'auto';
				if(isset($content['option']['background-size']) && $content['option']['background-size'] <> ''){
					$bg_size = $content['option']['background-size'];
				}
				$bg_position = 'auto';
				if(isset($content['option']['background-position']) && $content['option']['background-position'] <> ''){
					$bg_position = $content['option']['background-position'];
				}
				if(isset($content['option']['background-hook']) && $content['option']['background-hook'] == 'enable'){
					echo 'style="'.$university_education_solid_bg.';'.$padding . $border . '" ><style scoped>';
					echo '#' . esc_attr($content['option']['element-item-id']) . ':after{';
					
					if(isset($content['option']['background-image-align']) && $content['option']['background-image-align'] == 'left'){
						echo ''.$university_education_solid_bg.';background-image: url(\'' . esc_url($background) . '\');background-position: '.$bg_position.';
							background-attachment: '.$bg_attachement.';
							background-repeat: no-repeat;
							background-size: '.$bg_size.';
							content: "";
							height: 100%;
							left: 0;
							position: absolute;
							top: 0;
							width: '.$bg_width.';';
						echo '}';
						echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
						echo ''.$university_education_solid_bg.';z-index:1;opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:'.$bg_width.';';
						echo '}';
					}else{
							echo ''.$university_education_solid_bg.';background-image: url(\'' . esc_url($background) . '\');background-position: '.$bg_position.';
							background-attachment: '.$bg_attachement.';
							background-repeat: no-repeat;
							background-size: '.$bg_size.';
							content: "";
							height: 100%;
							right: 0;
							position: absolute;
							top: 0;
							width: '.$bg_width.';';
						echo '}';
						echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
						echo ''.$university_education_solid_bg.';z-index:1;opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;right:0px;top:0px;height:100%;width:'.$bg_width.';';
						echo '}';
					}
					echo '#' . esc_attr($content['option']['element-item-id']) . '{';
					echo ' position:relative;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' > .container-fluid{';
					echo ' position:relative;z-index:99999;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
					echo ' position:relative;z-index:99999;';
					echo '}';
					
					echo '</style>';
				}else{
					if($background <> ''){
						echo 'style="'.$university_education_trans_bg.'; background-size:'.$bg_size.'; background-attachment:'.$bg_attachement.'; background-position:'.$bg_position.'; background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';			
					}else{
						echo 'style="'.$university_education_trans_bg.';' . $padding . $border . '" >';			
					}
					echo '<style scoped>';
					echo '#' . esc_attr($content['option']['element-item-id']) . '{';
					echo ' position:relative;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
					echo ' position:relative;z-index:99999;';
					echo '}';
					echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
					echo ''.$university_education_solid_bg.';opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:100%;';
					echo '}';
					echo '</style>';
				}
				
				
			// background pattern 
			}else if($content['option']['type'] == 'pattern'){
				$background = UOE_PATH . '/images/pattern/pattern-' . esc_attr($content['option']['pattern']) . '.png';
				echo 'style="background-image: url(\'' . esc_url($background) . '\'); ' . $padding . $border . '" >';
			
			// background MAP
			}else if($content['option']['type'] == 'map'){
				echo '><span class="footertransparent-bg"></span>';
				echo do_shortcode($content['option']['map_shortcode']);
				echo '';
			}else if($content['option']['type'] == 'color'){
				$content['option']['background-color'] = (empty($content['option']['background-color']))? ' ': $content['option']['background-color'];
				echo ' style="' . $padding . $border . ';background:'.esc_attr($content['option']['background-color']).'">';
				echo '';
			}else if($content['option']['type'] == 'video'){
				echo ' style="' . $padding . $border . ';">';
				echo '<style scoped>';
				echo '#' . esc_attr($content['option']['element-item-id']) . '{';
				echo ' position:relative;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ' .container{';
				echo ' position:relative;z-index:99999;';
				echo '}';
				echo '#' . esc_attr($content['option']['element-item-id']) . ':before{';
				echo ''.$university_education_solid_bg.';opacity:'.esc_attr($content['option']['opacity']).';content:"";position:absolute;left:0px;top:0px;height:100%;width:100%;z-index:10;';
				echo '}';
				echo '</style>';
				
				$content['option']['video_url'] = (empty($content['option']['video_url']))? UOE_PATH.'/images/ocean.ogv': $content['option']['video_url'];
				$content['option']['video_type'] = (empty($content['option']['video_type']))? 'ogg': $content['option']['video_type'];
				echo '
				    <script>
						jQuery(document).ready(function($) {
							var BV = new $.BigVideo({
								useFlashForFirefox:false,
								container: $("#inner-' . esc_attr($content['option']['element-item-id']) . '"),
								forceAutoplay:true,
								controls:false,
								doLoop:false,			
								shrinkable:true
							});
							BV.init();
							BV.show([
								{ type: "video/'.esc_attr($content['option']['video_type']).'",  src: "'.esc_url($content['option']['video_url']).'" },
								
							],{doLoop:true});
						});
					</script>
					<div class="kode-video-bg" id="inner-' . esc_attr($content['option']['element-item-id']) . '"></div>';
				
			}
			else if(!empty($padding) || !empty($border) ){
				echo 'style="' . $padding . $border . '" >';
			}
			$content['option']['container'] = (empty($content['option']['container']))? ' ': $content['option']['container'];
			if($content['option']['container'] == 'container-width'){
				echo '<div class="container">';
				echo '<div class="row">';
			}else{
				echo '<div class="container-fluid">';
				echo '<div class="row">';
			}
		
			foreach( $content['items'] as $item ){
				if( $item['item-type'] == 'wrapper' ){
					university_education_show_column_wrapper( $item );
				}else{
					university_education_show_element( $item );
				}
			}	
			
			echo '<div class="clear"></div>';
			if($content['option']['container'] == 'container-width'){
				echo '</div>'; // close container or Container
				echo '</div>'; // close container or Row
			}else{
				echo '</div>'; // close container or Row
				echo '</div>'; // close container or Container-fluid
			}			
			echo '</div>'; // close wrapper
		}
	}	
	
	// Column Sizes Bootstrap 3+
	if( !function_exists('university_education_get_column_class') ){
		function university_education_get_column_class( $size ){
			switch( $size ){
				case '1/12': return 'col-md-1 columns'; break;
				case '1/6': return 'col-md-2 column'; break;
				case '1/5': return 'one-fifth column'; break;
				case '1/4': return 'col-md-3 columns'; break;
				case '2/5': return 'col-md-5 columns'; break;
				case '1/3': return 'col-md-4 columns'; break;
				case '1/2': return 'col-md-6 columns'; break;
				case '3/5': return 'col-md-7 columns'; break;
				case '2/3': return 'col-md-8 columns'; break;
				case '3/4': return 'col-md-9 columns'; break;
				case '4/5': return 'col-md-10 columns'; break;
				case '1/1': return 'col-md-12 columns'; break;
				default : return 'col-md-12 columns'; break;
			}
		}
	}
	
	// show column wrapper
	if( !function_exists('university_education_show_column_wrapper') ){
		function university_education_show_column_wrapper( $content ){
			$university_education_counter = 0;
			$content['option']['element-item-class'] = (empty($content['option']['element-item-class']))? ' ': $content['option']['element-item-class'];
			echo '<div class="'.esc_attr($content['option']['element-item-class']).' ' . esc_attr(university_education_get_column_class( $content['size'] )) . '" ><div class="row">';
			foreach( $content['items'] as $item ){
				university_education_show_element( $item );
				$university_education_counter++;
			}			
			echo '</div></div>'; // end of column section
		}
	}	
	
	// show the item
	if( !function_exists('university_education_show_element') ){
		function university_education_show_element( $content ){
			global $university_education_theme_option,$university_education_post_option;
			switch ($content['type']){
				case 'accordion': echo university_education_get_accordion_item($content['option']); break;	
				case 'blog': echo university_education_get_blog_item($content['option']); break;
				case 'events': 
					if(class_exists('EM_Events')){
						echo university_education_get_events_item($content['option']); 
					}
				break;				
				case 'woo': 
					if(class_exists('WooCommerce')){
						echo university_education_get_woo_item($content['option']); 
					}
				break;
				case 'woo-slider': 
					if(function_exists('university_education_get_woo_item_slider') ){
						echo university_education_get_woo_item_slider($content['option']); 
					}
				break;
				case 'teacher': 
					if(function_exists('university_education_get_teacher_item') ){
						echo university_education_get_teacher_item($content['option']); 
					}
				break;
				case 'header-element': 
					
					$university_education_post_option = $content['option'];
					$university_education_theme_option['enable-header-top'] = 'enable';
					echo '<div class="kode-header-pagebuilder">';
					university_education_get_selected_header($university_education_post_option,$university_education_theme_option); 
					echo '</div>';
				break;				
				case 'blog-slider': echo university_education_get_blog_slider_item($content['option']); break;
				
				case 'column-service': echo university_education_get_column_service_item($content['option']); break;
				case 'service': echo university_education_get_service_item($content['option']); break;
				case 'headings': echo university_education_get_headings_item($content['option']); break;
				case 'teacher-profile': 
				if(function_exists('kode_get_teacher_profile_item')){
					echo kode_get_teacher_profile_item($content['option']);
				}
				break;
				case 'course': 
				if(function_exists('university_education_get_course_item')){
					echo university_education_get_course_item($content['option']);
				}
				break;
				case 'simple-column': echo university_education_get_simple_column_item($content['option']); break;
				case 'content': university_education_get_default_content_item($content['option']); break;
				case 'divider': echo university_education_get_divider_item($content['option']); break;
				case 'gallery': echo university_education_get_gallery_item($content['option']); break;				
				case 'post-slider': echo university_education_get_wp_post_slider($content['option']); break;				
				case 'call-to-action': echo university_education_get_call_to_action($content['option']); break;				
				case 'upcoming-event': 
				if(function_exists('university_education_get_upnext_event')){
					echo university_education_get_upnext_event($content['option']);
				}
				break;
				case 'project-facts': echo university_education_get_project_facts($content['option']); break;				
				case 'sidebar': echo '<div class="col-md-12"><div class="kode-widget kode-sidebar-element">';university_education_get_sidebar_item($content['option']);echo '</div></div>'; break;
				case 'slider': echo university_education_get_slider_item($content['option']); break;				
				case 'testimonial': 
				if(function_exists('university_education_get_testimonial_item') ){
					echo university_education_get_testimonial_item($content['option']); 
				}
				break;
				case 'courses-search': 
					if(function_exists('university_education_get_courses_search_form')){
						echo university_education_get_courses_search_form($content['option']);
					}
				break;
				
				default: $default['show-title'] = 'enable'; $default['show-content'] = 'enable'; echo university_education_get_content_item($default); break;
			}
		}	
	}
	
	
	
?>