<?php 
	/*	
	*	Kodeforest Framework File
	*	---------------------------------------------------------------------
	*	This file includes the functions to run the plugins - Theme Options
	*	---------------------------------------------------------------------
	*/

	if(is_ssl()){
		define('KODE_HTTP', 'https://');
	}else{
		define('KODE_HTTP', 'http://');
	}
	
	//Data validation HTMl
	if( !function_exists('university_education_esc_html') ){	
		function university_education_esc_html ($html) {
			return esc_html($html);
		}
	}	
	
	//Data Validation
	if( !function_exists('university_education_esc_html_excerpt') ){	
		function university_education_esc_html_excerpt ($html) {
			return esc_html(strip_tags($html));
		}
	}
	
	//Get the Theme Options
	$university_education_theme_option = get_option('university_education_admin_option', array());		
	
	
	//Define the content width - Clearing the ThemeCheck
	$university_education_theme_option['content-width'] = 960;
	
	//Default Variables
	$university_education_gallery_id = 0;
	$university_education_lightbox_id = 0;
	$university_education_crop_video = false;
	$university_education_excerpt_length = 55;
	$university_education_excerpt_read_more = true;
	$university_education_wrapper_id = 0;
	
	
	
	$university_education_thumbnail_size = array(
		'uoe-full-slider' => array('width'=>1600, 'height'=>900, 'crop'=>true),
		'uoe-post-thumbnail-size' => array('width'=>385, 'height'=>545, 'crop'=>true),
		'uoe-team-size' => array('width'=>350, 'height'=>350, 'crop'=>true),
		'uoe-serv-size' => array('width'=>345, 'height'=>315, 'crop'=>true),
		'uoe-small-serv-size' => array('width'=>260, 'height'=>325, 'crop'=>true),
		'uoe-blog-size' => array('width'=>570, 'height'=>300, 'crop'=>true),
		'uoe-property-modern' => array('width'=>370, 'height'=>225, 'crop'=>true),
		'uoe-blog-post-size' => array('width'=>1170, 'height'=>350, 'crop'=>true),
	);
	
	$university_education_thumbnail_size = apply_filters('custom-thumbnail-size', $university_education_thumbnail_size);
	// Create Sizes on the theme activation
	add_action( 'after_setup_theme', 'university_education_define_thumbnail_size' );
	if( !function_exists('university_education_define_thumbnail_size') ){
		function university_education_define_thumbnail_size(){
			add_theme_support( 'post-thumbnails' );
		
			global $university_education_thumbnail_size;		
			foreach($university_education_thumbnail_size as $university_education_size_slug => $university_education_size){
				add_image_size($university_education_size_slug, $university_education_size['width'], $university_education_size['height'], $university_education_size['crop']);
			}
		}
	}
	
	// add the image size filter to ThemeOptions
	add_filter('image_size_names_choose', 'university_education_define_custom_size_image');
	function university_education_define_custom_size_image( $sizes ){	
		$additional_size = array();
		
		global $university_education_thumbnail_size;
		foreach($university_education_thumbnail_size as $university_education_size_slug => $university_education_size){
			$additional_size[$university_education_size_slug] = $university_education_size_slug;
		}
		
		return array_merge($sizes, $additional_size);
	}
	
	// Get All The Sizes
	function university_education_get_thumbnail_list(){
		global $university_education_thumbnail_size;
		
		$sizes = array();
		foreach( get_intermediate_image_sizes() as $size ){
			if(in_array( $size, array( 'thumbnail', 'medium', 'large' )) ){
				$sizes[$size] = $size . ' -- ' . get_option($size . '_size_w') . 'x' . get_option($size . '_size_h');
			}else if( !empty($university_education_thumbnail_size[$size]) ){
				$sizes[$size] = $size . ' -- ' . $university_education_thumbnail_size[$size]['width'] . 'x' . $university_education_thumbnail_size[$size]['height'];
			}else{
			
			}
		}
		$sizes['full'] = esc_html__('full size (Real Images)', 'university-education');
		
		return $sizes;
	}	
	
	// Get All The Sizes
	function university_education_get_thumbnail_list_emptyfirst(){
		global $university_education_thumbnail_size;
		
		$sizes = array('0'=>'');
		foreach( get_intermediate_image_sizes() as $size ){
			if(in_array( $size, array( 'thumbnail', 'medium', 'large' )) ){
				$sizes[$size . ' -- ' . get_option($size . '_size_w') . 'x' . get_option($size . '_size_h')] = $size;
			}else if( !empty($university_education_thumbnail_size[$size]) ){
				$sizes[$size . ' -- ' . $university_education_thumbnail_size[$size]['width'] . 'x' . $university_education_thumbnail_size[$size]['height']] = $size;
			}else{
			
			}
		}
		$sizes['full size (Real Images)'] = esc_html__('full', 'university-education');
		
		return $sizes;
	}	
	
	
	// create page builder
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kf_function_utility.php');
	include_once(UOE_LOCAL_PATH . '/framework/kf_function_regist.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode-demo-options.php');
	
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kf_function_utility.php');	
	
	
	// Create Theme Options
	include_once(UOE_LOCAL_PATH . '/framework/include/kf_pagebuilder.php');	
	include_once(UOE_LOCAL_PATH . '/framework/include/kf_themeoption.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/kf_themeoption_color.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/kf_themeoption_typo.php');
	
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kode-include-script.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kode_google_fonts.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kf-file-system.php');
	
	// Frontend Assets & functions
		
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode_loadstyle.php');
	

	//Events
	if(class_exists('EM_Events')){
		include_once( UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode-events-options.php');
	}
	//WooCommerce
	if(class_exists('WooCommerce')){
		include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode-woo-options.php');
	}
	//IgnitionDeck
	if(class_exists('Deck')){
		include_once( UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode-igni-options.php');
	}
	
	
	// create page options
	include_once( UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode-post-options.php');
	
	//Frontend
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/elements/kf_media_center.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/elements/kode_page_elements.php');
	include_once( UOE_LOCAL_PATH . '/framework/include/kode_front_func/elements/kf_blogging.php');		
	
	// page builder template
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kf_themeoptions_html.php');	
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kf_theme_meta.php');
	
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kf_pagebuilder_backend.php');	
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kf_pagebuilder_meta.php');	
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/kf_pagebuilder_scripts.php');	
	
	
?>