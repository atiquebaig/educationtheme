<?php
/**
 * A template for calling the left sidebar on everypage
 */
if( !function_exists('university_education_sidebar_left') ){
	function university_education_sidebar_left(){ 
		global $university_education_sidebar;
		if(is_active_sidebar('sidebar-'.$university_education_sidebar['left-sidebar'])){ ?>
			<?php if( $university_education_sidebar['type'] == 'left-sidebar' || $university_education_sidebar['type'] == 'both-sidebar' ){ ?>
			<div class="kode-sidebar kode-left-sidebar columns">
				<?php dynamic_sidebar($university_education_sidebar['left-sidebar']); ?>
			</div>
			<?php }
		}
	}
}
university_education_sidebar_left();
?>