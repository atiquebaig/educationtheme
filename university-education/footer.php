	</div>
    <!--Twitter Wrap End-->
	<?php $university_education_theme_option = get_option('university_education_admin_option', array()); ?>
	<?php if(isset($university_education_theme_option['show-newsletter']) && $university_education_theme_option['show-newsletter'] == 'enable'){ 
		if( function_exists('university_education_newsletter_script') ){
			university_education_newsletter_script($university_education_theme_option);
		}
	} ?>
	<!--NEWS LETTERS END-->
	<!--FOOTER START-->
	<?php if(isset($university_education_theme_option['show-footer']) && $university_education_theme_option['show-footer'] == 'enable'){ ?>
	<footer>
		<!--EDU2 FOOTER CONTANT WRAP START-->
			<div class="container">
				<div class="row">
					<?php dynamic_sidebar('Footer'); ?>
				</div>
			</div>
	</footer>
	<?php } ?>
	<!--FOOTER END-->
	<!--COPYRIGHTS START-->
	<div class="edu2_copyright_wrap">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="edu2_ft_logo_wrap">
						<a class="footer-logo" href="<?php echo esc_url(home_url('/')); ?>" >
							<?php 
								if(empty($university_education_theme_option['footer-logo-image'])){ 
									echo university_education_get_image(UOE_PATH . '/images/logo_2.png');
								}else{
									if(university_education_get_image($university_education_theme_option['footer-logo-image']) <> ''){
										echo university_education_get_image($university_education_theme_option['footer-logo-image']);	
									}else{
										echo university_education_get_image(UOE_PATH . '/images/logo_2.png');
									}
								}
							?>						
						</a>
					</div>
				</div>
				
				<?php if(isset($university_education_theme_option['show-copyright']) && $university_education_theme_option['show-copyright'] == 'enable'){ ?>
				<div class="col-md-6">
					<div class="copyright_des">
						<?php echo wp_kses($university_education_theme_option['kode-copyright-text'],array('ul'=>array(),'li'=>array(),'a'=>array('href'=>array())));?>						
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<!--COPYRIGHTS START--> 
	<?php 
		if( function_exists('university_education_sidebar_script') ){ 
			university_education_sidebar_script($university_education_theme_option);
		}
	?>
</div>
<!--Kode Wrapper End-->	
<?php wp_footer(); ?>		
</body>
</html>