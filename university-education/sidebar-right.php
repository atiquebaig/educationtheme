<?php
/**
 * A template for calling the right sidebar in everypage
 */
if( !function_exists('university_education_sidebar_right') ){
	function university_education_sidebar_right(){ 
		global $university_education_sidebar;
		if(is_active_sidebar('sidebar-'.$university_education_sidebar['right-sidebar'])){ ?>
			<?php if( $university_education_sidebar['type'] == 'right-sidebar' || $university_education_sidebar['type'] == 'both-sidebar' ){ ?>
			<div class="kode-sidebar kode-right-sidebar columns">
				<?php dynamic_sidebar($university_education_sidebar['right-sidebar']); ?>
			</div>
			<?php }
		}
	}
}	
university_education_sidebar_right();
 ?>