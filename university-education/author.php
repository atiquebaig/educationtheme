<?php
	/*	
	*	Kodeforest Author Page
	*	---------------------------------------------------------------------
	*	This file includes the function to create / control the Author
	*	---------------------------------------------------------------------
	*/
	get_header();
	
	?>
	<div class="content">
		<div class="container">
			<div class="row">
			<?php 
				$university_education_theme_option = get_option('university_education_admin_option', array());	
				$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
				if( !empty($university_education_post_option) ){
					$university_education_post_option = json_decode( $university_education_post_option, true );					
				}
			
				$university_education_sidebar = array(
					'type'=>$university_education_theme_option['archive-sidebar-template'],
					'left-sidebar'=>$university_education_theme_option['archive-sidebar-left'], 
					'right-sidebar'=>$university_education_theme_option['archive-sidebar-right']
				); 
				
				
				$university_education_sidebar = university_education_get_sidebar_class($university_education_sidebar);
				if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'left-sidebar'){ ?>
					<div class="<?php echo esc_attr($university_education_sidebar['left'])?>">
						<?php get_sidebar('left'); ?>
					</div>	
				<?php } ?>
				<div class="<?php echo esc_attr($university_education_sidebar['center'])?> kode-main-content">
						<?php
							if( is_tax('event-categories') && is_tax('event-tags') ){
								// set the excerpt length
								if( !empty($university_education_theme_option['archive-num-excerpt']) ){
									$university_education_excerpt_length = $university_education_theme_option['archive-num-excerpt'];
									add_filter('excerpt_length', 'university_education_set_excerpt_length');
								} 
							
								$university_education_lightbox_id++;
								$settings['num-fetch'] = -1;
								$settings['element-item-class'] = '';
								$settings['event-style'] = 'event-grid-view';
								$settings['tag'] = strtolower(single_cat_title('', false));							
								$settings['element-item-id'] = '';
								$settings['margin-bottom'] = 30;							
								$settings['thumbnail-size'] = 'full';
								$settings['scope'] = 'all';
								$settings['event-size'] = 3;
								
								//$university_education_post_settings['blog-style'] = $university_education_theme_option['archive-blog-style'];	

								echo '<div class="kode-event-column-archive">';
								$settings['order'] = 'asc';
								$settings['pagination'] = 'disable';
								echo university_education_get_events_item($settings);
								echo '<div class="clear"></div>';
								echo '</div>';
							
							}else if( is_tax('work_category') && is_tax('work_tag') ){
								// set the excerpt length
								if( !empty($university_education_theme_option['archive-num-excerpt']) ){
									$university_education_excerpt_length = $university_education_theme_option['archive-num-excerpt'];
									add_filter('excerpt_length', 'university_education_set_excerpt_length');
								} 

								
								$university_education_lightbox_id++;
								$settings['num-fetch'] = -1;
								$settings['element-item-class'] = '';
								$settings['tag'] = strtolower(single_cat_title('', false));
								
								$settings['work-padding'] = 'with-padding';
								$settings['element-item-id'] = '';
								$settings['margin-bottom'] = 30;
								$settings['work-filterable'] = 'disable';
								$settings['thumbnail-size'] = 'full';
								$settings['work-size'] = 4;
								
								//$university_education_post_settings['blog-style'] = $university_education_theme_option['archive-blog-style'];	

								echo '<div class="kode-work-column-archive">';
								$settings['work-style'] = 'style-2';
								$settings['pagination'] = 'enable';
								echo university_education_get_work_item($settings);
								echo '<div class="clear"></div>';
								echo '</div>';
							
							
							}else{
									// set the excerpt length
								if( !empty($university_education_theme_option['archive-num-excerpt']) ){
									$university_education_excerpt_length = $university_education_theme_option['archive-num-excerpt'];
									add_filter('excerpt_length', 'university_education_set_excerpt_length');
								} 

								
								$university_education_lightbox_id++;
								$university_education_post_settings['excerpt'] = intval($university_education_theme_option['archive-num-excerpt']);
								$university_education_post_settings['thumbnail-size'] = 'full';
								$university_education_post_settings['title-num-fetch'] = 200;
								$university_education_post_settings['blog-style'] = $university_education_theme_option['archive-blog-style'];					
								echo '<div class="kode-blog-list-archive kode-fullwidth-blog row">';
								if($university_education_theme_option['archive-blog-style'] == 'blog-full'){
									echo university_education_get_blog_full($wp_query);
								}else if($university_education_theme_option['archive-blog-style'] == 'blog-medium'){
									echo '<div class="kode-blog-list-archive kode-mediium-blog margin-bottom">';
									echo university_education_get_blog_medium($wp_query);			
									echo '</div>';
								}else{
									$blog_size = 3;
									echo '<div class="kode-blog-list-archive kode-blog-grid margin-bottom-30">';
									echo university_education_get_blog_grid($wp_query, $blog_size, 'fitRows');
									echo '</div>';	
								}
								echo '<div class="clear"></div>';
								echo '</div>';
								remove_filter('excerpt_length', 'university_education_set_excerpt_length');
								
								$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
								echo university_education_get_pagination($wp_query->max_num_pages, $paged);													
							
							}
						?>
					</div>
				<?php
				if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'right-sidebar' && $university_education_sidebar['right'] != ''){ ?>
					<div class="<?php echo esc_attr($university_education_sidebar['right'])?>">
						<?php get_sidebar('right'); ?>
					</div>	
				<?php } ?>
			</div><!-- Row -->	
		</div><!-- Container -->		
	</div><!-- content -->
	<?php
	
	
	get_footer(); ?>