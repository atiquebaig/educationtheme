<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7 ltie8 ltie9" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8 ltie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />    
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />	
	
	<?php 
	$university_education_theme_option = get_option('university_education_admin_option', array());
	wp_head();
?>
</head>
<body <?php body_class();?> id="home">
<div class="body-wrapper" data-home="<?php echo esc_url(home_url('/')); ?>">
<?php university_education_get_header($university_education_theme_option); get_template_part( 'header', 'title' ); ?>
	<div class="content-wrapper">
	
	