<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			if( empty($university_education_post_option['sidebar']) || $university_education_post_option['sidebar'] == 'default-sidebar' ){
				$university_education_sidebar = array(
					'type'=>$university_education_theme_option['all-products-sidebar'],
					'left-sidebar'=>$university_education_theme_option['all-products-sidebar-left'], 
					'right-sidebar'=>$university_education_theme_option['all-products-sidebar-right']
				); 
			}else{
				$university_education_sidebar = array(
					'type'=>$university_education_post_option['sidebar'],
					'left-sidebar'=>$university_education_post_option['left-sidebar'], 
					'right-sidebar'=>$university_education_post_option['right-sidebar']
				); 				
			}
			$university_education_sidebar = university_education_get_sidebar_class($university_education_sidebar);
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="<?php echo esc_attr($university_education_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="<?php echo esc_attr($university_education_sidebar['center'])?>">
				<div class="kode-item woocommerce-content-item">
					<div class="woocommerce-content">
					<?php woocommerce_content(); ?>
					</div>				
				</div>				
			</div>
			<?php
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'right-sidebar' && $university_education_sidebar['right'] != ''){ ?>
				<div class="<?php echo esc_attr($university_education_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>