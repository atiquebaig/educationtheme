<?php 
if( !function_exists('university_education_fetch_post_format_thumbnail') ){
	function university_education_fetch_post_format_thumbnail(){
		if( !is_single() ){ 
			global $post,$university_education_post_settings; 
			
			$university_education_post_settings['content'] = get_the_content();
			$university_education_theme_option = get_option('university_education_admin_option', array());
		}else{
			global $post,$university_education_post_settings;
			$university_education_theme_option = get_option('university_education_admin_option', array());
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			
			$university_education_post_settings = $university_education_post_option;
			$university_education_theme_option['thumbnail-size'] = 'full';
		}
		
		if ( has_post_thumbnail() && ! post_password_required() ){
			
			$k_post_option = json_decode(get_post_meta($post->ID, 'post-option', true), true);		
			
			$thumbnail_size = (empty($university_education_post_settings['thumbnail-size']))? $university_education_theme_option['thumbnail-size']: $university_education_post_settings['thumbnail-size'];
			
			//Get Thumbnail Width and Height
			$university_education_img_size = university_education_get_video_size($thumbnail_size);
			$university_education_width = $university_education_img_size['width'];
			$university_education_height = $university_education_img_size['height'];
			
			// Get Slider Images
			$k_post_option['slider'] = (empty($k_post_option['slider']))? ' ': $k_post_option['slider'];
			$raw_slider_data = university_education_decode_stopbackslashes($k_post_option['slider']);	
			$filter_slider_data = json_decode(university_education_stripslashes($raw_slider_data), true);		
			
			//Get Media Type Selected
			$k_post_option['post_media_type'] = (empty($k_post_option['post_media_type']))? ' ': $k_post_option['post_media_type'];
			
			//Condition for Audio, Video, Slider
			if($k_post_option['post_media_type'] == 'audio'){
				if(isset($k_post_option['university_education_audio']) && strpos($k_post_option['university_education_audio'],'soundcloud')){				
					echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[soundcloud url="'.esc_url($k_post_option['university_education_audio']).'" comments="yes" auto_play="no" color="#ff7700" width="100%" height="'.esc_attr($university_education_height).'px"][/soundcloud]') . '</div>';				
				}else{				
					$university_education_get_image = university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
					if(!empty($university_education_get_image)){
						echo '<div class="kode-blog-thumbnail kode-ux">';
							if( is_single() ){
									echo university_education_get_image(get_post_thumbnail_id(), $thumbnail_size, true);	
									echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['university_education_audio'].'"][/audio]') . '</div>';
							}else{
								$temp_option = json_decode(get_post_meta(get_the_ID(), 'post-option', true), true);
								echo university_education_get_image(get_post_thumbnail_id(), $thumbnail_size,true);
								if(isset($k_post_option['university_education_audio'])){
									echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['university_education_audio'].'"][/audio]') . '</div>';							
								}
								
							}						
						echo '</div>';
					}else{
						echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['university_education_audio'].'"][/audio]') . '</div>';
					}
					
				}
			}else if($k_post_option['post_media_type'] == 'video'){
				echo '<div class="kode-blog-thumbnail kode-video">' . university_education_get_video($k_post_option['university_education_video'],$thumbnail_size).'</div>';
			}else if($k_post_option['post_media_type'] == 'slider'){
				echo university_education_get_slider( $filter_slider_data, esc_attr($thumbnail_size), 'bxslider' );
			}else{
				$university_education_get_image = university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
				if(!empty($university_education_get_image)){
					echo '<div class="kode-blog-thumbnail kode-ux">';
						if( is_single() ){
								echo university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);						
						}else{
							$temp_option = json_decode(get_post_meta(get_the_ID(), 'post-option', true), true);
							
							
							echo university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size));
							
						}
					echo '</div>';
				}
			}
			
		} 
	}
}
university_education_fetch_post_format_thumbnail();