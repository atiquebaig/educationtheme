<?php
if( !function_exists('university_education_fetch_post_format_news') ){
	function university_education_fetch_post_format_news(){
		if( in_array(get_post_format(), array('aside', 'link', 'quote')) ){
			get_template_part('single/content', get_post_format());
		}else{
			
			global $university_education_post_settings;
			if( empty($university_education_post_settings['blog-style']) || $university_education_post_settings['blog-style'] == 'blog-full' ){
				get_template_part('single/content-news-full');
			}else if( strpos($university_education_post_settings['blog-style'], 'blog-grid') !== false ){
				get_template_part('single/content-news-grid');
			}else if( strpos($university_education_post_settings['blog-style'], 'blog-medium') !== false ){
				get_template_part('single/content-news-medium');
			}else{
				get_template_part('single/content-news-grid');
			}
			
		} 
	}
}
university_education_fetch_post_format_news();