<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('university_education_fetch_post_full') ){
	function university_education_fetch_post_full(){		
		global $university_education_post_settings,$university_education_theme_option; 
		if(isset($university_education_post_settings['excerpt']) && $university_education_post_settings['excerpt'] < 0) global $university_education_more; $university_education_more = 0;
		if(isset($university_education_post_settings['title-num-fetch'])){
			$title_num_fetch = $university_education_post_settings['title-num-fetch'];
		}		
		$university_education_post_settings['content'] = get_the_content();
		if(!isset($university_education_post_settings['title-num-fetch']) && empty($university_education_post_settings['title-num-fetch'])){
			$title_num_fetch = 100;
		}
		$thumbnail_size = (empty($university_education_post_settings['thumbnail-size']))? $university_education_theme_option['uoe-post-thumbnail-size']: $university_education_post_settings['thumbnail-size'];?>
		
		<!--EDUCATION BLOG PAGE WRAP START-->
		<article id="post-<?php the_ID(); ?>" <?php post_class('edu2_blogpg_wrap'); ?>>		
			<div class="edu2_blogpg_des">
				<?php $university_education_get_image = university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
				if(!empty($university_education_get_image)){ 
					$format = get_post_format(); ?>
					<div class="blog_slider_thumb">
						<?php get_template_part('single/thumbnail', get_post_format()); ?>
						<?php if($format == 'gallery' || $format == 'image' ){ ?>
							
						<?php }else{ ?>
							<div class="kode-author"><?php echo get_avatar(get_the_author_meta('ID'), 90);?></div>
						<?php }?>
					</div>
					<?php 
				}
				//Sticky Post Condition
				if(get_the_title() <> ''){ ?>
					<?php if( is_sticky(get_the_ID()) ){ ?>
						<ul class="kode-post-meta">
							<li class="blog-info blog-date"><i class="fa fa-bullhorn"></i><?php esc_attr_e('Featured','university-education');?></li>							
							<?php echo university_education_get_blog_info(array('date','author','comment','views'), false, '','li');?>
						</ul>
					<?php }else{ ?>
						<ul class="kode-post-meta">
							<?php echo university_education_get_blog_info(array('date','author','comment','views'), false, '','li');?>
						</ul>
					<?php }?>
					<h5 class="kode-post-title"><a href="<?php echo esc_url(get_permalink());?>"><?php the_title();?></a></h5>
				<?php }else{ ?>
					<?php if( is_sticky(get_the_ID()) ){ ?>
						<ul class="kode-post-meta">
							<li class="blog-info blog-date"><i class="fa fa-bullhorn"></i><?php esc_attr_e('Featured','university-education');?></li>							
							<li class="blog-info blog-date"><i class="fa fa-calendar-o"></i><a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_date(get_option('date_format'));?></a></li>
							<?php echo university_education_get_blog_info(array('author','comment','views'), false, '','li');?>
						</ul>
					<?php }else{ ?>
						<ul class="kode-post-meta">
							<li class="blog-info blog-date"><i class="fa fa-calendar-o"></i><a href="<?php echo esc_url(get_permalink());?>"><?php echo get_the_date(get_option('date_format'));?></a></li>
							<?php echo university_education_get_blog_info(array('author','comment','views'), false, '','li');?>
						</ul>
					<?php }?>					
				<?php
				}
					if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo university_education_content_filter($university_education_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $university_education_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . get_the_excerpt() . '</p>';
						if(isset($university_education_theme_option['blog-see-more']) && $university_education_theme_option['blog-see-more'] <> ''){
						echo '<a class="blog-readmore" href="' . esc_url(get_permalink()) . '">'.esc_attr__('Read More','university-education').' <i class="fa fa-arrow-right"></i></a>';
						}
					echo '</div>';
						
					}
				?>
			</div>
		</article>
		<!--EDUCATION BLOG PAGE WRAP END-->
		
	<?php 
	}
}
university_education_fetch_post_full();
?>