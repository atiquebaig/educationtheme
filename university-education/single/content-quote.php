<?php
/**
 * The template for displaying quote post format
 */
if( !function_exists('university_education_fetch_quote') ){
	function university_education_fetch_quote(){
	if( !is_single() ){ 
		global $university_education_post_settings;
	}	
	$post_format_data = '';
	$content = trim(get_the_content(esc_html__( 'Read More', 'university-education' )));	
	if(preg_match('#^\[university_education_quote[\s\S]+\[/university_education_quote\]#', $content, $match)){ 
		$post_format_data = university_education_content_filter($match[0]);
		$content = substr($content, strlen($match[0]));
	}else if(preg_match('#^<blockquote[\s\S]+</blockquote>#', $content, $match)){ 
		$post_format_data = university_education_content_filter($match[0]);
		$content = substr($content, strlen($match[0]));
	}		
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('lib-blog-2'); ?>>
	<div class="kode-blog-content">
		<div class="kode-top-quote">
			<?php echo university_education_wp_kses_strip_tags($post_format_data); ?>
		</div>
		<div class="kode-quote-author">
		<?php 
			if( is_single() || $university_education_post_settings['excerpt'] < 0 ){
				echo university_education_content_filter($content, true); 
			}else{
				echo university_education_content_filter($content); 
			}
		?>	
		</div>
	</div>
</article><!-- #post -->
<?php 
	}
}
university_education_fetch_quote();