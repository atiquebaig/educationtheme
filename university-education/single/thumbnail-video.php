<?php
/**
 * The template for displaying video post format
 */
if( !function_exists('university_education_fetch_post_format_video') ){
	function university_education_fetch_post_format_video(){
		if( !is_single() ){ 
			global $post, $university_education_post_settings;
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_post_settings['content'] = get_the_content();
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
		}else{
			global $post;
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			
		}

		$post_format_data = '';
		$content = trim(get_the_content(esc_html__('Read More', 'university-education')));	
		if(preg_match('#^https?://\S+#', $content, $match)){ 		
			if( is_single() || $university_education_post_settings['blog-style'] == 'blog-full' ){
				$post_format_data = university_education_get_video($match[0], 'full');
			}else{
				$post_format_data = university_education_get_video($match[0], $university_education_post_settings['thumbnail-size']);
				
			}
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));				
		}else if(preg_match('#^\[video\s.+\[/video\]#', $content, $match)){ 
			$post_format_data = do_shortcode($match[0]);
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));
		}else if(preg_match('#^\[embed.+\[/embed\]#', $content, $match)){ 
			global $wp_embed; 
			$post_format_data = $wp_embed->run_shortcode($match[0]);
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));
		}else{
			$university_education_post_settings['content'] = $content;
		}

		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail kode-video">' . $post_format_data . '</div>';
		}else{
			$k_post_option = json_decode(get_post_meta($post->ID, 'post-option', true), true);		
			$thumbnail_size = (empty($university_education_post_settings['thumbnail-size']))? $university_education_theme_option['thumbnail-size']: $university_education_post_settings['thumbnail-size'];
			
			//Get Thumbnail Width and Height
			$university_education_img_size = university_education_get_video_size($thumbnail_size);
			$university_education_width = $university_education_img_size['width'];
			$university_education_height = $university_education_img_size['height'];
			
			// Get Slider Images
			$k_post_option['slider'] = (empty($k_post_option['slider']))? ' ': $k_post_option['slider'];
			$raw_slider_data = university_education_decode_stopbackslashes($k_post_option['slider']);	
			$filter_slider_data = json_decode(university_education_stripslashes($raw_slider_data), true);		
			
			//Get Media Type Selected
			$k_post_option['post_media_type'] = (empty($k_post_option['post_media_type']))? ' ': $k_post_option['post_media_type'];
			if(!empty($k_post_option['university_education_video'])){
				echo '<div class="kode-blog-thumbnail kode-video">' . university_education_get_video($k_post_option['university_education_video'],$thumbnail_size).'</div>';	
			}
		}
	}
}
university_education_fetch_post_format_video();