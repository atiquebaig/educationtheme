<?php
if( !function_exists('university_education_fetch_post_format') ){
	function university_education_fetch_post_format(){
		if( in_array(get_post_format(), array('aside', 'link', 'quote')) ){
			global $university_education_post_settings;
			get_template_part('single/content', get_post_format());
		}else{
			
			global $university_education_post_settings, $current_size;			
			if( empty($university_education_post_settings['blog-style']) || $university_education_post_settings['blog-style'] == 'blog-full' ){
				get_template_part('single/content-full');
			}else if( $university_education_post_settings['blog-style'] == 'blog-modern' ){
				get_template_part('single/content-modern-full');
			}else if( $university_education_post_settings['blog-style'] == 'blog-small' ){
				get_template_part('single/content-small');
			}else{
				get_template_part('single/content-grid');
			}
			
		} 
		
	}
	
}
university_education_fetch_post_format();