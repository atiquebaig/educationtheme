<?php
/**
 * The default template for displaying standard post format
 */
 
 if( !function_exists('university_education_fetch_post_grid') ){
	function university_education_fetch_post_grid(){
	global $university_education_post_settings,$university_education_post_option; ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('blog_3_wrap'); ?>>	
		<!--BLOG 3 SIDE BAR START-->
		<ul class="blog_3_sidebar">
			<?php echo university_education_get_blog_info(array('comment'), false, '','li');?>
			<?php echo university_education_get_blog_info(array('day'), false, '','li');?>
			<?php echo university_education_get_blog_info(array('Month'), false, '','li');?>
		</ul>
		<!--BLOG 3 SIDE BAR END-->
		<!--BLOG 3 DES START-->
		<div class="blog_3_des">
			<figure>
				<?php get_template_part('single/thumbnail', get_post_format()); ?>
				<figcaption><a href="#"><i class="fa fa-search-plus"></i></a></figcaption>
			</figure>
			<ul class="blog-post-meta">
				<?php echo university_education_get_blog_info(array('views'), false, '','li');?>
				<?php echo university_education_get_blog_info(array('author'), false, '','li');?>
			</ul>
			<h5><a href="<?php echo esc_url(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$university_education_post_settings['title-num-fetch']);?></a></h5>
			<?php 
				if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
					echo '<div class="kode-blog-content">';
					echo university_education_content_filter($university_education_post_settings['content'], true);
					wp_link_pages( array( 
						'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
						'after' => '</div>', 
						'link_before' => '<span>', 
						'link_after' => '</span>' )
					);
					echo '</div>';
				}else if( $university_education_post_settings['excerpt'] != 0 ){
					echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . get_the_excerpt() . '</p>';
					if(isset($university_education_theme_option['blog-read-more']) && $university_education_theme_option['blog-read-more'] <> ''){
					echo '<a class="readmore" href="'.esc_url(get_permalink()).'">'.esc_attr__('Read More','university-education').' <i class="fa fa-long-arrow-right"></i></a>';
					}
				echo '</div>';
					
				}
			?>
		</div>
		<!--BLOG 3 DES END-->
	</article>
<?php }
}
university_education_fetch_post_grid();
?>