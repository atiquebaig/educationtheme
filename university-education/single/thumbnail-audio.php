<?php
/**
 * The template for displaying audio post format
 */
if( !function_exists('university_education_fetch_post_format_audio') ){
	function university_education_fetch_post_format_audio(){
		if( !is_single() ){ 
			global $university_education_post_settings; 
		}else{
			global $university_education_post_settings, $university_education_theme_option, $university_education_post_option,$post;
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			
		}
		
		$post_format_data = '';
		$content = trim(get_the_content(esc_html__('Read More', 'university-education')));		
		if(preg_match('#^https?://\S+#', $content, $match)){ 				
			$post_format_data = do_shortcode('[audio src="' . $match[0] . '" ][/audio]');
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));					
		}else if(preg_match('#^\[audio\s.+\[/audio\]#', $content, $match)){ 
			$post_format_data = do_shortcode($match[0]);
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));
		}else{
			$university_education_post_settings['content'] = $content;
		}	

		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail kode-audio">' . $post_format_data . '</div>';
		}else{
			$k_post_option = json_decode(get_post_meta($post->ID, 'post-option', true), true);		
			
			$thumbnail_size = (empty($university_education_post_settings['thumbnail-size']))? $university_education_theme_option['uoe-post-thumbnail-size']: $university_education_post_settings['thumbnail-size'];
			
			//Get Thumbnail Width and Height
			$university_education_img_size = university_education_get_video_size($thumbnail_size);
			$university_education_width = $university_education_img_size['width'];
			$university_education_height = $university_education_img_size['height'];
			
			// Get Slider Images
			$k_post_option['slider'] = (empty($k_post_option['slider']))? ' ': $k_post_option['slider'];
			$raw_slider_data = university_education_decode_stopbackslashes($k_post_option['slider']);	
			$filter_slider_data = json_decode(university_education_stripslashes($raw_slider_data), true);		
			
			//Get Media Type Selected
			$k_post_option['post_media_type'] = (empty($k_post_option['post_media_type']))? ' ': $k_post_option['post_media_type'];
			if(!empty($k_post_option['university_education_audio'])){
				if(strpos($k_post_option['university_education_audio'],'soundcloud')){
					echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[soundcloud url="'.esc_url($k_post_option['university_education_audio']).'" comments="yes" auto_play="no" color="#ff7700" width="100%" height="'.esc_attr($university_education_height).'px"][/soundcloud]') . '</div>';				
				}else{				
					$university_education_get_image = university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
					if(!empty($university_education_get_image)){
						echo '<div class="kode-blog-thumbnail">';
							if( is_single() ){
									echo university_education_get_image(get_post_thumbnail_id(), $thumbnail_size, true);	
									echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['university_education_audio'].'"][/audio]') . '</div>';
							}else{
								$temp_option = json_decode(get_post_meta(get_the_ID(), 'post-option', true), true);
								echo university_education_get_image(get_post_thumbnail_id(), $thumbnail_size,true);
								echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['university_education_audio'].'"][/audio]') . '</div>';							
								if( is_sticky() ){
									echo '<div class="kode-sticky-banner">';
									echo '<i class="fa fa-bullhorn" ></i>';
									echo esc_html__('Sticky Post', 'university-education');
									echo '</div>';
								}
							}						
						echo '</div>';
					}else{
						if(!empty($k_post_option['university_education_audio'])){
							echo '<div class="kode-blog-thumbnail kode-audio">' . do_shortcode('[audio mp3="'.$k_post_option['university_education_audio'].'"][/audio]') . '</div>';
						}
					}
					
				}
			}
		} 
	}
}
university_education_fetch_post_format_audio();