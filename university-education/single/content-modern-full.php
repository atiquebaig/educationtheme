<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('university_education_fetch_post_modern_full') ){
	function university_education_fetch_post_modern_full(){
		if( !is_single() ){ 
			global $university_education_post_settings; 
			if($university_education_post_settings['excerpt'] < 0) global $university_education_more; $university_education_more = 0;
		}else{
			global $university_education_post_settings, $university_education_theme_option;
		}
		$university_education_post_settings['content'] = get_the_content();
		if(!isset($university_education_post_settings['title-num-fetch']) && empty($university_education_post_settings['title-num-fetch'])){
			$university_education_post_settings['title-num-fetch'] = '100';
		}
		$thumbnail_size = (empty($university_education_post_settings['thumbnail-size']))? $university_education_theme_option['uoe-post-thumbnail-size']: $university_education_post_settings['thumbnail-size'];?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('kf_listing2_blog_wrap'); ?>>		
			<div class="kf_listing2_hdg_wrap">
				<div class="kf_blog2_date">
					<span><?php echo esc_attr(get_the_date('d M'));?></span>
				</div>
				<div class="kf_listing2_blog">
					<ul class="kf_blog_listing_meta">
						<?php echo university_education_get_blog_info(array('date','author','comment','views'), false, '','li');?>
						<li><i class="fa fa-heart-o"></i><a href="#"><?php esc_attr_e('99 + Like','university-education');?></a></li>
					</ul>
					<h4><a href="<?php echo esc_attr(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$university_education_post_settings['title-num-fetch']);?></a></h4>
				</div>
			</div>
			<div class="kf_listing2_blog_des">
				<figure>
					<?php get_template_part('single/thumbnail', get_post_format()); ?>
				</figure>
				<?php 
					if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo university_education_content_filter($university_education_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $university_education_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p>';
						if(isset($university_education_theme_option['blog-continue-reading']) && $university_education_theme_option['blog-continue-reading'] <> ''){
						echo '<a href="' . esc_url(get_permalink()) . '" class="read-more">'.esc_attr__('Read More','university-education').' <i class="fa fa-angle-right"></i></a>';
					}
					echo '</div>';
						
					}
				?>	
				<ul class="kf_blog_social_icon">
					<?php university_education_get_social_shares() ?>
				</ul>
			</div>
		</article>
	<?php 
	}
}
university_education_fetch_post_modern_full();
?>