<?php
/**
 * The template for displaying link post format
 */
if( !function_exists('university_education_fetch_post_link') ){
	function university_education_fetch_post_link(){
		if( !is_single() ){ 
			global $university_education_post_settings; 
			if($university_education_post_settings['excerpt'] < 0) global $university_education_more; $university_education_more = 0;
		}else{
			global $university_education_post_settings, $university_education_theme_option;
		}
		
		if(!isset($university_education_post_settings['title-num-fetch']) && empty($university_education_post_settings['title-num-fetch'])){
			$university_education_post_settings['title-num-fetch'] = '21';
		}	
		
		$post_format_data = '';
		$content = trim(get_the_content(esc_html__( 'Read More', 'university-education' )));
		if(preg_match('#^<a.+href=[\'"]([^\'"]+).+</a>#', $content, $match)){ 
			$post_format_data = $match[1];
			$content = substr($content, strlen($match[0]));
		}else if(preg_match('#^https?://\S+#', $content, $match)){
			$post_format_data = $match[0];
			$content = substr($content, strlen($match[0]));
		}?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('lib-blog-2'); ?>>
			<div class="kode-blog-info kode-text">
				<h6><?php echo esc_attr(get_the_date('D'));?> <?php echo esc_attr(get_the_date('d'));?>, <?php echo esc_attr(get_the_date('M'));?>, <?php echo esc_attr(get_the_date('Y'));?></h6>
				<?php get_template_part('single/thumbnail', get_post_format()); ?>
				<div class="kode_pet_blog_des">
					<h5 class="kode-post-title"><a href="<?php echo esc_url(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$university_education_post_settings['title-num-fetch']);?></a></h5>
					<?php if(isset($university_education_theme_option['blog-post-meta']) && $university_education_theme_option['blog-post-meta'] == 'enable'){ ?>
						<ul>
							<?php echo university_education_get_blog_info(array('author','comment','category'), false, '','li');?>			
						</ul>
					<?php }?>
					<?php 
						if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
							echo '<div class="kode-blog-content">';
							echo university_education_content_filter($university_education_post_settings['content'], true);
							wp_link_pages( array( 
								'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
								'after' => '</div>', 
								'link_before' => '<span>', 
								'link_after' => '</span>' )
							);
							echo '</div>';
						}else if( $university_education_post_settings['excerpt'] != 0 ){
							echo '<div class="kode-blog-content university_education_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p>';
							if(isset($university_education_theme_option['blog-see-more']) && $university_education_theme_option['blog-see-more'] <> ''){
							echo '<a href="' . esc_url(get_permalink()) . '" class="hvr-sweep-to-right">'.esc_attr__('Read More','university-education').'</a>';
						}
						echo '</div>';
							
						}
					?>			
				</div>
			</div>
		</article>
	<?php 
	}
}
university_education_fetch_post_link();
