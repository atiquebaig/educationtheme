<?php
	function university_education_fetch_post_format(){
		if( in_array(get_post_format(), array('aside', 'link', 'quote')) ){
			get_template_part('single/content', get_post_format());
		}else{
			
			global $university_education_post_settings;
			if( empty($university_education_post_settings['blog-style']) || $university_education_post_settings['blog-style'] == 'blog-full' ){
				get_template_part('single/content-full');
			}else if( $university_education_post_settings['blog-style'] == 'blog-list' ){
				get_template_part('single/content-medium');
			}else if( strpos($university_education_post_settings['blog-style'], 'blog-press') !== false ){
				get_template_part('single/content-small');
			}else if( strpos($university_education_post_settings['blog-style'], 'blog-small') !== false ){
				get_template_part('single/content-simple');
			}else{
				get_template_part('single/content-grid');
			}
			
		} 
	}
?>