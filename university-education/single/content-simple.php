<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('university_education_fetch_simple') ){
	function university_education_fetch_simple(){
	global $university_education_post_settings,$post,$university_education_post_option; ?>
	<article id="blog-simple-<?php the_ID(); ?>" <?php post_class('blog-simple'); ?>>
		<div class="kode_recent_wrap">
			<figure>
				<?php get_template_part('single/thumbnail', get_post_format());?>
				<figcaption class="kode_recent_des">
					<h4><?php echo esc_attr(substr(get_the_title(),0,$university_education_post_settings['title-num-fetch'])); ?></h4>
					<h6><?php echo esc_attr(get_the_date('M d Y'));?></h6>
					<?php 
						if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
							echo '<div class="kode-blog-content">';
							echo university_education_content_filter($university_education_post_settings['content'], true);
							wp_link_pages( array( 
								'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
								'after' => '</div>', 
								'link_before' => '<span>', 
								'link_after' => '</span>' )
							);
							echo '</div>';
						}else if( $university_education_post_settings['excerpt'] != 0 ){
							echo '<div class="kode-blog-content"><p>' . esc_attr(get_the_excerpt()) . '</p><a class="kode_link_1" href="'.esc_url(get_permalink()).'">'.esc_attr__('Read More','university-education').'</a></div>';
						}
					?>				
				</figcaption>
			</figure>  
		</div>
	</article>
<?php }
}
university_education_fetch_simple();