<?php
/**
 * The template for displaying posts in the Aside post format
 */
	if( !function_exists('university_education_fetch_post_format_aside') ){
		function university_education_fetch_post_format_aside(){
			if( !is_single() ){ 
				global $university_education_post_settings;
			} ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('kode-blog-info'); ?>>
				<div class="kode-blog-content">
					<?php 
						if( is_single() || $university_education_post_settings['excerpt'] < 0 ){
							echo university_education_content_filter(get_the_content(esc_html__( 'Read More', 'university-education' )), true); 
						}else{
							echo university_education_content_filter(get_the_content(esc_html__( 'Read More', 'university-education' ))); 
						}
					?>
				</div>
			</article>
<?php 
		}
	}
	university_education_fetch_post_format_aside();
?>
