<?php 

if( !function_exists('university_education_fetch_page') ){
	function university_education_fetch_page(){
		while ( have_posts() ){ the_post();
			$content = university_education_content_filter(get_the_content(), true); 
			if(!empty($content)){
				?>
				<div class="container">
					<div class="kode-item k-content">
						<?php echo esc_attr($content); ?>
					</div>
				</div>
				<?php
			}
		} 
	}
}	
university_education_fetch_page();
?>