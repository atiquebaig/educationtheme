<?php
/**
 * The template for displaying image post format
 */
	
if( !function_exists('university_education_fetch_post_format_image') ){
	function university_education_fetch_post_format_image(){
		$post_format_data = '';
		$university_education_post_settings = '';
		$content = trim(get_the_content(esc_html__( 'Read More', 'university-education' )));
		if(preg_match('#^<a.+<img.+/></a>|^<img.+/>#', $content, $match)){ 
			$post_format_data = $match[0];
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));
		}else if(preg_match('#^https?://\S+#', $content, $match)){
			$post_format_data = university_education_get_image($match[0], 'full', true);
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));					
		}else{
			$university_education_post_settings['content'] = $content;
		}
		
		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail">';
			echo esc_attr($post_format_data); 
			
			if( !is_single() && is_sticky() ){
				echo '<div class="kode-sticky-banner">';
				echo '<i class="icon-bullhorn" ></i>';
				echo esc_html__('Sticky Post', 'university-education');
				echo '</div>';
			}					
			echo '</div>';
			echo '<figcaption><a href="'.esc_url(get_permalink()).'"><i class="fa fa-plus"></i></a></figcaption>';
		} 
	}
}	
university_education_fetch_post_format_image();