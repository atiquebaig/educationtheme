<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('university_education_fetch_small') ){
	function university_education_fetch_small(){
	global $university_education_post_settings,$post,$university_education_post_option, $university_education_counter;?>
	<article id="blog-press-<?php the_ID(); ?>" <?php post_class('blog-press kode-item'); ?>>
		<div class="kf_blog_post_wrap">
			<h6>.0<?php echo esc_attr($post->ID);?></h6>
			<figure>
				<?php get_template_part('single/thumbnail', get_post_format());?>
			</figure>
			<div class="kf_blog_des">
				<h6><a href="<?php echo esc_url(get_permalink());?>"><?php echo esc_attr(get_the_title());?></a></h6>
				<ul class="kf_blog_post_meta">
					<?php echo university_education_get_blog_info(array('date'), false, '','li');?>
					<?php echo university_education_get_blog_info(array('author'), false, '','li');?>
					<?php echo university_education_get_blog_info(array('views'), false, '','li');?>
				</ul>
				<?php 
					if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo university_education_content_filter($university_education_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $university_education_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content"><p>' . esc_attr(get_the_excerpt()) . '</p>
						<a class="kode_link_2" href="'.esc_url(get_permalink()).'">'.esc_attr__('Read More','university-education').'</a></div>';
					}
				?>					
			</div>
		</div>
	</article>
<?php 
	}
}
university_education_fetch_small();