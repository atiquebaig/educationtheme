<?php
/**
 * The template for displaying video post format
 */
if( !function_exists('university_education_fetch_post_format_gallery') ){
	function university_education_fetch_post_format_gallery(){
		global $post, $university_education_post_settings,$university_education_theme_option; 
		$university_education_theme_option = get_option('university_education_admin_option', array());	
		$post_format_data = '';
		$content = trim($post->post_content);	
		if(preg_match('#\[gallery[^\]]+]#', $content, $match)){ 
			if( is_single() ){
				$post_format_data = do_shortcode(get_post_gallery());
			}else{
				preg_match('#\[gallery.+columns\s?=\s?\"([^\"]+).+]#', $match[0], $match2);	
				if(empty($match2)){
					preg_match('#\[gallery.+ids\s?=\s?\"([^\"]+).+]#', $match[0], $match2);					
				}			
				if(is_numeric($match2[1])){
					$post_format_data = '';
				}else{
					$post_format_data = university_education_get_bx_slider(explode(',', $match2[1]), array('size'=>$university_education_post_settings['thumbnail-size']));
				}
			}
			$university_education_post_settings['content'] = substr($content, strlen($match[0]));
		}else{
			$university_education_post_settings['content'] = $content;
			
		}
		
		if ( !empty($post_format_data) ){
			echo '<div class="kode-blog-thumbnail kode-gallery">' . $post_format_data . '</div>';
		}else{
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			$thumbnail_size = (empty($university_education_post_settings['thumbnail-size']))? $university_education_theme_option['uoe-post-thumbnail-size']: $university_education_post_settings['thumbnail-size'];
			// Get Slider Images
			$university_education_post_option['slider'] = (empty($university_education_post_option['slider']))? ' ': $university_education_post_option['slider'];
			$raw_slider_data = university_education_decode_stopbackslashes($university_education_post_option['slider']);	
			$filter_slider_data = json_decode(university_education_stripslashes($raw_slider_data), true);		
			
			if(isset($filter_slider_data) && $filter_slider_data != ''){
				echo '<div class="kode-blog-thumbnail">';
				echo university_education_get_slider( $filter_slider_data, $thumbnail_size, 'bxslider' );
				echo '</div>';
			}
		}
	}
}
university_education_fetch_post_format_gallery();