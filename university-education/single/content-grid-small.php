<?php
/**
 * The default template for displaying standard post format
 */
 
if( !function_exists('university_education_fetch_post_grid_small') ){
	function university_education_fetch_post_grid_small(){
	global $university_education_post_settings,$university_education_post_option; 
	$title_num_fetch = $university_education_post_settings['title-num-fetch'];
	?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('lib-blog-post'); ?>>			
		<div class="kode-thumb">
			<?php get_template_part('single/thumbnail', get_post_format()); ?>
			<div class="lib-btns">
				<a title="" data-toggle="tooltip" href="#" data-original-title="Title"><i class="fa fa-search"></i></a>
				<a title="" data-toggle="tooltip" href="<?php echo esc_url(get_permalink());?>" data-original-title="Title"><i class="fa fa-picture-o"></i></a>
			</div>
		</div>
		<div class="kode-text">
			<h2><a href="<?php echo esc_url(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$title_num_fetch);?></a></h2>
			<ul>
				<?php echo university_education_get_blog_info(array('author'), false, '','li');?>
				<?php echo university_education_get_blog_info(array('date'), false, '','li');?>
			</ul>
			<a class="more" href="<?php echo esc_url(get_permalink());?>"><i class="fa fa-chevron-right"></i></a>
		</div>
	</article>
<?php }

}
university_education_fetch_post_grid_small();
