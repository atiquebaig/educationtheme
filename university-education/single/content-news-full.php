<?php
/**
 * The default template for displaying standard post format
 */
 
if( !function_exists('university_education_fetch_post_news_full') ){
	function university_education_fetch_post_news_full(){
	global $university_education_post_settings; ?>
	<article id="blog-<?php the_ID(); ?>" <?php post_class('kode-ux kode-blog-full-wrap'); ?>>
		<div class="blog_grid_wrap">
			<figure>
				<?php get_template_part('single/thumbnail', get_post_format()); ?>
				<div class="img_link_icon">
					<a href="<?php echo esc_url(get_permalink()); ?>"><i class="fa fa-external-link "></i></a>
				</div>
			</figure>
			<div class="blog_grid_des">		
				<div class="blog_grid_post">
					<h5><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h5>
					<?php echo university_education_get_blog_info(array('author'), false, '','span');?>
					<?php echo university_education_get_blog_info(array('date'), false, '','span');?>
					<?php echo university_education_get_blog_info(array('comment'), false, '','span');?>
					<?php 
					if( $university_education_post_settings['excerpt'] < 0 ){
					global $university_education_more; $university_education_more = 0;

						echo '<div class="kode-blog-content">';
							echo university_education_content_filter($university_education_post_settings['content'], true);
							wp_link_pages( array(
								'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
								'after' => '</div>', 
								'link_before' => '<span>', 
								'link_after' => '</span>' )
							);
						echo '</div>';
					}else if( $university_education_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content"><p>' . get_the_excerpt() . '</p>';
						if(isset($university_education_theme_option['blog-read-more']) && $university_education_theme_option['blog-read-more'] <> ''){
							echo '	<a href="' . esc_url(get_permalink()) . '" class="kd-readmore th-bordercolor thbg-colorhover">'.esc_attr__('Read More','university-education').' <i class="fa fa-long-arrow-right"></i></a>';
						}	
							echo '</div>';
					}
					?>			
				</div>
			</div>
		</div>
	</article> 
<?php }

}
university_education_fetch_post_news_full();