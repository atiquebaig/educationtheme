<?php
/**
 * The default template for displaying standard post format
 */
if( !function_exists('university_education_fetch_post_medium') ){
	function university_education_fetch_post_medium(){
		global $university_education_post_settings;?>
		<article id="post-<?php the_ID(); ?>" <?php post_class('lib-blog-2'); ?>>
			<!--BLOG THUMB START-->
			<?php $university_education_get_image = university_education_get_image(get_post_thumbnail_id(), esc_attr($thumbnail_size), true);
			if(!empty($university_education_get_image)){ ?>
			<div class="kode-thumb">
				<?php get_template_part('single/thumbnail', get_post_format()); ?>
				 <div class="kode-blog-date">
					 <p><?php echo esc_attr(get_the_date('d M'));?></p>
					 <span><?php echo esc_attr(get_the_date('Y'));?></span>
				 </div>
			</div> 
			<?php }?> 	
			 <!--BLOG THUMB END-->
			 <!--BLOG TEXT START-->
			 <div class="kode-text">
				 <h3><a href="<?php echo esc_url(get_permalink());?>"><?php echo substr(esc_attr(get_the_title()),0,$university_education_post_settings['title-num-fetch']);?></a></h3>
				 <div class="kode-meta">
					 <ul>
						 <?php echo university_education_get_blog_info(array('author','comment'), false, '','li');?>		
					 </ul>
				 </div>
				 <?php 
					if( is_single() || $university_education_post_settings['excerpt'] < 0 || $university_education_post_settings['excerpt'] == 'full'){
						echo '<div class="kode-blog-content">';
						echo university_education_content_filter($university_education_post_settings['content'], true);
						wp_link_pages( array( 
							'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
							'after' => '</div>', 
							'link_before' => '<span>', 
							'link_after' => '</span>' )
						);
						echo '</div>';
					}else if( $university_education_post_settings['excerpt'] != 0 ){
						echo '<div class="kode-blog-content kode_pet_blog_des"><p>' . esc_attr(get_the_excerpt()) . '</p>';
						if(isset($university_education_theme_option['blog-see-more']) && $university_education_theme_option['blog-see-more'] <> ''){
						echo '<a href="' . esc_url(get_permalink()) . '" class="more">'.esc_attr__('Read More','university-education').' <i class="fa fa-angle-right"></i></a>';
						}
					echo '</div>';
						
					}
				?>					 
			 </div>
			 <!--BLOG TEXT END-->		 
		</article>
	<?php 
	}
}
university_education_fetch_post_medium();