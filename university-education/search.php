<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 		
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			if( empty($university_education_post_option['sidebar']) || $university_education_post_option['sidebar'] == 'default-sidebar' ){
				$university_education_sidebar = array(
					'type'=>$university_education_theme_option['archive-sidebar-template'],
					'left-sidebar'=>$university_education_theme_option['archive-sidebar-left'], 
					'right-sidebar'=>$university_education_theme_option['archive-sidebar-right']
				); 
			}else{
				$university_education_sidebar = array(
					'type'=>$university_education_post_option['sidebar'],
					'left-sidebar'=>$university_education_post_option['left-sidebar'], 
					'right-sidebar'=>$university_education_post_option['right-sidebar']
				); 				
			}
			
			$university_education_sidebar = university_education_get_sidebar_class($university_education_sidebar);
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="<?php echo esc_attr($university_education_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php } ?>
			<div class="kode-main-content <?php echo esc_attr($university_education_sidebar['center'])?>">
				<?php
					if( have_posts() ){
						// set the excerpt length
						if( !empty($university_education_theme_option['archive-num-excerpt']) ){
							$university_education_excerpt_length = $university_education_theme_option['archive-num-excerpt'];
							add_filter('excerpt_length', 'university_education_set_excerpt_length');
						} 

						
						$university_education_lightbox_id++;
						$university_education_post_settings['excerpt'] = intval($university_education_theme_option['archive-num-excerpt']);
						$university_education_post_settings['thumbnail-size'] = 'full';			
						$university_education_post_settings['title-num-fetch'] = '25';
						$university_education_post_settings['blog-style'] = $university_education_theme_option['archive-blog-style'];							
					
						echo '<div class="kode-blog-list-archive kode-fullwidth-blog row">';
						if($university_education_theme_option['archive-blog-style'] == 'blog-full'){
							echo university_education_get_blog_full($wp_query);
						}else if($university_education_theme_option['archive-blog-style'] == 'blog-medium'){
							echo '<div class="kode-blog-list-archive kode-mediium-blog margin-bottom-30">';
							echo university_education_get_blog_medium($wp_query);			
							echo '</div>';
						}else{
							$blog_size = 3;
							echo '<div class="kode-blog-list-archive kode-blog-grid margin-bottom-30">';
							echo university_education_get_blog_grid($wp_query, $blog_size, 'fitRows');
							echo '</div>';	
						}
						echo '</div>';
						remove_filter('excerpt_length', 'university_education_set_excerpt_length');
						
						$paged = (get_query_var('paged'))? get_query_var('paged') : 1;
						echo university_education_get_pagination($wp_query->max_num_pages, $paged);
					}else{ ?>
					<!--// Main Content //-->
					<div class="main-content">
						<div class="row">
							<div class="col-md-12">
								<div class="kode-pagecontent search-page-kode col-md-12">
									<div class="kode-404-page">
										<h2><?php esc_html_e('Not Found', 'university-education'); ?></h2>
										<span><?php esc_html_e('OOPS, PAGE NOT FOUND', 'university-education'); ?></span>
										<p><?php esc_html_e('Nothing matched your search criteria. Please try again with different keywords.', 'university-education'); ?></p>										
										<div class="kode-innersearch">
											<div class="kode-search">
												<?php echo get_search_form();?>
											</div>											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--// Main Content //-->
				<?php } ?>
			</div>
			<?php
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'right-sidebar' && $university_education_sidebar['right'] != ''){ ?>
				<div class="<?php echo esc_attr($university_education_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>