<?php get_header(); ?>
<div class="content">
	<!-- Sidebar With Content Section-->
	<?php 
		$university_education_content_raw = json_decode(university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'university_education_content', true)), true);
		$university_education_content_raw = (empty($university_education_content_raw))? array(): $university_education_content_raw;
		$university_education_theme_option = get_option('university_education_admin_option', array());
		
		if(isset($university_education_theme_option['book-search-style']) && $university_education_theme_option['book-search-style'] == 0){
			
		}else{
			echo '<div class="margin-minus-50-top kode-property-search-listing">';
			if(function_exists('university_education_get_search_form_result')){
				echo university_education_get_search_form_result();
			}
			echo '</div>';	
		}
		
		if( !empty($university_education_content_raw) ){ 
			echo '<div class="vc-wrapper container">';
			while ( have_posts() ){ the_post();
				if( has_shortcode( get_the_content(), 'vc_row' ) ) {
					echo university_education_content_filter(get_the_content(), true); 
				}
			}
			echo '</div>';
			
			echo '<div class="pagebuilder-wrapper">';
			university_education_show_page_builder($university_education_content_raw);
			echo '</div>';
			
			
		}else{
			echo '<div class="container">';
				$default['show-title'] = 'enable';
				$default['show-content'] = 'enable'; 
				echo university_education_get_default_content_item($default);
			echo '</div>';
		}
	?>
</div><!-- content -->
<?php get_footer(); ?>