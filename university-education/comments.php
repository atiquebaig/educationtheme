<?php
/**
 * The template for displaying Comments.
 */
if ( post_password_required() )
	return;
?>

<div id="kodecomments" class="section-comment">
	<?php if(have_comments()){ ?>
		<div class="blog-detl_heading">
			<?php 
				if( get_comments_number() <= 1 ){
					echo '<h5>'.esc_attr(get_comments_number()) . ' <span class="thcolor">' . esc_html__('Recent Comments', 'university-education').'</span></h5>'; 
				}else{
					echo '<h5>'.esc_attr(get_comments_number()) . ' <span class="thcolor">' . esc_html__('Recent Comments', 'university-education').'</span></h5>'; 
				}
			?>	
			<div class="kode-maindivider"><span></span></div>
		</div>

		<ul class="coment_list">
			<?php wp_list_comments(array('callback' => 'university_education_comment_list', 'style' => 'ul')); ?>
		</ul><!-- .commentlist -->

		<?php if (get_comment_pages_count() > 1 && get_option('page_comments')){ ?>
			<nav id="comment-nav-below" class="navigation">
				<h1 class="assistive-text section-heading"><?php echo esc_html__( 'Comment navigation', 'university-education' ); ?></h1>
				<div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'university-education' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'university-education' ) ); ?></div>
			</nav>
		<?php }
	}
	?>
	<div class="blog_pg_form">		
		<div class="row">
			<?php
				$commenter = wp_get_current_commenter();
				$req = get_option( 'require_name_email' );
				$aria_req = ($req ? " aria-required='true'" : '');
				
				$args = array(
					'id_form'           => 'commentform',
					'id_submit'         => 'submit',
					'title_reply'       => esc_html__('Leave a Message', 'university-education'),
					'title_reply_to'    => esc_html__('Leave a Message to %s', 'university-education'),
					'cancel_reply_link' => esc_html__('Cancel Reply', 'university-education'),
					'label_submit'      => esc_html__('Send Comment', 'university-education'),
					'comment_notes_before' => '',
					'comment_notes_after' => '',

					'must_log_in' => '<p class="must-log-in col-md-12">' .
						sprintf( university_education_wp_kses_strip_tags(__('You must be <a href="%s">logged in</a> to post a comment.', 'university-education')),
						esc_url(wp_login_url(apply_filters( 'the_permalink', get_permalink()))) ) . '</p>',
					'logged_in_as' => '<p class="logged-in-as col-md-12">' .
						sprintf(  university_education_wp_kses_strip_tags(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'university-education')),
						esc_url(admin_url('profile.php')), $user_identity, esc_url(wp_logout_url(apply_filters('the_permalink', get_permalink( )))) ) . '</p>',

					'fields' => apply_filters('comment_form_default_fields', array(
						'author' =>
							'<div class="col-md-6"><input id="kode-author" placeholder="' . esc_html__('Name', 'university-education') . '" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
							'" data-default="' . esc_html__('Name', 'university-education') . '"/></div>',
						'email' => 
							'<div class="col-md-6"><input id="kode-email" placeholder="' . esc_html__('Email', 'university-education') . '" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
							'" data-default="' . esc_html__('Email', 'university-education') . '"/></div>',
					)),
					'comment_field' =>  '
						<div class="col-md-12">' .
							'<textarea id="kode-comment" placeholder="' . esc_html__('Message', 'university-education') . '" name="comment">' .
						'</textarea></div>'
					
				);
				comment_form($args); 
			?>
		</div>
	</div>
</div><!-- kode-comment-area -->	    							