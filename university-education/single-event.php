<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			if( empty($university_education_post_option['sidebar']) || $university_education_post_option['sidebar'] == 'default-sidebar' ){
				$university_education_sidebar = array(
					'type'=>$university_education_theme_option['post-sidebar-template'],
					'left-sidebar'=>$university_education_theme_option['post-sidebar-left'], 
					'right-sidebar'=>$university_education_theme_option['post-sidebar-right']
				); 
			}else{
				$university_education_sidebar = array(
					'type'=>$university_education_post_option['sidebar'],
					'left-sidebar'=>$university_education_post_option['left-sidebar'], 
					'right-sidebar'=>$university_education_post_option['right-sidebar']
				); 				
			}
			$university_education_sidebar = university_education_get_sidebar_class($university_education_sidebar);
			?>
			<div class="kode-center-content-tf kode-item  <?php echo esc_attr($university_education_sidebar['center'])?>">
			<?php while ( have_posts() ){ the_post(); 
				global $EM_Event,$post;
				$event_year = esc_attr(date('Y',$EM_Event->start));
				$event_month = esc_attr(date('m',$EM_Event->start));
				$event_day = esc_attr(date('d',$EM_Event->start));
				$event_start_time = esc_attr(date("H:i a", strtotime($EM_Event->start_time)));			
				$event_end_time = esc_attr(date("H:i a", strtotime($EM_Event->end_time)));
				$location = '';
				if(isset($EM_Event->get_location()->address)){
					$location = esc_attr($EM_Event->get_location()->address);
				}else{
					$location = '';				
				}
				
				$location_town = '';
				if(isset($EM_Event->get_location()->address)){
					$location_town = $EM_Event->get_location()->town;
				}else{
					$location_town = '';				
				}
				$university_education_theme_option['single-event-feature-size'];
				?>
				<!--EVENT DETAIL START-->
				<!--EVENT CONVOCATION OUTER Wrap START-->
				<div class="kf_convocation_outer_wrap">	
					<div class="convocation_slider">

						<?php if(isset($university_education_theme_option['single-event-feature-image']) && $university_education_theme_option['single-event-feature-image'] == 'enable'){ ?>
							<?php echo get_the_post_thumbnail( $EM_Event->post_id, $university_education_theme_option['single-event-feature-size']);?>
						<?php } ?>
					</div>

					<!--EVENT CONVOCATION  Wrap START-->
					<div class="kf_convocation_wrap">
						<h4><span><?php the_title();?></span></h4>
						<ul class="convocation_timing">
							<li><i class="fa fa-calendar"></i><?php echo esc_attr(get_the_date('d')); ?> <?php echo esc_attr(get_the_date('M')); ?>, <?php echo esc_attr(get_the_date('Y')); ?></li>
							<li><i class="fa fa-clock-o"></i><?php echo esc_attr($event_start_time); ?> - <?php echo esc_attr($event_end_time); ?></li>
						</ul>

						<!--EVENT CONVOCATION DES START-->
						<div class="kf_convocation_des">
							<p><?php echo $EM_Event->post_content; ?></p>
							<a href=""><i class="fa fa-plus"></i><?php echo esc_attr__('Google Calender','university-education'); ?></a>
							<a href=""><i class="fa fa-plus"></i><?php echo esc_attr__('Local Expert','university-education'); ?></a>

							<!--EVENT CONVOCATION MAP  Wrap START-->
							<div class="kf_convocation_map">
								<?php echo do_shortcode('[map address="'.esc_attr($location).'" type="roadmap" zoom="14" scrollwheel="no" scale="no" zoom_pancontrol="no"][/map]');?>
								<a class="convocation_link" href="#"><?php echo esc_attr__('Details','university-education'); ?></a>
								<a class="convocation_link" href="#"><?php echo esc_attr__('Organizer','university-education'); ?></a>
								<a class="convocation_link" href="#"><?php echo esc_attr__('Venue','university-education'); ?></a>
							</div>
							<!--EVENT CONVOCATION MAP  Wrap END-->

						</div>
						<!--EVENT CONVOCATION DES END-->

					</div>
					<!--EVENT CONVOCATION  Wrap END-->
				</div>

				<?php if(isset($university_education_theme_option['single-event-organizer']) && $university_education_theme_option['single-event-organizer'] == 'enable'){ ?>
					<?php if(isset($university_education_post_option['team_member'])){ ?>
					<div class="kf_event_speakers">
						<div class="heading_5">
							<h4><span><?php echo esc_attr__('EVENT','university-education'); ?></span><?php echo esc_attr__(' SPEAKERS','university-education');?></h4>
						</div>
						<div class="row">
							<?php foreach($university_education_post_option['team_member'] as $team_id){
								$university_education_team_option = university_education_decode_stopbackslashes(get_post_meta($team_id, 'post-option', true ));
								if( !empty($university_education_team_option) ){
									$university_education_team_option = json_decode( $university_education_team_option, true );					
								}
								$designation = $university_education_team_option['designation'];
								$phone = $university_education_team_option['phone'];
								$website = $university_education_team_option['website'];
								$email = $university_education_team_option['email'];
								$facebook = $university_education_team_option['facebook'];
								$twitter = $university_education_team_option['twitter'];
								$youtube = $university_education_team_option['youtube'];
								$pinterest = $university_education_team_option['pinterest'];
								?>
								<div class="col-md-4 col-sm-4">
									<div class="kf_event_speakers_des">
										<figure><a href="<?php echo esc_url(get_permalink($team_id));?>"><?php echo get_the_post_thumbnail( $team_id, array(300,300));?></a></figure>
										<h5><a href="<?php echo esc_url(get_permalink($team_id));?>"><?php echo esc_attr(get_the_title($team_id));?></a></h5>
										<p><?php echo esc_attr($designation)?></p>
									</div>
								</div>
							<?php }?>
						</div>
					</div>
					<?php }?>	
				<?php }?>
				
				<?php if(isset($university_education_theme_option['single-event-gallery']) && $university_education_theme_option['single-event-gallery'] == 'enable'){ ?>	
				<div class="kf_event_gallery">
					<div class="heading_5">
						<h4><span><?php echo esc_attr__('Event','university-education');?></span> <?php echo esc_attr__('Gallery','university-education');?></h4>
					</div>
					<ul class="event_gallery_des">
				<?php  
				$slider_option = json_decode($university_education_post_option['slider'], true);
					$slide_order = $slider_option[0];
					$slide_data = $slider_option[1];					
					
					$slides = array();
					if(!empty($slide_order)){
						foreach( $slide_order as $slide_id ){
							$slides[$slide_id] = $slide_data[$slide_id];
						}
					}
					$university_education_post_slider['slider'] = $slides;
					foreach($university_education_post_slider['slider'] as $slide_id => $slide){
						$image_src = wp_get_attachment_image_src($slide_id, 'full');
						echo '
						<li class="gallery-item">
							<a data-rel="prettyphoto[]" href="'.esc_url($image_src[0]).'">';
								echo university_education_get_image($slide_id,array(150,150));
							echo '</a>
						</li>';
						
					}
				?>					
					</ul>
				</div>
				
				<?php } ?>
				<div class="event-nav">
				<?php previous_post_link('<div class="event_link prev">%link <i class="fa fa-angle-left"></i></div>', ''.esc_html__('Previous Event', 'university-education').''); ?>
				<?php next_post_link('<div class="event_link next">%link <i class="fa fa-angle-right"></i></div>', ''.esc_html__('Next Event', 'university-education').''); ?>
				</div>
				<div class="booking_form">
					<h2><?php echo esc_attr__('Booking Form','university-education')?></h2>
					<?php university_education_booking_form_event_manager();?>

				</div>
				<?php if(isset($university_education_theme_option['single-event-comments']) && $university_education_theme_option['single-event-comments'] == 'enable'){ ?>
					<!-- Blog Detail -->
					<?php comments_template( '', true ); ?>
				<?php } ?>
			<?php }?>		
			</div>
			<?php
			if(isset($university_education_sidebar['left']) && $university_education_sidebar['left'] <> ''){				
				if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'left-sidebar'){ ?>
					<div class="pull-left <?php echo esc_attr($university_education_sidebar['left'])?>">
						<?php get_sidebar('left'); ?>
					</div>	
				<?php }
			}			
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'right-sidebar' && $university_education_sidebar['right'] != ''){ ?>
				<div class="pull-right <?php echo esc_attr($university_education_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } ?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>