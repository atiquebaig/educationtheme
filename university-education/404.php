<?php
/**
 * The template for displaying 404 pages (Not Found).
 */
get_header(); ?>
<?php $university_education_theme_option = get_option('university_education_admin_option', array()); ?>

	<!--Content Wrap Start-->
		<div class="kf_content_wrap">
			<section class="error_outer_wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="error_wrap">
								<div class="error_des">
									<span><?php echo esc_attr__('404','university-education');?></span>
									<h3><?php echo esc_attr__('Sorry Page Not Found','university-education');?></h3>
									<p><?php echo esc_attr__('The page you are looking for is not available or has been removed. Try going to ','university-education');?><strong><?php echo esc_attr__('HOME PAGE','university-education');?></strong><?php echo esc_attr__(' by using the button below.','university-education');?></p>
								</div>
								<div class="error_thumb">
									<figure>
										<img src="<?php echo UOE_PATH?>/images/404_thumb.jpg" alt=""/>
										<figcaption><a href="<?php echo esc_url(home_url('/'))?>"><i class="fa fa-home"></i><?php echo esc_attr__('GO TO HOME','university-education'); ?></a></figcaption>
									</figure>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

<?php get_footer(); ?>