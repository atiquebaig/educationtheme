<?php
/*
 * The template for displaying a header title section
 */
		
	$header_background = university_education_header_title_background('default-post-title-background');
	$university_education_theme_option = get_option('university_education_admin_option', array());
	$university_education_plugin_option = get_option('university_education_plugin_option', array());
	$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
	if( !empty($university_education_post_option) ){
		$university_education_post_option = json_decode( $university_education_post_option, true );					
	}
	if( is_page() && (empty($university_education_post_option['show-sub']) || $university_education_post_option['show-sub'] != 'disable') ){ 
	$page_background = ''; $page_title = get_the_title(); 
	if(!empty($university_education_post_option['page-caption'])){ $page_caption = $university_education_post_option['page-caption'];} ?>
		<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!--KF INR BANNER DES Wrap Start-->
						<div class="kf_inr_ban_des">
							<div class="inr_banner_heading">
								<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
							</div>
						   
							<div class="kf_inr_breadcrumb">
								<?php if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
									<?php university_education_get_breadcumbs();?>
								<?php }?>
							</div>
						</div>
						<!--KF INR BANNER DES Wrap End-->
					</div>
				</div>
			</div>
		</div>
	<?php }else if( is_single() && $post->post_type == 'post' ){ 
		
		if( !empty($university_education_post_option['page-title']) ){
			$page_title = $university_education_post_option['page-title'];
			$page_caption = $university_education_post_option['page-caption'];
		}else{
			$page_title = '';
			$page_caption = '';
		} 
		if($page_title == ''){
			$page_title = get_the_title();
		}
		
		$header_background = university_education_header_title_background('default-post-title-background');
	?>
		<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!--KF INR BANNER DES Wrap Start-->
						<div class="kf_inr_ban_des">
							<div class="inr_banner_heading">
								<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
							</div>
						   
							<div class="kf_inr_breadcrumb">
								<?php if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
									<?php university_education_get_breadcumbs();?>
								<?php }?>
							</div>
						</div>
						<!--KF INR BANNER DES Wrap End-->
					</div>
				</div>
			</div>
		</div>
	<?php }else if( is_single() && $post->post_type == 'property'  ){ // for custom post type
	if( $university_education_plugin_option['show-property-subheader'] == 'enable'){
			if( !empty($university_education_post_option['page-title']) ){
			$page_title = $university_education_post_option['page-title'];
			$page_caption = $university_education_post_option['page-caption'];
		}else{
			$page_title = '';
			$page_caption = '';
		} 
		if($page_title == ''){
			$page_title = get_the_title();
		}
		
		$header_background = university_education_header_title_background('default-post-title-background');
		echo'
		<div '.wp_kses($header_background,array('strong'=>array(),'a'=>array())).' class="kf_property_sub_banner '. esc_attr($university_education_theme_option['kode-header-style']).'">
			<div class="container">
				<div class="row">
					<div class="kf_sub_banner_hdg col-md-5">
						<h3>'.esc_attr(university_education_text_filter($page_title)).'</h3>
					</div>
					<div class="kf_property_breadcrumb col-md-7">';
						 if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ 
							 university_education_get_breadcumbs();
						 }
						echo' 
					</div>
				</div>
			</div>
		</div>';
		}
		else{
			
		}
		
		}else if( is_single() ){ // for custom post type
		
		if( !empty($university_education_post_option['page-title']) ){
			$page_title = $university_education_post_option['page-title'];
			$page_caption = $university_education_post_option['page-caption'];
		}else{
			$page_title = '';
			$page_caption = '';
		} 
		if($page_title == ''){
			$page_title = get_the_title();
		}
		
		$header_background = university_education_header_title_background('default-post-title-background');
	?>
		<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!--KF INR BANNER DES Wrap Start-->
						<div class="kf_inr_ban_des">
							<div class="inr_banner_heading">
								<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
							</div>
						   
							<div class="kf_inr_breadcrumb">
								<?php if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
									<?php university_education_get_breadcumbs();?>
								<?php }?>
							</div>
						</div>
						<!--KF INR BANNER DES Wrap End-->
					</div>
				</div>
			</div>
		</div>
		
	<?php }else if( is_404() ){ 
		$page_title = '404 Error Page';
		$header_background = university_education_header_title_background('default-404-title-background');
	?>
		<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!--KF INR BANNER DES Wrap Start-->
						<div class="kf_inr_ban_des">
							<div class="inr_banner_heading">
								<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
							</div>
						   
							<div class="kf_inr_breadcrumb">
								<?php if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
									<?php university_education_get_breadcumbs();?>
								<?php }?>
							</div>
						</div>
						<!--KF INR BANNER DES Wrap End-->
					</div>
				</div>
			</div>
		</div>
		
	<?php }else if( is_tax('features') || is_tax('status') ){ 
	
		
		// $page_title = esc_html__('', 'university-education');
		$caption = '';
		$university_education_theme_option['enable-breadcrumbs'] = 'disable';
		$page_title = single_cat_title('', false);
		$university_education_theme_option = get_option('university_education_admin_option', array());
		$header_background = university_education_header_title_background('features-background');
		
		
	?>
	
		<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!--KF INR BANNER DES Wrap Start-->
						<div class="kf_inr_ban_des">
							<div class="inr_banner_heading">
								<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
							</div>
						   
							<div class="kf_inr_breadcrumb">
								<?php if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
									<?php university_education_get_breadcumbs();?>
								<?php }?>
							</div>
						</div>
						<!--KF INR BANNER DES Wrap End-->
					</div>
				</div>
			</div>
		</div>
	<?php }else if( is_archive() || is_search() ){
		$page_title = single_cat_title('', false);
		
		if( is_search() ){
			$page_title = esc_html__('Search Results', 'university-education');
			$caption = get_search_query();
		}else if( is_category()){
			$page_title = $page_title;
			$caption = single_cat_title('', false);
		}else if( is_tag() || is_tax('work_tag') || is_tax('product_tag') ){
			$page_title = esc_html__('Tag','university-education');
			$caption = single_cat_title('', false);
		}else if( is_day() ){
			$page_title = esc_html__('Day','university-education');
			$caption = get_the_date('F j, Y');
		}else if( is_month() ){
			$page_title = esc_html__('Month','university-education');
			$caption = get_the_date('F Y');
		}else if( is_year() ){
			$page_title = esc_html__('Year','university-education');
			$caption = get_the_date('Y');
		}else if( is_author() ){
			$page_title = esc_html__('By','university-education');
			
			$author_id = get_query_var('author');
			$author = get_user_by('id', $author_id);
			$page_title = $author->display_name;					
		}else if( is_post_type_archive('product') ){
			$page_title = esc_html__('Shop', 'university-education');
			$caption = '';
		}else{
			$page_title = get_the_title();
			$caption = '';
			
		}
		$header_background = university_education_header_title_background('default-search-archive-title-background');
		
	?>
		<div <?php echo wp_kses($header_background,array('strong'=>array(),'a'=>array()));?> class="kf_inr_banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<!--KF INR BANNER DES Wrap Start-->
						<div class="kf_inr_ban_des">
							<div class="inr_banner_heading">
								<h3><?php echo esc_attr(university_education_text_filter($page_title)); ?></h3>
							</div>
						   
							<div class="kf_inr_breadcrumb">
								<?php if($university_education_theme_option['enable-breadcrumbs'] == 'enable'){ ?>
									<?php university_education_get_breadcumbs();?>
								<?php }?>
							</div>
						</div>
						<!--KF INR BANNER DES Wrap End-->
					</div>
				</div>
			</div>
		</div>
		
		
	<?php } 