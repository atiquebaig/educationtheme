<?php get_header(); ?>
<div class="content">
	<div class="container">
		<div class="row">
		<?php 
			
			$university_education_theme_option = get_option('university_education_admin_option', array());	
			// $university_education_theme_option['post-thumbnail-size'] = '';
			$university_education_post_option = university_education_decode_stopbackslashes(get_post_meta(get_the_ID(), 'post-option', true ));
			if( !empty($university_education_post_option) ){
				$university_education_post_option = json_decode( $university_education_post_option, true );					
			}
			
			if( empty($university_education_post_option['sidebar']) || $university_education_post_option['sidebar'] == 'default-sidebar' ){
				$university_education_sidebar = array(
					'type'=>$university_education_theme_option['post-sidebar-template'],
					'left-sidebar'=>$university_education_theme_option['post-sidebar-left'], 
					'right-sidebar'=>$university_education_theme_option['post-sidebar-right']
				); 
			}else{
				$university_education_sidebar = array(
					'type'=>$university_education_post_option['sidebar'],
					'left-sidebar'=>$university_education_post_option['left-sidebar'], 
					'right-sidebar'=>$university_education_post_option['right-sidebar']
				); 				
			}
			$university_education_theme_option['thumbnail-size'] = 'full';
			$university_education_sidebar = university_education_get_sidebar_class($university_education_sidebar);?>
			<div class="<?php echo esc_attr($university_education_sidebar['center'])?>">
				<div class="kode-center-content-tf kode-item kode-blog-full-single kode-single-detail">
				<?php while ( have_posts() ){ the_post();global $post; ?>
					<?php //if(isset($university_education_theme_option['post-detail-style']) && $university_education_theme_option['post-detail-style'] == 'style-1'){ ?>
						<!--KF_BLOG DETAIL_WRAP START-->
						<div class="kf_blog_detail_wrap">
							<?php $university_education_get_image = university_education_get_image(get_post_thumbnail_id(), $university_education_theme_option['thumbnail-size'], true);
							if(!empty($university_education_get_image)){ ?>
							<!-- BLOG DETAIL THUMBNAIL START-->
							<div class="blog_detail_thumbnail">
								<figure>
									<?php 
										//Enable /Disable Feature Image
										if($university_education_theme_option['single-post-feature-image'] == 'enable'){
											
											$university_education_post_settings['thumbnail-size'] = $university_education_theme_option['thumbnail-size'];
											get_template_part('single/thumbnail', get_post_format());
										}
									?>
									<figcaption><a href="#"><?php echo esc_attr__('education','university-education'); ?></a></figcaption>
								</figure>
							</div>
							<!-- BLOG DETAIL THUMBNAIL END-->
							<?php }?>
							<!--KF_BLOG DETAIL_DES START-->
							<div class="kf_blog_detail_des">
								<ul class="kode-post-meta">
									<?php echo university_education_get_blog_info(array('date','author','views','comment','category'), false, '','li');?>									
								</ul>
								<?php the_content();
									wp_link_pages( array( 
										'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'university-education' ) . '</span>', 
										'after' => '</div>', 
										'link_before' => '<span>', 
										'link_after' => '</span>' )
									);
								?>
							</div>
							<!--KF_BLOG DETAIL_DES END-->
							<?php if(isset($university_education_theme_option['single-post-tags']) && $university_education_theme_option['single-post-tags'] == 'enable'){ ?>
								<div class="kf_blog_detail_tag">
									<span class="kf_tag_icon"><i class="fa fa-tags"></i></span>
									<ul class="kf_tag_list">
										<?php echo university_education_get_blog_info(array('tag'), false, '','li');?>
									</ul>
									<?php university_education_get_social_shares()?>
								</div>	
							<?php } ?>
						</div>	
					<?php //}else{ 
					//Post Author Enable / Disable
					if($university_education_theme_option['single-post-author'] == 'enable'){ ?>					
						<?php if(get_the_author_meta('description') <> ''){ ?>
						<div class="about_autor">
							<figure><?php echo get_avatar(get_the_author_meta('ID'), 90); ?></figure>
							<div class="about-autor_des">
								<h6><?php the_author_posts_link(); ?></h6>
								<?php if(get_the_author_meta('description') <> ''){ ?>
									<p><?php echo esc_attr(get_the_author_meta('description')); ?></p>
								<?php }?>
								<ul class="autor_social">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
						</div>						
					<?php }
					}
					//Recommended Posts Start
					if(isset($university_education_theme_option['single-recommended-post']) && $university_education_theme_option['single-recommended-post'] == 'enable'){ ?>
					<!--Recommended For You Wrap Start-->
					<?php university_education_related_posts($post->ID);?>
					<!--Recommended For You Wrap End-->
					<?php
					} //Recommended Posts Start Condition Ends
					//--Next and Previous Wrap Start-->
					if($university_education_theme_option['single-next-pre'] == 'enable'){ ?>
					<div class="kf_pagination">
						<ul class="pagination">
							<li class="pull-left">
								<?php previous_post_link('<div class="kode-next thcolorhover next-nav inner-post">%link</div>', ' <i class="fa fa-long-arrow-left"></i> Previous <h6>%title</h6>', true); ?>
							</li>
							<li class="pull-right">
								<?php next_post_link('<div class="kode-next thcolorhover next-nav inner-post">%link</div>', 'Next <i class="fa fa-long-arrow-right"></i> <h6>%title</h6>', true); ?>
							</li>
						</ul>
					</div>
					<?php } ?>
					<!--Next and Previous Wrap End-->					
					<?php if(isset($university_education_theme_option['single-post-comments']) && $university_education_theme_option['single-post-comments'] == 'enable'){ ?>
					<!-- Blog Detail -->
					<?php comments_template( '', true ); ?>
					<?php } ?>
				<?php } ?>
				</div>
				<div class="clear clearfix"></div>
			</div>
			<?php	
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'left-sidebar'){ ?>
				<div class="pull-left <?php echo esc_attr($university_education_sidebar['left'])?>">
					<?php get_sidebar('left'); ?>
				</div>	
			<?php }			
			if($university_education_sidebar['type'] == 'both-sidebar' || $university_education_sidebar['type'] == 'right-sidebar' && $university_education_sidebar['right'] != ''){ ?>
				<div class="pull-right <?php echo esc_attr($university_education_sidebar['right'])?>">
					<?php get_sidebar('right'); ?>
				</div>	
			<?php } 			
			?>
		</div><!-- Row -->	
	</div><!-- Container -->		
</div><!-- content -->
<?php get_footer(); ?>