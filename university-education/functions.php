<?php
	/*	
	*	Kodeforest Function File
	*	---------------------------------------------------------------------
	*	This file include all of important function and features of the theme
	*	---------------------------------------------------------------------
	*/
	
	define('UOE_AJAX_URL', admin_url('admin-ajax.php'));	
	define('UOE_PATH', get_template_directory_uri());
	define('UOE_LOCAL_PATH', get_template_directory());
	define('UOE_PLUGIN_URL', plugins_url());
	
	//WP Customizer
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_meta/wp_customizer.php');
	
	//Responsive Menu
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kf_responsive_menu.php');
	
	// Framework
	include_once(UOE_LOCAL_PATH . '/framework/kf_framework.php' );
	include_once(UOE_LOCAL_PATH . '/framework/script-handler.php' );
	include_once(UOE_LOCAL_PATH . '/framework/include/kode_front_func/kode_header.php' );
	include_once(UOE_LOCAL_PATH . '/framework/external/import_export/kodeforest-importer.php' );
	
	//Custom Widgets
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/compact-contact-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/recent-post-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/featured-teacher-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/gallery-post-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/flickr-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/contact-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/map-contact-widget.php');
	include_once(UOE_LOCAL_PATH . '/framework/include/custom_widgets/latest-post-widget.php');
	require_once(UOE_LOCAL_PATH . '/framework/include/tgm_library/tgm-activation.php');
	
	// plugin support	
	include_once(UOE_LOCAL_PATH . '/framework/include/tgm_library/kode-activation.php');

	$university_education_theme_option = get_option('university_education_admin_option', array());
	//Load Fonts
	
	if( empty($university_education_theme_option['upload-font']) ){ $university_education_theme_option['upload-font'] = ''; }
	$university_education_font_controller = new university_education_font_loader( json_decode($university_education_theme_option['upload-font'], true) );	

	// a comment callback function to create comment list
	if ( !function_exists('university_education_comment_list') ){
		function university_education_comment_list( $comment, $args, $depth ){
			$GLOBALS['comment'] = $comment;
			switch ( $comment->comment_type ){
				case 'pingback' :
				case 'trackback' :
				?>	
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<p><?php esc_html_e( 'Pingback :', 'university-education' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'university-education' ), '<span class="edit-link">', '</span>' ); ?></p>
				<?php break; ?>

				<?php default : global $post; ?>
			
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<div class="comment_wrap">
						<figure>
							<?php echo get_avatar( $comment, 100 ); ?>
						</figure>
						<div class="comment_des">							
							<div class="comment_des_hed">
								<cite><?php echo get_comment_author_link(); ?> <span> <?php echo esc_attr(get_comment_date()); ?></span></cite>
								<?php comment_reply_link( array_merge($args, array('before' => ' ', 'reply_text' => esc_html__('Reply', 'university-education'), 'depth' => $depth, 'max_depth' => $args['max_depth'])) ); ?>
							</div>
							<?php comment_text(); ?>
							<?php if( '0' == $comment->comment_approved ){ ?>
								<p class="comment-awaiting-moderation"><?php echo esc_html__( 'Your comment is awaiting moderation.', 'university-education' ); ?></p>
							<?php } ?>
							<?php edit_comment_link( esc_html__( 'Edit', 'university-education' ), '<div class="edit-link">', '</div>' ); ?>
						</div>
					</div>
				<?php
				break;
			}
		}
	}	

	
	
	
	// Handles JavaScript detection.
	function university_education_javascript_detection() {
		echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
	}
	add_action( 'wp_head', 'university_education_javascript_detection', 0 );
	
	
	